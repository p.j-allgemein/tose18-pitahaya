package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetSequence;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ProjectRepositoryTest {
	UserRepository userRepo = null;
	ProjectRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	private static void populateRepo(ProjectRepository p) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(i + 1,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummy = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter), 
										LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			
			p.insert(dummy);
		}
	}
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
								  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			u.insert(dummy);
		}
	}
	
	private static void populateForeignKeys(Project p)
	{
		CostRepository cRep = new CostRepository();
		ProjectphaseRepository ppRep = new ProjectphaseRepository();
		ProjectriskRepository prRep = new ProjectriskRepository();
		ProjectstatusRepository psRep = new ProjectstatusRepository();
		RoleRepository rRep = new RoleRepository();
		UserRepository uRep = new UserRepository();
		UserProjectRepository upRep = new UserProjectRepository();
		WorkpackageRepository wRep = new WorkpackageRepository();
		
		Projectstatus status = new Projectstatus(0, p, "Grün", "Grün", "Grün", "Grün", "Grün");
		psRep.insert(status);
		List<User> users = new ArrayList<>(uRep.getAll());
		List<Role> roles = new ArrayList<>(rRep.getByName("Projektmitarbeiter"));
		List<Projectphase> phases = new ArrayList<>(ppRep.getAll());
		
		for (int i = 0; i < 2; ++i)
		{
			Projectrisk pr = new Projectrisk(i, "Foreign" + i, "Foreign" + i, "Foreign" + i, i, i, "Foreign" + i,
											 users.get(i), i, p);
			prRep.insert(pr);
			User2Project up = new User2Project(users.get(i), p, roles.get(0));
			upRep.insert(up);
			Workpackage wp = new Workpackage(0, p, phases.get(i), "Foreign" + i, i, i, LocalDate.parse("2018.03.01", formatter), 
											 LocalDate.parse("2019.12.31", formatter), false);
			long wpId = wRep.insert(wp);
			wp = wRep.getById(wpId);
			Cost c = new Cost(0, wp, users.get(i), "Foreign" + i, i, LocalDate.parse("2018.05.01", formatter));
			cRep.insert(c);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		userRepo = new UserRepository();
		repo = new ProjectRepository();
		populateUserRepo(userRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projects");
		resetDB("Users");
		resetSequence("Projectnumber");
	}

	@Test
	void testDBData() {
		assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo);
		List<Project> list = new ArrayList<>(repo.getAll());
		assertThat("Freshly populated DB must hold 10 Projects",list.size(),is(10));
		assertThat("Test Element 1", list.get(0).toString(), is("Project:{id: 1; projektnummer: 0; " +
				   "projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; " +
				   "vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; " +
				   "mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; " +
				   "projekstatus: FakeStatus0; projektbudget: 0.000000}"));
	}
	
	@Test
	void testGetProjectByName()
	{
		populateRepo(repo);
		
		List<Project> project = new ArrayList<>(repo.getByName("FakeProject0"));
		assertThat("One Element in List", project.size(),is(1));
		assertThat("Test Element One", project.get(0).toString(), is("Project:{id: 1; projektnummer: 0; " +
				   "projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; " +
				   "vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; " +
				   "mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; " +
				   "projekstatus: FakeStatus0; projektbudget: 0.000000}"));
	}
	
	@Test
	void testGetProjectByNameNotFound()
	{
		populateRepo(repo);
		
		List<Project> project = new ArrayList<>(repo.getByName("ABC"));
		assertThat("Nothing in List", project.isEmpty(),is(true));
	}
	
	@Test
	void testGetProjectId3()
	{
		populateRepo(repo);
		
		Project project = repo.getById(3);
		assertThat("Test User Three", project.toString(), is("Project:{id: 3; projektnummer: 2; " +
				   "projektname: FakeProject2; projektleiter: User:{id: 3; username: FakeUser200; name: FakeName200; " +
				   "vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; " +
				   "mail: FakeMail200; telefon: FakeTel200}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; " +
				   "projekstatus: FakeStatus2; projektbudget: 2.000000}"));
	}
	
	@Test
	void testGetProjectIdNotFound()
	{
		populateRepo(repo);
		
		assertThat("Project does not exist", repo.getById(99),is(nullValue()));
	}
	
	@ParameterizedTest
	@CsvSource({"1,1,One,FakeUser100,2018.03.01,2019.12.31,One,One,11.11",
				"2,2,Two,FakeUser200,2018.03.01,2019.12.31,Two,Two,22.22",
				"3,3,Three,FakeUser300,2018.03.01,2019.12.31,Three,Three,33.33"})
	void testInsertProjects(long id, long projektnummer, String projektname, String projektleiter, String projektstart,
							String projektende, String projektphase, String projektstatus, double projektbudget)
	{
		populateRepo(repo);
		List<User> users = new ArrayList<>(userRepo.getByName(projektleiter));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(projektleiter));
		Project p = new Project(id, projektnummer, projektname, users.get(0), LocalDate.parse(projektstart, formatter), 
				LocalDate.parse(projektende, formatter), projektphase, projektstatus, projektbudget);
		
		long dbId = repo.insert(p);
		Project fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("projektnummer = projektnummer", fromDB.getProjektnummer(),is(projektnummer) );
		assertThat("projektname = projektname", fromDB.getProjektname(),is(projektname) );
		assertThat("user = projektleiter", fromDB.getProjektleiter().getUsername(),is(projektleiter) );
		assertThat("projektstart = projektstart", fromDB.getProjektstart().format(formatter), is(projektstart) );
		assertThat("projektende = projektende", fromDB.getProjektende().format(formatter),is(projektende) );
		assertThat("projektphase = projektphase", fromDB.getProjektphase(),is(projektphase) );
		assertThat("projektstatus = projektstatus", fromDB.getProjektstatus(),is(projektstatus) );
		assertThat("projektbudget = projektbudget", fromDB.getProjektbudget(),is(projektbudget) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,1,One,2018.03.01,2019.12.31,One,One,11.11",
				"2,2,Two,2018.03.01,2019.12.31,Two,Two,22.22",
				"3,3,Three,2018.03.01,2019.12.31,Three,Three,33.33"})
	void testInsertProjectsNullPl(long id, long projektnummer, String projektname, String projektstart,
								  String projektende, String projektphase, String projektstatus, double projektbudget)
	{
		populateRepo(repo);
		
		Project p = new Project(id, projektnummer, projektname, null, LocalDate.parse(projektstart, formatter), 
				LocalDate.parse(projektende, formatter), projektphase, projektstatus, projektbudget);
		
		long dbId = repo.insert(p);
		Project fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("projektnummer = projektnummer", fromDB.getProjektnummer(),is(projektnummer) );
		assertThat("projektname = projektname", fromDB.getProjektname(),is(projektname) );
		assertThat("user = projektleiter", fromDB.getProjektleiter(),nullValue() );
		assertThat("projektstart = projektstart", fromDB.getProjektstart().format(formatter), is(projektstart) );
		assertThat("projektende = projektende", fromDB.getProjektende().format(formatter),is(projektende) );
		assertThat("projektphase = projektphase", fromDB.getProjektphase(),is(projektphase) );
		assertThat("projektstatus = projektstatus", fromDB.getProjektstatus(),is(projektstatus) );
		assertThat("projektbudget = projektbudget", fromDB.getProjektbudget(),is(projektbudget) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,1,One,FakeUser100,2018.03.01,2019.12.31,One,One,11.11,11,Eins,FakeUser400,2018.01.01,2020.12.31,Eins,Eins,111.11",
				"2,2,Two,FakeUser200,2018.03.01,2019.12.31,Two,Two,22.22,22,Zwei,FakeUser500,2018.01.01,2020.12.31,Zwei,Zwei,222.22",
				"3,3,Three,FakeUser300,2018.03.01,2019.12.31,Three,Three,33.33,33,Drei,FakeUser600,2018.01.01,2020.12.31,Drei,Drei,333.33"})		
	void testUpdateProjects(long id, long projektnummer, String projektname, String projektleiter, String projektstart,
			 String projektende, String projektphase, String projektstatus, double projektbudget, long newProjektnummer, 
			 String newProjektname, String newProjektleiter, String newProjektstart,
			 String newProjektende, String newProjektphase, String newProjektstatus, double newProjektbudget)
	{
		populateRepo(repo);
		List<User> users = new ArrayList<>(userRepo.getByName(projektleiter));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(projektleiter));
		Project p = new Project(id, projektnummer, projektname, users.get(0), LocalDate.parse(projektstart, formatter), 
				LocalDate.parse(projektende, formatter), projektphase, projektstatus, projektbudget);
		
		long dbId = repo.insert(p);
		
		users = new ArrayList<>(userRepo.getByName(newProjektleiter));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(newProjektleiter));
		
		p.setId(dbId);
		p.setProjektnummer(newProjektnummer);
		p.setProjektname(newProjektname);
		p.setProjektleiter(users.get(0));
		p.setProjektstart(LocalDate.parse(newProjektstart, formatter));
		p.setProjektende(LocalDate.parse(newProjektende, formatter));
		p.setProjektphase(newProjektphase);
		p.setProjektstatus(newProjektstatus);
		p.setProjektbudget(newProjektbudget);
		repo.save(p);
		
		Project fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("projektnummer = projektnummer", fromDB.getProjektnummer(),is(newProjektnummer) );
		assertThat("projektname = projektname", fromDB.getProjektname(),is(newProjektname) );
		assertThat("user = projektleiter", fromDB.getProjektleiter().getUsername(),is(newProjektleiter) );
		assertThat("projektstart = projektstart", fromDB.getProjektstart().format(formatter), is(newProjektstart) );
		assertThat("projektende = projektende", fromDB.getProjektende().format(formatter),is(newProjektende) );
		assertThat("projektphase = projektphase", fromDB.getProjektphase(),is(newProjektphase) );
		assertThat("projektstatus = projektstatus", fromDB.getProjektstatus(),is(newProjektstatus) );
		assertThat("projektbudget = projektbudget", fromDB.getProjektbudget(),is(newProjektbudget) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"1,1,One,FakeUser100,2018.03.01,2019.12.31,One,One,11.11",
				"2,2,Two,FakeUser200,2018.03.01,2019.12.31,Two,Two,22.22",
				"3,3,Three,FakeUser300,2018.03.01,2019.12.31,Three,Three,33.33"})
	void testDeleteProjects(long id, long projektnummer, String projektname, String projektleiter, String projektstart,
							String projektende, String projektphase, String projektstatus, double projektbudget)
	{
		populateRepo(repo);
		
		List<User> users = new ArrayList<>(userRepo.getByName(projektleiter));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(projektleiter));
		Project p = new Project(id, projektnummer, projektname, users.get(0), LocalDate.parse(projektstart, formatter), 
				LocalDate.parse(projektende, formatter), projektphase, projektstatus, projektbudget);
		
		long dbId = repo.insert(p);
		Project fromDB = repo.getById(dbId);
		assertThat("Project was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThat("Projekt is should be deleted", repo.getById(dbId),is(nullValue()));
	}
	
	@ParameterizedTest
	@CsvSource({"1,1,One,FakeUser100,2018.03.01,2019.12.31,One,One,11.11",
				"2,2,Two,FakeUser200,2018.03.01,2019.12.31,Two,Two,22.22",
				"3,3,Three,FakeUser300,2018.03.01,2019.12.31,Three,Three,33.33"})
	void testDeleteProjectsWithForeignKeys(long id, long projektnummer, String projektname, String projektleiter,
										   String projektstart, String projektende, String projektphase, 
										   String projektstatus, double projektbudget)
	{
		populateRepo(repo);
		
		List<User> users = new ArrayList<>(userRepo.getByName(projektleiter));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(projektleiter));
		Project p = new Project(id, projektnummer, projektname, users.get(0), LocalDate.parse(projektstart, formatter), 
				LocalDate.parse(projektende, formatter), projektphase, projektstatus, projektbudget);
		
		long dbId = repo.insert(p);
		Project fromDB = repo.getById(dbId);
		populateForeignKeys(fromDB);
		assertThat("Project was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThat("Projekt is should be deleted", repo.getById(dbId),is(nullValue()));
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo);
		for(Project i : repo.getAll())
		{
			repo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}
	
	@ParameterizedTest
	@CsvSource({"1,1,One,FakeUser100,2018.03.01,2019.12.31,One,One,11.11",
				"2,2,Two,FakeUser200,2018.03.01,2019.12.31,Two,Two,22.22",
				"3,3,Three,FakeUser300,2018.03.01,2019.12.31,Three,Three,33.33"})
	void testGetByOneName(long id, long projektnummer, String projektname, String projektleiter, String projektstart,
			 			  String projektende, String projektphase, String projektstatus, double projektbudget)
	{
		populateRepo(repo);
		
		List<User> users = new ArrayList<>(userRepo.getByName(projektleiter));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(projektleiter));
		Project p = new Project(id, projektnummer, projektname, users.get(0), LocalDate.parse(projektstart, formatter), 
				LocalDate.parse(projektende, formatter), projektphase, projektstatus, projektbudget);
		
		long dbId = repo.insert(p);
		List<Project> fromDB = new ArrayList<>(repo.getByName(projektname));
		assertThat("Exactly one item was returned", fromDB.size(),is(1));
		
		Project elementFromDB = fromDB.iterator().next();
		assertThat("id = id", elementFromDB.getId(),is(dbId) );
		assertThat("projektnummer = projektnummer", elementFromDB.getProjektnummer(),is(projektnummer) );
		assertThat("projektname = projektname", elementFromDB.getProjektname(),is(projektname) );
		assertThat("user = projektleiter", elementFromDB.getProjektleiter().getUsername(),is(projektleiter) );
		assertThat("projektstart = projektstart", elementFromDB.getProjektstart().format(formatter), is(projektstart) );
		assertThat("projektende = projektende", elementFromDB.getProjektende().format(formatter),is(projektende) );
		assertThat("projektphase = projektphase", elementFromDB.getProjektphase(),is(projektphase) );
		assertThat("projektstatus = projektstatus", elementFromDB.getProjektstatus(),is(projektstatus) );
		assertThat("projektbudget = projektbudget", elementFromDB.getProjektbudget(),is(projektbudget) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,1,One,FakeUser100,2018.03.01,2019.12.31,One,One,11.11",
				"2,2,Two,FakeUser200,2018.03.01,2019.12.31,Two,Two,22.22",
				"3,3,Three,FakeUser300,2018.03.01,2019.12.31,Three,Three,33.33"})
	void testGetManyItemsByName(int count, long projektnummer, String projektname, String projektleiter, String projektstart,
			  					String projektende, String projektphase, String projektstatus, double projektbudget)
	{
		populateRepo(repo);
		
		for(int n = 0; n < count; ++n) {
			List<User> users = new ArrayList<>(userRepo.getByName(projektleiter));
			Project p = new Project(0, projektnummer, projektname, users.get(0), LocalDate.parse(projektstart, formatter), 
					LocalDate.parse(projektende, formatter), projektphase, projektstatus, projektbudget);
			repo.insert(p);
		}
		
		List<Project> fromDB = new ArrayList<>(repo.getByName(projektname));
		assertThat("Exactly one item was returned", fromDB.size(),is(count));
		
		for(	Project elementFromDB : fromDB)
		{
			assertThat("projektnummer = projektnummer", elementFromDB.getProjektnummer(),is(projektnummer) );
			assertThat("projektname = projektname", elementFromDB.getProjektname(),is(projektname) );
			assertThat("user = projektleiter", elementFromDB.getProjektleiter().getUsername(),is(projektleiter) );
			assertThat("projektstart = projektstart", elementFromDB.getProjektstart().format(formatter), is(projektstart) );
			assertThat("projektende = projektende", elementFromDB.getProjektende().format(formatter),is(projektende) );
			assertThat("projektphase = projektphase", elementFromDB.getProjektphase(),is(projektphase) );
			assertThat("projektstatus = projektstatus", elementFromDB.getProjektstatus(),is(projektstatus) );
			assertThat("projektbudget = projektbudget", elementFromDB.getProjektbudget(),is(projektbudget) );
		}
	}
	
	@Test
	void testUpdateProjectNullProjektleiter()
	{
		populateRepo(repo);
		Project p = repo.getById(1);
		p.setProjektleiter(null);
		repo.save(p);
		
		Project fromDB = repo.getById(p.getId());
		assertThat("id = id", fromDB.getId(),is(p.getId()) );
		assertThat("projektnummer = projektnummer", fromDB.getProjektnummer(),is(p.getProjektnummer()) );
		assertThat("projektname = projektname", fromDB.getProjektname(),is(p.getProjektname()) );
		assertThat("user = projektleiter", fromDB.getProjektleiter(),is(nullValue()) );
		assertThat("projektstart = projektstart", fromDB.getProjektstart().format(formatter), is(p.getProjektstart().format(formatter)) );
		assertThat("projektende = projektende", fromDB.getProjektende().format(formatter),is(p.getProjektende().format(formatter)) );
		assertThat("projektphase = projektphase", fromDB.getProjektphase(),is(p.getProjektphase()) );
		assertThat("projektstatus = projektstatus", fromDB.getProjektstatus(),is(p.getProjektstatus()) );
		assertThat("projektbudget = projektbudget", fromDB.getProjektbudget(),is(p.getProjektbudget()) );	
	}
	
	@Test
	void testUpdateProjectUnknownProjektleiter()
	{
		populateRepo(repo);
		Project p = repo.getById(1);
		User u = new User(0,"TestUser", "TestName", "TestVorname", new Role(3, "TestRole"),
				  "TestAbteilung", "TestMail", "TestTel");
		
		p.setProjektleiter(u);
		repo.save(p);
		
		Project fromDB = repo.getById(p.getId());
		assertThat("id = id", fromDB.getId(),is(p.getId()) );
		assertThat("projektnummer = projektnummer", fromDB.getProjektnummer(),is(p.getProjektnummer()) );
		assertThat("projektname = projektname", fromDB.getProjektname(),is(p.getProjektname()) );
		assertThat("user = projektleiter", fromDB.getProjektleiter().getUsername(),is(u.getUsername()) );
		assertThat("projektstart = projektstart", fromDB.getProjektstart().format(formatter), is(p.getProjektstart().format(formatter)) );
		assertThat("projektende = projektende", fromDB.getProjektende().format(formatter),is(p.getProjektende().format(formatter)) );
		assertThat("projektphase = projektphase", fromDB.getProjektphase(),is(p.getProjektphase()) );
		assertThat("projektstatus = projektstatus", fromDB.getProjektstatus(),is(p.getProjektstatus()) );
		assertThat("projektbudget = projektbudget", fromDB.getProjektbudget(),is(p.getProjektbudget()) );	
	}
	
	@Test
	void testGetNoProjectsByName()
	{
		populateRepo(repo);
		
		List<Project> fromDB = new ArrayList<>(repo.getByName("NotExistingItem"));
		assertThat("Exactly one item was returned", fromDB.size(),is(0));
		
	}
	
	@Test
	void testGetProjectByUsername()
	{
		populateRepo(repo);
		
		List<Project> project = new ArrayList<>(repo.getByUsername("FakeUser0"));
		assertThat("One Element in List", project.size(),is(1));
		assertThat("Test Element One", project.get(0).toString(), is("Project:{id: 1; projektnummer: 0; projektname: FakeProject0; " +
																	 "projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; " +
																	 "vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; " +
																	 "abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; " +
																	 "start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; " +
																	 "projekstatus: FakeStatus0; projektbudget: 0.000000}"));
	}
	
	@Test
	void testGetProjectByUsernameNotFound()
	{
		List<Project> project = null;
		try
		{
			populateRepo(repo);
		
			project = new ArrayList<>(repo.getByUsername("ABC"));
			assertThat("Nothing in List", project.toString(), is("[]"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unecpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testGetProjectnumberId()
	{
		try
		{
			Long id = repo.getProjectNumberId();
			assertThat("Generated Id", id, is(1L));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unecpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testCheckProjectnameExists()
	{
		populateRepo(repo);
		
		assertThat("Project exists", repo.checkProjectname("FakeProject0"), is(true));
	}
	
	@Test
	void testCheckProjectnameNotExists()
	{
		populateRepo(repo);
		
		assertThat("Project not exists", repo.checkProjectname("NotExist"), is(false));
	}
}
