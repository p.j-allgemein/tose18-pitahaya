package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;

public class CostTest {
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Test
	void testEmptyConstructorYieldsEmptyObject() {
		Cost c = new Cost();
		assertThat("Id",c.getId(),is(equalTo(0l)));
		assertThat("Workpackage",c.getWorkpackage(),is(equalTo(null)));
		assertThat("User",c.getUser(),is(equalTo(null)));
		assertThat("Beschreibung",c.getBeschreibung(),is(equalTo(null)));
		assertThat("Betrag",c.getBetrag(),is(equalTo(0.0)));
		assertThat("Datum",c.getDatum(),is(equalTo(null)));
	}
	
	@Test
	void testWorkpackageNull()
	{
		Cost c = new Cost(1, null, null, null, 0.0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			c.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Workpackage fehlt")));
	}
	
	@Test
	void testUserNull()
	{
		Cost c = new Cost(1, new Workpackage(), null, null, 0.0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			c.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("User fehlt")));
	}
	
	@Test
	void testBeschreibungNull()
	{
		Cost c = new Cost(1, new Workpackage(), new User(), null, 0.0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			c.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Beschreibung fehlt")));
	}
	
	@Test
	void testBeschreibungEmpty()
	{
		Cost c = new Cost(1, new Workpackage(), new User(), "", 0.0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			c.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Beschreibung fehlt")));
	}
	
	@Test
	void testBetragNegativ()
	{
		Cost c = new Cost(1, new Workpackage(), new User(), "A", -20.01, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			c.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Betrag fehlt")));
	}
	
	@Test
	void testDatumNull()
	{
		Cost c = new Cost(1, new Workpackage(), new User(), "A", 20.01, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			c.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Datum fehlt")));
	}
	
	@Test
	void testValid()
	{
		Cost c = new Cost(1, new Workpackage(), new User(), "A", 20.01, LocalDate.now());
		c.validate();
	}
}
