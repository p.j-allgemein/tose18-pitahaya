package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class UserTest {
	@Test
	void testEmptyConstructorYieldsEmptyObject() {
		User u = new User();
		assertThat("Id",u.getId(),is(equalTo(0l)));
		assertThat("Username",u.getUsername(),is(equalTo(null)));
		assertThat("Name",u.getName(),is(equalTo(null)));
		assertThat("Vorname",u.getVorname(),is(equalTo(null)));
		assertThat("Role",u.getRole(),is(equalTo(null)));
		assertThat("Abteilung",u.getAbteilung(),is(equalTo(null)));
		assertThat("Mail",u.getMail(),is(equalTo(null)));
		assertThat("Telefon",u.getTelefon(),is(equalTo(null)));
	}
	
	@Test
	void testUsernameNull()
	{
		User u = new User(1, null, null, null, null, null, null, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Username fehlt")));
	}
	
	@Test
	void testUsernameEmpty()
	{
		User u = new User(1, "", null, null, null, null, null, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Username fehlt")));
	}
	
	@Test
	void testNameNull()
	{
		User u = new User(1, "A", null, null, null, null, null, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Name fehlt")));
	}
	
	@Test
	void testNameEmpty()
	{
		User u = new User(1, "A", "", null, null, null, null, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Name fehlt")));
	}
	
	@Test
	void testVornameNull()
	{
		User u = new User(1, "A", "A", null, null, null, null, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Vorname fehlt")));
	}
	
	@Test
	void testVornameEmpty()
	{
		User u = new User(1, "A", "A", "", null, null, null, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Vorname fehlt")));
	}
	
	@Test
	void testRoleNull()
	{
		User u = new User(1, "A", "A", "A", null, null, null, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Rolle für den User wurde nicht gesetzt/gefunden")));
	}
	
	@Test
	void testAbteilungNull()
	{
		User u = new User(1, "A", "A", "A", new Role(), null, null, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Abteilung fehlt")));
	}
	
	@Test
	void testAbteilungEmpty()
	{
		User u = new User(1, "A", "A", "A", new Role(), "", null, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Abteilung fehlt")));
	}
	
	@Test
	void testMailNull()
	{
		User u = new User(1, "A", "A", "A", new Role(), "A", null, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Mail fehlt")));
	}
	
	@Test
	void testMailEmpty()
	{
		User u = new User(1, "A", "A", "A", new Role(), "A", "", null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Mail fehlt")));
	}
	
	@Test
	void testTelefonNull()
	{
		User u = new User(1, "A", "A", "A", new Role(), "A", "A", null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Telefon fehlt")));
	}
	
	@Test
	void testTelefonEmpty()
	{
		User u = new User(1, "A", "A", "A", new Role(), "A", "A", "");
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			u.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Telefon fehlt")));
	}
}
