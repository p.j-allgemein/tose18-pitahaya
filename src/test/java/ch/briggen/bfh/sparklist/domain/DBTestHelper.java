package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;

import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBTestHelper {
	private static final Logger log = LoggerFactory.getLogger(DBTestHelper.class);
	
	public static void initDataSourceForTest(String testName) throws Exception {
		JdbcDataSource ds = new JdbcDataSource();
		ds.setURL("jdbc:h2:mem:"+testName+";TRACE_LEVEL_SYSTEM_OUT=2;DB_CLOSE_DELAY=60");
		ds.setUser("sa");
		ds.setPassword("");
		
		JdbcRepositoryHelper.overrideDefalutDataSource(ds);
	}
	
	public static void initDB() throws SQLException
	{
		try(Connection c = JdbcRepositoryHelper.getConnection())
		{
			ClassLoader myLoader = ItemRepositoryTest.class.getClassLoader();
			InputStream sqlscript = myLoader.getResourceAsStream("initdatabase.sql");
			RunScript.execute(c,new InputStreamReader(sqlscript));
		}
	}
	
	public static void resetDB(String tablename) throws SQLException
	{
		try(Connection conn = getConnection())
		{
			Statement stmt = conn.createStatement();
			stmt.execute("DELETE FROM " + tablename);
			
			stmt = conn.createStatement();
			stmt.execute("ALTER TABLE " + tablename + " ALTER COLUMN ID RESTART WITH 1");	
		}
		catch(SQLException e)
		{
			String msg = "SQL error while reseting " + tablename;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	public static void resetSequence(String sequencename) throws SQLException
	{
		try(Connection conn = getConnection())
		{
			Statement stmt = conn.createStatement();
			stmt.execute("ALTER SEQUENCE " + sequencename + " RESTART WITH 1 NOCACHE");	
		}
		catch(SQLException e)
		{
			String msg = "SQL error while reseting " + sequencename;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
}
