package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ProjectphaseRepositoryTest {
	ProjectphaseRepository repo = null;
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		repo = new ProjectphaseRepository();
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projectphases");
	}

	@Test
	void testDBData() {
		assertThat("New DB must contain data",repo.getAll().isEmpty(),is(false));
		assertThat("Test", repo.getAll().toString(), is("[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}]"));
		assertThat("DB contains amount of elements", repo.getAll().size(), is(4));
	}
	
	@Test
	void testAllData()
	{
		List<String> list = repo.getAll().stream().map(e -> e.getPhasenname()).collect(Collectors.toList());
		assertThat("First Element is Initialisierung",list.get(0),is("Initialisierung"));
		assertThat("Second Element is Konzeption",list.get(1),is("Konzeption"));
		assertThat("Third Element is Realisierung",list.get(2),is("Realisierung"));
	}
	
	@Test
	void testGetProjectphaseInitialisierung()
	{
		List<Projectphase> pp = new ArrayList<>(repo.getByName("Initialisierung"));
		assertThat("One Element in List", pp.size(),is(1));
		assertThat("Data of Phase Initialisierung", pp.toString(),is("[Projectphase:{id: 1; phasenname: Initialisierung}]"));
	}
	
	@Test
	void testGetProjectphaseNotFound()
	{
		List<Projectphase> pp = new ArrayList<>(repo.getByName("ABC"));
		assertThat("List is Empty", pp.isEmpty(),is(true));
	}
	
	@Test
	void testGetProjectphaseId3()
	{
		Projectphase pp = repo.getById(3);
		assertThat("Data of Phase Realisierung", pp.toString(),is("Projectphase:{id: 3; phasenname: Realisierung}"));
	}
	
	@Test
	void testGetProjectphaseIdNotFound()
	{
		assertThat("Projectphase does not exist", repo.getById(99),is(nullValue()));
	}
	
	@ParameterizedTest
	@CsvSource({"6,Neu","7,Abgeschlossen","8,Abgebrochen"})
	void testInsertItems(long id,String name)
	{
		Projectphase i = new Projectphase(id,name);
		long dbId = repo.insert(i);
		Projectphase fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getPhasenname(),is(name) );

	}
	
	@ParameterizedTest
	@CsvSource({"6,Neu,NeuNeu","7,Abgeschlossen,NeuAbgeschlossen","8,Abgebrochen,NeuAbgebrochen"})
	void testUpdateItems(long id,String name,String newName)
	{
		Projectphase i = new Projectphase(id,name);
		long dbId = repo.insert(i);
		i.setId(dbId);
		i.setPhasenname(newName);
		repo.save(i);
		
		Projectphase fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getPhasenname(),is(newName) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"6,Neu","7,Abgeschlossen","8,Abgebrochen"})
	void testDeleteItems(long id,String name)
	{
		Projectphase i = new Projectphase(id,name);
		long dbId = repo.insert(i);
		Projectphase fromDB = repo.getById(dbId);
		assertThat("Item was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThat("Projectphase is should be deleted", repo.getById(dbId),is(nullValue()));
	}
}
