package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class UserRepositoryTest {
	RoleRepository roleRepo = null;
	UserRepository repo = null;
	
	private static void populateRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(1, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			u.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		repo = new UserRepository();
		roleRepo = new RoleRepository();
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Users");
	}

	@Test
	void testDBData() {
		assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo);
		List<User> list = new ArrayList<>(repo.getAll());
		assertThat("Freshly populated DB must hold 10 users",list.size(),is(10));
		assertThat("Test Element 1", list.get(0).toString(), is("User:{id: 1; username: FakeUser0; " +
				   "name: FakeName0; vorname: FakeVorname0; role: Role:{id: 1; rolename: Admin};" +
				   " abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}"));
	}
	
	@Test
	void testGetUserByName()
	{
		populateRepo(repo);
		
		List<User> user = new ArrayList<>(repo.getByName("FakeUser0"));
		assertThat("One Element in List", user.size(),is(1));
		assertThat("Test Element One", user.get(0).toString(), is("User:{id: 1; username: FakeUser0; " +
				   "name: FakeName0; vorname: FakeVorname0; role: Role:{id: 1; rolename: Admin};" +
				   " abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}"));
	}
	
	@Test
	void testGetUserByNameNotFound()
	{
		populateRepo(repo);
		
		List<User> user = new ArrayList<>(repo.getByName("ABC"));
		assertThat("One Element in List", user.isEmpty(),is(true));
	}
	
	@Test
	void testGetUserId3()
	{
		populateRepo(repo);
		
		User user = repo.getById(3);
		assertThat("Test User Three", user.toString(), is("User:{id: 3; username: FakeUser2; " +
				   "name: FakeName2; vorname: FakeVorname2; role: Role:{id: 1; rolename: Admin};" +
				   " abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}"));
	}
	
	@Test
	void testGetUserIdNotFound()
	{
		populateRepo(repo);
		
		assertThat("User does not exist", repo.getById(99),is(nullValue()));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,One,One,Admin,One,One,One",
				"2,Two,Two,Two,Portfoliomanager,Two,Two,Two",
				"3,Three,Three,Three,Projektleiter,Three,Three,Three"})
	void testInsertUsers(long id,String username, String name, String vorname, String rolename,
						 String abteilung, String mail, String telefon)
	{
		populateRepo(repo);
		List<Role> roles = new ArrayList<>(roleRepo.getByName(rolename));
		assertThat("roles list is not empty", roles.isEmpty(), is(false));
		assertThat("roles", roles.get(0).toString(), is("Role:{id: " + id + "; rolename: " + rolename + "}"));
		User u = new User(id,username,name,vorname, (!roles.isEmpty() ? roles.get(0) : null), abteilung, mail, telefon);
		
		long dbId = repo.insert(u);
		User fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("username = username", fromDB.getUsername(),is(username) );
		assertThat("name = name", fromDB.getName(),is(name) );
		assertThat("vorname = vorname", fromDB.getVorname(),is(vorname) );
		assertThat("role = rolename", fromDB.getRole().getRolename(), is(rolename) );
		assertThat("abteilung = abteilung", fromDB.getAbteilung(),is(abteilung) );
		assertThat("mail = mail", fromDB.getMail(),is(mail) );
		assertThat("telefon = telefon", fromDB.getTelefon(),is(telefon) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,One,One,Admin,One,One,One,Eins,Eins,Eins,Portfoliomanager,Eins,Eins,Eins",
				"2,Two,Two,Two,Portfoliomanager,Two,Tow,Two,Zwei,Zwei,Zwei,Projektleiter,Zwei,Zwei,Zwei",
				"3,Three,Three,Three,Projektleiter,Three,Three,Three,Drei,Drei,Drei,Admin,Drei,Drei,Drei"})
	void testUpdateUsers(long id,String username, String name, String vorname, String rolename,
			 			 String abteilung, String mail, String telefon, String newUsername, String newName,
			 			 String newVorname, String newRolename, String newAbteilung, String newMail, String newTelefon)
	{
		populateRepo(repo);
		
		List<Role> roles = new ArrayList<>(roleRepo.getByName(rolename));
		assertThat("roles list is not empty", roles.isEmpty(), is(false));
		assertThat("rolename", roles.get(0).getRolename(), is(rolename));
		User u = new User(id,username,name,vorname, (!roles.isEmpty() ? roles.get(0) : null), abteilung, mail, telefon);
		long dbId = repo.insert(u);
		
		roles = new ArrayList<>(roleRepo.getByName(newRolename));
		assertThat("roles list is not empty", roles.isEmpty(), is(false));
		assertThat("rolename", roles.get(0).getRolename(), is(newRolename));
		
		u.setId(dbId);
		u.setUsername(newUsername);
		u.setName(newName);
		u.setVorname(newVorname);
		u.setRole(roles.get(0));
		u.setAbteilung(newAbteilung);
		u.setMail(newMail);
		u.setTelefon(newTelefon);
		repo.save(u);
		
		User fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("username = newUsername", fromDB.getUsername(),is(newUsername) );
		assertThat("name = newName", fromDB.getName(),is(newName) );
		assertThat("vorname = newVorname", fromDB.getVorname(),is(newVorname) );
		assertThat("role = newRolename", fromDB.getRole().getRolename(), is(newRolename) );
		assertThat("abteilung = newAbteilung", fromDB.getAbteilung(),is(newAbteilung) );
		assertThat("mail = newMail", fromDB.getMail(),is(newMail) );
		assertThat("telefon = newTelefon", fromDB.getTelefon(),is(newTelefon) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,One,One,Admin,One,One,One",
				"2,Two,Two,Two,Portfoliomanager,Two,Two,Two",
				"3,Three,Three,Three,Projektleiter,Three,Three,Three"})
	void testDeleteUsers(long id,String username, String name, String vorname, String rolename,
			 			 String abteilung, String mail, String telefon)
	{
		populateRepo(repo);
		
		List<Role> roles = new ArrayList<>(roleRepo.getByName(rolename));
		assertThat("roles list is not empty", roles.isEmpty(), is(false));
		assertThat("roles", roles.get(0).toString(), is("Role:{id: " + id + "; rolename: " + rolename + "}"));
		User u = new User(id,username,name,vorname, (!roles.isEmpty() ? roles.get(0) : null), abteilung, mail, telefon);
		
		long dbId = repo.insert(u);
		User fromDB = repo.getById(dbId);
		assertThat("User was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThat("User is should be deleted", repo.getById(dbId),is(nullValue()));
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo);
		for(User i : repo.getAll())
		{
			repo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,One,One,Admin,One,One,One",
				"2,Two,Two,Two,Portfoliomanager,Two,Two,Two",
				"3,Three,Three,Three,Projektleiter,Three,Three,Three"})
	void testGetByOneName(long id,String username, String name, String vorname, String rolename,
			 			  String abteilung, String mail, String telefon)
	{
		populateRepo(repo);
		
		List<Role> roles = new ArrayList<>(roleRepo.getByName(rolename));
		assertThat("roles list is not empty", roles.isEmpty(), is(false));
		assertThat("roles", roles.get(0).toString(), is("Role:{id: " + id + "; rolename: " + rolename + "}"));
		User u = new User(id,username,name,vorname, (!roles.isEmpty() ? roles.get(0) : null), abteilung, mail, telefon);
		
		long dbId = repo.insert(u);
		List<User> fromDB = new ArrayList<>(repo.getByName(name));
		assertThat("Exactly one item was returned", fromDB.size(),is(1));
		
		User elementFromDB = fromDB.iterator().next();
		assertThat("id = id", elementFromDB.getId(),is(dbId) );
		assertThat("username = username", elementFromDB.getUsername(),is(username) );
		assertThat("name = name", elementFromDB.getName(),is(name) );
		assertThat("vorname = vorname", elementFromDB.getVorname(),is(vorname) );
		assertThat("role = rolename", elementFromDB.getRole().getRolename(), is(rolename) );
		assertThat("abteilung = abteilung", elementFromDB.getAbteilung(),is(abteilung) );
		assertThat("mail = mail", elementFromDB.getMail(),is(mail) );
		assertThat("telefon = telefon", elementFromDB.getTelefon(),is(telefon) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,One,One,Admin,One,One,One",
				"2,Two,Two,Two,Portfoliomanager,Two,Two,Two",
				"3,Three,Three,Three,Projektleiter,Three,Three,Three"})
	void testGetManyItemsByName(int count, String username, String name, String vorname, String rolename,
			 					String abteilung, String mail, String telefon)
	{
		populateRepo(repo);
		
		for(int n = 0; n < count; ++n) {
			List<Role> roles = new ArrayList<>(roleRepo.getByName(rolename));
			User u = new User(0,username,name,vorname, (!roles.isEmpty() ? roles.get(0) : null), abteilung, mail, telefon);
			repo.insert(u);
		}
		
		List<User> fromDB = new ArrayList<>(repo.getByName(name));
		assertThat("Exactly one item was returned", fromDB.size(),is(count));
		
		for(	User elementFromDB : fromDB)
		{
			assertThat("username = username", elementFromDB.getUsername(),is(username) );
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("vorname = vorname", elementFromDB.getVorname(),is(vorname) );
			assertThat("role = rolename", elementFromDB.getRole().getRolename(), is(rolename) );
			assertThat("abteilung = abteilung", elementFromDB.getAbteilung(),is(abteilung) );
			assertThat("mail = mail", elementFromDB.getMail(),is(mail) );
			assertThat("telefon = telefon", elementFromDB.getTelefon(),is(telefon) );
		}
	}
	
	@Test
	void testGetNoItemsByName()
	{
		populateRepo(repo);
		
		List<User> fromDB = new ArrayList<>(repo.getByName("NotExistingItem"));
		assertThat("Exactly one item was returned", fromDB.size(),is(0));
		
	}
	
	@Test
	void testGetUserByRolenameKnownRoleWithUsers()
	{
		populateRepo(repo);
		
		List<User> user = new ArrayList<>(repo.getByRolename("Admin"));
		assertThat("One Element in List", user.size(),is(10));
		assertThat("Test Element One", user.get(0).toString(), is("User:{id: 1; username: FakeUser0; " +
				   "name: FakeName0; vorname: FakeVorname0; role: Role:{id: 1; rolename: Admin};" +
				   " abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}"));
	}
	
	@Test
	void testGetUserByRolenameKnownRoleWithoutUsers()
	{
		populateRepo(repo);
		
		List<User> user = new ArrayList<>(repo.getByRolename("Projektleiter"));
		assertThat("One Element in List", user.size(),is(0));
		assertThat("Test Element One", user.toString(), is("[]"));
	}
	
	@Test
	void testGetUserByRolenameUnknownRole()
	{
		populateRepo(repo);
		
		try
		{
			repo.getByRolename("ABC");
			fail("excpected Exception");
		}
		catch(Exception e)
		{
			assertThat("Expected RepositoryException", e.getMessage(), is("no role with name ABC found in Database"));
		}
	}
}
