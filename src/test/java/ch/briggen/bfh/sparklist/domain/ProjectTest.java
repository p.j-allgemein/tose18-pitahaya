package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;

public class ProjectTest {

	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Test
	void testEmptyConstructorYieldsEmptyObject() {
		Project p = new Project();
		assertThat("Id",p.getId(),is(equalTo(0l)));
		assertThat("Projektnummer",p.getProjektnummer(),is(equalTo(0l)));
		assertThat("Projektname",p.getProjektname(),is(equalTo(null)));
		assertThat("Projektleiter",p.getProjektleiter(),is(equalTo(null)));
		assertThat("Projektstart",p.getProjektstart(),is(equalTo(null)));
		assertThat("Projektende",p.getProjektende(),is(equalTo(null)));
		assertThat("Projektphase",p.getProjektphase(),is(equalTo(null)));
		assertThat("Projektstatus",p.getProjektstatus(),is(equalTo(null)));
		assertThat("Projektbudget",p.getProjektbudget(),is(equalTo(0.0)));
	}
	
	@Test
	void testProjektnummerZero()
	{
		Project p = new Project(1, 0, null, null, null, null, null, null, 0.0);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektnummer fehlt")));
	}
	
	@Test
	void testProjektnameNull()
	{
		Project p = new Project(1, 5, null, null, null, null, null, null, 0.0);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektname fehlt")));
	}
	
	@Test
	void testProjektnameEmpty()
	{
		Project p = new Project(1, 5, "", null, null, null, null, null, 0.0);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektname fehlt")));
	}
	
	@Test
	void testProjektleiterNull()
	{
		Project p = new Project(1, 5, "A", null, null, null, null, null, 0.0);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektleiter fehlt")));
	}
	
	@Test
	void testProjektstartNull()
	{
		Project p = new Project(1, 5, "A", new User(), null, null, null, null, 0.0);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektstart fehlt")));
	}
	
	@Test
	void testProjektendeNull()
	{
		Project p = new Project(1, 5, "A", new User(), LocalDate.now(), null, null, null, 0.0);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektende fehlt")));
	}
	
	@Test
	void testProjektphaseNull()
	{
		Project p = new Project(1, 5, "A", new User(), LocalDate.now(), LocalDate.now(), null, null, 0.0);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektphase fehlt")));
	}
	
	@Test
	void testProjektphaseEmpty()
	{
		Project p = new Project(1, 5, "A", new User(), LocalDate.now(), LocalDate.now(), "", null, 0.0);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektphase fehlt")));
	}
	
	@Test
	void testProjektstatusNull()
	{
		Project p = new Project(1, 5, "A", new User(), LocalDate.now(), LocalDate.now(), "A", null, 0.0);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektstatus fehlt")));
	}
	
	@Test
	void testProjektstatusEmpty()
	{
		Project p = new Project(1, 5, "A", new User(), LocalDate.now(), LocalDate.now(), "A", "", 0.0);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektstatus fehlt")));
	}
	
	@Test
	void testProjektbudgetZero()
	{
		Project p = new Project(1, 5, "A", new User(), LocalDate.now(), LocalDate.now(), "A", "A", 0.0);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektbudget fehlt")));
	}
	
	@Test
	void testProjektbudgetNegativ()
	{
		Project p = new Project(1, 5, "A", new User(), LocalDate.now(), LocalDate.now(), "A", "A", -10.2);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektbudget fehlt")));
	}
}
