package ch.briggen.bfh.sparklist.domain;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import org.h2.tools.RunScript;

public class RoleDBTestHelper extends DBTestHelper {

	public static void initData() throws SQLException
	{
		try(Connection c = JdbcRepositoryHelper.getConnection())
		{
			ClassLoader myLoader = RoleRepositoryTest.class.getClassLoader();
			InputStream sqlscript = myLoader.getResourceAsStream("insertinitdata.sql");
			RunScript.execute(c,new InputStreamReader(sqlscript));
		}
	}
}
