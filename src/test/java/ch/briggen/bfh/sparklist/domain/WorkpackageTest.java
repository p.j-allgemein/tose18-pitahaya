package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;

public class WorkpackageTest {
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Test
	void testEmptyConstructorYieldsEmptyObject() {
		Workpackage w = new Workpackage();
		assertThat("Id",w.getId(),is(equalTo(0l)));
		assertThat("Project",w.getProject(),is(equalTo(null)));
		assertThat("Projectphase",w.getProjectphase(),is(equalTo(null)));
		assertThat("Workpackagename",w.getWorkpackagename(),is(equalTo(null)));
		assertThat("PlannedValue",w.getPlannedValue(),is(equalTo(0.0)));
		assertThat("EarnedValue",w.getEarnedValue(),is(equalTo(0.0)));
		assertThat("Startdate",w.getStartdate(),is(equalTo(null)));
		assertThat("Enddate",w.getEnddate(),is(equalTo(null)));
		assertThat("Done",w.isDone(),is(equalTo(false)));
	}
	
	@Test
	void testProjectNull()
	{
		Workpackage w = new Workpackage(1, null, null, null, 0.0, 0.0, null, null, false);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			w.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projekt fehlt")));
	}
	
	@Test
	void testProjectphaseNull()
	{
		Workpackage w = new Workpackage(1, new Project(), null, null, 0.0, 0.0, null, null, false);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			w.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Projektphase fehlt")));
	}
	
	@Test
	void testWorkpackagenameNull()
	{
		Workpackage w = new Workpackage(1, new Project(), new Projectphase(), null, 0.0, 0.0, null, null, false);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			w.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Workpackagename fehlt")));
	}
	
	@Test
	void testWorkpackagenameEmpty()
	{
		Workpackage w = new Workpackage(1, new Project(), new Projectphase(), "", 0.0, 0.0, null, null, false);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			w.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Workpackagename fehlt")));
	}
	
	@Test
	void testPlannedValueNegativ()
	{
		Workpackage w = new Workpackage(1, new Project(), new Projectphase(), "A", -35.31, 0.0, null, null, false);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			w.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("PlannedValue fehlt")));
	}
	
	@Test
	void testEarnedValueNegativ()
	{
		Workpackage w = new Workpackage(1, new Project(), new Projectphase(), "A", 1.0, -12.30, null, null, false);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			w.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("EarnedValue fehlt")));
	}
	
	@Test
	void testStartdateNull()
	{
		Workpackage w = new Workpackage(1, new Project(), new Projectphase(), "A", 1.0, 1.0, null, null, false);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			w.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Startdate fehlt")));
	}
	
	@Test
	void testEnddateNull()
	{
		Workpackage w = new Workpackage(1, new Project(), new Projectphase(), "A", 1.0, 1.0, LocalDate.now(), null, false);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			w.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Enddate fehlt")));
	}
	
	@Test
	void testValid()
	{
		Workpackage w = new Workpackage(1, new Project(), new Projectphase(), "A", 1.0, 1.0, LocalDate.now(), LocalDate.now(), false);
		w.validate();
	}
}
