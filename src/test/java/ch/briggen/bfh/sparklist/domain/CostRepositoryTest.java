package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetSequence;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class CostRepositoryTest {
	UserRepository userRepo = null;
	CostRepository repo = null;
	ProjectRepository projectRepo = null;
	WorkpackageRepository workRepo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	private static void populateRepo(CostRepository c) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(i + 1,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummyProject = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter), 
										LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			Workpackage dummyWp = new Workpackage(0, dummyProject, new Projectphase(3, "FakePhase" + i), "FakeWP" + i, i, i,
												  LocalDate.parse("2018.03.01", formatter),
												  LocalDate.parse("2019.12.31", formatter), false);
			Cost dummy = new Cost(0, dummyWp, dummyUser, "FakeCost" + i, i, LocalDate.parse("2018.03.01", formatter));
			
			c.insert(dummy);
		}
	}
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i * 1000,"FakeUser" + i * 1000, "FakeName" + i * 1000, "FakeVorname" + i * 1000, new Role(3, "FakeRole" + i * 1000),
								  "FakeAbteilung" + i * 1000, "FakeMail" + i * 1000, "FakeT" + i * 1000);
			u.insert(dummy);
		}
	}
	
	private static void populateWorkpackageRepo(WorkpackageRepository w) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
					  				  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			Project dummyProject = new Project(0, i * 100, "FakeProject" + i * 100, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i * 100, "FakeStatus" + i * 100, i * 100);
			Workpackage dummy = new Workpackage(0, dummyProject, new Projectphase(3, "FakePhase" + i * 100), "FakeWP" + i * 100, i * 100, i * 100,
												LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
												false);
			
			w.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		userRepo = new UserRepository();
		repo = new CostRepository();
		projectRepo = new ProjectRepository();
		workRepo = new WorkpackageRepository();
		populateUserRepo(userRepo);
		populateWorkpackageRepo(workRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Workpackages");
		resetDB("Projects");
		resetDB("Users");
		resetSequence("Projectnumber");
	}

	@Test
	void testDBData() {
		assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo);
		List<Cost> list = new ArrayList<>(repo.getAll());
		assertThat("Freshly populated DB must hold 10 Projects",list.size(),is(10));
		assertThat("Test Element 1", list.get(0).toString(), is("Cost:{id: 1; workpackage: Workpackage:{id: 11; " +
				"project: Project:{id: 11; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; " +
				"username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; " +
				"abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeT0}; start: 2018.03.01; end: 2019.12.31; " +
				"projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; " +
				"projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP0; " +
				"plannedValue: 0.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; " +
				"user: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; " +
				"rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeT0}; " +
				"beschreibung: FakeCost0; betrag: 0.000000; datum: 2018.03.01}"));
	}
	
	@Test
	void testGetCostByName()
	{
		populateRepo(repo);
		
		List<Cost> cost = new ArrayList<>(repo.getByName("FakeCost0"));
		assertThat("One Element in List", cost.size(),is(1));
		assertThat("Test Element One", cost.get(0).toString(), is("Cost:{id: 1; workpackage: Workpackage:{id: 11; " +
				"project: Project:{id: 11; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; " +
				"username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; " +
				"abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeT0}; start: 2018.03.01; end: 2019.12.31; " +
				"projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; " +
				"projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP0; " +
				"plannedValue: 0.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; " +
				"user: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; " +
				"rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeT0}; " +
				"beschreibung: FakeCost0; betrag: 0.000000; datum: 2018.03.01}"));
	}
	
	@Test
	void testGetCostByNameNotFound()
	{
		populateRepo(repo);
		
		List<Cost> cost = new ArrayList<>(repo.getByName("ABC"));
		assertThat("Nothing in List", cost.isEmpty(),is(true));
	}
	
	@Test
	void testGetCostId3()
	{
		populateRepo(repo);
		
		Cost cost = repo.getById(3);
		assertThat("Test User Three", cost.toString(), is("Cost:{id: 3; workpackage: Workpackage:{id: 13; " +
				"project: Project:{id: 13; projektnummer: 2; projektname: FakeProject2; projektleiter: User:{id: 3; " +
				"username: FakeUser2000; name: FakeName2000; vorname: FakeVorname2000; role: Role:{id: 3; " +
				"rolename: Projektleiter}; abteilung: FakeAbteilung2000; mail: FakeMail2000; telefon: FakeT2000}; " +
				"start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; projekstatus: FakeStatus2; " +
				"projektbudget: 2.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; " +
				"workpackagename: FakeWP2; plannedValue: 2.000000; earnedValue: 2.000000; startdate: 2018.03.01; " +
				"enddate: 2019.12.31; done: false}; user: User:{id: 3; username: FakeUser2000; name: FakeName2000; " +
				"vorname: FakeVorname2000; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2000; " +
				"mail: FakeMail2000; telefon: FakeT2000}; beschreibung: FakeCost2; betrag: 2.000000; datum: 2018.03.01}"));
	}
	
	@Test
	void testGetCostIdNotFound()
	{
		populateRepo(repo);
		
		assertThat("Project does not exist", repo.getById(99),is(nullValue()));
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeWP0,FakeUser100,One,11.11,2018.03.01",
				"2,FakeWP1,FakeUser200,Two,22.22,2018.03.02",
				"3,FakeWP2,FakeUser300,Three,33.33,2018.03.03"})
	void testInsertCosts(long id, String workpackagename, String username, String beschreibung, double betrag,
							String datum)
	{
		populateRepo(repo);
		List<User> users = new ArrayList<>(userRepo.getByName(username));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(username));
		List<Workpackage> wps = new ArrayList<>(workRepo.getByName(workpackagename));
		assertThat("workpackages list is not empty", wps.isEmpty(), is(false));
		assertThat("workpackages.workpackagename", wps.get(0).getWorkpackagename(), is(workpackagename));
		Cost c = new Cost(id, wps.get(0), users.get(0), beschreibung, betrag, LocalDate.parse(datum, formatter));
		
		long dbId = repo.insert(c);
		Cost fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("workpackage = workpackagename", fromDB.getWorkpackage().getWorkpackagename(),is(workpackagename) );
		assertThat("user = username", fromDB.getUser().getUsername(),is(username) );
		assertThat("beschreibung = beschreibung", fromDB.getBeschreibung(),is(beschreibung) );
		assertThat("betrag = betrag", fromDB.getBetrag(),is(betrag) );
		assertThat("datum = datum", fromDB.getDatum().format(formatter), is(datum) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeUser100,One,11.11,2018.03.01",
				"2,FakeUser200,Two,22.22,2018.03.02",
				"3,FakeUser300,Three,33.33,2018.03.03"})
	void testInsertCostsNullWP(long id, String username, String beschreibung, double betrag, String datum)
	{
		populateRepo(repo);
		List<User> users = new ArrayList<>(userRepo.getByName(username));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(username));
		Cost c = new Cost(id, null, users.get(0), beschreibung, betrag, LocalDate.parse(datum, formatter));
		
		long dbId = repo.insert(c);
		Cost fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("workpackage = workpackagename", fromDB.getWorkpackage(),nullValue() );
		assertThat("user = username", fromDB.getUser().getUsername(),is(username) );
		assertThat("beschreibung = beschreibung", fromDB.getBeschreibung(),is(beschreibung) );
		assertThat("betrag = betrag", fromDB.getBetrag(),is(betrag) );
		assertThat("datum = datum", fromDB.getDatum().format(formatter), is(datum) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeWP0,One,11.11,2018.03.01",
				"2,FakeWP1,Two,22.22,2018.03.02",
				"3,FakeWP2,Three,33.33,2018.03.03"})
	void testInsertCostsNullUser(long id, String workpackagename, String beschreibung, double betrag, String datum)
	{
		populateRepo(repo);
		List<Workpackage> wps = new ArrayList<>(workRepo.getByName(workpackagename));
		assertThat("workpackages list is not empty", wps.isEmpty(), is(false));
		assertThat("workpackages.workpackagename", wps.get(0).getWorkpackagename(), is(workpackagename));
		Cost c = new Cost(id, wps.get(0), null, beschreibung, betrag, LocalDate.parse(datum, formatter));
		
		long dbId = repo.insert(c);
		Cost fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("workpackage = workpackagename", fromDB.getWorkpackage().getWorkpackagename(),is(workpackagename) );
		assertThat("user = username", fromDB.getUser(),nullValue() );
		assertThat("beschreibung = beschreibung", fromDB.getBeschreibung(),is(beschreibung) );
		assertThat("betrag = betrag", fromDB.getBetrag(),is(betrag) );
		assertThat("datum = datum", fromDB.getDatum().format(formatter), is(datum) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeWP0,FakeUser100,One,11.11,2018.03.01,FakeWP4,FakeUser400,Eins,111.11,2018.02.01",
				"2,FakeWP1,FakeUser200,Two,22.22,2018.03.02,FakeWP5,FakeUser500,Zwei,222.22,2018.02.02",
				"3,FakeWP2,FakeUser300,Three,33.33,2018.03.03,FakeWP5,FakeUser500,Drei,333.33,2018.02.03"})
	void testUpdateCosts(long id, String workpackagename, String username, String beschreibung, double betrag,
						String datum, String newWorkpackagename, String newUsername, String newBeschreibung,
						double newBetrag, String newDatum)
	{
		populateRepo(repo);
		
		List<User> users = new ArrayList<>(userRepo.getByName(username));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(username));
		List<Workpackage> wps = new ArrayList<>(workRepo.getByName(workpackagename));
		assertThat("workpackages list is not empty", wps.isEmpty(), is(false));
		assertThat("workpackages.workpackagename", wps.get(0).getWorkpackagename(), is(workpackagename));
		Cost c = new Cost(id, wps.get(0), users.get(0), beschreibung, betrag, LocalDate.parse(datum, formatter));
		
		long dbId = repo.insert(c);
		
		users = new ArrayList<>(userRepo.getByName(newUsername));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(newUsername));
		wps = new ArrayList<>(workRepo.getByName(newWorkpackagename));
		assertThat("workpackages list is not empty", wps.isEmpty(), is(false));
		assertThat("workpackages.workpackagename", wps.get(0).getWorkpackagename(), is(newWorkpackagename));
		
		c.setId(dbId);
		c.setWorkpackage(wps.get(0));
		c.setUser(users.get(0));
		c.setBeschreibung(newBeschreibung);
		c.setBetrag(newBetrag);
		c.setDatum(LocalDate.parse(newDatum, formatter));
		repo.save(c);
		
		Cost fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("workpackage = workpackagename", fromDB.getWorkpackage().getWorkpackagename(),is(newWorkpackagename) );
		assertThat("user = username", fromDB.getUser().getUsername(),is(newUsername) );
		assertThat("beschreibung = beschreibung", fromDB.getBeschreibung(),is(newBeschreibung) );
		assertThat("betrag = betrag", fromDB.getBetrag(),is(newBetrag) );
		assertThat("datum = datum", fromDB.getDatum().format(formatter), is(newDatum) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeWP0,FakeUser100,One,11.11,2018.03.01",
				"2,FakeWP1,FakeUser200,Two,22.22,2018.03.02",
				"3,FakeWP2,FakeUser300,Three,33.33,2018.03.03"})
	void testDeleteCosts(long id, String workpackagename, String username, String beschreibung, double betrag,
						String datum)
	{
		populateRepo(repo);
		
		List<User> users = new ArrayList<>(userRepo.getByName(username));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(username));
		List<Workpackage> wps = new ArrayList<>(workRepo.getByName(workpackagename));
		assertThat("workpackages list is not empty", wps.isEmpty(), is(false));
		assertThat("workpackages.workpackagename", wps.get(0).getWorkpackagename(), is(workpackagename));
		Cost c = new Cost(id, wps.get(0), users.get(0), beschreibung, betrag, LocalDate.parse(datum, formatter));
		
		long dbId = repo.insert(c);
		Cost fromDB = repo.getById(dbId);
		assertThat("Cost was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThat("Cost is should be deleted", repo.getById(dbId),is(nullValue()));
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo);
		for(Cost i : repo.getAll())
		{
			repo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeWP0,FakeUser100,One,11.11,2018.03.01",
				"2,FakeWP1,FakeUser200,Two,22.22,2018.03.02",
				"3,FakeWP2,FakeUser300,Three,33.33,2018.03.03"})
	void testGetByOneName(long id, String workpackagename, String username, String beschreibung, double betrag,
						String datum)
	{
		populateRepo(repo);
		
		List<User> users = new ArrayList<>(userRepo.getByName(username));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.projektleiter", users.get(0).getUsername(), is(username));
		List<Workpackage> wps = new ArrayList<>(workRepo.getByName(workpackagename));
		assertThat("workpackages list is not empty", wps.isEmpty(), is(false));
		assertThat("workpackages.workpackagename", wps.get(0).getWorkpackagename(), is(workpackagename));
		Cost c = new Cost(id, wps.get(0), users.get(0), beschreibung, betrag, LocalDate.parse(datum, formatter));
		
		long dbId = repo.insert(c);
		List<Cost> fromDB = new ArrayList<>(repo.getByName(beschreibung));
		assertThat("Exactly one item was returned", fromDB.size(),is(1));
		
		Cost elementFromDB = fromDB.iterator().next();
		assertThat("id = id", elementFromDB.getId(),is(dbId) );
		assertThat("workpackage = workpackagename", elementFromDB.getWorkpackage().getWorkpackagename(),is(workpackagename) );
		assertThat("user = username", elementFromDB.getUser().getUsername(),is(username) );
		assertThat("beschreibung = beschreibung", elementFromDB.getBeschreibung(),is(beschreibung) );
		assertThat("betrag = betrag", elementFromDB.getBetrag(),is(betrag) );
		assertThat("datum = datum", elementFromDB.getDatum().format(formatter), is(datum) );
	}
	
	@Test
	void testUpdateCostNullWorkpackage()
	{
		populateRepo(repo);
		
		Cost c = repo.getById(1);
		c.setWorkpackage(null);
		repo.save(c);
		
		Cost fromDB = repo.getById(c.getId());
		assertThat("id = id", fromDB.getId(),is(c.getId()) );
		assertThat("workpackage = workpackagename", fromDB.getWorkpackage(),nullValue() );
		assertThat("user = username", fromDB.getUser().getUsername(),is(c.getUser().getUsername()) );
		assertThat("beschreibung = beschreibung", fromDB.getBeschreibung(),is(c.getBeschreibung()) );
		assertThat("betrag = betrag", fromDB.getBetrag(),is(c.getBetrag()) );
		assertThat("datum = datum", fromDB.getDatum(), is(c.getDatum()) );
	}
	
	@Test
	void testUpdateCostUnknownWorkpackage()
	{
		populateRepo(repo);
		Project p = projectRepo.getById(1);
		Workpackage w = new Workpackage(0, p, new Projectphase(0, "Test"), "WP-5699", 235, 234,
						LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter), false);
		
		Cost c = repo.getById(1);
		c.setWorkpackage(w);
		repo.save(c);
		
		Cost fromDB = repo.getById(c.getId());
		assertThat("id = id", fromDB.getId(),is(c.getId()) );
		assertThat("workpackage = workpackagename", fromDB.getWorkpackage().getWorkpackagename(),is("WP-5699") );
		assertThat("user = username", fromDB.getUser().getUsername(),is(c.getUser().getUsername()) );
		assertThat("beschreibung = beschreibung", fromDB.getBeschreibung(),is(c.getBeschreibung()) );
		assertThat("betrag = betrag", fromDB.getBetrag(),is(c.getBetrag()) );
		assertThat("datum = datum", fromDB.getDatum(), is(c.getDatum()) );
	}
	
	@Test
	void testUpdateCostNullUser()
	{
		populateRepo(repo);
		
		Cost c = repo.getById(1);
		c.setUser(null);
		repo.save(c);
		
		Cost fromDB = repo.getById(c.getId());
		assertThat("id = id", fromDB.getId(),is(c.getId()) );
		assertThat("workpackage = workpackagename", fromDB.getWorkpackage().getWorkpackagename(),is(c.getWorkpackage().getWorkpackagename()) );
		assertThat("user = username", fromDB.getUser(),nullValue() );
		assertThat("beschreibung = beschreibung", fromDB.getBeschreibung(),is(c.getBeschreibung()) );
		assertThat("betrag = betrag", fromDB.getBetrag(),is(c.getBetrag()) );
		assertThat("datum = datum", fromDB.getDatum(), is(c.getDatum()) );
	}
	
	@Test
	void testUpdateCostUnknownUser()
	{
		populateRepo(repo);
		User u = new User(0,"TestUser", "TestName", "TestVorname", new Role(3, "TestRole"),
						  "TestAbteilung", "TestMail", "TestTel");
		
		Cost c = repo.getById(1);
		c.setUser(u);
		repo.save(c);
		
		Cost fromDB = repo.getById(c.getId());
		assertThat("id = id", fromDB.getId(),is(c.getId()) );
		assertThat("workpackage = workpackagename", fromDB.getWorkpackage().getWorkpackagename(),is(c.getWorkpackage().getWorkpackagename()) );
		assertThat("user = username", fromDB.getUser().getUsername(),is("TestUser") );
		assertThat("beschreibung = beschreibung", fromDB.getBeschreibung(),is(c.getBeschreibung()) );
		assertThat("betrag = betrag", fromDB.getBetrag(),is(c.getBetrag()) );
		assertThat("datum = datum", fromDB.getDatum(), is(c.getDatum()) );
	}
	
	@Test
	void testInsertCostNullUser()
	{
		populateRepo(repo);
		
		Workpackage w = workRepo.getById(11);
		
		Cost c = new Cost(0, w, null, "TestCost", 234.34, LocalDate.parse("2018.03.01", formatter));
		long dbId = repo.insert(c);
		
		Cost fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("workpackage = workpackagename", fromDB.getWorkpackage().getWorkpackagename(),is(c.getWorkpackage().getWorkpackagename()) );
		assertThat("user = username", fromDB.getUser(),nullValue() );
		assertThat("beschreibung = beschreibung", fromDB.getBeschreibung(),is(c.getBeschreibung()) );
		assertThat("betrag = betrag", fromDB.getBetrag(),is(c.getBetrag()) );
		assertThat("datum = datum", fromDB.getDatum(), is(c.getDatum()) );
	}
	
	@Test
	void testInsertCostUnknownUser()
	{
		populateRepo(repo);
		Workpackage w = workRepo.getById(11);
		User u = new User(0,"TestUser", "TestName", "TestVorname", new Role(3, "TestRole"),
						  "TestAbteilung", "TestMail", "TestTel");
		
		Cost c = new Cost(0, w, u, "TestCost", 234.34, LocalDate.parse("2018.03.01", formatter));
		long dbId = repo.insert(c);
		
		Cost fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("workpackage = workpackagename", fromDB.getWorkpackage().getWorkpackagename(),is(c.getWorkpackage().getWorkpackagename()) );
		assertThat("user = username", fromDB.getUser().getUsername(),is("TestUser") );
		assertThat("beschreibung = beschreibung", fromDB.getBeschreibung(),is(c.getBeschreibung()) );
		assertThat("betrag = betrag", fromDB.getBetrag(),is(c.getBetrag()) );
		assertThat("datum = datum", fromDB.getDatum(), is(c.getDatum()) );
	}
	
	@Test
	void testGetNoCostByName()
	{
		populateRepo(repo);
		
		List<Cost> fromDB = new ArrayList<>(repo.getByName("NotExistingItem"));
		assertThat("Exactly one item was returned", fromDB.size(),is(0));
		
	}
	
	@Test
	void testGetCostByWorkpackagename()
	{
		populateRepo(repo);
		
		List<Cost> costs = new ArrayList<>(repo.getByWorkpackage("FakeWP1"));
		assertThat("One Element in List", costs.size(),is(1));
		assertThat("Test Element One", costs.get(0).toString(), is("Cost:{id: 2; workpackage: Workpackage:{id: 12; " +
				"project: Project:{id: 12; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 2; " +
				"username: FakeUser1000; name: FakeName1000; vorname: FakeVorname1000; role: Role:{id: 3; " +
				"rolename: Projektleiter}; abteilung: FakeAbteilung1000; mail: FakeMail1000; telefon: FakeT1000}; " +
				"start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; " +
				"projektbudget: 1.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; " +
				"workpackagename: FakeWP1; plannedValue: 1.000000; earnedValue: 1.000000; startdate: 2018.03.01; " +
				"enddate: 2019.12.31; done: false}; user: User:{id: 2; username: FakeUser1000; name: FakeName1000; " +
				"vorname: FakeVorname1000; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1000; " +
				"mail: FakeMail1000; telefon: FakeT1000}; beschreibung: FakeCost1; betrag: 1.000000; datum: 2018.03.01}"));
	}
	
	@Test
	void testGetCostByWorkpackageId()
	{
		populateRepo(repo);
		
		List<Cost> costs = new ArrayList<>(repo.getByWorkpackage("11"));
		assertThat("One Element in List", costs.size(),is(1));
		assertThat("Test Element One", costs.get(0).toString(), is("Cost:{id: 1; workpackage: Workpackage:{id: 11; " +
				"project: Project:{id: 11; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; " +
				"username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; "+ 
				"abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeT0}; start: 2018.03.01; end: 2019.12.31; " +
				"projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; projectphase: " +
				"Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP0; plannedValue: 0.000000; " +
				"earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: User:{id: 1; " +
				"username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; " +
				"abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeT0}; beschreibung: FakeCost0; betrag: 0.000000; " +
				"datum: 2018.03.01}"));
	}
	
	@Test
	void testGetCostByWorkpackagenameNotFound()
	{
		List<Cost> costs = null;
		try
		{
			populateRepo(repo);
		
			costs = new ArrayList<>(repo.getByWorkpackage("ABC"));
			assertThat("Nothing in List", costs.toString(), is("[]"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unecpected Exception " + e.getMessage());
		}
	}
}
