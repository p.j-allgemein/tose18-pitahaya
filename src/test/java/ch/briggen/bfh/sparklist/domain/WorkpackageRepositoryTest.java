package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetSequence;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class WorkpackageRepositoryTest {
	private ProjectRepository projectRepo = null;
	private ProjectphaseRepository phaseRepo = null;
	private UserRepository userRepo = null;
	private WorkpackageRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	private static void populateRepo(WorkpackageRepository w) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								      "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummyProject = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			Workpackage dummy = new Workpackage(0, dummyProject, new Projectphase(3, "FakePhase" + i), "FakeWP" + i, i, i,
												LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
												false);
			
			w.insert(dummy);
		}
	}
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
								  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			u.insert(dummy);
		}
	}
	
	private static void populateProjectRepo(ProjectRepository p) {
		UserRepository urep = new UserRepository();
		for(int i = 0; i < 10; ++i) {
			List<User> dummyUsers = new ArrayList<>(urep.getByName("FakeUser" + i * 100));
			Project dummy = new Project(i * 100, i * 100, "FakeProject" + i * 100, dummyUsers.get(0),
									    LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
									    "FakePhase" + i * 100, "FakeStatus" + i * 100, i * 100);
			p.insert(dummy);
		}
	}
	
	private static void populateForeignKeys(Workpackage w)
	{
		CostRepository cRep = new CostRepository();
		UserRepository uRep = new UserRepository();
		
		List<User> users = new ArrayList<>(uRep.getAll());
		
		for (int i = 0; i < 2; ++i)
		{
			Cost c = new Cost(0, w, users.get(i), "Foreign" + i, i, LocalDate.parse("2018.05.01", formatter));
			cRep.insert(c);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		projectRepo = new ProjectRepository();
		phaseRepo = new ProjectphaseRepository();
		userRepo = new UserRepository();
		repo = new WorkpackageRepository();
		populateUserRepo(userRepo);
		populateProjectRepo(projectRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projects");
		resetDB("Users");
		resetSequence("Projectnumber");
	}

	@Test
	void testDBData() {
		assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo);
		List<Workpackage> list = new ArrayList<>(repo.getAll());
		assertThat("Freshly populated DB must hold 10 Projectrisks",list.size(),is(10));
		assertThat("Test Element 1", list.get(0).toString(), is("Workpackage:{id: 1; project: Project:{id: 11; projektnummer: 0; " +
				"projektname: FakeProject0; projektleiter: User:{id: 11; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; " +
				"role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; " +
				"start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; " +
				"projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP0; plannedValue: 0.000000; " +
				"earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}"));
	}
	
	@Test
	void testGetWorkpackageByName()
	{
		populateRepo(repo);
		
		List<Workpackage> wp = new ArrayList<>(repo.getByName("FakeWP0"));
		assertThat("One Element in List", wp.size(),is(1));
		assertThat("Test Element One", wp.get(0).toString(), is("Workpackage:{id: 1; project: Project:{id: 11; projektnummer: 0; " +
				"projektname: FakeProject0; projektleiter: User:{id: 11; username: FakeUser0; name: FakeName0; " +
				"vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; " +
				"telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; " +
				"projektbudget: 0.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; " +
				"workpackagename: FakeWP0; plannedValue: 0.000000; earnedValue: 0.000000; startdate: 2018.03.01; " +
				"enddate: 2019.12.31; done: false}"));
	}
	
	@Test
	void testGetWorkpackageByNameNotFound()
	{
		populateRepo(repo);
		
		List<Workpackage> wp = new ArrayList<>(repo.getByName("ABC"));
		assertThat("Nothing in List", wp.isEmpty(),is(true));
	}
	
	@Test
	void testGetWorkpackageId3()
	{
		populateRepo(repo);
		
		Workpackage workpackage = repo.getById(3);
		assertThat("Test User Three", workpackage.toString(), is("Workpackage:{id: 3; project: Project:{id: 13; projektnummer: 2; " +
				"projektname: FakeProject2; projektleiter: User:{id: 13; username: FakeUser2; name: FakeName2; " +
				"vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; " +
				"telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; projekstatus: FakeStatus2; " +
				"projektbudget: 2.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; " +
				"workpackagename: FakeWP2; plannedValue: 2.000000; earnedValue: 2.000000; startdate: 2018.03.01; " +
				"enddate: 2019.12.31; done: false}"));
	}
	
	@Test
	void testGetWorkpackageIdNotFound()
	{
		populateRepo(repo);
		
		assertThat("Project does not exist", repo.getById(99),is(nullValue()));
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject100,Initialisierung,One,11.11,11.11,2018.03.01,2019.12.31,false",
				"2,FakeProject200,Konzeption,Two,22.22,22.22,2018.03.01,2019.12.31,false",
				"3,FakeProject300,Realisierung,Three,33.33,33.33,2018.03.01,2019.12.31,false"})
	void testInsertWorkpackage(long id, String projectname, String phasenname, String workpackagename, double plannedValue,
			   				   double earnedValue, String startdate, String enddate, boolean done)
	{
		populateRepo(repo);
		
		List<Project> projects = new ArrayList<>(projectRepo.getByName(projectname));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(projectname));
		List<Projectphase> phases = new ArrayList<>(phaseRepo.getByName(phasenname));
		assertThat("phases list is not empty", phases.isEmpty(), is(false));
		assertThat("phases.phasenname", phases.get(0).getPhasenname(), is(phasenname));
		Workpackage w = new Workpackage(id, projects.get(0), phases.get(0), workpackagename, plannedValue, earnedValue,
										LocalDate.parse(startdate, formatter), LocalDate.parse(enddate, formatter), done);
		
		long dbId = repo.insert(w);
		Workpackage fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("project = projectname", fromDB.getProject().getProjektname(),is(projectname) );
		assertThat("projectphase = phasenname", fromDB.getProjectphase().getPhasenname(),is(phasenname) );
		assertThat("workpackagename = workpackagename", fromDB.getWorkpackagename(),is(workpackagename) );
		assertThat("plannedValue = plannedValue", fromDB.getPlannedValue(), is(plannedValue) );
		assertThat("earnedValue = earnedValue", fromDB.getEarnedValue(),is(earnedValue) );
		assertThat("startdate = startdate", fromDB.getStartdate().format(formatter), is(startdate) );
		assertThat("enddate = enddate", fromDB.getEnddate().format(formatter),is(enddate) );
		assertThat("done = done", fromDB.isDone(),is(done) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject100,ABC,One,11.11,11.11,2018.03.01,2019.12.31,false",
				"2,FakeProject200,DEF,Two,22.22,22.22,2018.03.01,2019.12.31,false",
				"3,FakeProject300,GFH,Three,33.33,33.33,2018.03.01,2019.12.31,false"})
	void testInsertWorkpackageUnknownPhase(long id, String projectname, String phasenname, String workpackagename,
										   double plannedValue, double earnedValue, String startdate, String enddate, boolean done)
	{
		populateRepo(repo);
		
		List<Project> projects = new ArrayList<>(projectRepo.getByName(projectname));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(projectname));
		List<Projectphase> phases = new ArrayList<>(phaseRepo.getByName(phasenname));
		assertThat("phases list is not empty", phases.isEmpty(), is(true));
		Workpackage w = new Workpackage(id, projects.get(0), new Projectphase(0, phasenname), workpackagename, plannedValue, earnedValue,
										LocalDate.parse(startdate, formatter), LocalDate.parse(enddate, formatter), done);
		
		long dbId = repo.insert(w);
		Workpackage fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("project = projectname", fromDB.getProject().getProjektname(),is(projectname) );
		assertThat("projectphase = phasenname", fromDB.getProjectphase().getPhasenname(),is(phasenname) );
		assertThat("workpackagename = workpackagename", fromDB.getWorkpackagename(),is(workpackagename) );
		assertThat("plannedValue = plannedValue", fromDB.getPlannedValue(), is(plannedValue) );
		assertThat("earnedValue = earnedValue", fromDB.getEarnedValue(),is(earnedValue) );
		assertThat("startdate = startdate", fromDB.getStartdate().format(formatter), is(startdate) );
		assertThat("enddate = enddate", fromDB.getEnddate().format(formatter),is(enddate) );
		assertThat("done = done", fromDB.isDone(),is(done) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,Initialisierung,One,11.11,11.11,2018.03.01,2019.12.31,false",
				"2,Konzeption,Two,22.22,22.22,2018.03.01,2019.12.31,false",
				"3,Realisierung,Three,33.33,33.33,2018.03.01,2019.12.31,false"})
	void testInsertWorkpackageNullProject(long id, String phasenname, String workpackagename, double plannedValue,
										  double earnedValue, String startdate, String enddate, boolean done)
	{
		populateRepo(repo);
		
		List<Projectphase> phases = new ArrayList<>(phaseRepo.getByName(phasenname));
		assertThat("phases list is not empty", phases.isEmpty(), is(false));
		assertThat("phases.phasenname", phases.get(0).getPhasenname(), is(phasenname));
		Workpackage w = new Workpackage(id, null, phases.get(0), workpackagename, plannedValue, earnedValue,
										LocalDate.parse(startdate, formatter), LocalDate.parse(enddate, formatter), done);
		
		long dbId = repo.insert(w);
		Workpackage fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("project = projectname", fromDB.getProject(),nullValue() );
		assertThat("projectphase = phasenname", fromDB.getProjectphase().getPhasenname(),is(phasenname) );
		assertThat("workpackagename = workpackagename", fromDB.getWorkpackagename(),is(workpackagename) );
		assertThat("plannedValue = plannedValue", fromDB.getPlannedValue(), is(plannedValue) );
		assertThat("earnedValue = earnedValue", fromDB.getEarnedValue(),is(earnedValue) );
		assertThat("startdate = startdate", fromDB.getStartdate().format(formatter), is(startdate) );
		assertThat("enddate = enddate", fromDB.getEnddate().format(formatter),is(enddate) );
		assertThat("done = done", fromDB.isDone(),is(done) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject100,One,11.11,11.11,2018.03.01,2019.12.31,false",
				"2,FakeProject200,Two,22.22,22.22,2018.03.01,2019.12.31,false",
				"3,FakeProject300,Three,33.33,33.33,2018.03.01,2019.12.31,false"})
	void testInsertWorkpackageNullProjectphase(long id, String projectname, String workpackagename, 
											   double plannedValue, double earnedValue, String startdate, String enddate,
											   boolean done)
	{
		populateRepo(repo);
		
		List<Project> projects = new ArrayList<>(projectRepo.getByName(projectname));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(projectname));
		Workpackage w = new Workpackage(id, projects.get(0), null, workpackagename, plannedValue, earnedValue,
										LocalDate.parse(startdate, formatter), LocalDate.parse(enddate, formatter), done);
		
		long dbId = repo.insert(w);
		Workpackage fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("project = projectname", fromDB.getProject().getProjektname(),is(projectname) );
		assertThat("projectphase = phasenname", fromDB.getProjectphase(),nullValue() );
		assertThat("workpackagename = workpackagename", fromDB.getWorkpackagename(),is(workpackagename) );
		assertThat("plannedValue = plannedValue", fromDB.getPlannedValue(), is(plannedValue) );
		assertThat("earnedValue = earnedValue", fromDB.getEarnedValue(),is(earnedValue) );
		assertThat("startdate = startdate", fromDB.getStartdate().format(formatter), is(startdate) );
		assertThat("enddate = enddate", fromDB.getEnddate().format(formatter),is(enddate) );
		assertThat("done = done", fromDB.isDone(),is(done) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject100,Initialisierung,One,11.11,11.11,2018.03.01,2019.12.31,false,FakeProject400,Konzeption,Eins,111.11,111.11,2018.02.01,2020.12.31,true",
				"2,FakeProject200,Konzeption,Two,22.22,22.22,2018.03.01,2019.12.31,false,FakeProject500,Realisierung,Zwei,222.22,222.22,2018.02.01,2020.12.31,true",
				"3,FakeProject300,Realisierung,Three,33.33,33.33,2018.03.01,2019.12.31,false,FakeProject600,Einführung,Drei,333.33,333.33,2018.02.01,2020.12.31,true"})
	void testUpdateWorkpackage(long id, String projectname, String phasenname, String workpackagename, double plannedValue,
							   double earnedValue, String startdate, String enddate, boolean done, String newProjectname,
							   String newPhasenname, String newWorkpackagename, double newPlannedValue, double newEarnedValue,
							   String newStartdate, String newEnddate, boolean newDone)
	{
		populateRepo(repo);
				
		List<Project> projects = new ArrayList<>(projectRepo.getByName(projectname));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(projectname));
		List<Projectphase> phases = new ArrayList<>(phaseRepo.getByName(phasenname));
		assertThat("phases list is not empty", phases.isEmpty(), is(false));
		assertThat("phases.phasenname", phases.get(0).getPhasenname(), is(phasenname));
		Workpackage w = new Workpackage(id, projects.get(0), phases.get(0), workpackagename, plannedValue, earnedValue,
										LocalDate.parse(startdate, formatter), LocalDate.parse(enddate, formatter), done);
		
		long dbId = repo.insert(w);
		
		projects = new ArrayList<>(projectRepo.getByName(newProjectname));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(newProjectname));
		phases = new ArrayList<>(phaseRepo.getByName(newPhasenname));
		assertThat("phases list is not empty", phases.isEmpty(), is(false));
		assertThat("phases.phasenname", phases.get(0).getPhasenname(), is(newPhasenname));
		
		w.setId(dbId);
		w.setProject(projects.get(0));
		w.setProjectphase(phases.get(0));
		w.setWorkpackagename(newWorkpackagename);
		w.setPlannedValue(newPlannedValue);
		w.setEarnedValue(newEarnedValue);
		w.setStartdate(LocalDate.parse(newStartdate, formatter));
		w.setEnddate(LocalDate.parse(newEnddate, formatter));
		w.setDone(newDone);
		repo.save(w);
		
		Workpackage fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("project = projectname", fromDB.getProject().getProjektname(),is(newProjectname) );
		assertThat("projectphase = phasenname", fromDB.getProjectphase().getPhasenname(), is(newPhasenname) );
		assertThat("workpackagename = workpackagename", fromDB.getWorkpackagename(),is(newWorkpackagename) );
		assertThat("plannedValue = plannedValue", fromDB.getPlannedValue(), is(newPlannedValue) );
		assertThat("earnedValue = earnedValue", fromDB.getEarnedValue(),is(newEarnedValue) );
		assertThat("startdate = startdate", fromDB.getStartdate().format(formatter), is(newStartdate) );
		assertThat("enddate = enddate", fromDB.getEnddate().format(formatter),is(newEnddate) );
		assertThat("done = done", fromDB.isDone(),is(newDone) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject100,Initialisierung,One,11.11,11.11,2018.03.01,2019.12.31,false",
				"2,FakeProject200,Konzeption,Two,22.22,22.22,2018.03.01,2019.12.31,false",
				"3,FakeProject300,Realisierung,Three,33.33,33.33,2018.03.01,2019.12.31,false"})
	void testDeleteWorkpackage(long id, String projectname, String phasenname, String workpackagename, double plannedValue,
			   				   double earnedValue, String startdate, String enddate, boolean done)
	{
		populateRepo(repo);
		
		List<Project> projects = new ArrayList<>(projectRepo.getByName(projectname));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(projectname));
		List<Projectphase> phases = new ArrayList<>(phaseRepo.getByName(phasenname));
		assertThat("phases list is not empty", phases.isEmpty(), is(false));
		assertThat("phases.phasenname", phases.get(0).getPhasenname(), is(phasenname));
		Workpackage w = new Workpackage(id, projects.get(0), phases.get(0), workpackagename, plannedValue, earnedValue,
										LocalDate.parse(startdate, formatter), LocalDate.parse(enddate, formatter), done);
		
		long dbId = repo.insert(w);
		Workpackage fromDB = repo.getById(dbId);
		assertThat("Workpackage was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThat("Workpackage is should be deleted", repo.getById(dbId),is(nullValue()));
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject100,Initialisierung,One,11.11,11.11,2018.03.01,2019.12.31,false",
				"2,FakeProject200,Konzeption,Two,22.22,22.22,2018.03.01,2019.12.31,false",
				"3,FakeProject300,Realisierung,Three,33.33,33.33,2018.03.01,2019.12.31,false"})
	void testDeleteWorkpackageForeignKeys(long id, String projectname, String phasenname, String workpackagename, 
										  double plannedValue, double earnedValue, String startdate, String enddate, boolean done)
	{
		populateRepo(repo);
		
		List<Project> projects = new ArrayList<>(projectRepo.getByName(projectname));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(projectname));
		List<Projectphase> phases = new ArrayList<>(phaseRepo.getByName(phasenname));
		assertThat("phases list is not empty", phases.isEmpty(), is(false));
		assertThat("phases.phasenname", phases.get(0).getPhasenname(), is(phasenname));
		Workpackage w = new Workpackage(id, projects.get(0), phases.get(0), workpackagename, plannedValue, earnedValue,
										LocalDate.parse(startdate, formatter), LocalDate.parse(enddate, formatter), done);
		
		long dbId = repo.insert(w);
		Workpackage fromDB = repo.getById(dbId);
		populateForeignKeys(fromDB);
		assertThat("Workpackage was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThat("Workpackage is should be deleted", repo.getById(dbId),is(nullValue()));
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo);
		for(Workpackage i : repo.getAll())
		{
			repo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject100,Initialisierung,One,11.11,11.11,2018.03.01,2019.12.31,false",
				"2,FakeProject200,Konzeption,Two,22.22,22.22,2018.03.01,2019.12.31,false",
				"3,FakeProject300,Realisierung,Three,33.33,33.33,2018.03.01,2019.12.31,false"})
	void testGetByOneName(long id, String projectname, String phasenname, String workpackagename, double plannedValue,
		   				  double earnedValue, String startdate, String enddate, boolean done)
	{
		populateRepo(repo);
		
		List<Project> projects = new ArrayList<>(projectRepo.getByName(projectname));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(projectname));
		List<Projectphase> phases = new ArrayList<>(phaseRepo.getByName(phasenname));
		assertThat("phases list is not empty", phases.isEmpty(), is(false));
		assertThat("phases.phasenname", phases.get(0).getPhasenname(), is(phasenname));
		Workpackage w = new Workpackage(id, projects.get(0), phases.get(0), workpackagename, plannedValue, earnedValue,
										LocalDate.parse(startdate, formatter), LocalDate.parse(enddate, formatter), done);
		
		long dbId = repo.insert(w);
		List<Workpackage> fromDB = new ArrayList<>(repo.getByName(workpackagename));
		assertThat("Exactly one item was returned", fromDB.size(),is(1));
		
		Workpackage elementFromDB = fromDB.iterator().next();
		assertThat("id = id", elementFromDB.getId(),is(dbId) );
		assertThat("project = projectname", elementFromDB.getProject().getProjektname(),is(projectname) );
		assertThat("projectphase = phasenname", elementFromDB.getProjectphase().getPhasenname(),is(phasenname) );
		assertThat("workpackagename = workpackagename", elementFromDB.getWorkpackagename(),is(workpackagename) );
		assertThat("plannedValue = plannedValue", elementFromDB.getPlannedValue(), is(plannedValue) );
		assertThat("earnedValue = earnedValue", elementFromDB.getEarnedValue(),is(earnedValue) );
		assertThat("startdate = startdate", elementFromDB.getStartdate().format(formatter), is(startdate) );
		assertThat("enddate = enddate", elementFromDB.getEnddate().format(formatter),is(enddate) );
		assertThat("done = done", elementFromDB.isDone(),is(done) );
	}
	
	@Test
	void testUpdateWorkpackageNullProject()
	{
		populateRepo(repo);
		Workpackage w = repo.getById(1);
		w.setProject(null);
		repo.save(w);
		
		Workpackage fromDB = repo.getById(w.getId());
		assertThat("id = id", fromDB.getId(),is(w.getId()) );
		assertThat("project = projectname", fromDB.getProject(),nullValue() );
		assertThat("projectphase = phasenname", fromDB.getProjectphase().getPhasenname(),is(w.getProjectphase().getPhasenname()) );
		assertThat("workpackagename = workpackagename", fromDB.getWorkpackagename(),is(w.getWorkpackagename()) );
		assertThat("plannedValue = plannedValue", fromDB.getPlannedValue(), is(w.getPlannedValue()) );
		assertThat("earnedValue = earnedValue", fromDB.getEarnedValue(),is(w.getEarnedValue()) );
		assertThat("startdate = startdate", fromDB.getStartdate(), is(w.getStartdate()) );
		assertThat("enddate = enddate", fromDB.getEnddate(),is(w.getEnddate()) );
		assertThat("done = done", fromDB.isDone(),is(w.isDone()) );
	}
	
	@Test
	void testUpdateWorkpackageUnknownProjekt()
	{
		User u = userRepo.getById(1);
		Project dummy = new Project(0, 55, "TestProject", u, LocalDate.parse("2018.03.01", formatter), 
				LocalDate.parse("2019.12.31", formatter), "TestPhase", "TestStatus", 651.0);
		
		populateRepo(repo);
		Workpackage w = repo.getById(1);
		w.setProject(dummy);
		repo.save(w);
		
		Workpackage fromDB = repo.getById(w.getId());
		assertThat("id = id", fromDB.getId(),is(w.getId()) );
		assertThat("project = projectname", fromDB.getProject().getProjektname(),is("TestProject") );
		assertThat("projectphase = phasenname", fromDB.getProjectphase().getPhasenname(),is(w.getProjectphase().getPhasenname()) );
		assertThat("workpackagename = workpackagename", fromDB.getWorkpackagename(),is(w.getWorkpackagename()) );
		assertThat("plannedValue = plannedValue", fromDB.getPlannedValue(), is(w.getPlannedValue()) );
		assertThat("earnedValue = earnedValue", fromDB.getEarnedValue(),is(w.getEarnedValue()) );
		assertThat("startdate = startdate", fromDB.getStartdate(), is(w.getStartdate()) );
		assertThat("enddate = enddate", fromDB.getEnddate(),is(w.getEnddate()) );
		assertThat("done = done", fromDB.isDone(),is(w.isDone()) );
	}
	
	@Test
	void testUpdateWorkpackageNullProjectphase()
	{
		populateRepo(repo);
		Workpackage w = repo.getById(1);
		w.setProjectphase(null);
		repo.save(w);
		
		Workpackage fromDB = repo.getById(w.getId());
		assertThat("id = id", fromDB.getId(),is(w.getId()) );
		assertThat("project = projectname", fromDB.getProject().getProjektname(), is(w.getProject().getProjektname()) );
		assertThat("projectphase = phasenname", fromDB.getProjectphase(),nullValue() );
		assertThat("workpackagename = workpackagename", fromDB.getWorkpackagename(),is(w.getWorkpackagename()) );
		assertThat("plannedValue = plannedValue", fromDB.getPlannedValue(), is(w.getPlannedValue()) );
		assertThat("earnedValue = earnedValue", fromDB.getEarnedValue(),is(w.getEarnedValue()) );
		assertThat("startdate = startdate", fromDB.getStartdate(), is(w.getStartdate()) );
		assertThat("enddate = enddate", fromDB.getEnddate(),is(w.getEnddate()) );
		assertThat("done = done", fromDB.isDone(),is(w.isDone()) );
	}
	
	@Test
	void testUpdateWorkpackageUnknownProjektphase()
	{
		Projectphase dummy = new Projectphase(0, "TestPhase");
		
		populateRepo(repo);
		Workpackage w = repo.getById(1);
		w.setProjectphase(dummy);
		repo.save(w);
		
		Workpackage fromDB = repo.getById(w.getId());
		assertThat("id = id", fromDB.getId(),is(w.getId()) );
		assertThat("project = projectname", fromDB.getProject().getProjektname(),is(w.getProject().getProjektname()) );
		assertThat("projectphase = phasenname", fromDB.getProjectphase().getPhasenname(),is("TestPhase") );
		assertThat("workpackagename = workpackagename", fromDB.getWorkpackagename(),is(w.getWorkpackagename()) );
		assertThat("plannedValue = plannedValue", fromDB.getPlannedValue(), is(w.getPlannedValue()) );
		assertThat("earnedValue = earnedValue", fromDB.getEarnedValue(),is(w.getEarnedValue()) );
		assertThat("startdate = startdate", fromDB.getStartdate(), is(w.getStartdate()) );
		assertThat("enddate = enddate", fromDB.getEnddate(),is(w.getEnddate()) );
		assertThat("done = done", fromDB.isDone(),is(w.isDone()) );
	}
	
	@Test
	void testGetNoWorkpackageByName()
	{
		populateRepo(repo);
		
		List<Workpackage> fromDB = new ArrayList<>(repo.getByName("NotExistingItem"));
		assertThat("Exactly one item was returned", fromDB.size(),is(0));
		
	}
	
	@Test
	void testGetWorkpackageByPhasenname()
	{
		populateRepo(repo);
		
		List<Workpackage> wps = new ArrayList<>(repo.getByPhase("Realisierung"));
		assertThat("One Element in List", wps.size(),is(10));
		assertThat("Test Element One", wps.get(0).toString(), is("Workpackage:{id: 1; project: Project:{id: 11; projektnummer: 0; " +
				"projektname: FakeProject0; projektleiter: User:{id: 11; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; " +
				"role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; " +
				"start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; " +
				"projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP0; plannedValue: 0.000000; " +
				"earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}"));
	}
	
	@Test
	void testGetWorkpackageByPhaseNotFound()
	{
		List<Workpackage> wps = null;
		try
		{
			populateRepo(repo);
		
			wps = new ArrayList<>(repo.getByPhase("ABC"));
			assertThat("Nothing in List", wps.toString(), is("[]"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unecpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testGetWorkpackageByProjectname()
	{
		populateRepo(repo);
		
		List<Workpackage> wps = new ArrayList<>(repo.getByProject("FakeProject2"));
		assertThat("One Element in List", wps.size(),is(1));
		assertThat("Test Element One", wps.get(0).toString(), is("Workpackage:{id: 3; project: Project:{id: 13; projektnummer: 2; " +
				"projektname: FakeProject2; projektleiter: User:{id: 13; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; " +
				"role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}; " +
				"start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; projekstatus: FakeStatus2; projektbudget: 2.000000}; " +
				"projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP2; plannedValue: 2.000000; " +
				"earnedValue: 2.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}"));
	}
	
	@Test
	void testGetWorkpackageByProjectId()
	{
		populateRepo(repo);
		
		List<Workpackage> wps = new ArrayList<>(repo.getByProject("13"));
		assertThat("One Element in List", wps.size(),is(1));
		assertThat("Test Element One", wps.get(0).toString(), is("Workpackage:{id: 3; project: Project:{id: 13; projektnummer: 2; " +
				"projektname: FakeProject2; projektleiter: User:{id: 13; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; " +
				"role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}; " +
				"start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; projekstatus: FakeStatus2; projektbudget: 2.000000}; " +
				"projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP2; plannedValue: 2.000000; " +
				"earnedValue: 2.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}"));
	}
	
	@Test
	void testGetWorkpackageByProjectnameNotFound()
	{
		List<Workpackage> wps = null;
		try
		{
			populateRepo(repo);
		
			wps = new ArrayList<>(repo.getByProject("ABC"));
			assertThat("Nothing in List", wps.toString(), is("[]"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unecpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testCheckWorkpackagenameExists()
	{
		populateRepo(repo);
		
		assertThat("Project exists", repo.checkWorkpackage("FakeWP0"), is(true));
	}
	
	@Test
	void testCheckWorkpackagenameNotExists()
	{
		populateRepo(repo);
		
		assertThat("Project not exists", repo.checkWorkpackage("NotExist"), is(false));
	}
}
