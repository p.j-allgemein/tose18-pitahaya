package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class ProjectriskTest {
	
	@Test
	void testEmptyConstructorYieldsEmptyObject() {
		Projectrisk p = new Projectrisk();
		assertThat("Id",p.getId(),is(equalTo(0l)));
		assertThat("Ursache",p.getUrsache(),is(equalTo(null)));
		assertThat("Gefährdungsart",p.getGefaehrdungsart(),is(equalTo(null)));
		assertThat("Eintrittswahrscheinlichkeit",p.getEintrittswahrscheinlichkeit(),is(equalTo(0.0)));
		assertThat("Auswirkungen",p.getAuswirkungen(),is(equalTo(0.0)));
		assertThat("Massnahmen",p.getMassnahmen(),is(equalTo(null)));
		assertThat("Verantwortlicher",p.getVerantwortlicher(),is(equalTo(null)));
		assertThat("Priorität",p.getPrioritaet(),is(equalTo(0l)));
	}
	
	@Test
	void testRisikonameNull()
	{
		Projectrisk p = new Projectrisk(0, null, null, null, 0.0, 0.0, null, null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Risikoname fehlt")));
	}
	
	@Test
	void testRisikonameEmpty()
	{
		Projectrisk p = new Projectrisk(0, "", null, null, 0.0, 0.0, null, null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Risikoname fehlt")));
	}
	
	@Test
	void testUrsacheNull()
	{
		Projectrisk p = new Projectrisk(0, "A", null, null, 0.0, 0.0, null, null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Ursache fehlt")));
	}
	
	@Test
	void testUrsacheEmpty()
	{
		Projectrisk p = new Projectrisk(0, "A", "", null, 0.0, 0.0, null, null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Ursache fehlt")));
	}
	
	@Test
	void testGefaehrdungsartNull()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", null, 0.0, 0.0, null, null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Gefährdungsart fehlt")));
	}
	
	@Test
	void testGefaherdungsartEmpty()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", "", 0.0, 0.0, null, null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Gefährdungsart fehlt")));
	}
	
	@Test
	void testEintrittswahrscheinlichkeitZero()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", "A", 0.0, 0.0, null, null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Eintrittswahrscheinlichkeit fehlt")));
	}
	
	@Test
	void testEintrittswahrscheinlichkeitNegativ()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", "A", -53.84, 0.0, null, null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Eintrittswahrscheinlichkeit fehlt")));
	}
	
	@Test
	void testAuswirkungenZero()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", "A", 564.54, 0.0, null, null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Auswirkungen fehlt")));
	}
	
	@Test
	void testAuswirkungenNegativ()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", "A", 53.84, -46354.45, null, null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Auswirkungen fehlt")));
	}
	
	@Test
	void testMassnahmenNull()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", "A", 53.84, 46354.45, null, null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Massnahmen fehlt")));
	}
	
	@Test
	void testMassnahmenEmpty()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", "A", 53.84, 46354.45, "", null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Massnahmen fehlt")));
	}
	
	@Test
	void testVerantwortlicherNull()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", "A", 53.84, 46354.45, "A", null, 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Verantwortlicher fehlt")));
	}
	
	@Test
	void testPrioritaetZero()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", "A", 53.84, 46354.45, "A", new User(), 0, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Priorität fehlt")));
	}
	
	@Test
	void testPrioritaetNegativ()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", "A", 53.84, 46354.45, "A", new User(), -54, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Priorität fehlt")));
	}
	
	@Test
	void testProjectNull()
	{
		Projectrisk p = new Projectrisk(0, "A", "A", "A", 53.84, 46354.45, "A", new User(), 54, null);
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
			p.validate();
		});
		assertThat("IllegalArgumentException.getMessage", exception.getMessage(), is(equalTo("Project fehlt")));
	}
}
