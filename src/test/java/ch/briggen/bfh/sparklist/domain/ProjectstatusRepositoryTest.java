package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetSequence;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ProjectstatusRepositoryTest {
	ProjectRepository projectRepo = null;
	ProjectstatusRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	private static void populateRepo(ProjectstatusRepository p, ProjectRepository pr) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(i + 1,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								      "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummyProject = new Project(i + 1, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			dummyProject.setId(pr.insert(dummyProject));
			Projectstatus dummy = new Projectstatus(0, dummyProject, "Grün", "Gelb", "Neutral", "Rot", "Grün");
			
			p.insert(dummy);
		}
	}
	
	private static void populateProjectRepo(ProjectRepository p) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(i + 1,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
				      				  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummy = new Project(i * 100, i * 100, "FakeProject" + i * 100, dummyUser,
									    LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
									    "FakePhase" + i * 100, "FakeStatus" + i * 100, i * 100);
			p.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		projectRepo = new ProjectRepository();
		repo = new ProjectstatusRepository();
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projectstatus");
		resetDB("Projects");
		resetDB("Users");
		resetSequence("Projectnumber");
	}

	@Test
	void testDBData() {
		assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo, projectRepo);
		List<Projectstatus> list = new ArrayList<>(repo.getAll());
		assertThat("Freshly populated DB must hold 10 Projectstatuss",list.size(),is(10));
		assertThat("Test Element 1", list.get(0).toString(), is("Projectstatus:{id: 1; project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; projectstatus: Grün; ressourcestatus: Gelb; riskstatus: Neutral; timestatus: Rot; budgetstatus: Grün}"));
	}
	
	@Test
	void testGetProjectstatusByName()
	{
		populateRepo(repo, projectRepo);
		
		List<Projectstatus> project = new ArrayList<>(repo.getByName("FakeProject0"));
		assertThat("One Element in List", project.size(),is(1));
		assertThat("Test Element One", project.get(0).toString(), is("Projectstatus:{id: 1; project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; projectstatus: Grün; ressourcestatus: Gelb; riskstatus: Neutral; timestatus: Rot; budgetstatus: Grün}"));
	}
	
	@Test
	void testGetProjectstatusByNameNotFound()
	{
		populateRepo(repo, projectRepo);
		
		List<Projectstatus> project = new ArrayList<>(repo.getByName("ABC"));
		assertThat("Nothing in List", project.isEmpty(),is(true));
	}
	
	@Test
	void testGetProjectstatusId3()
	{
		populateRepo(repo, projectRepo);
		
		Projectstatus Projectstatus = repo.getById(3);
		assertThat("Test User Three", Projectstatus.toString(), is("Projectstatus:{id: 3; project: Project:{id: 3; projektnummer: 2; projektname: FakeProject2; projektleiter: User:{id: 3; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; projekstatus: FakeStatus2; projektbudget: 2.000000}; projectstatus: Grün; ressourcestatus: Gelb; riskstatus: Neutral; timestatus: Rot; budgetstatus: Grün}"));
	}
	
	@Test
	void testGetProjectstatusIdNotFound()
	{
		populateRepo(repo, projectRepo);
		
		assertThat("Project does not exist", repo.getById(99),is(nullValue()));
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject1,Grün,Grün,Grün,Grün,Grün",
				"2,FakeProject2,Neutral,Neutral,Neutral,Neutral,Neutral",
				"3,FakeProject3,Rot,Rot,Rot,Rot,Rot"})
	void testInsertProjectstatus(long id, String project, String projectstatus, String ressourcestatus,
								 String riskstatus, String timestatus, String budgetstatus)
	{
		populateRepo(repo, projectRepo);
		List<Project> projects = new ArrayList<>(projectRepo.getByName(project));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(project));
		Projectstatus p = new Projectstatus(id, projects.get(0), projectstatus, ressourcestatus, riskstatus, timestatus, budgetstatus);
		
		long dbId = repo.insert(p);
		Projectstatus fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("projectstatus = projectstatus", fromDB.getProjectstatus(),is(projectstatus) );
		assertThat("ressourcestatus = ressourcestatus", fromDB.getRessourcestatus(),is(ressourcestatus) );
		assertThat("riskstatus = riskstatus", fromDB.getRiskstatus(),is(riskstatus) );
		assertThat("timestatus = timestatus", fromDB.getTimestatus(), is(timestatus) );
		assertThat("budgetstatus = budgetstatus", fromDB.getBudgetstatus(), is(budgetstatus) );
		assertThat("project = project", fromDB.getProject().getProjektname(),is(project) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject1,Violett,Grün,Grün,Grün,Grün",
				"2,FakeProject2,Neutral,Orange,Neutral,Neutral,Neutral",
				"3,FakeProject3,Rot,Rot,Blau,Rot,Rot",
				"4,FakeProject4,Rot,Rot,Rot,Weiss,Rot",
				"5,FakeProject5,Gelb,Gelb,Gelb,Gelb,Pink"})
	void testInsertProjectstatussInvalidValues(long id, String project, String projectstatus, String ressourcestatus,
				 							   String riskstatus, String timestatus, String budgetstatus)
	{
		populateRepo(repo, projectRepo);
		List<Project> projects = new ArrayList<>(projectRepo.getByName(project));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(project));
		Projectstatus p = new Projectstatus(id, projects.get(0), projectstatus, ressourcestatus, riskstatus, timestatus, budgetstatus);
		
		long dbId = 0;
		
		try
		{
			dbId = repo.insert(p);
			fail("Expected Exception ");
		}
		catch (RepositoryException e)
		{
			assertThat("id = id", dbId,is(0L) );
		}
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject1,Grün,Grün,Grün,Grün,Grün,FakeProject4,Neutral,Neutral,Neutral,Neutral,Neutral",
				"2,FakeProject2,Neutral,Neutral,Neutral,Neutral,Neutral,FakeProject5,Rot,Rot,Rot,Rot,Rot",
				"3,FakeProject3,Rot,Rot,Rot,Rot,Rot,FakeProject6,Gelb,Gelb,Gelb,Gelb,Gelb"})
	void testUpdateProjectstatuss(long id, String project, String projectstatus, String ressourcestatus,
			   					  String riskstatus, String timestatus, String budgetstatus, String newProject, String newProjectstatus,
			   					  String newRessourcestatus, String newRiskstatus, String newTimestatus, String newBudgetstatus)
	{
		populateRepo(repo, projectRepo);
		List<Project> projects = new ArrayList<>(projectRepo.getByName(project));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(project));
		Projectstatus p = new Projectstatus(id, projects.get(0), projectstatus, ressourcestatus, riskstatus, timestatus, budgetstatus);
		
		long dbId = repo.insert(p);
		
		projects = new ArrayList<>(projectRepo.getByName(newProject));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(newProject));
		
		p.setId(dbId);
		p.setProjectstatus(newProjectstatus);
		p.setRessourcestatus(newRessourcestatus);
		p.setRiskstatus(newRiskstatus);
		p.setTimestatus(newTimestatus);
		p.setBudgetstatus(newBudgetstatus);
		p.setProject(projects.get(0));
		repo.save(p);
		
		Projectstatus fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("projectstatus = projectstatus", fromDB.getProjectstatus(),is(newProjectstatus) );
		assertThat("ressourcestatus = ressourcestatus", fromDB.getRessourcestatus(),is(newRessourcestatus) );
		assertThat("riskstatus = riskstatus", fromDB.getRiskstatus(),is(newRiskstatus) );
		assertThat("timestatus = timestatus", fromDB.getTimestatus(), is(newTimestatus) );
		assertThat("budgetstatus = budgetstatus", fromDB.getBudgetstatus(), is(newBudgetstatus) );
		assertThat("project = project", fromDB.getProject().getProjektname(),is(newProject) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject1,Grün,Grün,Grün,Grün,Grün",
				"2,FakeProject2,Neutral,Neutral,Neutral,Neutral,Neutral",
				"3,FakeProject3,Rot,Rot,Rot,Rot,Rot"})
	void testDeleteProjectstatuss(long id, String project, String projectstatus, String ressourcestatus,
			 					  String riskstatus, String timestatus, String budgetstatus)
	{
		populateRepo(repo, projectRepo);
		
		List<Project> projects = new ArrayList<>(projectRepo.getByName(project));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(project));
		Projectstatus p = new Projectstatus(id, projects.get(0), projectstatus, ressourcestatus, riskstatus, timestatus, budgetstatus);
		
		long dbId = repo.insert(p);
		Projectstatus fromDB = repo.getById(dbId);
		assertThat("Project was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThat("Projekt is should be deleted", repo.getById(dbId),is(nullValue()));
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo, projectRepo);
		for(Projectstatus i : repo.getAll())
		{
			repo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeProject100,Grün,Grün,Grün,Grün,Grün",
				"2,FakeProject200,Neutral,Neutral,Neutral,Neutral,Neutral",
				"3,FakeProject300,Rot,Rot,Rot,Rot,Rot"})
	void testGetByOneName(long id, String project, String projectstatus, String ressourcestatus,
			  			  String riskstatus, String timestatus, String budgetstatus)
	{
		populateProjectRepo(projectRepo);
		
		List<Project> projects = new ArrayList<>(projectRepo.getByName(project));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(project));
		Projectstatus p = new Projectstatus(id, projects.get(0), projectstatus, ressourcestatus, riskstatus, timestatus, budgetstatus);
		
		long dbId = repo.insert(p);
		List<Projectstatus> fromDB = new ArrayList<>(repo.getByName(project));
		assertThat("Exactly one item was returned", fromDB.size(),is(1));
		
		Projectstatus elementFromDB = fromDB.iterator().next();
		assertThat("id = id", elementFromDB.getId(),is(dbId) );
		assertThat("projectstatus = projectstatus", elementFromDB.getProjectstatus(),is(projectstatus) );
		assertThat("ressourcestatus = ressourcestatus", elementFromDB.getRessourcestatus(),is(ressourcestatus) );
		assertThat("riskstatus = riskstatus", elementFromDB.getRiskstatus(),is(riskstatus) );
		assertThat("timestatus = timestatus", elementFromDB.getTimestatus(), is(timestatus) );
		assertThat("budgetstatus = budgetstatus", elementFromDB.getBudgetstatus(), is(budgetstatus) );
		assertThat("project = project", elementFromDB.getProject().getProjektname(),is(project) );
	}
	
	@Test
	void testGetNoProjectstatussByName()
	{
		populateRepo(repo, projectRepo);
		
		List<Projectstatus> fromDB = new ArrayList<>(repo.getByName("NotExistingItem"));
		assertThat("Exactly one item was returned", fromDB.size(),is(0));
		
	}
	
	@Test
	void testGetProjectByProjectname()
	{
		populateRepo(repo, projectRepo);
		
		List<Projectstatus> project = new ArrayList<>(repo.getByProject("FakeProject2"));
		assertThat("One Element in List", project.size(),is(1));
		assertThat("Test Element One", project.get(0).toString(), is("Projectstatus:{id: 3; project: Project:{id: 3; projektnummer: 2; projektname: FakeProject2; projektleiter: User:{id: 3; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; projekstatus: FakeStatus2; projektbudget: 2.000000}; projectstatus: Grün; ressourcestatus: Gelb; riskstatus: Neutral; timestatus: Rot; budgetstatus: Grün}"));
	}
	
	@Test
	void testGetProjectByProjectId()
	{
		populateRepo(repo, projectRepo);
		
		List<Projectstatus> project = new ArrayList<>(repo.getByProject("2"));
		assertThat("One Element in List", project.size(),is(1));
		assertThat("Test Element One", project.get(0).toString(), is("Projectstatus:{id: 2; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 2; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectstatus: Grün; ressourcestatus: Gelb; riskstatus: Neutral; timestatus: Rot; budgetstatus: Grün}"));
	}
	
	@Test
	void testGetProjectByProjectnameNotFound()
	{
		List<Projectstatus> project = null;
		try
		{
			populateRepo(repo, projectRepo);
		
			project = new ArrayList<>(repo.getByProject("ABC"));
			assertThat("Nothing in List", project.toString(), is("[]"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unecpected Exception " + e.getMessage());
		}
	}
	
}
