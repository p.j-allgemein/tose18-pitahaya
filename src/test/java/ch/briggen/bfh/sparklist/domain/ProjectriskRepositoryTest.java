package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetSequence;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ProjectriskRepositoryTest {
	UserRepository userRepo = null;
	ProjectRepository projectRepo = null;
	ProjectriskRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	private static void populateRepo(ProjectriskRepository p) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								      "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummyProject = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			Projectrisk dummy = new Projectrisk(0, "FakeName" + i, "FakeUrsache" + i, "FakeGefaehrdung" + i, i, i, "FakeMassnahmen" + i,
												dummyUser, i, dummyProject);
			
			p.insert(dummy);
		}
	}
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
								  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			u.insert(dummy);
		}
	}
	
	private static void populateProjectRepo(ProjectRepository p) {
		UserRepository urep = new UserRepository();
		for(int i = 0; i < 10; ++i) {
			List<User> dummyUsers = new ArrayList<>(urep.getByName("FakeUser" + i * 100));
			Project dummy = new Project(i * 100, i * 100, "FakeProject" + i * 100, dummyUsers.get(0),
									    LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
									    "FakePhase" + i * 100, "FakeStatus" + i * 100, i * 100);
			p.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		userRepo = new UserRepository();
		projectRepo = new ProjectRepository();
		repo = new ProjectriskRepository();
		populateUserRepo(userRepo);
		populateProjectRepo(projectRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projectrisks");
		resetDB("Projects");
		resetDB("Users");
		resetSequence("Projectnumber");
	}

	@Test
	void testDBData() {
		assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo);
		List<Projectrisk> list = new ArrayList<>(repo.getAll());
		assertThat("Freshly populated DB must hold 10 Projectrisks",list.size(),is(10));
		assertThat("Test Element 1", list.get(0).toString(), is("Projectrisk:{id: 1; risikoname: FakeName0; ursache: FakeUrsache0; " +
				   "gefaehrdungsart: FakeGefaehrdung0; eintrittswahrscheinlichkeit: 0.000000; auswirkungen: 0.000000; " +
				   "massnahmen: FakeMassnahmen0; verantwortlicher: User:{id: 11; username: FakeUser0; name: FakeName0; " +
				   "vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: " +
				   "FakeMail0; telefon: FakeTel0}; prioritaet: 0; project: Project:{id: 11; projektnummer: 0; " +
				   "projektname: FakeProject0; projektleiter: User:{id: 12; username: FakeUser0; name: FakeName0; " +
				   "vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; " +
				   "mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; " +
				   "projekstatus: FakeStatus0; projektbudget: 0.000000}}"));
	}
	
	@Test
	void testGetProjectriskByName()
	{
		populateRepo(repo);
		
		List<Projectrisk> project = new ArrayList<>(repo.getByName("FakeUrsache0"));
		assertThat("One Element in List", project.size(),is(1));
		assertThat("Test Element One", project.get(0).toString(), is("Projectrisk:{id: 1; risikoname: FakeName0; ursache: FakeUrsache0; " +
				   "gefaehrdungsart: FakeGefaehrdung0; eintrittswahrscheinlichkeit: 0.000000; auswirkungen: 0.000000; " +
				   "massnahmen: FakeMassnahmen0; verantwortlicher: User:{id: 11; username: FakeUser0; name: FakeName0; " +
				   "vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: " +
				   "FakeMail0; telefon: FakeTel0}; prioritaet: 0; project: Project:{id: 11; projektnummer: 0; " +
				   "projektname: FakeProject0; projektleiter: User:{id: 12; username: FakeUser0; name: FakeName0; " +
				   "vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; " +
				   "mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; " +
				   "projekstatus: FakeStatus0; projektbudget: 0.000000}}"));
	}
	
	@Test
	void testGetProjectriskByNameNotFound()
	{
		populateRepo(repo);
		
		List<Projectrisk> project = new ArrayList<>(repo.getByName("ABC"));
		assertThat("Nothing in List", project.isEmpty(),is(true));
	}
	
	@Test
	void testGetProjectId3()
	{
		populateRepo(repo);
		
		Projectrisk projectrisk = repo.getById(3);
		assertThat("Test User Three", projectrisk.toString(), is("Projectrisk:{id: 3; risikoname: FakeName2; ursache: FakeUrsache2; " +
				   "gefaehrdungsart: FakeGefaehrdung2; eintrittswahrscheinlichkeit: 2.000000; auswirkungen: 2.000000; " +
				   "massnahmen: FakeMassnahmen2; verantwortlicher: User:{id: 15; username: FakeUser2; name: FakeName2; " +
				   "vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; " +
				   "mail: FakeMail2; telefon: FakeTel2}; prioritaet: 2; project: Project:{id: 13; projektnummer: 2; " +
				   "projektname: FakeProject2; projektleiter: User:{id: 16; username: FakeUser2; name: FakeName2; " +
				   "vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; " +
				   "mail: FakeMail2; telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; " +
				   "projekstatus: FakeStatus2; projektbudget: 2.000000}}"));
	}
	
	@Test
	void testGetProjectriskIdNotFound()
	{
		populateRepo(repo);
		
		assertThat("Project does not exist", repo.getById(99),is(nullValue()));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,One,One,11.11,11.11,One,FakeUser100,1,FakeProject100",
				"2,Two,Two,Two,22.22,22.22,Two,FakeUser200,2,FakeProject200",
				"3,Three,Three,Three,33.33,33.33,Three,FakeUser300,3,FakeProject200"})
	void testInsertProjectisks(long id, String risikoname, String ursache, String gefaehrdungsart, double eintrittswahrscheinlichkeit,
			   				   double auswirkungen, String massnahmen, String verantwortlicher, long prioritaet, String project)
	{
		populateRepo(repo);
		List<User> users = new ArrayList<>(userRepo.getByName(verantwortlicher));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.verantwortlicher", users.get(0).getUsername(), is(verantwortlicher));
		List<Project> projects = new ArrayList<>(projectRepo.getByName(project));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(project));
		Projectrisk p = new Projectrisk(id, risikoname, ursache, gefaehrdungsart, eintrittswahrscheinlichkeit, auswirkungen,
										massnahmen, users.get(0), prioritaet, projects.get(0));
		
		long dbId = repo.insert(p);
		Projectrisk fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("risikoname = risikoname", fromDB.getRisikoname(),is(risikoname) );
		assertThat("ursache = ursache", fromDB.getUrsache(),is(ursache) );
		assertThat("gefaehrdungsart = gefaehrdungsart", fromDB.getGefaehrdungsart(),is(gefaehrdungsart) );
		assertThat("eintrittswahrscheinlichkeit = eintrittswahrscheinlichkeit", fromDB.getEintrittswahrscheinlichkeit(), is(eintrittswahrscheinlichkeit) );
		assertThat("auswirkungen = auswirkungen", fromDB.getAuswirkungen(),is(auswirkungen) );
		assertThat("massnahmen = massnahmen", fromDB.getMassnahmen(),is(massnahmen) );
		assertThat("user = verantwortlicher", fromDB.getVerantwortlicher().getUsername(),is(verantwortlicher) );
		assertThat("prioritaet = prioritaet", fromDB.getPrioritaet(),is(prioritaet) );
		assertThat("project = project", fromDB.getProject().getProjektname(),is(project) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,One,One,11.11,11.11,One,1,FakeProject100",
				"2,Two,Two,Two,22.22,22.22,Two,2,FakeProject200",
				"3,Three,Three,Three,33.33,33.33,Three,3,FakeProject200"})
	void testInsertProjectrisksNullUser(long id, String risikoname, String ursache, String gefaehrdungsart, double eintrittswahrscheinlichkeit,
			   					  double auswirkungen, String massnahmen, long prioritaet, String project)
	{
		populateRepo(repo);
		
		List<Project> projects = new ArrayList<>(projectRepo.getByName(project));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(project));
		
		Projectrisk p = new Projectrisk(id, risikoname, ursache, gefaehrdungsart, eintrittswahrscheinlichkeit, auswirkungen,
				massnahmen, null, prioritaet, projects.get(0));
		
		long dbId = repo.insert(p);
		Projectrisk fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("risikoname = risikoname", fromDB.getRisikoname(),is(risikoname) );
		assertThat("ursache = ursache", fromDB.getUrsache(),is(ursache) );
		assertThat("gefaehrdungsart = gefaehrdungsart", fromDB.getGefaehrdungsart(),is(gefaehrdungsart) );
		assertThat("eintrittswahrscheinlichkeit = eintrittswahrscheinlichkeit", fromDB.getEintrittswahrscheinlichkeit(), is(eintrittswahrscheinlichkeit) );
		assertThat("auswirkungen = auswirkungen", fromDB.getAuswirkungen(),is(auswirkungen) );
		assertThat("massnahmen = massnahmen", fromDB.getMassnahmen(),is(massnahmen) );
		assertThat("user = verantwortlicher", fromDB.getVerantwortlicher(), nullValue() );
		assertThat("prioritaet = prioritaet", fromDB.getPrioritaet(),is(prioritaet) );
		assertThat("project = project", fromDB.getProject().getProjektname(),is(project) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,One,One,11.11,11.11,One,FakeUser100,1",
				"2,Two,Two,Two,22.22,22.22,Two,FakeUser200,2",
				"3,Three,Three,Three,33.33,33.33,Three,FakeUser300,3"})
	void testInsertProjectisksNullProject(long id, String risikoname, String ursache, String gefaehrdungsart, double eintrittswahrscheinlichkeit,
			   				   			  double auswirkungen, String massnahmen, String verantwortlicher, long prioritaet)
	{
		populateRepo(repo);
		List<User> users = new ArrayList<>(userRepo.getByName(verantwortlicher));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.verantwortlicher", users.get(0).getUsername(), is(verantwortlicher));
		Projectrisk p = new Projectrisk(id, risikoname, ursache, gefaehrdungsart, eintrittswahrscheinlichkeit, auswirkungen,
										massnahmen, users.get(0), prioritaet, null);
		
		long dbId = repo.insert(p);
		Projectrisk fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("risikoname = risikoname", fromDB.getRisikoname(),is(risikoname) );
		assertThat("ursache = ursache", fromDB.getUrsache(),is(ursache) );
		assertThat("gefaehrdungsart = gefaehrdungsart", fromDB.getGefaehrdungsart(),is(gefaehrdungsart) );
		assertThat("eintrittswahrscheinlichkeit = eintrittswahrscheinlichkeit", fromDB.getEintrittswahrscheinlichkeit(), is(eintrittswahrscheinlichkeit) );
		assertThat("auswirkungen = auswirkungen", fromDB.getAuswirkungen(),is(auswirkungen) );
		assertThat("massnahmen = massnahmen", fromDB.getMassnahmen(),is(massnahmen) );
		assertThat("user = verantwortlicher", fromDB.getVerantwortlicher().getUsername(),is(verantwortlicher) );
		assertThat("prioritaet = prioritaet", fromDB.getPrioritaet(),is(prioritaet) );
		assertThat("project = project", fromDB.getProject(), nullValue() );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,One,One,11.11,11.11,One,FakeUser100,1,FakeProject100,Eins,Eins,Eins,111.11,111.11,Eins,FakeUser400,4,FakeProject400",
				"2,Two,Two,Two,22.22,22.22,Two,FakeUser200,2,FakeProject200,Zwei,Zwei,Zwei,222.22,222.22,Zwei,FakeUser500,5,FakeProject500",
				"3,Three,Three,Three,33.33,33.33,Three,FakeUser300,3,FakeProject300,Drei,Drei,Drei,333.33,333.33,Drei,FakeUser600,6,FakeProject600"})
	void testUpdateProjectrisks(long id, String risikoname, String ursache, String gefaehrdungsart, double eintrittswahrscheinlichkeit,
			   double auswirkungen, String massnahmen, String verantwortlicher, long prioritaet, String project, 
			   String newRisikoname, String newUrsache, String newGefaehrdungsart, double newEintrittswahrscheinlichkeit,
			   double newAuswirkungen, String newMassnahmen, String newVerantwortlicher, long newPrioritaet, String newProject)
	{
		populateRepo(repo);
		List<User> users = new ArrayList<>(userRepo.getByName(verantwortlicher));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.verantwortlicher", users.get(0).getUsername(), is(verantwortlicher));
		List<Project> projects = new ArrayList<>(projectRepo.getByName(project));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(project));
		Projectrisk p = new Projectrisk(id, risikoname, ursache, gefaehrdungsart, eintrittswahrscheinlichkeit, auswirkungen,
										massnahmen, users.get(0), prioritaet, projects.get(0));
		
		long dbId = repo.insert(p);
		
		users = new ArrayList<>(userRepo.getByName(newVerantwortlicher));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.verantwortlicher", users.get(0).getUsername(), is(newVerantwortlicher));
		projects = new ArrayList<>(projectRepo.getByName(newProject));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(newProject));
		
		p.setId(dbId);
		p.setRisikoname(newRisikoname);
		p.setUrsache(newUrsache);
		p.setGefaehrdungsart(newGefaehrdungsart);
		p.setEintrittswahrscheinlichkeit(newEintrittswahrscheinlichkeit);
		p.setAuswirkungen(newAuswirkungen);
		p.setMassnahmen(newMassnahmen);
		p.setVerantwortlicher(users.get(0));
		p.setPrioritaet(newPrioritaet);
		p.setProject(projects.get(0));
		repo.save(p);
		
		Projectrisk fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("risikoname = risikoname", fromDB.getRisikoname(),is(newRisikoname) );
		assertThat("ursache = ursache", fromDB.getUrsache(),is(newUrsache) );
		assertThat("gefaehrdungsart = gefaehrdungsart", fromDB.getGefaehrdungsart(),is(newGefaehrdungsart) );
		assertThat("eintrittswahrscheinlichkeit = eintrittswahrscheinlichkeit", fromDB.getEintrittswahrscheinlichkeit(), is(newEintrittswahrscheinlichkeit) );
		assertThat("auswirkungen = auswirkungen", fromDB.getAuswirkungen(),is(newAuswirkungen) );
		assertThat("massnahmen = massnahmen", fromDB.getMassnahmen(),is(newMassnahmen) );
		assertThat("user = verantwortlicher", fromDB.getVerantwortlicher().getUsername(),is(newVerantwortlicher) );
		assertThat("prioritaet = prioritaet", fromDB.getPrioritaet(),is(newPrioritaet) );
		assertThat("project = project", fromDB.getProject().getProjektname(),is(newProject) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,One,One,11.11,11.11,One,FakeUser100,1,FakeProject100",
				"2,Two,Two,Two,22.22,22.22,Two,FakeUser200,2,FakeProject200",
				"3,Three,Three,Three,33.33,33.33,Three,FakeUser300,3,FakeProject200"})
	void testDeleteProjectrisks(long id, String risikoname, String ursache, String gefaehrdungsart, double eintrittswahrscheinlichkeit,
			   double auswirkungen, String massnahmen, String verantwortlicher, long prioritaet, String project)
	{
		populateRepo(repo);
		
		List<User> users = new ArrayList<>(userRepo.getByName(verantwortlicher));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.verantwortlicher", users.get(0).getUsername(), is(verantwortlicher));
		List<Project> projects = new ArrayList<>(projectRepo.getByName(project));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(project));
		Projectrisk p = new Projectrisk(id, risikoname, ursache, gefaehrdungsart, eintrittswahrscheinlichkeit, auswirkungen,
										massnahmen, users.get(0), prioritaet, projects.get(0));
		
		long dbId = repo.insert(p);
		Projectrisk fromDB = repo.getById(dbId);
		assertThat("Project was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThat("Projekt is should be deleted", repo.getById(dbId),is(nullValue()));
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo);
		for(Projectrisk i : repo.getAll())
		{
			repo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,One,One,11.11,11.11,One,FakeUser100,1,FakeProject100",
			"2,Two,Two,Two,22.22,22.22,Two,FakeUser200,2,FakeProject200",
			"3,Three,Three,Three,33.33,33.33,Three,FakeUser300,3,FakeProject200"})
	void testGetByOneName(long id, String risikoname, String ursache, String gefaehrdungsart, double eintrittswahrscheinlichkeit,
			   double auswirkungen, String massnahmen, String verantwortlicher, long prioritaet, String project)
	{
		populateRepo(repo);
		
		List<User> users = new ArrayList<>(userRepo.getByName(verantwortlicher));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		assertThat("users.verantwortlicher", users.get(0).getUsername(), is(verantwortlicher));
		List<Project> projects = new ArrayList<>(projectRepo.getByName(project));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		assertThat("project.projektname", projects.get(0).getProjektname(), is(project));
		Projectrisk p = new Projectrisk(id, risikoname, ursache, gefaehrdungsart, eintrittswahrscheinlichkeit, auswirkungen,
										massnahmen, users.get(0), prioritaet, projects.get(0));
		
		long dbId = repo.insert(p);
		List<Projectrisk> fromDB = new ArrayList<>(repo.getByName(ursache));
		assertThat("Exactly one item was returned", fromDB.size(),is(1));
		
		Projectrisk elementFromDB = fromDB.iterator().next();
		assertThat("id = id", elementFromDB.getId(),is(dbId) );
		assertThat("risikoname = risikoname", elementFromDB.getRisikoname(),is(risikoname) );
		assertThat("ursache = ursache", elementFromDB.getUrsache(),is(ursache) );
		assertThat("gefaehrdungsart = gefaehrdungsart", elementFromDB.getGefaehrdungsart(),is(gefaehrdungsart) );
		assertThat("eintrittswahrscheinlichkeit = eintrittswahrscheinlichkeit", elementFromDB.getEintrittswahrscheinlichkeit(), is(eintrittswahrscheinlichkeit) );
		assertThat("auswirkungen = auswirkungen", elementFromDB.getAuswirkungen(),is(auswirkungen) );
		assertThat("massnahmen = massnahmen", elementFromDB.getMassnahmen(),is(massnahmen) );
		assertThat("user = verantwortlicher", elementFromDB.getVerantwortlicher().getUsername(),is(verantwortlicher) );
		assertThat("prioritaet = prioritaet", elementFromDB.getPrioritaet(),is(prioritaet) );
		assertThat("project = project", elementFromDB.getProject().getProjektname(),is(project) );
	}
	
	@Test
	void testGetNoProjectrisksByName()
	{
		populateRepo(repo);
		
		List<Projectrisk> fromDB = new ArrayList<>(repo.getByName("NotExistingItem"));
		assertThat("Exactly one item was returned", fromDB.size(),is(0));
		
	}
	
	@Test
	void testGetProjectByUsername()
	{
		populateRepo(repo);
		
		List<Projectrisk> project = new ArrayList<>(repo.getByUsername("FakeUser2"));
		assertThat("One Element in List", project.size(),is(1));
		assertThat("Test Element One", project.get(0).toString(), is("Projectrisk:{id: 3; risikoname: FakeName2; ursache: FakeUrsache2; " +
				   "gefaehrdungsart: FakeGefaehrdung2; eintrittswahrscheinlichkeit: 2.000000; auswirkungen: 2.000000; " +
				   "massnahmen: FakeMassnahmen2; verantwortlicher: User:{id: 15; username: FakeUser2; name: FakeName2; " +
				   "vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; " +
				   "mail: FakeMail2; telefon: FakeTel2}; prioritaet: 2; project: Project:{id: 13; projektnummer: 2; " +
				   "projektname: FakeProject2; projektleiter: User:{id: 16; username: FakeUser2; name: FakeName2; " +
				   "vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; " +
				   "mail: FakeMail2; telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; " +
				   "projekstatus: FakeStatus2; projektbudget: 2.000000}}"));
	}
	
	@Test
	void testGetProjectriskByUsernameNotFound()
	{
		List<Projectrisk> project = null;
		try
		{
			populateRepo(repo);
		
			project = new ArrayList<>(repo.getByUsername("ABC"));
			assertThat("Nothing in List", project.toString(), is("[]"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unecpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testGetProjectriskByProjectname()
	{
		populateRepo(repo);
		
		List<Projectrisk> project = new ArrayList<>(repo.getByProject("FakeProject2"));
		assertThat("One Element in List", project.size(),is(1));
		assertThat("Test Element One", project.get(0).toString(), is("Projectrisk:{id: 3; risikoname: FakeName2; ursache: FakeUrsache2; " +
				   "gefaehrdungsart: FakeGefaehrdung2; eintrittswahrscheinlichkeit: 2.000000; auswirkungen: 2.000000; " +
				   "massnahmen: FakeMassnahmen2; verantwortlicher: User:{id: 15; username: FakeUser2; name: FakeName2; " +
				   "vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; " +
				   "mail: FakeMail2; telefon: FakeTel2}; prioritaet: 2; project: Project:{id: 13; projektnummer: 2; " +
				   "projektname: FakeProject2; projektleiter: User:{id: 16; username: FakeUser2; name: FakeName2; " +
				   "vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; " +
				   "mail: FakeMail2; telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; " +
				   "projekstatus: FakeStatus2; projektbudget: 2.000000}}"));
	}
	
	@Test
	void testGetProjectriskByProjectId()
	{
		populateRepo(repo);
		
		List<Projectrisk> project = new ArrayList<>(repo.getByProject("13"));
		assertThat("One Element in List", project.size(),is(1));
		assertThat("Test Element One", project.get(0).toString(), is("Projectrisk:{id: 3; risikoname: FakeName2; ursache: FakeUrsache2; " +
				   "gefaehrdungsart: FakeGefaehrdung2; eintrittswahrscheinlichkeit: 2.000000; auswirkungen: 2.000000; " +
				   "massnahmen: FakeMassnahmen2; verantwortlicher: User:{id: 15; username: FakeUser2; name: FakeName2; " +
				   "vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; " +
				   "mail: FakeMail2; telefon: FakeTel2}; prioritaet: 2; project: Project:{id: 13; projektnummer: 2; " +
				   "projektname: FakeProject2; projektleiter: User:{id: 16; username: FakeUser2; name: FakeName2; " +
				   "vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; " +
				   "mail: FakeMail2; telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; " +
				   "projekstatus: FakeStatus2; projektbudget: 2.000000}}"));
	}
	
	@Test
	void testGetProjectriskByProjectnameNotFound()
	{
		List<Projectrisk> project = null;
		try
		{
			populateRepo(repo);
		
			project = new ArrayList<>(repo.getByProject("ABC"));
			assertThat("Nothing in List", project.toString(), is("[]"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unecpected Exception " + e.getMessage());
		}
	}
}
