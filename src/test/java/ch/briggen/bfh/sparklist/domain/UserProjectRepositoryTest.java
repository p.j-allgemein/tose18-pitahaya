package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetSequence;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class UserProjectRepositoryTest {
	UserRepository userRepo = null;
	ProjectRepository projectRepo = null;
	UserProjectRepository repo = null;
	RoleRepository roleRepo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	private static void populateRepo(UserProjectRepository up, UserRepository u, ProjectRepository p, RoleRepository r) {
		RoleRepository rrep = new RoleRepository();
		for(int i = 0; i < 10; ++i) {
			Role dummyRole = rrep.getById((i % 5) + 1);
			User dummyUser = new User(i + 200,"FakeUser" + (i + 200), "FakeName" + (i + 200), "FakeVorname" + (i + 200), dummyRole,
								      "FakeAbteilung" + (i + 200), "FakeMail" + (i + 200), "FakeTel" + (i + 200));
			dummyUser.setId(u.insert(dummyUser));
			Project dummyProject = new Project(i + 1, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			dummyProject.setId(p.insert(dummyProject));
			User2Project dummy = new User2Project(dummyUser, dummyProject, dummyRole);
			
			up.insert(dummy);
		}
	}
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
								  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			u.insert(dummy);
		}
	}
	
	private static void populateProjectRepo(ProjectRepository p) {
		UserRepository urep = new UserRepository();
		for(int i = 0; i < 10; ++i) {
			List<User> dummyUsers = new ArrayList<>(urep.getByName("FakeUser" + i * 100));
			Project dummy = new Project(i * 100, i * 100, "FakeProject" + i * 100, dummyUsers.get(0),
									    LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
									    "FakePhase" + i * 100, "FakeStatus" + i * 100, i * 100);
			p.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		userRepo = new UserRepository();
		projectRepo = new ProjectRepository();
		repo = new UserProjectRepository();
		roleRepo = new RoleRepository();
		populateUserRepo(userRepo);
		populateProjectRepo(projectRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.insert(new User2Project(userRepo.getById(1), projectRepo.getById(1), null));
		repo.getAll().stream().forEach(e -> repo.delete(e.getUser().getId(), e.getProject().getId(), (e.getRole() != null ? e.getRole().getId() : 0)));
		resetDB("Projects");
		resetDB("Users");
		resetSequence("Projectnumber");
	}

	@Test
	void testDBData() {
		assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<User2Project> list = new ArrayList<>(repo.getAll());
		assertThat("Freshly populated DB must hold 10 Projectrisks",list.size(),is(10));
		assertThat("Test Element 1", list.get(0).toString(), is("User2Project:{user: User:{id: 11; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}; project: Project:{id: 11; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 11; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; role: Role:{id: 1; rolename: Admin}}"));
	}
	
	@Test
	void testGetProjectEmployeeById()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		
		User2Project user = repo.getById(11, 11, 1);
		assertThat("Test User2Project", user.toString(), is("User2Project:{user: User:{id: 11; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}; project: Project:{id: 11; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 11; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; role: Role:{id: 1; rolename: Admin}}"));
	}
	
	@Test
	void testGetProjectEmployeeIdNotFound()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		
		assertThat("User2Project does not exist", repo.getById(99, 99, 99),is(nullValue()));
	}
	
	@Test
	void testGetProjectEmployeeObjectIdUserNull()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		
		try
		{
			repo.getById(null, null, null);
		}
		catch (IllegalArgumentException e)
		{
			assertThat("Exception User2Project by Object", e.getMessage() ,is("getById missing user and/or project"));
		}
		
		
	}
	
	@Test
	void testGetProjectEmployeeObjectIdProjectNull()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		
		try
		{
			repo.getById(new User(), null, null);
		}
		catch (IllegalArgumentException e)
		{
			assertThat("Exception User2Project by Object", e.getMessage() ,is("getById missing user and/or project"));
		}
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeUser200,FakeProject5,Admin",
				"2,FakeUser201,FakeProject5,Projektmitarbeiter",
				"3,FakeUser202,FakeProject5,Projektmitarbeiter"})
	void testInsert(long userId,String username, String projectname, String rolename)
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<Role> roles = new ArrayList<>(roleRepo.getByName(rolename));
		assertThat("roles list is not empty", roles.isEmpty(), is(false));
		List<User> users = new ArrayList<>(userRepo.getByName(username));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		List<Project> projects = new ArrayList<>(projectRepo.getByName(projectname));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		User2Project u2p = new User2Project(users.get(0), projects.get(0), roles.get(0));
		
		repo.insert(u2p);
		User2Project fromDB = repo.getById(users.get(0).getId(), projects.get(0).getId(), roles.get(0).getId());
		assertThat("user = username", fromDB.getUser().getUsername(),is(username) );
		assertThat("project = projectname", fromDB.getProject().getProjektname(),is(projectname) );
		assertThat("role = rolename", fromDB.getRole().getRolename(), is(rolename) );
	}
	
	@Test
	void testInsertDoubleEntry()
	{
		try
		{
			populateRepo(repo, userRepo, projectRepo, roleRepo);
			
			List<Role> roles = new ArrayList<>(roleRepo.getByName("Portfoliomanager"));
			assertThat("roles list is not empty", roles.isEmpty(), is(false));
			List<User> users = new ArrayList<>(userRepo.getByName("FakeUser201"));
			assertThat("users list is not empty", users.isEmpty(), is(false));
			List<Project> projects = new ArrayList<>(projectRepo.getByName("FakeProject1"));
			assertThat("projects list is not empty", projects.isEmpty(), is(false));
			User2Project u2p = new User2Project(users.get(0), projects.get(0), roles.get(0));
			
			repo.insert(u2p);
		}
		catch (IllegalArgumentException e)
		{
			assertThat("Expected IllegalArgumentException", e.getMessage(), is("Combination of userId, projectId and roleid already exist"));
		}
	}
	
	@ParameterizedTest
	@CsvSource({"1,FakeUser200,FakeProject5,Admin",
				"2,FakeUser201,FakeProject5,Projektmitarbeiter",
				"3,FakeUser202,FakeProject5,Projektmitarbeiter"})
	void testDeleteUsers(long userId,String username, String projectname, String rolename)
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<Role> roles = new ArrayList<>(roleRepo.getByName(rolename));
		assertThat("roles list is not empty", roles.isEmpty(), is(false));
		List<User> users = new ArrayList<>(userRepo.getByName(username));
		assertThat("users list is not empty", users.isEmpty(), is(false));
		List<Project> projects = new ArrayList<>(projectRepo.getByName(projectname));
		assertThat("projects list is not empty", projects.isEmpty(), is(false));
		User2Project u2p = new User2Project(users.get(0), projects.get(0), roles.get(0));
		
		repo.insert(u2p);
		User2Project fromDB = repo.getById(users.get(0).getId(), projects.get(0).getId(), roles.get(0).getId());
		assertThat("user = username", fromDB.getUser().getUsername(),is(username) );
		assertThat("project = projectname", fromDB.getProject().getProjektname(),is(projectname) );
		assertThat("role = rolename", fromDB.getRole().getRolename(), is(rolename) );
		
		assertThat("User2Project was written to DB",fromDB,not(nullValue()));
		repo.delete(fromDB.getUser(), fromDB.getProject(), fromDB.getRole());
		assertThat("User2Project is should be deleted", repo.getById(fromDB.getUser(), fromDB.getProject(), fromDB.getRole()),is(nullValue()));
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		for(User2Project i : repo.getAll())
		{
			repo.delete(i.getUser(), i.getProject(), i.getRole());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testDeleteProjectEmployeeObjectIdUserNull()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		
		try
		{
			repo.delete(null, null, null);
		}
		catch (IllegalArgumentException e)
		{
			assertThat("Exception User2Project by Object", e.getMessage() ,is("delete missing user and/or project"));
		}
		
		
	}
	
	@Test
	void testDeleteProjectEmployeeObjectIdProjectNull()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		
		try
		{
			repo.delete(new User(), null, null);
		}
		catch (IllegalArgumentException e)
		{
			assertThat("Exception User2Project by Object", e.getMessage() ,is("delete missing user and/or project"));
		}
	}
	
	@Test
	void testDeleteProjectEmployeeObjectIdRoleNull()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		
		try
		{
			repo.delete(new User(), new Project(), null);
		}
		catch (IllegalArgumentException e)
		{
			assertThat("Exception User2Project by Object", e.getMessage() ,is("delete missing user and/or project"));
		}
	}
	
	@Test
	void testGetByUsernameNotFound()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<User2Project> list = new ArrayList<>(repo.getByUsername("ABC"));
		assertThat("GetByUsername nothing found", list.toString(), is("[]"));
	}
	
	@Test
	void testGetByUsernameNullName()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<User2Project> list = new ArrayList<>(repo.getByUsername(null));
		assertThat("GetByUsername nothing found", list.toString(), is("[]"));
	}
	
	@Test
	void testGetByUsernameExist()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<User2Project> list = new ArrayList<>(repo.getByUsername("FakeUser201"));
		assertThat("getByUsername 1 Element found", list.toString(), is("[User2Project:{user: User:{id: 12; username: FakeUser201; " +
														 "name: FakeName201; vorname: FakeVorname201; role: Role:{id: 2; " +
														 "rolename: Portfoliomanager}; abteilung: FakeAbteilung201; " +
														 "mail: FakeMail201; telefon: FakeTel201}; project: Project:{id: 12; " +
														 "projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; " +
														 "username: FakeUser201; name: FakeName201; vorname: FakeVorname201; " +
														 "role: Role:{id: 2; rolename: Portfoliomanager}; " +
														 "abteilung: FakeAbteilung201; mail: FakeMail201; telefon: FakeTel201}; " +
														 "start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; " +
														 "projekstatus: FakeStatus1; projektbudget: 1.000000}; " +
														 "role: Role:{id: 2; rolename: Portfoliomanager}}]"));
	}
	
	@Test
	void testGetByProjectnameNotFound()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<User2Project> list = new ArrayList<>(repo.getByProjectname("ABC"));
		assertThat("getByProjectname nothing found", list.toString(), is("[]"));
	}
	
	@Test
	void testGetByProjectnameNullName()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<User2Project> list = new ArrayList<>(repo.getByProjectname(null));
		assertThat("getByProjectname nothing found", list.toString(), is("[]"));
	}
	
	@Test
	void testGetByProjectnameExist()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<User2Project> list = new ArrayList<>(repo.getByProjectname("FakeProject1"));
		assertThat("getByProjectname 1 element found", list.toString(), is("[User2Project:{user: User:{id: 12; username: FakeUser201; " +
														 "name: FakeName201; vorname: FakeVorname201; role: Role:{id: 2; " +
														 "rolename: Portfoliomanager}; abteilung: FakeAbteilung201; " +
														 "mail: FakeMail201; telefon: FakeTel201}; project: Project:{id: 12; " +
														 "projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; " +
														 "username: FakeUser201; name: FakeName201; vorname: FakeVorname201; " +
														 "role: Role:{id: 2; rolename: Portfoliomanager}; " +
														 "abteilung: FakeAbteilung201; mail: FakeMail201; telefon: FakeTel201}; " +
														 "start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; " +
														 "projekstatus: FakeStatus1; projektbudget: 1.000000}; " +
														 "role: Role:{id: 2; rolename: Portfoliomanager}}]"));
	}
	
	@Test
	void testGetByRoleNotFound()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<User2Project> list = new ArrayList<>(repo.getByRole("ABC"));
		assertThat("getByRole nothing found", list.toString(), is("[]"));
	}
	
	@Test
	void testGetByRoleNullName()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<User2Project> list = new ArrayList<>(repo.getByRole(null));
		assertThat("getByRole nothing found", list.toString(), is("[]"));
	}
	
	@Test
	void testGetByRoleExist()
	{
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<User2Project> list = new ArrayList<>(repo.getByRole("Admin"));
		assertThat("getByRole 1 element found", list.toString(), is("[User2Project:{user: User:{id: 11; username: FakeUser200; name: FakeName200; " +
																	"vorname: FakeVorname200; role: Role:{id: 1; rolename: Admin}; " +
																	"abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}; " +
																	"project: Project:{id: 11; projektnummer: 0; projektname: FakeProject0; " +
																	"projektleiter: User:{id: 11; username: FakeUser200; name: FakeName200; " +
																	"vorname: FakeVorname200; role: Role:{id: 1; rolename: Admin}; " +
																	"abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}; " +
																	"start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; " +
																	"projekstatus: FakeStatus0; projektbudget: 0.000000}; role: Role:{id: 1; " +
																	"rolename: Admin}}, User2Project:{user: User:{id: 16; username: FakeUser205; " +
																	"name: FakeName205; vorname: FakeVorname205; " +
																	"role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung205; " +
																	"mail: FakeMail205; telefon: FakeTel205}; project: Project:{id: 16; " +
																	"projektnummer: 5; projektname: FakeProject5; projektleiter: User:{id: 16; " +
																	"username: FakeUser205; name: FakeName205; vorname: FakeVorname205; " +
																	"role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung205; " +
																	"mail: FakeMail205; telefon: FakeTel205}; start: 2018.03.01; " +
																	"end: 2019.12.31; projektphase: FakePhase5; projekstatus: FakeStatus5; " +
																	"projektbudget: 5.000000}; role: Role:{id: 1; rolename: Admin}}]"));
	}
	
	@Test
	void testGetByRoleUserWithoutRole()
	{
		repo.insert(new User2Project(userRepo.getById(2), projectRepo.getById(9), null));
		
		populateRepo(repo, userRepo, projectRepo, roleRepo);
		List<User2Project> list = new ArrayList<>(repo.getByRole("Admin"));
		assertThat("getByRole 1 element found", list.toString(), is("[User2Project:{user: User:{id: 11; username: FakeUser200; name: FakeName200; " +
				"vorname: FakeVorname200; role: Role:{id: 1; rolename: Admin}; " +
				"abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}; " +
				"project: Project:{id: 11; projektnummer: 0; projektname: FakeProject0; " +
				"projektleiter: User:{id: 11; username: FakeUser200; name: FakeName200; " +
				"vorname: FakeVorname200; role: Role:{id: 1; rolename: Admin}; " +
				"abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}; " +
				"start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; " +
				"projekstatus: FakeStatus0; projektbudget: 0.000000}; role: Role:{id: 1; " +
				"rolename: Admin}}, User2Project:{user: User:{id: 16; username: FakeUser205; " +
				"name: FakeName205; vorname: FakeVorname205; " +
				"role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung205; " +
				"mail: FakeMail205; telefon: FakeTel205}; project: Project:{id: 16; " +
				"projektnummer: 5; projektname: FakeProject5; projektleiter: User:{id: 16; " +
				"username: FakeUser205; name: FakeName205; vorname: FakeVorname205; " +
				"role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung205; " +
				"mail: FakeMail205; telefon: FakeTel205}; start: 2018.03.01; " +
				"end: 2019.12.31; projektphase: FakePhase5; projekstatus: FakeStatus5; " +
				"projektbudget: 5.000000}; role: Role:{id: 1; rolename: Admin}}]"));
	}
}
