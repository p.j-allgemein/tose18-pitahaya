package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class RoleRepositoryTest {
	RoleRepository repo = null;
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		repo = new RoleRepository();
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Roles");
	}

	@Test
	void testDBData() {
		assertThat("New DB must contain data",repo.getAll().isEmpty(),is(false));
		assertThat("Test", repo.getAll().toString(), is("[Role:{id: 1; rolename: Admin}, Role:{id: 2; rolename: Portfoliomanager}, Role:{id: 3; rolename: Projektleiter}, Role:{id: 4; rolename: Stakeholder}, Role:{id: 5; rolename: Projektmitarbeiter}]"));
		assertThat("DB contains amount of elements", repo.getAll().size(), is(5));
	}
	
	@Test
	void testAllData()
	{
		List<String> list = repo.getAll().stream().map(e -> e.getRolename()).collect(Collectors.toList());
		assertThat("First Element is Admin",list.get(0),is("Admin"));
		assertThat("Second Element is Admin",list.get(1),is("Portfoliomanager"));
		assertThat("Third Element is Admin",list.get(2),is("Projektleiter"));
	}
	
	@Test
	void testGetRoleProjektleiter()
	{
		List<Role> pl = new ArrayList<>(repo.getByName("Projektleiter"));
		assertThat("One Element in List", pl.size(),is(1));
		assertThat("Data of Projektleiter", pl.toString(),is("[Role:{id: 3; rolename: Projektleiter}]"));
	}
	
	@Test
	void testGetRoleNotFound()
	{
		List<Role> pl = new ArrayList<>(repo.getByName("ABC"));
		assertThat("List is Empty", pl.isEmpty(),is(true));
	}
	
	@Test
	void testGetRoleId3()
	{
		Role pl = repo.getById(3);
		assertThat("Data of Projektleiter", pl.toString(),is("Role:{id: 3; rolename: Projektleiter}"));
	}
	
	@Test
	void testGetRoleIdNotFound()
	{
		assertThat("Role does not exist", repo.getById(99),is(nullValue()));
	}
	
	@ParameterizedTest
	@CsvSource({"4,Employee","5,Support","6,Stakeholder"})
	void testInsertItems(long id,String name)
	{
		Role i = new Role(id,name);
		long dbId = repo.insert(i);
		Role fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getRolename(),is(name) );

	}
	
	@ParameterizedTest
	@CsvSource({"4,Employee,Mitarbeiter,","5,Support,Supporter","6,Stakeholder,Anspruchsgruppe"})
	void testUpdateItems(long id,String name,String newName)
	{
		Role i = new Role(id,name);
		long dbId = repo.insert(i);
		i.setId(dbId);
		i.setRolename(newName);
		repo.save(i);
		
		Role fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getRolename(),is(newName) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"4,Employee","5,Supporter","6,Stakeholder"})
	void testDeleteItems(long id,String name)
	{
		Role i = new Role(id,name);
		long dbId = repo.insert(i);
		Role fromDB = repo.getById(dbId);
		assertThat("Item was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThat("Role is should be deleted", repo.getById(dbId),is(nullValue()));
	}
}
