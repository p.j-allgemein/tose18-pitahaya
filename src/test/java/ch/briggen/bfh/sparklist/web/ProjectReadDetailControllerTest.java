package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Cost;
import ch.briggen.bfh.sparklist.domain.CostRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Projectphase;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.Projectstatus;
import ch.briggen.bfh.sparklist.domain.ProjectstatusRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.RoleRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.User2Project;
import ch.briggen.bfh.sparklist.domain.UserProjectRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import ch.briggen.bfh.sparklist.domain.Workpackage;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class ProjectReadDetailControllerTest {
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	ProjectReadDetailController controller = null;
	CostRepository costRepo = null;
	UserRepository userRepo = null;
	ProjectRepository repo = null;
	UserProjectRepository u2pRepo = null;
	ProjectstatusRepository statusRepo = null;
	WorkpackageRepository workRepo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	private static void populateRepo(UserProjectRepository up, UserRepository u, ProjectRepository p, ProjectstatusRepository ps) {
		RoleRepository rrep = new RoleRepository();
		for(int i = 0; i < 10; ++i) {
			Role dummyRole = rrep.getById((i % 5) + 1);
			User dummyUser = new User(i + 200,"FakeUser" + (i + 200), "FakeName" + (i + 200), "FakeVorname" + (i + 200), dummyRole,
								      "FakeAbteilung" + (i + 200), "FakeMail" + (i + 200), "FakeTel" + (i + 200));
			dummyUser.setId(u.insert(dummyUser));
			Project dummyProject = new Project(i + 1, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			dummyProject.setId(p.insert(dummyProject));
			User2Project dummy = new User2Project(dummyUser, dummyProject, dummyRole);
			up.insert(dummy);
			Projectstatus statusDummy = new Projectstatus(0, dummyProject, "Grün", "Gelb", "Neutral", "Rot", "Grün");
			ps.insert(statusDummy);
		}
	}
	
	private static void populateRepo(ProjectRepository p) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummy = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter), 
										LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			
			p.insert(dummy);
		}
	}
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
								  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			u.insert(dummy);
		}
	}
	
	private static void populateCostRepo(CostRepository c) {
		ProjectRepository prep = new ProjectRepository();
		UserRepository urep = new UserRepository();
		ProjectphaseRepository phrep = new ProjectphaseRepository();
		for(int i = 0; i < 20; ++i) {
			Project p = prep.getById(2);
			User u = urep.getById(5);
			Projectphase ph =  phrep.getById((i % 4) + 1);
			Workpackage dummyWp = new Workpackage(0, p, ph, "FakeWP" + i * 100, i * 100, i % 2 == 0 ? i % 2 : i * 100,
												LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
												i % 2 == 0 ? true : false);
			Cost dummy = new Cost(0, dummyWp, u, "FakeText" + i, i + 520.2, LocalDate.parse("2018.03.01", formatter));
			c.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		controller = new ProjectReadDetailController(LocalDate.parse("2018.05.22", formatter));
		userRepo = new UserRepository();
		repo = new ProjectRepository();
		u2pRepo = new UserProjectRepository();
		statusRepo = new ProjectstatusRepository();
		costRepo = new CostRepository();
		workRepo = new WorkpackageRepository();
		populateUserRepo(userRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		costRepo.getAll().stream().forEach(e -> costRepo.delete(e.getId()));
		workRepo.getAll().stream().forEach(e -> workRepo.delete(e.getId()));
		u2pRepo.getAll().stream().forEach(e -> u2pRepo.delete(e.getUser().getId(), e.getProject().getId(), (e.getRole() != null ? e.getRole().getId() : 0)));
		statusRepo.getAll().stream().forEach(e -> statusRepo.delete(e.getId()));
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}
	
	@Test
	void testHandleNothing() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn(null);
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{error=projekt-id unbekannt bzw. nicht mitgegeben, session_user=username, " +
															  "plList=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}]}") );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleWithZeroId() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("0");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{error=projekt-id unbekannt bzw. nicht mitgegeben, session_user=username, " +
															  "plList=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}]}") );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleNothingFound() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("id")).thenReturn("99");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{projektphasen=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], " + 
					"postAction=/project/update, projectDetail=null, session_user=username, " + 
					"plList=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}, " + 
					"User:{id: 11; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, User:{id: 13; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, User:{id: 14; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, User:{id: 15; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, User:{id: 16; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}, User:{id: 17; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}, User:{id: 18; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}, User:{id: 19; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}, User:{id: 20; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}]}") );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandle() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("id")).thenReturn("1");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=[], projectrisikoDetail=[], postAction=/project/update, stakeholder=[], session_user=username, " + 
					"plList=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}, " + 
					"User:{id: 11; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, User:{id: 13; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, User:{id: 14; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, User:{id: 15; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, User:{id: 16; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}, User:{id: 17; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}, User:{id: 18; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}, User:{id: 19; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}, User:{id: 20; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}], " + 
					"evaWP={}, " + 
					"evaPhasen={Initialisierung=EarnedValueAnalysis:{name: Initialisierung; budget: 0.000000; plannedValue: 0.000000; earnedValue: 0.000000; actualCost: 0.000000; costDifference: 0.000000; costEfficency: 0.000000; degreeOfCompletion: 0.000000; planDifference: 0.000000}, Einführung=EarnedValueAnalysis:{name: Einführung; budget: 0.000000; plannedValue: 0.000000; earnedValue: 0.000000; actualCost: 0.000000; costDifference: 0.000000; costEfficency: 0.000000; degreeOfCompletion: 0.000000; planDifference: 0.000000}, " + 
					"Konzeption=EarnedValueAnalysis:{name: Konzeption; budget: 0.000000; plannedValue: 0.000000; earnedValue: 0.000000; actualCost: 0.000000; costDifference: 0.000000; costEfficency: 0.000000; degreeOfCompletion: 0.000000; planDifference: 0.000000}, Realisierung=EarnedValueAnalysis:{name: Realisierung; budget: 0.000000; plannedValue: 0.000000; earnedValue: 0.000000; actualCost: 0.000000; costDifference: 0.000000; costEfficency: 0.000000; degreeOfCompletion: 0.000000; planDifference: 0.000000}}, " + 
					"projektphasen=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], " + 
					"wpKosten={}, " + 
					"evaProject=EarnedValueAnalysis:{name: FakeProject0; budget: 0.000000; plannedValue: 0.000000; earnedValue: 0.000000; actualCost: 0.000000; costDifference: 0.000000; costEfficency: 0.000000; degreeOfCompletion: 0.000000; planDifference: 0.000000}, " + 
					"projektstatus=Projectstatus:{id: 0; project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 11; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; projectstatus: Grün; ressourcestatus: Grün; riskstatus: Grün; timestatus: Grün; budgetstatus: Grün}, " + 
					"projectDetail=Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 11; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}}") );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleWithStakeholdersAndEmployees() {
		try
		{
			populateRepo(repo);
			populateRepo(u2pRepo, userRepo, repo, statusRepo);
			
			when(mockRequest.queryParams("id")).thenReturn("14");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=[], projectrisikoDetail=[], postAction=/project/update, " + 
					"stakeholder=[User:{id: 24; username: FakeUser203; name: FakeName203; vorname: FakeVorname203; role: Role:{id: 4; rolename: Stakeholder}; abteilung: FakeAbteilung203; mail: FakeMail203; telefon: FakeTel203}], " + 
					"session_user=username, " + 
					"plList=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}, " + 
					"User:{id: 11; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, User:{id: 13; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, User:{id: 14; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, User:{id: 15; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, User:{id: 16; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}, User:{id: 17; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}, User:{id: 18; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}, User:{id: 19; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}, User:{id: 20; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}, User:{id: 23; username: FakeUser202; name: FakeName202; vorname: FakeVorname202; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung202; mail: FakeMail202; telefon: FakeTel202}, User:{id: 28; username: FakeUser207; name: FakeName207; vorname: FakeVorname207; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung207; mail: FakeMail207; telefon: FakeTel207}], " + 
					"evaWP={}, " + 
					"evaPhasen={Initialisierung=EarnedValueAnalysis:{name: Initialisierung; budget: 3.000000; plannedValue: 0.000000; earnedValue: 0.000000; actualCost: 0.000000; costDifference: 0.000000; costEfficency: 0.000000; degreeOfCompletion: 0.360000; planDifference: 0.360000}, Einführung=EarnedValueAnalysis:{name: Einführung; budget: 3.000000; plannedValue: 0.000000; earnedValue: 0.000000; actualCost: 0.000000; costDifference: 0.000000; costEfficency: 0.000000; degreeOfCompletion: 0.360000; planDifference: 0.360000}, " + 
					"Konzeption=EarnedValueAnalysis:{name: Konzeption; budget: 3.000000; plannedValue: 0.000000; earnedValue: 0.000000; actualCost: 0.000000; costDifference: 0.000000; costEfficency: 0.000000; degreeOfCompletion: 0.360000; planDifference: 0.360000}, Realisierung=EarnedValueAnalysis:{name: Realisierung; budget: 3.000000; plannedValue: 0.000000; earnedValue: 0.000000; actualCost: 0.000000; costDifference: 0.000000; costEfficency: 0.000000; degreeOfCompletion: 0.360000; planDifference: 0.360000}}, " + 
					"projektphasen=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], " + 
					"wpKosten={}, " + 
					"evaProject=EarnedValueAnalysis:{name: FakeProject3; budget: 3.000000; plannedValue: 0.000000; earnedValue: 0.000000; actualCost: 0.000000; costDifference: 0.000000; costEfficency: 0.000000; degreeOfCompletion: 0.360000; planDifference: 0.360000}, " + 
					"projektstatus=Projectstatus:{id: 0; project: Project:{id: 14; projektnummer: 3; projektname: FakeProject3; projektleiter: User:{id: 24; username: FakeUser203; name: FakeName203; vorname: FakeVorname203; role: Role:{id: 4; rolename: Stakeholder}; abteilung: FakeAbteilung203; mail: FakeMail203; telefon: FakeTel203}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase3; projekstatus: FakeStatus3; projektbudget: 3.000000}; projectstatus: Grün; ressourcestatus: Grün; riskstatus: Grün; timestatus: Grün; budgetstatus: Grün}, " + 
					"projectDetail=Project:{id: 14; projektnummer: 3; projektname: FakeProject3; projektleiter: User:{id: 24; username: FakeUser203; name: FakeName203; vorname: FakeVorname203; role: Role:{id: 4; rolename: Stakeholder}; abteilung: FakeAbteilung203; mail: FakeMail203; telefon: FakeTel203}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase3; projekstatus: FakeStatus3; projektbudget: 3.000000}}") );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleWithEVA() {
		try
		{
			populateRepo(repo);
			populateRepo(u2pRepo, userRepo, repo, statusRepo);
			populateCostRepo(costRepo);
			
			when(mockRequest.queryParams("id")).thenReturn("2");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=[], projectrisikoDetail=[], postAction=/project/update, stakeholder=[], session_user=username, " + 
					"plList=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " + 
					"User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, " + 
					"User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, " + 
					"User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, " + 
					"User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, " + 
					"User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, " + 
					"User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, " + 
					"User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, " + 
					"User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, " + 
					"User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}, " + 
					"User:{id: 11; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " + 
					"User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, " + 
					"User:{id: 13; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, " + 
					"User:{id: 14; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, " + 
					"User:{id: 15; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, " + 
					"User:{id: 16; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}, " + 
					"User:{id: 17; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}, " + 
					"User:{id: 18; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}, " + 
					"User:{id: 19; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}, " + 
					"User:{id: 20; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}, " + 
					"User:{id: 23; username: FakeUser202; name: FakeName202; vorname: FakeVorname202; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung202; mail: FakeMail202; telefon: FakeTel202}, " + 
					"User:{id: 28; username: FakeUser207; name: FakeName207; vorname: FakeVorname207; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung207; mail: FakeMail207; telefon: FakeTel207}], " + 
					"evaWP={FakeWP900=EarnedValueAnalysis:{name: Konzeption; budget: 900.000000; plannedValue: 900.000000; earnedValue: 108.000000; actualCost: 529.200000; costDifference: -421.200000; costEfficency: 0.200000; degreeOfCompletion: 108.000000; planDifference: -792.000000}, " + 
					"FakeWP800=EarnedValueAnalysis:{name: Initialisierung; budget: 800.000000; plannedValue: 800.000000; earnedValue: 0.000000; actualCost: 528.200000; costDifference: -432.200000; costEfficency: 0.180000; degreeOfCompletion: 96.000000; planDifference: -704.000000}, " + 
					"FakeWP700=EarnedValueAnalysis:{name: Einführung; budget: 700.000000; plannedValue: 700.000000; earnedValue: 84.000000; actualCost: 527.200000; costDifference: -443.200000; costEfficency: 0.160000; degreeOfCompletion: 84.000000; planDifference: -616.000000}, " + 
					"FakeWP600=EarnedValueAnalysis:{name: Realisierung; budget: 600.000000; plannedValue: 600.000000; earnedValue: 0.000000; actualCost: 526.200000; costDifference: -454.200000; costEfficency: 0.140000; degreeOfCompletion: 72.000000; planDifference: -528.000000}, " + 
					"FakeWP1100=EarnedValueAnalysis:{name: Einführung; budget: 1100.000000; plannedValue: 1100.000000; earnedValue: 132.000000; actualCost: 531.200000; costDifference: -399.200000; costEfficency: 0.250000; degreeOfCompletion: 132.000000; planDifference: -968.000000}, " + 
					"FakeWP1200=EarnedValueAnalysis:{name: Initialisierung; budget: 1200.000000; plannedValue: 1200.000000; earnedValue: 0.000000; actualCost: 532.200000; costDifference: -388.200000; costEfficency: 0.270000; degreeOfCompletion: 144.000000; planDifference: -1056.000000}, " + 
					"FakeWP1000=EarnedValueAnalysis:{name: Realisierung; budget: 1000.000000; plannedValue: 1000.000000; earnedValue: 0.000000; actualCost: 530.200000; costDifference: -410.200000; costEfficency: 0.230000; degreeOfCompletion: 120.000000; planDifference: -880.000000}, " + 
					"FakeWP100=EarnedValueAnalysis:{name: Konzeption; budget: 100.000000; plannedValue: 100.000000; earnedValue: 12.000000; actualCost: 521.200000; costDifference: -509.200000; costEfficency: 0.020000; degreeOfCompletion: 12.000000; planDifference: -88.000000}, " + 
					"FakeWP500=EarnedValueAnalysis:{name: Konzeption; budget: 500.000000; plannedValue: 500.000000; earnedValue: 60.000000; actualCost: 525.200000; costDifference: -465.200000; costEfficency: 0.110000; degreeOfCompletion: 60.000000; planDifference: -440.000000}, " + 
					"FakeWP400=EarnedValueAnalysis:{name: Initialisierung; budget: 400.000000; plannedValue: 400.000000; earnedValue: 0.000000; actualCost: 524.200000; costDifference: -476.200000; costEfficency: 0.090000; degreeOfCompletion: 48.000000; planDifference: -352.000000}, " + 
					"FakeWP300=EarnedValueAnalysis:{name: Einführung; budget: 300.000000; plannedValue: 300.000000; earnedValue: 36.000000; actualCost: 523.200000; costDifference: -487.200000; costEfficency: 0.070000; degreeOfCompletion: 36.000000; planDifference: -264.000000}, " + 
					"FakeWP0=EarnedValueAnalysis:{name: Initialisierung; budget: 0.000000; plannedValue: 0.000000; earnedValue: 0.000000; actualCost: 520.200000; costDifference: -520.200000; costEfficency: 0.000000; degreeOfCompletion: 0.000000; planDifference: 0.000000}, " + 
					"FakeWP200=EarnedValueAnalysis:{name: Realisierung; budget: 200.000000; plannedValue: 200.000000; earnedValue: 0.000000; actualCost: 522.200000; costDifference: -498.200000; costEfficency: 0.050000; degreeOfCompletion: 24.000000; planDifference: -176.000000}, " + 
					"FakeWP1500=EarnedValueAnalysis:{name: Einführung; budget: 1500.000000; plannedValue: 1500.000000; earnedValue: 180.000000; actualCost: 535.200000; costDifference: -355.200000; costEfficency: 0.340000; degreeOfCompletion: 180.000000; planDifference: -1320.000000}, " + 
					"FakeWP1600=EarnedValueAnalysis:{name: Initialisierung; budget: 1600.000000; plannedValue: 1600.000000; earnedValue: 0.000000; actualCost: 536.200000; costDifference: -344.200000; costEfficency: 0.360000; degreeOfCompletion: 192.000000; planDifference: -1408.000000}, " + 
					"FakeWP1300=EarnedValueAnalysis:{name: Konzeption; budget: 1300.000000; plannedValue: 1300.000000; earnedValue: 156.000000; actualCost: 533.200000; costDifference: -377.200000; costEfficency: 0.290000; degreeOfCompletion: 156.000000; planDifference: -1144.000000}, " + 
					"FakeWP1400=EarnedValueAnalysis:{name: Realisierung; budget: 1400.000000; plannedValue: 1400.000000; earnedValue: 0.000000; actualCost: 534.200000; costDifference: -366.200000; costEfficency: 0.310000; degreeOfCompletion: 168.000000; planDifference: -1232.000000}, " + 
					"FakeWP1900=EarnedValueAnalysis:{name: Einführung; budget: 1900.000000; plannedValue: 1900.000000; earnedValue: 228.000000; actualCost: 539.200000; costDifference: -311.200000; costEfficency: 0.420000; degreeOfCompletion: 228.000000; planDifference: -1672.000000}, " + 
					"FakeWP1700=EarnedValueAnalysis:{name: Konzeption; budget: 1700.000000; plannedValue: 1700.000000; earnedValue: 204.000000; actualCost: 537.200000; costDifference: -333.200000; costEfficency: 0.380000; degreeOfCompletion: 204.000000; planDifference: -1496.000000}, " + 
					"FakeWP1800=EarnedValueAnalysis:{name: Realisierung; budget: 1800.000000; plannedValue: 1800.000000; earnedValue: 0.000000; actualCost: 538.200000; costDifference: -322.200000; costEfficency: 0.400000; degreeOfCompletion: 216.000000; planDifference: -1584.000000}}, " + 
					"evaPhasen={Initialisierung=EarnedValueAnalysis:{name: Initialisierung; budget: 1.000000; plannedValue: 480.000000; earnedValue: 0.000000; actualCost: 2641.000000; costDifference: -2640.880000; costEfficency: 0.000000; degreeOfCompletion: 0.120000; planDifference: -479.880000}, Einführung=EarnedValueAnalysis:{name: Einführung; budget: 1.000000; plannedValue: 660.000000; earnedValue: 660.000000; actualCost: 2656.000000; costDifference: -2655.880000; costEfficency: 0.000000; degreeOfCompletion: 0.120000; planDifference: -659.880000}, " +
					"Konzeption=EarnedValueAnalysis:{name: Konzeption; budget: 1.000000; plannedValue: 540.000000; earnedValue: 540.000000; actualCost: 2646.000000; costDifference: -2645.880000; costEfficency: 0.000000; degreeOfCompletion: 0.120000; planDifference: -539.880000}, Realisierung=EarnedValueAnalysis:{name: Realisierung; budget: 1.000000; plannedValue: 600.000000; earnedValue: 0.000000; actualCost: 2651.000000; costDifference: -2650.880000; costEfficency: 0.000000; degreeOfCompletion: 0.120000; planDifference: -599.880000}}, " + 
					"projektphasen=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], " + 
					"wpKosten={FakeWP900=[Cost:{id: 10; workpackage: Workpackage:{id: 10; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 2; phasenname: Konzeption}; workpackagename: FakeWP900; plannedValue: 900.000000; earnedValue: 900.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText9; betrag: 529.200000; datum: 2018.03.01}], " + 
					"FakeWP800=[Cost:{id: 9; workpackage: Workpackage:{id: 9; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 1; phasenname: Initialisierung}; workpackagename: FakeWP800; plannedValue: 800.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: true}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText8; betrag: 528.200000; datum: 2018.03.01}], " + 
					"FakeWP700=[Cost:{id: 8; workpackage: Workpackage:{id: 8; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 4; phasenname: Einführung}; workpackagename: FakeWP700; plannedValue: 700.000000; earnedValue: 700.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText7; betrag: 527.200000; datum: 2018.03.01}], " + 
					"FakeWP600=[Cost:{id: 7; workpackage: Workpackage:{id: 7; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP600; plannedValue: 600.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: true}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText6; betrag: 526.200000; datum: 2018.03.01}], " + 
					"FakeWP1100=[Cost:{id: 12; workpackage: Workpackage:{id: 12; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 4; phasenname: Einführung}; workpackagename: FakeWP1100; plannedValue: 1100.000000; earnedValue: 1100.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText11; betrag: 531.200000; datum: 2018.03.01}], " + 
					"FakeWP1200=[Cost:{id: 13; workpackage: Workpackage:{id: 13; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 1; phasenname: Initialisierung}; workpackagename: FakeWP1200; plannedValue: 1200.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: true}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText12; betrag: 532.200000; datum: 2018.03.01}], " + 
					"FakeWP1000=[Cost:{id: 11; workpackage: Workpackage:{id: 11; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP1000; plannedValue: 1000.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: true}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText10; betrag: 530.200000; datum: 2018.03.01}], " + 
					"FakeWP100=[Cost:{id: 2; workpackage: Workpackage:{id: 2; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 2; phasenname: Konzeption}; workpackagename: FakeWP100; plannedValue: 100.000000; earnedValue: 100.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText1; betrag: 521.200000; datum: 2018.03.01}], " + 
					"FakeWP500=[Cost:{id: 6; workpackage: Workpackage:{id: 6; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 2; phasenname: Konzeption}; workpackagename: FakeWP500; plannedValue: 500.000000; earnedValue: 500.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText5; betrag: 525.200000; datum: 2018.03.01}], " + 
					"FakeWP400=[Cost:{id: 5; workpackage: Workpackage:{id: 5; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 1; phasenname: Initialisierung}; workpackagename: FakeWP400; plannedValue: 400.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: true}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText4; betrag: 524.200000; datum: 2018.03.01}], " + 
					"FakeWP300=[Cost:{id: 4; workpackage: Workpackage:{id: 4; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 4; phasenname: Einführung}; workpackagename: FakeWP300; plannedValue: 300.000000; earnedValue: 300.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText3; betrag: 523.200000; datum: 2018.03.01}], " + 
					"FakeWP200=[Cost:{id: 3; workpackage: Workpackage:{id: 3; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP200; plannedValue: 200.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: true}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText2; betrag: 522.200000; datum: 2018.03.01}], " + 
					"FakeWP0=[Cost:{id: 1; workpackage: Workpackage:{id: 1; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 1; phasenname: Initialisierung}; workpackagename: FakeWP0; plannedValue: 0.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: true}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText0; betrag: 520.200000; datum: 2018.03.01}], " + 
					"FakeWP1500=[Cost:{id: 16; workpackage: Workpackage:{id: 16; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 4; phasenname: Einführung}; workpackagename: FakeWP1500; plannedValue: 1500.000000; earnedValue: 1500.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText15; betrag: 535.200000; datum: 2018.03.01}], " + 
					"FakeWP1600=[Cost:{id: 17; workpackage: Workpackage:{id: 17; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 1; phasenname: Initialisierung}; workpackagename: FakeWP1600; plannedValue: 1600.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: true}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText16; betrag: 536.200000; datum: 2018.03.01}], " + 
					"FakeWP1300=[Cost:{id: 14; workpackage: Workpackage:{id: 14; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 2; phasenname: Konzeption}; workpackagename: FakeWP1300; plannedValue: 1300.000000; earnedValue: 1300.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText13; betrag: 533.200000; datum: 2018.03.01}], " + 
					"FakeWP1400=[Cost:{id: 15; workpackage: Workpackage:{id: 15; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP1400; plannedValue: 1400.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: true}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText14; betrag: 534.200000; datum: 2018.03.01}], " + 
					"FakeWP1900=[Cost:{id: 20; workpackage: Workpackage:{id: 20; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 4; phasenname: Einführung}; workpackagename: FakeWP1900; plannedValue: 1900.000000; earnedValue: 1900.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText19; betrag: 539.200000; datum: 2018.03.01}], " + 
					"FakeWP1700=[Cost:{id: 18; workpackage: Workpackage:{id: 18; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 2; phasenname: Konzeption}; workpackagename: FakeWP1700; plannedValue: 1700.000000; earnedValue: 1700.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText17; betrag: 537.200000; datum: 2018.03.01}], " + 
					"FakeWP1800=[Cost:{id: 19; workpackage: Workpackage:{id: 19; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP1800; plannedValue: 1800.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: true}; user: User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}; beschreibung: FakeText18; betrag: 538.200000; datum: 2018.03.01}]}, " + 
					"evaProject=EarnedValueAnalysis:{name: FakeProject1; budget: 1.000000; plannedValue: 2280.000000; earnedValue: 1200.000000; actualCost: 10594.000000; costDifference: -10593.880000; costEfficency: 0.000000; degreeOfCompletion: 0.120000; planDifference: -2279.880000}, " + 
					"projektstatus=Projectstatus:{id: 0; project: Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}; projectstatus: Grün; ressourcestatus: Grün; riskstatus: Grün; timestatus: Grün; budgetstatus: Grün}, " + 
					"projectDetail=Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}}") );			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
