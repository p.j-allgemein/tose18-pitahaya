package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verifyZeroInteractions;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class UserEditControllerTest {
	UserEditController controller = null;
	UserRepository repo = null;
	
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(1, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			u.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new UserEditController();
		repo = new UserRepository();
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Users");
		resetDB("Roles");
	}

	@Test
	void testHandleUnknownUser() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("id")).thenReturn("1234");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("registration") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=null, " +
														      "postAction=/users/update, " +
															  "roleList=[Role:{id: 1; rolename: Admin}, Role:{id: 2; rolename: Portfoliomanager}, Role:{id: 3; rolename: Projektleiter}, Role:{id: 4; rolename: Stakeholder}, Role:{id: 5; rolename: Projektmitarbeiter}], " +
														      "session_user=test}") );
			
			verify(mockRequest).queryParams("id");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleKnownUser() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("id")).thenReturn("1");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("registration") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
															  "postAction=/users/update, " +
															  "roleList=[Role:{id: 1; rolename: Admin}, Role:{id: 2; rolename: Portfoliomanager}, Role:{id: 3; rolename: Projektleiter}, Role:{id: 4; rolename: Stakeholder}, Role:{id: 5; rolename: Projektmitarbeiter}], " +
															  "session_user=test}") );
			
			verify(mockRequest).queryParams("id");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleIdNull() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("id")).thenReturn(null);
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("registration") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=User:{id: 0; username: null; name: null; vorname: null; role: null; abteilung: null; mail: null; telefon: null}, " +
															  "postAction=/users/new, " +
															  "roleList=[Role:{id: 1; rolename: Admin}, Role:{id: 2; rolename: Portfoliomanager}, Role:{id: 3; rolename: Projektleiter}, Role:{id: 4; rolename: Stakeholder}, Role:{id: 5; rolename: Projektmitarbeiter}], " +
															  "session_user=test}") );
			
			verify(mockRequest).queryParams("id");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleIdZero() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("id")).thenReturn("0");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("registration") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=User:{id: 0; username: null; name: null; vorname: null; role: null; abteilung: null; mail: null; telefon: null}, " +
															  "postAction=/users/new, " +
															  "roleList=[Role:{id: 1; rolename: Admin}, Role:{id: 2; rolename: Portfoliomanager}, Role:{id: 3; rolename: Projektleiter}, Role:{id: 4; rolename: Stakeholder}, Role:{id: 5; rolename: Projektmitarbeiter}], " +
															  "session_user=test}") );
			
			verify(mockRequest).queryParams("id");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
