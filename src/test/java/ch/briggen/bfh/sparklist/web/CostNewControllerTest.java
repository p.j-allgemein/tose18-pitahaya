package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.CostRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.Projectphase;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import ch.briggen.bfh.sparklist.domain.Workpackage;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class CostNewControllerTest {
	CostNewController controller = null;
	CostRepository repo = null;
	UserRepository userRepo = null;
	WorkpackageRepository workRepo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	private Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateWorkpackageRepo(WorkpackageRepository w) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
					  				  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			Project dummyProject = new Project(0, i * 100, "FakeProject" + i * 100, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i * 100, "FakeStatus" + i * 100, i * 100);
			Workpackage dummy = new Workpackage(0, dummyProject, new Projectphase(3, "FakePhase" + i * 100), "FakeWP" + i * 100, i * 100, i * 100,
												LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
												false);
			
			w.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new CostNewController();
		userRepo = new UserRepository();
		repo = new CostRepository();
		workRepo = new WorkpackageRepository();
		populateWorkpackageRepo(workRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Workpackages");
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}

	@Test
	void testHandleNullCost() {
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn(null);
			when(mockRequest.queryParams("costDetail.workpackagename")).thenReturn(null);
			when(mockRequest.queryParams("costDetail.username")).thenReturn(null);
			when(mockRequest.queryParams("costDetail.id")).thenReturn(null);
			when(mockRequest.queryParams("costDetail.betrag")).thenReturn(null);
			when(mockRequest.queryParams("costDetail.datum")).thenReturn(null);
			when(mockRequest.queryParams("costDetail.beschreibung")).thenReturn(null);
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("kosten_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{projektId=null, " +
					"userList=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}], " +
					"costDetail=Cost:{id: 0; workpackage: null; user: null; beschreibung: null; betrag: 0.000000; datum: null}, " +
					"workpackageList=[], postAction=/kosten/new, error=Workpackage null wurde nicht gefunden, session_user=username}") );
			
			verify(mockRequest, times(2)).queryParams("projectId");
			verify(mockRequest, times(2)).queryParams("costDetail.workpackagename");
			verify(mockRequest).queryParams("costDetail.username");
			verify(mockRequest).queryParams("costDetail.id");
			verify(mockRequest).queryParams("costDetail.betrag");
			verify(mockRequest).queryParams("costDetail.datum");
			verify(mockRequest).queryParams("costDetail.beschreibung");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/projektsteckbrief?id=1");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleEmptyCost() {
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn("");
			when(mockRequest.queryParams("costDetail.workpackagename")).thenReturn("");
			when(mockRequest.queryParams("costDetail.username")).thenReturn("");
			when(mockRequest.queryParams("costDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("costDetail.betrag")).thenReturn("0.0");
			when(mockRequest.queryParams("costDetail.datum")).thenReturn("");
			when(mockRequest.queryParams("costDetail.beschreibung")).thenReturn("");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("kosten_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{projektId=, " +
					"userList=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}], " +
					"costDetail=Cost:{id: 0; workpackage: null; user: null; beschreibung: ; betrag: 0.000000; datum: null}, " +
					"workpackageList=[], postAction=/kosten/new, error=Workpackage  wurde nicht gefunden, session_user=username}") );
			
			verify(mockRequest, times(2)).queryParams("projectId");
			verify(mockRequest, times(2)).queryParams("costDetail.workpackagename");
			verify(mockRequest).queryParams("costDetail.username");
			verify(mockRequest, times(2)).queryParams("costDetail.id");
			verify(mockRequest, times(2)).queryParams("costDetail.betrag");
			verify(mockRequest, times(2)).queryParams("costDetail.datum");
			verify(mockRequest).queryParams("costDetail.beschreibung");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/projektsteckbrief?id=1");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleRealCost() {
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn("1234");
			when(mockRequest.queryParams("costDetail.workpackagename")).thenReturn("FakeWP0");
			when(mockRequest.queryParams("costDetail.username")).thenReturn("FakeUser0");
			when(mockRequest.queryParams("costDetail.id")).thenReturn("1");
			when(mockRequest.queryParams("costDetail.betrag")).thenReturn("1.0");
			when(mockRequest.queryParams("costDetail.datum")).thenReturn("2018-03-01");
			when(mockRequest.queryParams("costDetail.beschreibung")).thenReturn("DummyText");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest, never()).queryParams("projectId");
			verify(mockRequest, times(1)).queryParams("costDetail.workpackagename");
			verify(mockRequest).queryParams("costDetail.username");
			verify(mockRequest, times(2)).queryParams("costDetail.id");
			verify(mockRequest, times(2)).queryParams("costDetail.betrag");
			verify(mockRequest, times(3)).queryParams("costDetail.datum");
			verify(mockRequest).queryParams("costDetail.beschreibung");
			verify(mockRequest, never()).session();
			verify(mockSession, never()).attribute("USERNAME");
			verify(mockHttpServletResponse).sendRedirect("/projektsteckbrief?id=1");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleNumericWorkpackage() {
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn("1234");
			when(mockRequest.queryParams("costDetail.workpackagename")).thenReturn("1");
			when(mockRequest.queryParams("costDetail.username")).thenReturn("1");
			when(mockRequest.queryParams("costDetail.id")).thenReturn("1");
			when(mockRequest.queryParams("costDetail.betrag")).thenReturn("1.0");
			when(mockRequest.queryParams("costDetail.datum")).thenReturn("2018-03-01");
			when(mockRequest.queryParams("costDetail.beschreibung")).thenReturn("DummyText");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest, never()).queryParams("projectId");
			verify(mockRequest, times(1)).queryParams("costDetail.workpackagename");
			verify(mockRequest).queryParams("costDetail.username");
			verify(mockRequest, times(2)).queryParams("costDetail.id");
			verify(mockRequest, times(2)).queryParams("costDetail.betrag");
			verify(mockRequest, times(3)).queryParams("costDetail.datum");
			verify(mockRequest).queryParams("costDetail.beschreibung");
			verify(mockRequest, never()).session();
			verify(mockSession, never()).attribute("USERNAME");
			verify(mockHttpServletResponse).sendRedirect("/projektsteckbrief?id=1");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleNullUsername() {
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn("FakeProject0");
			when(mockRequest.queryParams("costDetail.workpackagename")).thenReturn("1");
			when(mockRequest.queryParams("costDetail.username")).thenReturn(null);
			when(mockRequest.queryParams("costDetail.id")).thenReturn("1");
			when(mockRequest.queryParams("costDetail.betrag")).thenReturn("1.0");
			when(mockRequest.queryParams("costDetail.datum")).thenReturn("2018-03-01");
			when(mockRequest.queryParams("costDetail.beschreibung")).thenReturn("DummyText");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("kosten_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{projektId=FakeProject0, " +
					"userList=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}], " +
					"costDetail=Cost:{id: 1; workpackage: Workpackage:{id: 1; project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP0; plannedValue: 0.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: null; beschreibung: DummyText; betrag: 1.000000; datum: 2018.03.01}, " +
					"workpackageList=[Workpackage:{id: 1; project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP0; plannedValue: 0.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}], " +
					"postAction=/kosten/new, error=User null wurde nicht gefunden, session_user=username}") );
			
			verify(mockRequest, times(2)).queryParams("projectId");
			verify(mockRequest).queryParams("costDetail.workpackagename");
			verify(mockRequest, times(2)).queryParams("costDetail.username");
			verify(mockRequest, times(2)).queryParams("costDetail.id");
			verify(mockRequest, times(2)).queryParams("costDetail.betrag");
			verify(mockRequest, times(3)).queryParams("costDetail.datum");
			verify(mockRequest).queryParams("costDetail.beschreibung");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/projektsteckbrief?id=1");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleEmptyUsername() {
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn("FakeProject0");
			when(mockRequest.queryParams("costDetail.workpackagename")).thenReturn("1");
			when(mockRequest.queryParams("costDetail.username")).thenReturn("");
			when(mockRequest.queryParams("costDetail.id")).thenReturn("1");
			when(mockRequest.queryParams("costDetail.betrag")).thenReturn("1.0");
			when(mockRequest.queryParams("costDetail.datum")).thenReturn("2018-03-01");
			when(mockRequest.queryParams("costDetail.beschreibung")).thenReturn("DummyText");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("kosten_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{projektId=FakeProject0, " +
					"userList=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}], " +
					"costDetail=Cost:{id: 1; workpackage: Workpackage:{id: 1; project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP0; plannedValue: 0.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}; user: null; beschreibung: DummyText; betrag: 1.000000; datum: 2018.03.01}, " +
					"workpackageList=[Workpackage:{id: 1; project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP0; plannedValue: 0.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}], " +
					"postAction=/kosten/new, error=User  wurde nicht gefunden, session_user=username}") );
			
			verify(mockRequest, times(2)).queryParams("projectId");
			verify(mockRequest).queryParams("costDetail.workpackagename");
			verify(mockRequest, times(2)).queryParams("costDetail.username");
			verify(mockRequest, times(2)).queryParams("costDetail.id");
			verify(mockRequest, times(2)).queryParams("costDetail.betrag");
			verify(mockRequest, times(3)).queryParams("costDetail.datum");
			verify(mockRequest).queryParams("costDetail.beschreibung");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/projektsteckbrief?id=1");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleAlphanumericBetrag() {
		ModelAndView mav = null;
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn("1234");
			when(mockRequest.queryParams("costDetail.workpackagename")).thenReturn("1");
			when(mockRequest.queryParams("costDetail.username")).thenReturn("");
			when(mockRequest.queryParams("costDetail.id")).thenReturn("1");
			when(mockRequest.queryParams("costDetail.betrag")).thenReturn("A");
			when(mockRequest.queryParams("costDetail.datum")).thenReturn("2018-03-01");
			when(mockRequest.queryParams("costDetail.beschreibung")).thenReturn("DummyText");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			mav = controller.handle(mockRequest, response);
			fail("Expected Exception ");
		}
		catch (Exception e)
		{
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest, never()).queryParams("projectId");
			verify(mockRequest).queryParams("costDetail.workpackagename");
			verify(mockRequest).queryParams("costDetail.username");
			verify(mockRequest, times(2)).queryParams("costDetail.id");
			verify(mockRequest, times(2)).queryParams("costDetail.betrag");
			verify(mockRequest, never()).queryParams("costDetail.datum");
			verify(mockRequest, never()).queryParams("costDetail.beschreibung");
			verify(mockRequest, never()).session();
			verify(mockSession, never()).attribute("USERNAME");
			try
			{
				verify(mockHttpServletResponse, never()).sendRedirect("/projektsteckbrief?id=1");
			}
			catch (IOException ex)
			{
				fail("Unexpected IOException " + ex.getMessage());
			}
		}
	}
}
