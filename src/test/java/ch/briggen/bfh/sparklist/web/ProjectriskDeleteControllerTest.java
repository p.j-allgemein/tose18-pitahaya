package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.Projectrisk;
import ch.briggen.bfh.sparklist.domain.ProjectriskRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;

@ExtendWith(MockitoExtension.class)
public class ProjectriskDeleteControllerTest {
	ProjectriskDeleteController controller = null;
	ProjectriskRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateRepo(ProjectriskRepository p) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								      "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummyProject = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			Projectrisk dummy = new Projectrisk(0, "FakeName" + i, "FakeUrsache" + i, "FakeGefaehrdung" + i, i, i, "FakeMassnahmen" + i,
												dummyUser, i, dummyProject);
			
			p.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new ProjectriskDeleteController();
		repo = new ProjectriskRepository();
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projectrisks");
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}
	
	@Test
	void testHandleUnknownProjectrisk() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("id")).thenReturn("1234");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("id");
			verify(mockHttpServletResponse).sendRedirect("/projektsteckbrief?id=null");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleKnownProjectrisk() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("id")).thenReturn("1");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("id");
			verify(mockHttpServletResponse).sendRedirect("/projektsteckbrief?id=1");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
