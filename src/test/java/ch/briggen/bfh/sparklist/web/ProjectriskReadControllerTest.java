package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.Projectrisk;
import ch.briggen.bfh.sparklist.domain.ProjectriskRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;

@ExtendWith(MockitoExtension.class)
public class ProjectriskReadControllerTest {
	ProjectriskReadController controller = null;
	ProjectriskRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateProjectRepo(ProjectriskRepository p) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								      "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummyProject = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			Projectrisk dummy = new Projectrisk(0, "FakeName" + i, "FakeUrsache" + i, "FakeGefaehrdung" + i, i, i, "FakeMassnahmen" + i,
												dummyUser, i, dummyProject);
			
			p.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new ProjectriskReadController();
		repo = new ProjectriskRepository();
		populateProjectRepo(repo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projectrisks");
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}
	
	@Test
	void testHandleNothing() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn(null);
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{error=projekt-id unbekannt bzw. nicht mitgegeben}") );
			
			verify(mockRequest).queryParams("id");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleIdZero() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("0");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{error=projekt-id unbekannt bzw. nicht mitgegeben}") );
			
			verify(mockRequest).queryParams("id");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleWithIdNothingFound() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("13");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{projectrisikoDetail=[], postAction=/projektrisiko/update}") );
			
			verify(mockRequest).queryParams("id");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleWithId() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("1");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{projectrisikoDetail=[Projectrisk:{id: 1; risikoname: FakeName0; " +
										"ursache: FakeUrsache0; gefaehrdungsart: FakeGefaehrdung0; eintrittswahrscheinlichkeit: 0.000000; " +
										"auswirkungen: 0.000000; massnahmen: FakeMassnahmen0; verantwortlicher: User:{id: 1; username: FakeUser0; " +
										"name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; " +
										"abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; prioritaet: 0; " +
										"project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; " +
										"projektleiter: User:{id: 2; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; " +
										"role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; " +
										"telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; " +
										"projekstatus: FakeStatus0; projektbudget: 0.000000}}], postAction=/projektrisiko/update}") );
			
			verify(mockRequest).queryParams("id");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleWithNameNothingFound() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("ABC");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{projectrisikoDetail=[], postAction=/projektrisiko/update}") );
			
			verify(mockRequest).queryParams("id");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleWithName() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("FakeProject0");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{projectrisikoDetail=[Projectrisk:{id: 1; risikoname: FakeName0; " +
										"ursache: FakeUrsache0; gefaehrdungsart: FakeGefaehrdung0; eintrittswahrscheinlichkeit: 0.000000; " +
										"auswirkungen: 0.000000; massnahmen: FakeMassnahmen0; verantwortlicher: User:{id: 1; username: FakeUser0; " +
										"name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; " +
										"abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; prioritaet: 0; " +
										"project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; " +
										"projektleiter: User:{id: 2; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; " +
										"role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; " +
										"telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; " +
										"projekstatus: FakeStatus0; projektbudget: 0.000000}}], postAction=/projektrisiko/update}") );
			
			verify(mockRequest).queryParams("id");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
