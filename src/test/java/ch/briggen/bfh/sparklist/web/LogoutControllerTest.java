package ch.briggen.bfh.sparklist.web;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class LogoutControllerTest {
	LogoutController controller = null;
	
	@Mock
	private Request mockRequest;
	@Mock
	private Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	@BeforeEach
	void setUp() throws Exception {
		controller = new LogoutController();
		response = RequestResponseFactory.create(mockHttpServletResponse);
	}
	
	@Test
	void test() {
		try
		{
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attributes()).thenReturn(new HashSet<>());
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).session();
			verify(mockRequest).session(true);
			verify(mockSession).attributes();
			verify(mockHttpServletResponse).sendRedirect("/");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
