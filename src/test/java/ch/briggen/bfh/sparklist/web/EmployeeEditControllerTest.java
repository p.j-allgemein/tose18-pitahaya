package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.RoleRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserProjectRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class EmployeeEditControllerTest {
	EmployeeEditController controller = null;
	UserRepository userRepo = null;
	ProjectRepository projectRepo = null;
	UserProjectRepository repo = null;
	RoleRepository roleRepo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	private Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
								  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			u.insert(dummy);
		}
	}
	
	private static void populateProjectRepo(ProjectRepository p) {
		UserRepository urep = new UserRepository();
		for(int i = 0; i < 10; ++i) {
			List<User> dummyUsers = new ArrayList<>(urep.getByName("FakeUser" + i * 100));
			Project dummy = new Project(i * 100, i * 100, "FakeProject" + i * 100, dummyUsers.get(0),
									    LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
									    "FakePhase" + i * 100, "FakeStatus" + i * 100, i * 100);
			p.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new EmployeeEditController();
		userRepo = new UserRepository();
		projectRepo = new ProjectRepository();
		repo = new UserProjectRepository();
		roleRepo = new RoleRepository();
		populateUserRepo(userRepo);
		populateProjectRepo(projectRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}
	
	@Test
	void testHandle() {
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn("1234");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("mitarbeiter_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{userList=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}], employeeDetail=User2Project:{user: null; project: null; role: null}, postAction=/mitarbeiter/new, roleList=[Role:{id: 1; rolename: Admin}, Role:{id: 2; rolename: Portfoliomanager}, Role:{id: 3; rolename: Projektleiter}, Role:{id: 4; rolename: Stakeholder}, Role:{id: 5; rolename: Projektmitarbeiter}], projectId=1234, session_user=username}") );
			
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
