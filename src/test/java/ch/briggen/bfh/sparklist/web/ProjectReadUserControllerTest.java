package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class ProjectReadUserControllerTest {
	ProjectReadUserController controller = null;
	UserRepository userRepo = null;
	ProjectRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateRepo(ProjectRepository p) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(i + 1,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummy = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter), 
										LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			
			p.insert(dummy);
		}
	}
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
								  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			u.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new ProjectReadUserController();
		userRepo = new UserRepository();
		repo = new ProjectRepository();
		populateUserRepo(userRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}
	
	@Test
	void testHandleUnknownUser() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("username")).thenReturn("DummyUser");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("auswertungen") );
			assertThat("Model", mav.getModel().toString(), is("{list=[], session_user=test}") );
			
			verify(mockRequest).queryParams("username");
			verifyZeroInteractions(mockHttpServletResponse);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());			
		}
	}
	
	@Test
	void testHandleKnownUser() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("username")).thenReturn("FakeUser200");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("auswertungen") );
			assertThat("Model", mav.getModel().toString(), is("{list=[Project:{id: 3; projektnummer: 2; projektname: FakeProject2; " +
															  "projektleiter: User:{id: 3; username: FakeUser200; name: FakeName200; " +
															  "vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; " +
															  "abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}; " +
															  "start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; " +
															  "projekstatus: FakeStatus2; projektbudget: 2.000000}], session_user=test}") );
			
			verify(mockRequest).queryParams("username");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
