package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class ProjectNewControllerTest {
	ProjectNewController controller = null;
	UserRepository userRepo = null;
	ProjectRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	private Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
								  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			u.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new ProjectNewController();
		userRepo = new UserRepository();
		repo = new ProjectRepository();
		populateUserRepo(userRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}

	@Test
	void testHandleNullProject() {
		try
		{
			when(mockRequest.queryParams("projectDetail.projektleiter")).thenReturn(null);
			when(mockRequest.queryParams("projectDetail.id")).thenReturn(null);
			when(mockRequest.queryParams("projectDetail.projektnummer")).thenReturn(null);
			when(mockRequest.queryParams("projectDetail.projektname")).thenReturn(null);
			when(mockRequest.queryParams("projectDetail.projektstart")).thenReturn(null);
			when(mockRequest.queryParams("projectDetail.projektende")).thenReturn(null);
			when(mockRequest.queryParams("projectDetail.projektphase")).thenReturn(null);
			when(mockRequest.queryParams("projectDetail.projektstatus")).thenReturn(null);
			when(mockRequest.queryParams("projectDetail.projektbudget")).thenReturn(null);
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projekt_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{phaseDetail=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], " +
															  "userDetail=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}], " +
															  "postAction=/projects/new, " +
															  "projectDetail=Project:{id: 0; projektnummer: 0; projektname: null; projektleiter: null; start: null; end: null; projektphase: null; projekstatus: null; projektbudget: 0.000000}, " +
															  "error=Projektleiter null wurde nicht gefunden, session_user=username}") );
			
			verify(mockRequest, times(2)).queryParams("projectDetail.projektleiter");
			verify(mockRequest).queryParams("projectDetail.id");
			verify(mockRequest).queryParams("projectDetail.projektnummer");
			verify(mockRequest).queryParams("projectDetail.projektname");
			verify(mockRequest).queryParams("projectDetail.projektstart");
			verify(mockRequest).queryParams("projectDetail.projektende");
			verify(mockRequest).queryParams("projectDetail.projektphase");
			verify(mockRequest).queryParams("projectDetail.projektstatus");
			verify(mockRequest).queryParams("projectDetail.projektbudget");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/projekte");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleEmptyProject() {
		try
		{
			when(mockRequest.queryParams("projectDetail.projektleiter")).thenReturn("");
			when(mockRequest.queryParams("projectDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("projectDetail.projektnummer")).thenReturn("1");
			when(mockRequest.queryParams("projectDetail.projektname")).thenReturn("");
			when(mockRequest.queryParams("projectDetail.projektstart")).thenReturn("");
			when(mockRequest.queryParams("projectDetail.projektende")).thenReturn("");
			when(mockRequest.queryParams("projectDetail.projektphase")).thenReturn("");
			when(mockRequest.queryParams("projectDetail.projektstatus")).thenReturn("");
			when(mockRequest.queryParams("projectDetail.projektbudget")).thenReturn("0.0");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projekt_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{phaseDetail=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], " +
															  "userDetail=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}], " +
															  "postAction=/projects/new, " +
															  "projectDetail=Project:{id: 0; projektnummer: 1; projektname: ; projektleiter: null; start: null; end: null; projektphase: null; projekstatus: ; projektbudget: 0.000000}, " +
															  "error=Projektleiter  wurde nicht gefunden, session_user=username}") );
			
			verify(mockRequest, times(2)).queryParams("projectDetail.projektleiter");
			verify(mockRequest, times(2)).queryParams("projectDetail.id");
			verify(mockRequest, times(2)).queryParams("projectDetail.projektnummer");
			verify(mockRequest).queryParams("projectDetail.projektname");
			verify(mockRequest, times(2)).queryParams("projectDetail.projektstart");
			verify(mockRequest, times(2)).queryParams("projectDetail.projektende");
			verify(mockRequest).queryParams("projectDetail.projektphase");
			verify(mockRequest).queryParams("projectDetail.projektstatus");
			verify(mockRequest, times(2)).queryParams("projectDetail.projektbudget");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/projekte");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleRealProject() {
		try
		{
			when(mockRequest.queryParams("projectDetail.projektleiter")).thenReturn("FakeUser100");
			when(mockRequest.queryParams("projectDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("projectDetail.projektnummer")).thenReturn("1");
			when(mockRequest.queryParams("projectDetail.projektname")).thenReturn("DummyName");
			when(mockRequest.queryParams("projectDetail.projektstart")).thenReturn("2018-03-01");
			when(mockRequest.queryParams("projectDetail.projektende")).thenReturn("2019-12-31");
			when(mockRequest.queryParams("projectDetail.projektphase")).thenReturn("Realisierung");
			when(mockRequest.queryParams("projectDetail.projektstatus")).thenReturn("DummyStatus");
			when(mockRequest.queryParams("projectDetail.projektbudget")).thenReturn("11.11");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("projectDetail.projektleiter");
			verify(mockRequest, times(2)).queryParams("projectDetail.id");
			verify(mockRequest, times(2)).queryParams("projectDetail.projektnummer");
			verify(mockRequest).queryParams("projectDetail.projektname");
			verify(mockRequest, times(3)).queryParams("projectDetail.projektstart");
			verify(mockRequest, times(3)).queryParams("projectDetail.projektende");
			verify(mockRequest).queryParams("projectDetail.projektphase");
			verify(mockRequest).queryParams("projectDetail.projektstatus");
			verify(mockRequest, times(2)).queryParams("projectDetail.projektbudget");
			verify(mockRequest, never()).session();
			verify(mockSession, never()).attribute("USERNAME");
			verify(mockHttpServletResponse).sendRedirect("/projekte");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleNumericProjektleiter() {
		try
		{
			when(mockRequest.queryParams("projectDetail.projektleiter")).thenReturn("1");
			when(mockRequest.queryParams("projectDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("projectDetail.projektnummer")).thenReturn("1");
			when(mockRequest.queryParams("projectDetail.projektname")).thenReturn("DummyName");
			when(mockRequest.queryParams("projectDetail.projektstart")).thenReturn("2018-03-01");
			when(mockRequest.queryParams("projectDetail.projektende")).thenReturn("2019-12-31");
			when(mockRequest.queryParams("projectDetail.projektphase")).thenReturn("Konzeption");
			when(mockRequest.queryParams("projectDetail.projektstatus")).thenReturn("DummyStatus");
			when(mockRequest.queryParams("projectDetail.projektbudget")).thenReturn("11.11");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("projectDetail.projektleiter");
			verify(mockRequest, times(2)).queryParams("projectDetail.id");
			verify(mockRequest, times(2)).queryParams("projectDetail.projektnummer");
			verify(mockRequest).queryParams("projectDetail.projektname");
			verify(mockRequest, times(3)).queryParams("projectDetail.projektstart");
			verify(mockRequest, times(3)).queryParams("projectDetail.projektende");
			verify(mockRequest).queryParams("projectDetail.projektphase");
			verify(mockRequest).queryParams("projectDetail.projektstatus");
			verify(mockRequest, times(2)).queryParams("projectDetail.projektbudget");
			verify(mockRequest, never()).session();
			verify(mockSession, never()).attribute("USERNAME");
			verify(mockHttpServletResponse).sendRedirect("/projekte");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleProjectnameExists() {
		try
		{
			User user = userRepo.getById(1);
			Project dummy = new Project(0, 5, "FakeProject", user, LocalDate.parse("2018.03.01", formatter), 
					LocalDate.parse("2019.12.31", formatter), "FakePhase", "FakeStatus", 5);

			repo.insert(dummy);
			
			when(mockRequest.queryParams("projectDetail.projektleiter")).thenReturn("1");
			when(mockRequest.queryParams("projectDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("projectDetail.projektnummer")).thenReturn("1");
			when(mockRequest.queryParams("projectDetail.projektname")).thenReturn("FakeProject");
			when(mockRequest.queryParams("projectDetail.projektstart")).thenReturn("2018-03-01");
			when(mockRequest.queryParams("projectDetail.projektende")).thenReturn("2019-12-31");
			when(mockRequest.queryParams("projectDetail.projektphase")).thenReturn("Realisierung");
			when(mockRequest.queryParams("projectDetail.projektstatus")).thenReturn("DummyStatus");
			when(mockRequest.queryParams("projectDetail.projektbudget")).thenReturn("11.11");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projekt_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{phaseDetail=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], " +
															  "userDetail=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, User:{id: 3; username: FakeUser200; name: FakeName200; vorname: FakeVorname200; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung200; mail: FakeMail200; telefon: FakeTel200}, User:{id: 4; username: FakeUser300; name: FakeName300; vorname: FakeVorname300; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung300; mail: FakeMail300; telefon: FakeTel300}, User:{id: 5; username: FakeUser400; name: FakeName400; vorname: FakeVorname400; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung400; mail: FakeMail400; telefon: FakeTel400}, User:{id: 6; username: FakeUser500; name: FakeName500; vorname: FakeVorname500; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung500; mail: FakeMail500; telefon: FakeTel500}, User:{id: 7; username: FakeUser600; name: FakeName600; vorname: FakeVorname600; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung600; mail: FakeMail600; telefon: FakeTel600}, User:{id: 8; username: FakeUser700; name: FakeName700; vorname: FakeVorname700; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung700; mail: FakeMail700; telefon: FakeTel700}, User:{id: 9; username: FakeUser800; name: FakeName800; vorname: FakeVorname800; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung800; mail: FakeMail800; telefon: FakeTel800}, User:{id: 10; username: FakeUser900; name: FakeName900; vorname: FakeVorname900; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung900; mail: FakeMail900; telefon: FakeTel900}], " +
															  "postAction=/projects/new, " +
															  "projectDetail=Project:{id: 0; projektnummer: 1; projektname: FakeProject; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: Realisierung; projekstatus: DummyStatus; projektbudget: 11.110000}, " +
															  "error=Projektname existiert bereits in der Datenbank, session_user=username}") );
			
			verify(mockRequest).queryParams("projectDetail.projektleiter");
			verify(mockRequest, times(2)).queryParams("projectDetail.id");
			verify(mockRequest, times(2)).queryParams("projectDetail.projektnummer");
			verify(mockRequest).queryParams("projectDetail.projektname");
			verify(mockRequest, times(3)).queryParams("projectDetail.projektstart");
			verify(mockRequest, times(3)).queryParams("projectDetail.projektende");
			verify(mockRequest).queryParams("projectDetail.projektphase");
			verify(mockRequest).queryParams("projectDetail.projektstatus");
			verify(mockRequest, times(2)).queryParams("projectDetail.projektbudget");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/projekte");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
