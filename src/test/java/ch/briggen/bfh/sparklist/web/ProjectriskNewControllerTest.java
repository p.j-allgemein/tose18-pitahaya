package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.ProjectriskRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class ProjectriskNewControllerTest {
	ProjectriskNewController controller = null;
	UserRepository userRepo = null;
	ProjectRepository projectRepo = null;
	ProjectriskRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	private Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateProjectRepo(ProjectRepository p) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(i + 1,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummy = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter), 
										LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			
			p.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new ProjectriskNewController();
		userRepo = new UserRepository();
		projectRepo = new ProjectRepository();
		repo = new ProjectriskRepository();
		populateProjectRepo(projectRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projectrisks");
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}

	@Test
	void testHandleNullProjectrisk() {
		try
		{
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			when(mockRequest.queryParams("projectrisikoDetail.verantwortlicher")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.projektname")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.id")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.risikoname")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.ursache")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.gefaehrdungsart")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.auswirkungen")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.massnahmen")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.prioritaet")).thenReturn(null);
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektrisiko") );
			assertThat("Model", mav.getModel().toString(), is("{projektname=null, " +
					"userDetail=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, User:{id: 3; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, User:{id: 4; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, User:{id: 5; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, User:{id: 6; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}, User:{id: 7; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}, User:{id: 8; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}, User:{id: 9; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}, User:{id: 10; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}], " +
					"projectrisikoDetail=Projectrisk:{id: 0; risikoname: null; ursache: null; gefaehrdungsart: null; eintrittswahrscheinlichkeit: 0.000000; auswirkungen: 0.000000; massnahmen: null; verantwortlicher: null; prioritaet: 0; project: null}, " +
					"error=Verantwortlicher null wurde nicht gefunden, session_user=username}") );
			
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.verantwortlicher");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.projektname");
			verify(mockRequest).queryParams("projectrisikoDetail.id");
			verify(mockRequest).queryParams("projectrisikoDetail.risikoname");
			verify(mockRequest).queryParams("projectrisikoDetail.ursache");
			verify(mockRequest).queryParams("projectrisikoDetail.gefaehrdungsart");
			verify(mockRequest).queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit");
			verify(mockRequest).queryParams("projectrisikoDetail.auswirkungen");
			verify(mockRequest).queryParams("projectrisikoDetail.massnahmen");
			verify(mockRequest).queryParams("projectrisikoDetail.prioritaet");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/projektrisiko");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleNullProjectriskNullProject() {
		try
		{
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			when(mockRequest.queryParams("projectrisikoDetail.verantwortlicher")).thenReturn("1");
			when(mockRequest.queryParams("projectrisikoDetail.projektname")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.id")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.risikoname")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.ursache")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.gefaehrdungsart")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.auswirkungen")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.massnahmen")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.prioritaet")).thenReturn(null);
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektrisiko") );
			assertThat("Model", mav.getModel().toString(), is("{projektname=null, " +
					"userDetail=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, User:{id: 3; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, User:{id: 4; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, User:{id: 5; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, User:{id: 6; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}, User:{id: 7; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}, User:{id: 8; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}, User:{id: 9; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}, User:{id: 10; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}], " +
					"projectrisikoDetail=Projectrisk:{id: 0; risikoname: null; ursache: null; gefaehrdungsart: null; eintrittswahrscheinlichkeit: 0.000000; auswirkungen: 0.000000; massnahmen: null; verantwortlicher: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; prioritaet: 0; project: null}, " +
					"error=Projekt null wurde nicht gefunden, session_user=username}") );
			
			verify(mockRequest).queryParams("projectrisikoDetail.verantwortlicher");
			verify(mockRequest, times(3)).queryParams("projectrisikoDetail.projektname");
			verify(mockRequest).queryParams("projectrisikoDetail.id");
			verify(mockRequest).queryParams("projectrisikoDetail.risikoname");
			verify(mockRequest).queryParams("projectrisikoDetail.ursache");
			verify(mockRequest).queryParams("projectrisikoDetail.gefaehrdungsart");
			verify(mockRequest).queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit");
			verify(mockRequest).queryParams("projectrisikoDetail.auswirkungen");
			verify(mockRequest).queryParams("projectrisikoDetail.massnahmen");
			verify(mockRequest).queryParams("projectrisikoDetail.prioritaet");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/projektrisiko");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleNullProjectriskWithVerantwortlicherAndProject() {
		try
		{
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			when(mockRequest.queryParams("projectrisikoDetail.verantwortlicher")).thenReturn("1");
			when(mockRequest.queryParams("projectrisikoDetail.projektname")).thenReturn("1");
			when(mockRequest.queryParams("projectrisikoDetail.id")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.risikoname")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.ursache")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.gefaehrdungsart")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.auswirkungen")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.massnahmen")).thenReturn(null);
			when(mockRequest.queryParams("projectrisikoDetail.prioritaet")).thenReturn(null);
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektrisiko") );
			assertThat("Model", mav.getModel().toString(), is("{projektname=1, " +
					"userDetail=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, User:{id: 3; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, User:{id: 4; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, User:{id: 5; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, User:{id: 6; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}, User:{id: 7; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}, User:{id: 8; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}, User:{id: 9; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}, User:{id: 10; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}], " +
					"projectrisikoDetail=Projectrisk:{id: 0; risikoname: null; ursache: null; gefaehrdungsart: null; eintrittswahrscheinlichkeit: 0.000000; auswirkungen: 0.000000; massnahmen: null; verantwortlicher: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; prioritaet: 0; project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}}, " +
					"error=Risikoname fehlt, session_user=username}") );
			
			verify(mockRequest).queryParams("projectrisikoDetail.verantwortlicher");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.projektname");
			verify(mockRequest).queryParams("projectrisikoDetail.id");
			verify(mockRequest).queryParams("projectrisikoDetail.risikoname");
			verify(mockRequest).queryParams("projectrisikoDetail.ursache");
			verify(mockRequest).queryParams("projectrisikoDetail.gefaehrdungsart");
			verify(mockRequest).queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit");
			verify(mockRequest).queryParams("projectrisikoDetail.auswirkungen");
			verify(mockRequest).queryParams("projectrisikoDetail.massnahmen");
			verify(mockRequest).queryParams("projectrisikoDetail.prioritaet");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/projektrisiko");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleEmptyProjectrisk() {
		try
		{
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			when(mockRequest.queryParams("projectrisikoDetail.verantwortlicher")).thenReturn("1");
			when(mockRequest.queryParams("projectrisikoDetail.projektname")).thenReturn("1");
			when(mockRequest.queryParams("projectrisikoDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("projectrisikoDetail.risikoname")).thenReturn("");
			when(mockRequest.queryParams("projectrisikoDetail.ursache")).thenReturn("");
			when(mockRequest.queryParams("projectrisikoDetail.gefaehrdungsart")).thenReturn("");
			when(mockRequest.queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit")).thenReturn("0.0");
			when(mockRequest.queryParams("projectrisikoDetail.auswirkungen")).thenReturn("0.0");
			when(mockRequest.queryParams("projectrisikoDetail.massnahmen")).thenReturn("");
			when(mockRequest.queryParams("projectrisikoDetail.prioritaet")).thenReturn("0");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektrisiko") );
			assertThat("Model", mav.getModel().toString(), is("{projektname=1, " +
					"userDetail=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, User:{id: 2; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, User:{id: 3; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, User:{id: 4; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, User:{id: 5; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, User:{id: 6; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}, User:{id: 7; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}, User:{id: 8; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}, User:{id: 9; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}, User:{id: 10; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}], " +
					"projectrisikoDetail=Projectrisk:{id: 0; risikoname: ; ursache: ; gefaehrdungsart: ; eintrittswahrscheinlichkeit: 0.000000; auswirkungen: 0.000000; massnahmen: ; verantwortlicher: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; prioritaet: 0; project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}}, " +
					"error=Risikoname fehlt, session_user=username}") );
			
			verify(mockRequest).queryParams("projectrisikoDetail.verantwortlicher");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.projektname");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.id");
			verify(mockRequest).queryParams("projectrisikoDetail.risikoname");
			verify(mockRequest).queryParams("projectrisikoDetail.ursache");
			verify(mockRequest).queryParams("projectrisikoDetail.gefaehrdungsart");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.auswirkungen");
			verify(mockRequest).queryParams("projectrisikoDetail.massnahmen");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.prioritaet");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/projektrisiko");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleProjectriskInvalidAuswirkungen() {
		ModelAndView mav = null;
		try
		{
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			when(mockRequest.queryParams("projectrisikoDetail.verantwortlicher")).thenReturn("1");
			when(mockRequest.queryParams("projectrisikoDetail.projektname")).thenReturn("1");
			when(mockRequest.queryParams("projectrisikoDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("projectrisikoDetail.risikoname")).thenReturn("");
			when(mockRequest.queryParams("projectrisikoDetail.ursache")).thenReturn("");
			when(mockRequest.queryParams("projectrisikoDetail.gefaehrdungsart")).thenReturn("");
			when(mockRequest.queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit")).thenReturn("0.0");
			when(mockRequest.queryParams("projectrisikoDetail.auswirkungen")).thenReturn("A");
			when(mockRequest.queryParams("projectrisikoDetail.massnahmen")).thenReturn("");
			when(mockRequest.queryParams("projectrisikoDetail.prioritaet")).thenReturn("0");
			
			mav = controller.handle(mockRequest, response);
			fail("Expected Exception ");		
		}
		catch (Exception e)
		{
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("projectrisikoDetail.verantwortlicher");
			verify(mockRequest).queryParams("projectrisikoDetail.projektname");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.id");
			verify(mockRequest, never()).queryParams("projectrisikoDetail.risikoname");
			verify(mockRequest, never()).queryParams("projectrisikoDetail.ursache");
			verify(mockRequest, never()).queryParams("projectrisikoDetail.gefaehrdungsart");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.auswirkungen");
			verify(mockRequest, never()).queryParams("projectrisikoDetail.massnahmen");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.prioritaet");
			verify(mockRequest, never()).session();
			verify(mockSession, never()).attribute("USERNAME");
			try
			{
				verify(mockHttpServletResponse, never()).sendRedirect("/projektrisiko");	
			}
			catch (IOException ex)
			{
				fail("unexpected IOException ");	
			}
		}
	}
	
	@Test
	void testHandleRealProjectrisk() {
		try
		{
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			when(mockRequest.queryParams("projectrisikoDetail.verantwortlicher")).thenReturn("1");
			when(mockRequest.queryParams("projectrisikoDetail.projektname")).thenReturn("1");
			when(mockRequest.queryParams("projectrisikoDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("projectrisikoDetail.risikoname")).thenReturn("DummyRisiko");
			when(mockRequest.queryParams("projectrisikoDetail.ursache")).thenReturn("DummyRisiko");
			when(mockRequest.queryParams("projectrisikoDetail.gefaehrdungsart")).thenReturn("DummyRisiko");
			when(mockRequest.queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit")).thenReturn("55.00");
			when(mockRequest.queryParams("projectrisikoDetail.auswirkungen")).thenReturn("45.00");
			when(mockRequest.queryParams("projectrisikoDetail.massnahmen")).thenReturn("DummyRisiko");
			when(mockRequest.queryParams("projectrisikoDetail.prioritaet")).thenReturn("1");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("projectrisikoDetail.verantwortlicher");
			verify(mockRequest).queryParams("projectrisikoDetail.projektname");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.id");
			verify(mockRequest).queryParams("projectrisikoDetail.risikoname");
			verify(mockRequest).queryParams("projectrisikoDetail.ursache");
			verify(mockRequest).queryParams("projectrisikoDetail.gefaehrdungsart");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.auswirkungen");
			verify(mockRequest).queryParams("projectrisikoDetail.massnahmen");
			verify(mockRequest, times(2)).queryParams("projectrisikoDetail.prioritaet");
			verify(mockRequest, never()).session();
			verify(mockSession, never()).attribute("USERNAME");
			verify(mockHttpServletResponse).sendRedirect("/projektsteckbrief?id=1");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
