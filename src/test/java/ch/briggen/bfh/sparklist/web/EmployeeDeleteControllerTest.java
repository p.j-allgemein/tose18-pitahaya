package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.RoleRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.User2Project;
import ch.briggen.bfh.sparklist.domain.UserProjectRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class EmployeeDeleteControllerTest {
	EmployeeDeleteController controller = null;
	UserRepository userRepo = null;
	ProjectRepository projectRepo = null;
	UserProjectRepository repo = null;
	RoleRepository roleRepo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;

	private static void populateRepo(UserProjectRepository up, UserRepository u, ProjectRepository p, RoleRepository r) {
		RoleRepository rrep = new RoleRepository();
		for(int i = 0; i < 10; ++i) {
			Role dummyRole = rrep.getById((i % 5) + 1);
			User dummyUser = new User(i + 200,"FakeUser" + (i + 200), "FakeName" + (i + 200), "FakeVorname" + (i + 200), dummyRole,
								      "FakeAbteilung" + (i + 200), "FakeMail" + (i + 200), "FakeTel" + (i + 200));
			dummyUser.setId(u.insert(dummyUser));
			Project dummyProject = new Project(i + 1, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			dummyProject.setId(p.insert(dummyProject));
			User2Project dummy = new User2Project(dummyUser, dummyProject, dummyRole);
			
			up.insert(dummy);
		}
	}
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(0,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
								  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			u.insert(dummy);
		}
	}
	
	private static void populateProjectRepo(ProjectRepository p) {
		UserRepository urep = new UserRepository();
		for(int i = 0; i < 10; ++i) {
			List<User> dummyUsers = new ArrayList<>(urep.getByName("FakeUser" + i * 100));
			Project dummy = new Project(0, i * 100, "FakeProject" + i * 100, dummyUsers.get(0),
									    LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
									    "FakePhase" + i * 100, "FakeStatus" + i * 100, i * 100);
			p.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new EmployeeDeleteController();
		userRepo = new UserRepository();
		projectRepo = new ProjectRepository();
		repo = new UserProjectRepository();
		roleRepo = new RoleRepository();
		populateUserRepo(userRepo);
		populateProjectRepo(projectRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.insert(new User2Project(userRepo.getById(1), projectRepo.getById(1), null));
		repo.getAll().stream().forEach(e -> repo.delete(e.getUser().getId(), e.getProject().getId(), (e.getRole() != null ? e.getRole().getId() : 0)));
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}
	
	@Test
	void testHandleEmployeeNullValues() {
		ModelAndView mav = null;
		try
		{
			populateRepo(repo, userRepo, projectRepo, roleRepo);
			when(mockRequest.queryParams("employeeDetail.roleId")).thenReturn(null);
			when(mockRequest.queryParams("employeeDetail.userId")).thenReturn(null);
			when(mockRequest.queryParams("employeeDetail.projectId")).thenReturn(null);
			
			mav = controller.handle(mockRequest, response);
			fail("Expected Exception");
		}
		catch (Exception e)
		{
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("employeeDetail.roleId");
			verify(mockRequest).queryParams("employeeDetail.userId");
			verify(mockRequest).queryParams("employeeDetail.projectId");
			try
			{
				verify(mockHttpServletResponse, never()).sendRedirect("/projektsteckbrief");
			}
			catch (IOException ex)
			{
				fail("unexpected IOException " + ex.getMessage());
			}
		}
	}
	
	@Test
	void testHandleEmployeeEmptyValues() {
		ModelAndView mav = null;
		try
		{
			populateRepo(repo, userRepo, projectRepo, roleRepo);
			when(mockRequest.queryParams("employeeDetail.roleId")).thenReturn("");
			when(mockRequest.queryParams("employeeDetail.userId")).thenReturn("");
			when(mockRequest.queryParams("employeeDetail.projectId")).thenReturn("");
			
			mav = controller.handle(mockRequest, response);
			fail("Expected Exception");
		}
		catch (Exception e)
		{
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("employeeDetail.roleId");
			verify(mockRequest).queryParams("employeeDetail.userId");
			verify(mockRequest).queryParams("employeeDetail.projectId");
			try
			{
				verify(mockHttpServletResponse, never()).sendRedirect("/projektsteckbrief");
			}
			catch (IOException ex)
			{
				fail("unexpected IOException " + ex.getMessage());
			}
		}
	}
	
	@Test
	void testHandleEmployeeAlphabeticValues() {
		ModelAndView mav = null;
		try
		{
			populateRepo(repo, userRepo, projectRepo, roleRepo);
			when(mockRequest.queryParams("employeeDetail.roleId")).thenReturn("A");
			when(mockRequest.queryParams("employeeDetail.userId")).thenReturn("A");
			when(mockRequest.queryParams("employeeDetail.projectId")).thenReturn("A");
			
			mav = controller.handle(mockRequest, response);
			fail("Expected Exception");
		}
		catch (Exception e)
		{
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("employeeDetail.roleId");
			verify(mockRequest).queryParams("employeeDetail.userId");
			verify(mockRequest).queryParams("employeeDetail.projectId");
			try
			{
				verify(mockHttpServletResponse, never()).sendRedirect("/projektsteckbrief");
			}
			catch (IOException ex)
			{
				fail("unexpected IOException " + ex.getMessage());
			}
		}
	}
	
	@Test
	void testHandleEmployee() {
		ModelAndView mav = null;
		try
		{
			populateRepo(repo, userRepo, projectRepo, roleRepo);
			when(mockRequest.queryParams("employeeDetail.roleId")).thenReturn("4");
			when(mockRequest.queryParams("employeeDetail.userId")).thenReturn("14");
			when(mockRequest.queryParams("employeeDetail.projectId")).thenReturn("14");
			
			mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			assertThat("Remain 9 Elements", repo.getAll().size(), is(9) );
			
			verify(mockRequest).queryParams("employeeDetail.roleId");
			verify(mockRequest).queryParams("employeeDetail.userId");
			verify(mockRequest).queryParams("employeeDetail.projectId");
			verify(mockHttpServletResponse).sendRedirect("/projektsteckbrief");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleEmployeeNullRole() {
		ModelAndView mav = null;
		try
		{
			populateRepo(repo, userRepo, projectRepo, roleRepo);
			repo.insert(new User2Project(userRepo.getById(14), projectRepo.getById(15), null));
			when(mockRequest.queryParams("employeeDetail.roleId")).thenReturn(null);
			when(mockRequest.queryParams("employeeDetail.userId")).thenReturn("14");
			when(mockRequest.queryParams("employeeDetail.projectId")).thenReturn("15");
			
			mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			assertThat("Remain 10 Elements", repo.getAll().size(), is(10) );
			
			verify(mockRequest).queryParams("employeeDetail.roleId");
			verify(mockRequest).queryParams("employeeDetail.userId");
			verify(mockRequest).queryParams("employeeDetail.projectId");
			verify(mockHttpServletResponse).sendRedirect("/projektsteckbrief");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleEmployeeEmptyRole() {
		ModelAndView mav = null;
		try
		{
			populateRepo(repo, userRepo, projectRepo, roleRepo);
			repo.insert(new User2Project(userRepo.getById(14), projectRepo.getById(15), null));
			when(mockRequest.queryParams("employeeDetail.roleId")).thenReturn("");
			when(mockRequest.queryParams("employeeDetail.userId")).thenReturn("14");
			when(mockRequest.queryParams("employeeDetail.projectId")).thenReturn("15");
			
			mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			assertThat("Remain 10 Elements", repo.getAll().size(), is(10) );
			
			verify(mockRequest).queryParams("employeeDetail.roleId");
			verify(mockRequest).queryParams("employeeDetail.userId");
			verify(mockRequest).queryParams("employeeDetail.projectId");
			verify(mockHttpServletResponse).sendRedirect("/projektsteckbrief");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleEmployeeRoleZero() {
		ModelAndView mav = null;
		try
		{
			populateRepo(repo, userRepo, projectRepo, roleRepo);
			repo.insert(new User2Project(userRepo.getById(14), projectRepo.getById(15), null));
			when(mockRequest.queryParams("employeeDetail.roleId")).thenReturn("0");
			when(mockRequest.queryParams("employeeDetail.userId")).thenReturn("14");
			when(mockRequest.queryParams("employeeDetail.projectId")).thenReturn("15");
			
			mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			assertThat("Remain 10 Elements", repo.getAll().size(), is(10) );
			
			verify(mockRequest).queryParams("employeeDetail.roleId");
			verify(mockRequest).queryParams("employeeDetail.userId");
			verify(mockRequest).queryParams("employeeDetail.projectId");
			verify(mockHttpServletResponse).sendRedirect("/projektsteckbrief");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unexpected Exception " + e.getMessage());
		}
	}
}
