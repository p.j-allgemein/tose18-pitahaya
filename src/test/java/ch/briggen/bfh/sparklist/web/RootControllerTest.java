package ch.briggen.bfh.sparklist.web;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class RootControllerTest {
	RootController controller = null;
	UserRepository repo = null;
	
	@Mock
	private Request mockRequest;
	@Mock
	private Session mockSession;
	private Response response;
	
	@BeforeEach
	void setUp() throws Exception {
		controller = new RootController();
	}
	
	@Test
	void test() {
		try
		{
			when(mockRequest.session()).thenReturn(mockSession);
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("startseite") );
			assertThat("Model", mav.getModel().toString(), is("{}") );
			
			verify(mockRequest).session(true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
