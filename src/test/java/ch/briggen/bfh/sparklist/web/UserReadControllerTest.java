package ch.briggen.bfh.sparklist.web;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
class UserReadControllerTest {
	UserReadController controller = null;
	UserRepository repo = null;
	
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		controller = new UserReadController();
		repo = new UserRepository();
		response = RequestResponseFactory.create(mockHttpServletResponse);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Users");
		resetDB("Roles");
	}
	
	private static void populateRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(1, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			u.insert(dummy);
		}
	}

	@Test
	void testHandleNothing() {
		try
		{
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("profil") );
			assertThat("Model", mav.getModel().toString(), is("{list=[], session_user=test}") );
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandle() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("profil") );
			assertThat("Model", mav.getModel().toString(), is("{list=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
															  "User:{id: 2; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, " +
															  "User:{id: 3; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, " +
															  "User:{id: 4; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, " +
															  "User:{id: 5; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, " +
															  "User:{id: 6; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}, " +
															  "User:{id: 7; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}, " +
															  "User:{id: 8; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}, " +
															  "User:{id: 9; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}, " +
															  "User:{id: 10; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}], " +
															  "session_user=test}") );
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
