package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class UserReadDetailControllerTest {
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	UserReadDetailController controller = null;
	UserRepository repo = null;
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
								  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			u.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		controller = new UserReadDetailController();
		repo = new UserRepository();
		populateUserRepo(repo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}
	
	@Test
	void testHandleNothing() {
		ModelAndView mav = null;
		try
		{
			when(mockRequest.queryParams("id")).thenReturn(null);
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			mav = controller.handle(mockRequest, response);
			fail("expected Exception");
		}
		catch (Exception e)
		{
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
	}
	
	@Test
	void testHandleWithZeroId() {
		ModelAndView mav = null;
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("0");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			mav = controller.handle(mockRequest, response);
			fail("expected Exception");
		}
		catch (Exception e)
		{
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
	}
	
	@Test
	void testHandleWithZeroIdValidUsername() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("0");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("FakeUser100");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("profil") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=User:{id: 2; username: FakeUser100; name: FakeName100; vorname: FakeVorname100; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung100; mail: FakeMail100; telefon: FakeTel100}, postAction=users/update, session_user=FakeUser100}") );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandle() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("1");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("profil") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, postAction=users/update, session_user=username}") );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
