package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Projectphase;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.Workpackage;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class WorkpackageEditControllerTest {
	WorkpackageEditController controller = null;
	ProjectRepository projectRepo = null;
	ProjectphaseRepository phaseRepo = null;
	WorkpackageRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateRepo(WorkpackageRepository w) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								      "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummyProject = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			Workpackage dummy = new Workpackage(0, dummyProject, new Projectphase(3, "FakePhase" + i), "FakeWP" + i, i, i,
												LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
												false);
			
			w.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new WorkpackageEditController();
		repo = new WorkpackageRepository();
		projectRepo = new ProjectRepository();
		phaseRepo = new ProjectphaseRepository();
		populateRepo(repo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}
	
	@Test
	void testHandleUnknownWorkpackage() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("1234");
			when(mockRequest.queryParams("projectId")).thenReturn("1234");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("workpackage_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{projectList=[Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}, " + 
					"Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 2; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}, " + 
					"Project:{id: 3; projektnummer: 2; projektname: FakeProject2; projektleiter: User:{id: 3; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; projekstatus: FakeStatus2; projektbudget: 2.000000}, " + 
					"Project:{id: 4; projektnummer: 3; projektname: FakeProject3; projektleiter: User:{id: 4; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase3; projekstatus: FakeStatus3; projektbudget: 3.000000}, " + 
					"Project:{id: 5; projektnummer: 4; projektname: FakeProject4; projektleiter: User:{id: 5; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase4; projekstatus: FakeStatus4; projektbudget: 4.000000}, " + 
					"Project:{id: 6; projektnummer: 5; projektname: FakeProject5; projektleiter: User:{id: 6; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase5; projekstatus: FakeStatus5; projektbudget: 5.000000}, " + 
					"Project:{id: 7; projektnummer: 6; projektname: FakeProject6; projektleiter: User:{id: 7; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase6; projekstatus: FakeStatus6; projektbudget: 6.000000}, " + 
					"Project:{id: 8; projektnummer: 7; projektname: FakeProject7; projektleiter: User:{id: 8; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase7; projekstatus: FakeStatus7; projektbudget: 7.000000}, " + 
					"Project:{id: 9; projektnummer: 8; projektname: FakeProject8; projektleiter: User:{id: 9; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase8; projekstatus: FakeStatus8; projektbudget: 8.000000}, " + 
					"Project:{id: 10; projektnummer: 9; projektname: FakeProject9; projektleiter: User:{id: 10; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase9; projekstatus: FakeStatus9; projektbudget: 9.000000}], " + 
					"phaseDetail=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], doneList=[Done:{done: false}, Done:{done: true}], " + 
					"workpackageDetail=null, postAction=/workpackage/update, projectId=1234, session_user=username}") );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).queryParams("projectId");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleKnownWorkpackage() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("1");
			when(mockRequest.queryParams("projectId")).thenReturn("1");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("workpackage_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{projectList=[Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}, " + 
					"Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 2; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}, " + 
					"Project:{id: 3; projektnummer: 2; projektname: FakeProject2; projektleiter: User:{id: 3; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; projekstatus: FakeStatus2; projektbudget: 2.000000}, " + 
					"Project:{id: 4; projektnummer: 3; projektname: FakeProject3; projektleiter: User:{id: 4; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase3; projekstatus: FakeStatus3; projektbudget: 3.000000}, " + 
					"Project:{id: 5; projektnummer: 4; projektname: FakeProject4; projektleiter: User:{id: 5; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase4; projekstatus: FakeStatus4; projektbudget: 4.000000}, " + 
					"Project:{id: 6; projektnummer: 5; projektname: FakeProject5; projektleiter: User:{id: 6; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase5; projekstatus: FakeStatus5; projektbudget: 5.000000}, " + 
					"Project:{id: 7; projektnummer: 6; projektname: FakeProject6; projektleiter: User:{id: 7; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase6; projekstatus: FakeStatus6; projektbudget: 6.000000}, " + 
					"Project:{id: 8; projektnummer: 7; projektname: FakeProject7; projektleiter: User:{id: 8; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase7; projekstatus: FakeStatus7; projektbudget: 7.000000}, " + 
					"Project:{id: 9; projektnummer: 8; projektname: FakeProject8; projektleiter: User:{id: 9; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase8; projekstatus: FakeStatus8; projektbudget: 8.000000}, " + 
					"Project:{id: 10; projektnummer: 9; projektname: FakeProject9; projektleiter: User:{id: 10; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase9; projekstatus: FakeStatus9; projektbudget: 9.000000}], " + 
					"phaseDetail=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], doneList=[Done:{done: false}, Done:{done: true}], " + 
					"workpackageDetail=Workpackage:{id: 1; project: Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}; projectphase: Projectphase:{id: 3; phasenname: Realisierung}; workpackagename: FakeWP0; plannedValue: 0.000000; earnedValue: 0.000000; startdate: 2018.03.01; enddate: 2019.12.31; done: false}, " + 
					"postAction=/workpackage/update, projectId=1, session_user=username}") );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).queryParams("projectId");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleIdNull() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn(null);
			when(mockRequest.queryParams("projectId")).thenReturn(null);
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("workpackage_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{projectList=[Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}, " + 
					"Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 2; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}, " + 
					"Project:{id: 3; projektnummer: 2; projektname: FakeProject2; projektleiter: User:{id: 3; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; projekstatus: FakeStatus2; projektbudget: 2.000000}, " + 
					"Project:{id: 4; projektnummer: 3; projektname: FakeProject3; projektleiter: User:{id: 4; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase3; projekstatus: FakeStatus3; projektbudget: 3.000000}, " + 
					"Project:{id: 5; projektnummer: 4; projektname: FakeProject4; projektleiter: User:{id: 5; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase4; projekstatus: FakeStatus4; projektbudget: 4.000000}, " + 
					"Project:{id: 6; projektnummer: 5; projektname: FakeProject5; projektleiter: User:{id: 6; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase5; projekstatus: FakeStatus5; projektbudget: 5.000000}, " + 
					"Project:{id: 7; projektnummer: 6; projektname: FakeProject6; projektleiter: User:{id: 7; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase6; projekstatus: FakeStatus6; projektbudget: 6.000000}, " + 
					"Project:{id: 8; projektnummer: 7; projektname: FakeProject7; projektleiter: User:{id: 8; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase7; projekstatus: FakeStatus7; projektbudget: 7.000000}, " + 
					"Project:{id: 9; projektnummer: 8; projektname: FakeProject8; projektleiter: User:{id: 9; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase8; projekstatus: FakeStatus8; projektbudget: 8.000000}, " + 
					"Project:{id: 10; projektnummer: 9; projektname: FakeProject9; projektleiter: User:{id: 10; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase9; projekstatus: FakeStatus9; projektbudget: 9.000000}], " + 
					"phaseDetail=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], doneList=[Done:{done: false}, Done:{done: true}], " + 
					"workpackageDetail=Workpackage:{id: 0; project: null; projectphase: null; workpackagename: null; plannedValue: 0.000000; earnedValue: 0.000000; startdate: null; enddate: null; done: false}, " + 
					"postAction=/workpackage/new, projectId=null, session_user=username}") );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).queryParams("projectId");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleIdZero() {
		try
		{
			when(mockRequest.queryParams("id")).thenReturn("0");
			when(mockRequest.queryParams("projectId")).thenReturn("0");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("workpackage_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{projectList=[Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}, " + 
					"Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 2; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}, " + 
					"Project:{id: 3; projektnummer: 2; projektname: FakeProject2; projektleiter: User:{id: 3; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; projekstatus: FakeStatus2; projektbudget: 2.000000}, " + 
					"Project:{id: 4; projektnummer: 3; projektname: FakeProject3; projektleiter: User:{id: 4; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase3; projekstatus: FakeStatus3; projektbudget: 3.000000}, " + 
					"Project:{id: 5; projektnummer: 4; projektname: FakeProject4; projektleiter: User:{id: 5; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase4; projekstatus: FakeStatus4; projektbudget: 4.000000}, " + 
					"Project:{id: 6; projektnummer: 5; projektname: FakeProject5; projektleiter: User:{id: 6; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase5; projekstatus: FakeStatus5; projektbudget: 5.000000}, " + 
					"Project:{id: 7; projektnummer: 6; projektname: FakeProject6; projektleiter: User:{id: 7; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase6; projekstatus: FakeStatus6; projektbudget: 6.000000}, " + 
					"Project:{id: 8; projektnummer: 7; projektname: FakeProject7; projektleiter: User:{id: 8; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase7; projekstatus: FakeStatus7; projektbudget: 7.000000}, " + 
					"Project:{id: 9; projektnummer: 8; projektname: FakeProject8; projektleiter: User:{id: 9; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase8; projekstatus: FakeStatus8; projektbudget: 8.000000}, " + 
					"Project:{id: 10; projektnummer: 9; projektname: FakeProject9; projektleiter: User:{id: 10; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase9; projekstatus: FakeStatus9; projektbudget: 9.000000}], " + 
					"phaseDetail=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], doneList=[Done:{done: false}, Done:{done: true}], " + 
					"workpackageDetail=Workpackage:{id: 0; project: null; projectphase: null; workpackagename: null; plannedValue: 0.000000; earnedValue: 0.000000; startdate: null; enddate: null; done: false}, " + 
					"postAction=/workpackage/new, projectId=0, session_user=username}") );
			
			verify(mockRequest).queryParams("id");
			verify(mockRequest).queryParams("projectId");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
