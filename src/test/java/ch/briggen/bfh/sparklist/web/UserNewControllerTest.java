package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class UserNewControllerTest {
	UserNewController controller = null;
	UserRepository repo = null;
	
	@Mock
	private Request mockRequest;
	@Mock
	private Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(1, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			u.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new UserNewController();
		repo = new UserRepository();
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Users");
		resetDB("Roles");
	}

	@Test
	void testHandleNullUser() {
		try
		{
			when(mockRequest.queryParams("userDetail.role")).thenReturn(null);
			when(mockRequest.queryParams("userDetail.id")).thenReturn(null);
			when(mockRequest.queryParams("userDetail.username")).thenReturn(null);
			when(mockRequest.queryParams("userDetail.name")).thenReturn(null);
			when(mockRequest.queryParams("userDetail.vorname")).thenReturn(null);
			when(mockRequest.queryParams("userDetail.abteilung")).thenReturn(null);
			when(mockRequest.queryParams("userDetail.mail")).thenReturn(null);
			when(mockRequest.queryParams("userDetail.telefon")).thenReturn(null);
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("registration") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=User:{id: 0; username: null; name: null; vorname: null; role: null; abteilung: null; mail: null; telefon: null}, " +
															  "postAction=/users/new, " +
															  "roleList=[Role:{id: 1; rolename: Admin}, Role:{id: 2; rolename: Portfoliomanager}, Role:{id: 3; rolename: Projektleiter}, Role:{id: 4; rolename: Stakeholder}, Role:{id: 5; rolename: Projektmitarbeiter}], " +
															  "error=Username fehlt, session_user=username}") );
			
			verify(mockRequest).queryParams("userDetail.role");
			verify(mockRequest).queryParams("userDetail.id");
			verify(mockRequest).queryParams("userDetail.name");
			verify(mockRequest).queryParams("userDetail.vorname");
			verify(mockRequest).queryParams("userDetail.abteilung");
			verify(mockRequest).queryParams("userDetail.mail");
			verify(mockRequest).queryParams("userDetail.telefon");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleEmptyUser() {
		try
		{
			when(mockRequest.queryParams("userDetail.role")).thenReturn("");
			when(mockRequest.queryParams("userDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("userDetail.username")).thenReturn("");
			when(mockRequest.queryParams("userDetail.name")).thenReturn("");
			when(mockRequest.queryParams("userDetail.vorname")).thenReturn("");
			when(mockRequest.queryParams("userDetail.abteilung")).thenReturn("");
			when(mockRequest.queryParams("userDetail.mail")).thenReturn("");
			when(mockRequest.queryParams("userDetail.telefon")).thenReturn("");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("registration") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=User:{id: 0; username: ; name: ; vorname: ; role: null; abteilung: ; mail: ; telefon: }, " +
															  "postAction=/users/new, " +
															  "roleList=[Role:{id: 1; rolename: Admin}, Role:{id: 2; rolename: Portfoliomanager}, Role:{id: 3; rolename: Projektleiter}, Role:{id: 4; rolename: Stakeholder}, Role:{id: 5; rolename: Projektmitarbeiter}], " +
															  "error=Username fehlt, session_user=username}") );
			
			verify(mockRequest, times(1)).queryParams("userDetail.role");
			verify(mockRequest, times(2)).queryParams("userDetail.id");
			verify(mockRequest).queryParams("userDetail.name");
			verify(mockRequest).queryParams("userDetail.vorname");
			verify(mockRequest).queryParams("userDetail.abteilung");
			verify(mockRequest).queryParams("userDetail.mail");
			verify(mockRequest).queryParams("userDetail.telefon");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleRealUser() {
		try
		{
			when(mockRequest.queryParams("userDetail.role")).thenReturn("1");
			when(mockRequest.queryParams("userDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("userDetail.username")).thenReturn("DummyUser");
			when(mockRequest.queryParams("userDetail.name")).thenReturn("DummyName");
			when(mockRequest.queryParams("userDetail.vorname")).thenReturn("DummyVorname");
			when(mockRequest.queryParams("userDetail.abteilung")).thenReturn("DummyAbteilung");
			when(mockRequest.queryParams("userDetail.mail")).thenReturn("DummyMail");
			when(mockRequest.queryParams("userDetail.telefon")).thenReturn("DummyTel");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest, times(1)).queryParams("userDetail.role");
			verify(mockRequest, times(2)).queryParams("userDetail.id");
			verify(mockRequest).queryParams("userDetail.name");
			verify(mockRequest).queryParams("userDetail.vorname");
			verify(mockRequest).queryParams("userDetail.abteilung");
			verify(mockRequest).queryParams("userDetail.mail");
			verify(mockRequest).queryParams("userDetail.telefon");
			verify(mockRequest, never()).session();
			verify(mockSession, never()).attribute("USERNAME");
			verify(mockHttpServletResponse).sendRedirect("/");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleExistingUser() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("userDetail.role")).thenReturn("1");
			when(mockRequest.queryParams("userDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("userDetail.username")).thenReturn("FakeUser0");
			when(mockRequest.queryParams("userDetail.name")).thenReturn("FakeName0");
			when(mockRequest.queryParams("userDetail.vorname")).thenReturn("FakeVorname0");
			when(mockRequest.queryParams("userDetail.abteilung")).thenReturn("FakeAbteilung0");
			when(mockRequest.queryParams("userDetail.mail")).thenReturn("FakeMail0");
			when(mockRequest.queryParams("userDetail.telefon")).thenReturn("FakeTel0");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("registration") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
					"postAction=/users/new, roleList=[Role:{id: 1; rolename: Admin}, Role:{id: 2; rolename: Portfoliomanager}, Role:{id: 3; rolename: Projektleiter}, Role:{id: 4; rolename: Stakeholder}, Role:{id: 5; rolename: Projektmitarbeiter}], " +
					"error=User mit Name FakeUser0 existiert schon, session_user=username}") );
			
			verify(mockRequest, times(1)).queryParams("userDetail.role");
			verify(mockRequest, times(2)).queryParams("userDetail.id");
			verify(mockRequest).queryParams("userDetail.name");
			verify(mockRequest).queryParams("userDetail.vorname");
			verify(mockRequest).queryParams("userDetail.abteilung");
			verify(mockRequest).queryParams("userDetail.mail");
			verify(mockRequest).queryParams("userDetail.telefon");
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
			verify(mockHttpServletResponse, never()).sendRedirect("/");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
