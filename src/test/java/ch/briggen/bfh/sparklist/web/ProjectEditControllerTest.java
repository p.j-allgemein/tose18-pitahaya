package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetSequence;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class ProjectEditControllerTest {
	ProjectEditController controller = null;
	ProjectRepository repo = null;
	UserRepository userRepo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateRepo(ProjectRepository p, UserRepository u) {
		for(int i = 0; i < 5; ++i) {
			User dummyUser = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummy = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter), 
										LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			
			u.insert(dummyUser);
			p.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new ProjectEditController();
		userRepo = new UserRepository();
		repo = new ProjectRepository();
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
		resetSequence("Projectnumber");
	}

	@Test
	void testHandleUnknownProject() {
		try
		{
			populateRepo(repo, userRepo);
			
			when(mockRequest.queryParams("id")).thenReturn("1234");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projekt_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{phaseDetail=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], " +
															  "userDetail=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
															  "User:{id: 2; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
															  "User:{id: 3; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, " +
															  "User:{id: 4; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, " +
															  "User:{id: 5; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, " +
															  "User:{id: 6; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, " +
															  "User:{id: 7; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, " +
															  "User:{id: 8; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, " +
															  "User:{id: 9; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, " +
															  "User:{id: 10; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}], " +
															  "postAction=/project/update, projectDetail=null, session_user=test}") );
			
			verify(mockRequest).queryParams("id");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleKnownUser() {
		try
		{
			populateRepo(repo, userRepo);
			
			when(mockRequest.queryParams("id")).thenReturn("1");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projekt_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{phaseDetail=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], " +
																"userDetail=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
																"User:{id: 2; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
																"User:{id: 3; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, " +
																"User:{id: 4; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, " +
																"User:{id: 5; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, " +
																"User:{id: 6; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, " +
																"User:{id: 7; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, " +
																"User:{id: 8; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, " +
																"User:{id: 9; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, " +
																"User:{id: 10; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}], " +
																"postAction=/project/update, " +
																"projectDetail=Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 2; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}, " +
																"session_user=test}"));
			
			verify(mockRequest).queryParams("id");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleIdNull() {
		try
		{
			populateRepo(repo, userRepo);
			
			when(mockRequest.queryParams("id")).thenReturn(null);
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projekt_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{phaseDetail=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], " +
															  "userDetail=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
															  "User:{id: 2; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
															  "User:{id: 3; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, " +
															  "User:{id: 4; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, " +
															  "User:{id: 5; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, " +
															  "User:{id: 6; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, " +
															  "User:{id: 7; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, " +
															  "User:{id: 8; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, " +
															  "User:{id: 9; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, " +
															  "User:{id: 10; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}], " +
															  "postAction=/projects/new, " +
															  "projectDetail=Project:{id: 0; projektnummer: 1; projektname: null; projektleiter: null; start: null; end: null; projektphase: null; projekstatus: null; projektbudget: 0.000000}, " +
															  "session_user=test}") );
			
			verify(mockRequest).queryParams("id");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleIdZero() {
		try
		{
			populateRepo(repo, userRepo);
			
			when(mockRequest.queryParams("id")).thenReturn("0");
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("test");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projekt_erfassen") );
			assertThat("Model", mav.getModel().toString(), is("{phaseDetail=[Projectphase:{id: 1; phasenname: Initialisierung}, Projectphase:{id: 2; phasenname: Konzeption}, Projectphase:{id: 3; phasenname: Realisierung}, Projectphase:{id: 4; phasenname: Einführung}], " +
															  "userDetail=[User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
															  "User:{id: 2; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
															  "User:{id: 3; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, " +
															  "User:{id: 4; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}, " +
															  "User:{id: 5; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, " +
															  "User:{id: 6; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}, " +
															  "User:{id: 7; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, " +
															  "User:{id: 8; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}, " +
															  "User:{id: 9; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}, " +
															  "User:{id: 10; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}], " +
															  "postAction=/projects/new, " +
															  "projectDetail=Project:{id: 0; projektnummer: 1; projektname: null; projektleiter: null; start: null; end: null; projektphase: null; projekstatus: null; projektbudget: 0.000000}, " +
															  "session_user=test}") );
			
			verify(mockRequest).queryParams("id");
			verifyZeroInteractions(mockHttpServletResponse);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
