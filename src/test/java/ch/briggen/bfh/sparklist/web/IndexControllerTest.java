package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class IndexControllerTest {
	IndexController controller = null;
	UserRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;

	private static void populateRepo(UserRepository u) {
		int i = 0;
		ProjectRepository p = new ProjectRepository();
		User dummyUser = new User(i,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(1, "FakeRole" + i),
							  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
		u.insert(dummyUser);
		Project dummy = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter), 
									LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);

		p.insert(dummy);
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		controller = new IndexController();
		response = RequestResponseFactory.create(mockHttpServletResponse);
		repo = new UserRepository();
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}
	
	@Test
	void testWithoutUser() {
		ModelAndView mav = null;
		try
		{
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("FakeUser0");
			
			mav = controller.handle(mockRequest, response);
			fail("Expected Exception");
		}
		catch (Exception e)
		{
			assertThat("ModelAndView", mav, nullValue() );
			assertThat("Error-Message", e.getMessage(), is("User mit Name FakeUser0 konnte nicht gefunden werden") );
		}
	}
	
	@Test
	void testWithUser() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("FakeUser0");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("index") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=User:{id: 1; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
					"list=[Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 2; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 1; rolename: Admin}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}], " +
					"session_user=FakeUser0}") );
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
