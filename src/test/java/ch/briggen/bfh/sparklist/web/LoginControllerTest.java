package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class LoginControllerTest {
	LoginController controller = null;
	UserRepository repo = null;
	
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	@Captor
	private ArgumentCaptor<User> userCaptor;
	private Response response;
	
	private static void populateRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(1, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			u.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new LoginController();
		repo = new UserRepository();
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Users");
		resetDB("Roles");
	}

	@Test
	void testHandleUsernameNull() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("username")).thenReturn(null);
			when(mockRequest.queryParams("password")).thenReturn(null);
			when(mockRequest.requestMethod()).thenReturn("POST");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("login") );
			assertThat("Model", mav.getModel().toString(), is("{postAction=/login, error=Username ist leer. Bitte Username eingeben.}") );
			
			verify(mockRequest).queryParams("username");
			verify(mockRequest, never()).queryParams("password");
			verify(mockRequest, times(2)).requestMethod();
			verify(mockHttpServletResponse, never()).sendRedirect("/users");			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleUsernameEmpty() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("username")).thenReturn("");
			when(mockRequest.queryParams("password")).thenReturn("");
			when(mockRequest.requestMethod()).thenReturn("POST");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("login") );
			assertThat("Model", mav.getModel().toString(), is("{postAction=/login, error=Username ist leer. Bitte Username eingeben.}") );
			
			verify(mockRequest).queryParams("username");
			verify(mockRequest, never()).queryParams("password");
			verify(mockRequest, times(2)).requestMethod();
			verify(mockHttpServletResponse, never()).sendRedirect("/users");	
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleUsernameUnknown() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("username")).thenReturn("ABC");
			when(mockRequest.queryParams("password")).thenReturn("");
			when(mockRequest.requestMethod()).thenReturn("post");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("login") );
			assertThat("Model", mav.getModel().toString(), is("{postAction=/login, error=Username ABC konnte nicht gefunden werden}") );
			
			verify(mockRequest).queryParams("username");
			verify(mockRequest, never()).queryParams("password");
			verify(mockRequest, times(2)).requestMethod();
			verify(mockHttpServletResponse, never()).sendRedirect("/users");	
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleUsernameKnown() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("username")).thenReturn("FakeUser0");
			when(mockRequest.queryParams("password")).thenReturn("");
			when(mockRequest.requestMethod()).thenReturn("POST");
			when(mockRequest.session()).thenReturn(mockSession);
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("index") );
			assertThat("Model", mav.getModel().toString(), is("{userDetail=User:{id: 1; username: FakeUser0; name: FakeName0; " +
															  "vorname: FakeVorname0; role: Role:{id: 1; rolename: Admin}; " +
															  "abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}, " +
															  "session_user=FakeUser0}") );
			
			verify(mockRequest).queryParams("username");
			verify(mockRequest, never()).queryParams("password");
			verify(mockRequest, times(2)).requestMethod();
			verify(mockRequest, times(2)).session();
			verify(mockHttpServletResponse, never()).sendRedirect("/users");
			verify(mockSession).attribute(eq("USER"), userCaptor.capture());
			verify(mockSession).attribute("USERNAME", "FakeUser0");
			
			assertThat("User in Session", userCaptor.getValue().toString(), is("User:{id: 1; username: FakeUser0; name: FakeName0; " +
					  "vorname: FakeVorname0; role: Role:{id: 1; rolename: Admin}; " +
					  "abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}") );
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleRequestMethodGet() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("username")).thenReturn("FakeUser0");
			when(mockRequest.queryParams("password")).thenReturn("");
			when(mockRequest.requestMethod()).thenReturn("GET");
			when(mockRequest.session()).thenReturn(mockSession);
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("login") );
			assertThat("Model", mav.getModel().toString(), is("{}") );
			
			verify(mockRequest).queryParams("username");
			verify(mockRequest, never()).queryParams("password");
			verify(mockRequest, times(2)).requestMethod();
			verify(mockRequest, never()).session();
			verify(mockHttpServletResponse, never()).sendRedirect("/users");			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleRequestMethodNull() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.queryParams("username")).thenReturn("FakeUser0");
			when(mockRequest.queryParams("password")).thenReturn("");
			when(mockRequest.requestMethod()).thenReturn(null);
			when(mockRequest.session()).thenReturn(mockSession);
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("login") );
			assertThat("Model", mav.getModel().toString(), is("{}") );
			
			verify(mockRequest).queryParams("username");
			verify(mockRequest, never()).queryParams("password");
			verify(mockRequest, times(2)).requestMethod();
			verify(mockRequest, never()).session();
			verify(mockHttpServletResponse, never()).sendRedirect("/users");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
