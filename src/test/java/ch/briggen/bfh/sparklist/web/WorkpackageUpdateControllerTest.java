package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Projectphase;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.Workpackage;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class WorkpackageUpdateControllerTest {
	WorkpackageUpdateController controller = null;
	ProjectRepository projectRepo = null;
	ProjectphaseRepository phaseRepo = null;
	WorkpackageRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateRepo(WorkpackageRepository w) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								      "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummyProject = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			Workpackage dummy = new Workpackage(0, dummyProject, new Projectphase(3, "FakePhase" + i), "FakeWP" + i, i, i,
												LocalDate.parse("2018.03.01", formatter), LocalDate.parse("2019.12.31", formatter),
												false);
			
			w.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new WorkpackageUpdateController();
		repo = new WorkpackageRepository();
		projectRepo = new ProjectRepository();
		phaseRepo = new ProjectphaseRepository();
		populateRepo(repo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}
	
	@Test
	void testHandleNullWorkpackage() {
		try
		{
			when(mockRequest.queryParams()).thenReturn(new HashSet<>());
			when(mockRequest.queryParams("workpackageDetail.projektname")).thenReturn(null);
			when(mockRequest.queryParams("workpackageDetail.phasenname")).thenReturn(null);
			when(mockRequest.queryParams("workpackageDetail.id")).thenReturn(null);
			when(mockRequest.queryParams("workpackageDetail.plannedValue")).thenReturn(null);
			when(mockRequest.queryParams("workpackageDetail.earnedValue")).thenReturn(null);
			when(mockRequest.queryParams("workpackageDetail.workpackagename")).thenReturn(null);
			when(mockRequest.queryParams("workpackageDetail.startdate")).thenReturn(null);
			when(mockRequest.queryParams("workpackageDetail.enddate")).thenReturn(null);
			when(mockRequest.queryParams("workpackageDetail.done")).thenReturn("true");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{error=workpackageDetail ist leer, scheint ein Fehler zu sein}") );
			
			verify(mockRequest).queryParams("workpackageDetail.projektname");
			verify(mockRequest).queryParams("workpackageDetail.phasenname");
			verify(mockRequest).queryParams("workpackageDetail.id");
			verify(mockRequest).queryParams("workpackageDetail.plannedValue");
			verify(mockRequest).queryParams("workpackageDetail.earnedValue");
			verify(mockRequest).queryParams("workpackageDetail.workpackagename");
			verify(mockRequest).queryParams("workpackageDetail.startdate");
			verify(mockRequest).queryParams("workpackageDetail.enddate");
			verify(mockRequest).queryParams("workpackageDetail.done");
			verify(mockHttpServletResponse, never()).sendRedirect("/projektsteckbrief?id=1");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleEmptyWorkpackage() {
		try
		{
			when(mockRequest.queryParams()).thenReturn(new HashSet<>());
			when(mockRequest.queryParams("workpackageDetail.projektname")).thenReturn("");
			when(mockRequest.queryParams("workpackageDetail.phasenname")).thenReturn("");
			when(mockRequest.queryParams("workpackageDetail.id")).thenReturn("0");
			when(mockRequest.queryParams("workpackageDetail.plannedValue")).thenReturn("0.0");
			when(mockRequest.queryParams("workpackageDetail.earnedValue")).thenReturn("0.0");
			when(mockRequest.queryParams("workpackageDetail.workpackagename")).thenReturn("");
			when(mockRequest.queryParams("workpackageDetail.startdate")).thenReturn("");
			when(mockRequest.queryParams("workpackageDetail.enddate")).thenReturn("");
			when(mockRequest.queryParams("workpackageDetail.done")).thenReturn("true");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projektsteckbrief") );
			assertThat("Model", mav.getModel().toString(), is("{error=workpackageDetail ist leer, scheint ein Fehler zu sein}") );
			
			verify(mockRequest).queryParams("workpackageDetail.projektname");
			verify(mockRequest).queryParams("workpackageDetail.phasenname");
			verify(mockRequest, times(2)).queryParams("workpackageDetail.id");
			verify(mockRequest, times(2)).queryParams("workpackageDetail.plannedValue");
			verify(mockRequest, times(2)).queryParams("workpackageDetail.earnedValue");
			verify(mockRequest).queryParams("workpackageDetail.workpackagename");
			verify(mockRequest, times(2)).queryParams("workpackageDetail.startdate");
			verify(mockRequest, times(2)).queryParams("workpackageDetail.enddate");
			verify(mockRequest).queryParams("workpackageDetail.done");
			verify(mockHttpServletResponse, never()).sendRedirect("/projektsteckbrief?id=6");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleAlphanumericWorkpackageId() {
		ModelAndView mav = null;
		try
		{
			when(mockRequest.queryParams()).thenReturn(new HashSet<>());
			when(mockRequest.queryParams("workpackageDetail.projektname")).thenReturn("FakeProject5");
			when(mockRequest.queryParams("workpackageDetail.phasenname")).thenReturn("Initialisierung");
			when(mockRequest.queryParams("workpackageDetail.id")).thenReturn("A");
			when(mockRequest.queryParams("workpackageDetail.plannedValue")).thenReturn("11.11");
			when(mockRequest.queryParams("workpackageDetail.earnedValue")).thenReturn("0.0");
			when(mockRequest.queryParams("workpackageDetail.workpackagename")).thenReturn("DummyWP");
			when(mockRequest.queryParams("workpackageDetail.startdate")).thenReturn("2018-03-01");
			when(mockRequest.queryParams("workpackageDetail.enddate")).thenReturn("2018-12-31");
			when(mockRequest.queryParams("workpackageDetail.done")).thenReturn("false");
			
			mav = controller.handle(mockRequest, response);
			
			fail("Expected Exception");
		}
		catch (Exception e)
		{
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("workpackageDetail.projektname");
			verify(mockRequest).queryParams("workpackageDetail.phasenname");
			verify(mockRequest, times(2)).queryParams("workpackageDetail.id");
			verify(mockRequest, never()).queryParams("workpackageDetail.plannedValue");
			verify(mockRequest, never()).queryParams("workpackageDetail.earnedValue");
			verify(mockRequest, never()).queryParams("workpackageDetail.workpackagename");
			verify(mockRequest, never()).queryParams("workpackageDetail.startdate");
			verify(mockRequest, never()).queryParams("workpackageDetail.enddate");
			verify(mockRequest, never()).queryParams("workpackageDetail.done");
			try
			{
				verify(mockHttpServletResponse, never()).sendRedirect("/projektsteckbrief?id=5");
			}
			catch (IOException ex)
			{
				fail("unexpected IOException " + e.getMessage());
			}
		}
	}
	
	@Test
	void testHandleRealProject() {
		try
		{
			when(mockRequest.queryParams()).thenReturn(new HashSet<>());
			when(mockRequest.queryParams("workpackageDetail.projektname")).thenReturn("FakeProject5");
			when(mockRequest.queryParams("workpackageDetail.phasenname")).thenReturn("Initialisierung");
			when(mockRequest.queryParams("workpackageDetail.id")).thenReturn("5");
			when(mockRequest.queryParams("workpackageDetail.plannedValue")).thenReturn("11.11");
			when(mockRequest.queryParams("workpackageDetail.earnedValue")).thenReturn("0.0");
			when(mockRequest.queryParams("workpackageDetail.workpackagename")).thenReturn("DummyWP");
			when(mockRequest.queryParams("workpackageDetail.startdate")).thenReturn("2018-03-01");
			when(mockRequest.queryParams("workpackageDetail.enddate")).thenReturn("2018-12-31");
			when(mockRequest.queryParams("workpackageDetail.done")).thenReturn("false");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest).queryParams("workpackageDetail.projektname");
			verify(mockRequest).queryParams("workpackageDetail.phasenname");
			verify(mockRequest, times(2)).queryParams("workpackageDetail.id");
			verify(mockRequest, times(2)).queryParams("workpackageDetail.plannedValue");
			verify(mockRequest, times(2)).queryParams("workpackageDetail.earnedValue");
			verify(mockRequest).queryParams("workpackageDetail.workpackagename");
			verify(mockRequest, times(3)).queryParams("workpackageDetail.startdate");
			verify(mockRequest, times(3)).queryParams("workpackageDetail.enddate");
			verify(mockRequest).queryParams("workpackageDetail.done");
			verify(mockHttpServletResponse).sendRedirect("/projektsteckbrief?id=6");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
