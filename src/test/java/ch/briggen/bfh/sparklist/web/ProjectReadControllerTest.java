package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Session;

@ExtendWith(MockitoExtension.class)
public class ProjectReadControllerTest {
	@Mock
	private Request mockRequest;
	@Mock
	Session mockSession;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	ProjectReadController controller = null;
	UserRepository userRepo = null;
	ProjectRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	private static void populateRepo(ProjectRepository p) {
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								  "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummy = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter), 
										LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			
			p.insert(dummy);
		}
	}
	
	private static void populateUserRepo(UserRepository u) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(i * 100,"FakeUser" + i * 100, "FakeName" + i * 100, "FakeVorname" + i * 100, new Role(3, "FakeRole" + i * 100),
								  "FakeAbteilung" + i * 100, "FakeMail" + i * 100, "FakeTel" + i * 100);
			u.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		controller = new ProjectReadController();
		userRepo = new UserRepository();
		repo = new ProjectRepository();
		populateUserRepo(userRepo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}
	
	@Test
	void testHandleNothing() {
		try
		{
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projekte") );
			assertThat("Model", mav.getModel().toString(), is("{list=[], session_user=username}") );
			
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandle() {
		try
		{
			populateRepo(repo);
			
			when(mockRequest.session()).thenReturn(mockSession);
			when(mockSession.attribute("USERNAME")).thenReturn("username");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ViewName", mav.getViewName(), is("projekte") );
			assertThat("Model", mav.getModel().toString(), is("{list=[Project:{id: 1; projektnummer: 0; projektname: FakeProject0; projektleiter: User:{id: 11; username: FakeUser0; name: FakeName0; vorname: FakeVorname0; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung0; mail: FakeMail0; telefon: FakeTel0}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase0; projekstatus: FakeStatus0; projektbudget: 0.000000}, " +
															  "Project:{id: 2; projektnummer: 1; projektname: FakeProject1; projektleiter: User:{id: 12; username: FakeUser1; name: FakeName1; vorname: FakeVorname1; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung1; mail: FakeMail1; telefon: FakeTel1}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase1; projekstatus: FakeStatus1; projektbudget: 1.000000}, " +
															  "Project:{id: 3; projektnummer: 2; projektname: FakeProject2; projektleiter: User:{id: 13; username: FakeUser2; name: FakeName2; vorname: FakeVorname2; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung2; mail: FakeMail2; telefon: FakeTel2}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase2; projekstatus: FakeStatus2; projektbudget: 2.000000}, " +
															  "Project:{id: 4; projektnummer: 3; projektname: FakeProject3; projektleiter: User:{id: 14; username: FakeUser3; name: FakeName3; vorname: FakeVorname3; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung3; mail: FakeMail3; telefon: FakeTel3}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase3; projekstatus: FakeStatus3; projektbudget: 3.000000}, " +
															  "Project:{id: 5; projektnummer: 4; projektname: FakeProject4; projektleiter: User:{id: 15; username: FakeUser4; name: FakeName4; vorname: FakeVorname4; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung4; mail: FakeMail4; telefon: FakeTel4}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase4; projekstatus: FakeStatus4; projektbudget: 4.000000}, " +
															  "Project:{id: 6; projektnummer: 5; projektname: FakeProject5; projektleiter: User:{id: 16; username: FakeUser5; name: FakeName5; vorname: FakeVorname5; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung5; mail: FakeMail5; telefon: FakeTel5}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase5; projekstatus: FakeStatus5; projektbudget: 5.000000}, " +
															  "Project:{id: 7; projektnummer: 6; projektname: FakeProject6; projektleiter: User:{id: 17; username: FakeUser6; name: FakeName6; vorname: FakeVorname6; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung6; mail: FakeMail6; telefon: FakeTel6}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase6; projekstatus: FakeStatus6; projektbudget: 6.000000}, " +
															  "Project:{id: 8; projektnummer: 7; projektname: FakeProject7; projektleiter: User:{id: 18; username: FakeUser7; name: FakeName7; vorname: FakeVorname7; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung7; mail: FakeMail7; telefon: FakeTel7}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase7; projekstatus: FakeStatus7; projektbudget: 7.000000}, " +
															  "Project:{id: 9; projektnummer: 8; projektname: FakeProject8; projektleiter: User:{id: 19; username: FakeUser8; name: FakeName8; vorname: FakeVorname8; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung8; mail: FakeMail8; telefon: FakeTel8}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase8; projekstatus: FakeStatus8; projektbudget: 8.000000}, " +
															  "Project:{id: 10; projektnummer: 9; projektname: FakeProject9; projektleiter: User:{id: 20; username: FakeUser9; name: FakeName9; vorname: FakeVorname9; role: Role:{id: 3; rolename: Projektleiter}; abteilung: FakeAbteilung9; mail: FakeMail9; telefon: FakeTel9}; start: 2018.03.01; end: 2019.12.31; projektphase: FakePhase9; projekstatus: FakeStatus9; projektbudget: 9.000000}], " +
															  "session_user=username}") );
			
			verify(mockRequest).session();
			verify(mockSession).attribute("USERNAME");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("unexpected Exception " + e.getMessage());
		}
	}
}
