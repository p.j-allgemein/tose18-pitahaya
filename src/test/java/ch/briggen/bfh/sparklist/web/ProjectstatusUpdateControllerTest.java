package ch.briggen.bfh.sparklist.web;

import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.DBTestHelper.resetDB;
import static ch.briggen.bfh.sparklist.domain.RoleDBTestHelper.initData;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import ch.briggen.bfh.sparklist.MockitoExtension;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Projectstatus;
import ch.briggen.bfh.sparklist.domain.ProjectstatusRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.RequestResponseFactory;
import spark.Response;

@ExtendWith(MockitoExtension.class)
public class ProjectstatusUpdateControllerTest {
	ProjectstatusUpdateController controller = null;
	UserRepository userRepo = null;
	ProjectRepository projectRepo = null;
	ProjectstatusRepository repo = null;
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	@Mock
	private Request mockRequest;
	@Mock
	private HttpServletResponse mockHttpServletResponse;
	private Response response;
	
	private static void populateProjectRepo(ProjectstatusRepository p) {
		ProjectRepository prep = new ProjectRepository();
		for(int i = 0; i < 10; ++i) {
			User dummyUser = new User(0,"FakeUser" + i, "FakeName" + i, "FakeVorname" + i, new Role(3, "FakeRole" + i),
								      "FakeAbteilung" + i, "FakeMail" + i, "FakeTel" + i);
			Project dummyProject = new Project(0, i, "FakeProject" + i, dummyUser, LocalDate.parse("2018.03.01", formatter),
											   LocalDate.parse("2019.12.31", formatter), "FakePhase" + i, "FakeStatus" + i, i);
			dummyProject.setId(prep.insert(dummyProject));
			Projectstatus dummy = new Projectstatus(0, dummyProject, "Grün", "Neutral", "Gelb", "Rot", "Gelb");
			
			p.insert(dummy);
		}
		
		User dummyUser = new User(0,"FakeUser100", "FakeName100", "FakeVorname100", new Role(3, "FakeRole100"),
					      		  "FakeAbteilung100", "FakeMail100", "FakeTel100");
		Project dummyProject = new Project(0, 100, "FakeProject100", dummyUser, LocalDate.parse("2018.03.01", formatter),
								   LocalDate.parse("2019.12.31", formatter), "FakePhase100", "FakeStatus100", 100);
		prep.insert(dummyProject);
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		initData();
		
		response = RequestResponseFactory.create(mockHttpServletResponse);
		controller = new ProjectstatusUpdateController();
		userRepo = new UserRepository();
		projectRepo = new ProjectRepository();
		repo = new ProjectstatusRepository();
		populateProjectRepo(repo);
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		repo.getAll().stream().forEach(e -> repo.delete(e.getId()));
		resetDB("Projectstatus");
		resetDB("Projects");
		resetDB("Users");
		resetDB("Roles");
	}

	@Test
	void testHandleNullProjectstatus() {
		ModelAndView mav = null;
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn(null);
			when(mockRequest.queryParams("projectstatus")).thenReturn("Grün");
			when(mockRequest.queryParams("ressourcestatus")).thenReturn("Grün");
			when(mockRequest.queryParams("riskstatus")).thenReturn("Grün");
			when(mockRequest.queryParams("timestatus")).thenReturn("Grün");
			when(mockRequest.queryParams("budgetstatus")).thenReturn("Grün");
			
			mav = controller.handle(mockRequest, response);
			fail("Expected Exception");
		}
		catch (Exception e)
		{
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest, times(2)).queryParams("projectId");
			verify(mockRequest).queryParams("projectstatus");
			verify(mockRequest).queryParams("ressourcestatus");
			verify(mockRequest).queryParams("riskstatus");
			verify(mockRequest).queryParams("timestatus");
			verify(mockRequest).queryParams("budgetstatus");
		}
	}
	
	@Test
	void testHandleEmptyProjectstatus() {
		ModelAndView mav = null;
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn("");
			when(mockRequest.queryParams("projectstatus")).thenReturn("Grün");
			when(mockRequest.queryParams("ressourcestatus")).thenReturn("Grün");
			when(mockRequest.queryParams("riskstatus")).thenReturn("Grün");
			when(mockRequest.queryParams("timestatus")).thenReturn("Grün");
			when(mockRequest.queryParams("budgetstatus")).thenReturn("Grün");
			
			mav = controller.handle(mockRequest, response);
			fail("Expected Exception");
		}
		catch (Exception e)
		{
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest, times(2)).queryParams("projectId");
			verify(mockRequest).queryParams("projectstatus");
			verify(mockRequest).queryParams("ressourcestatus");
			verify(mockRequest).queryParams("riskstatus");
			verify(mockRequest).queryParams("timestatus");
			verify(mockRequest).queryParams("budgetstatus");
		}
	}
	
	@Test
	void testHandleProjectstatus() {
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn("1");
			when(mockRequest.queryParams("projectstatus")).thenReturn("Grün");
			when(mockRequest.queryParams("ressourcestatus")).thenReturn("Grün");
			when(mockRequest.queryParams("riskstatus")).thenReturn("Grün");
			when(mockRequest.queryParams("timestatus")).thenReturn("Grün");
			when(mockRequest.queryParams("budgetstatus")).thenReturn("Grün");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest, times(2)).queryParams("projectId");
			verify(mockRequest).queryParams("projectstatus");
			verify(mockRequest).queryParams("ressourcestatus");
			verify(mockRequest).queryParams("riskstatus");
			verify(mockRequest).queryParams("timestatus");
			verify(mockRequest).queryParams("budgetstatus");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unexpected Exception " + e.getMessage());
		}
	}
	
	@Test
	void testHandleNewProjectstatus() {
		try
		{
			when(mockRequest.queryParams("projectId")).thenReturn("11");
			when(mockRequest.queryParams("projectstatus")).thenReturn("Grün");
			when(mockRequest.queryParams("ressourcestatus")).thenReturn("Grün");
			when(mockRequest.queryParams("riskstatus")).thenReturn("Grün");
			when(mockRequest.queryParams("timestatus")).thenReturn("Grün");
			when(mockRequest.queryParams("budgetstatus")).thenReturn("Grün");
			
			ModelAndView mav = controller.handle(mockRequest, response);
			assertThat("ModelAndView", mav, nullValue() );
			
			verify(mockRequest, times(2)).queryParams("projectId");
			verify(mockRequest).queryParams("projectstatus");
			verify(mockRequest).queryParams("ressourcestatus");
			verify(mockRequest).queryParams("riskstatus");
			verify(mockRequest).queryParams("timestatus");
			verify(mockRequest).queryParams("budgetstatus");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail("Unexpected Exception " + e.getMessage());
		}
	}
}
