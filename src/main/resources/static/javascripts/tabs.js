function openTab(event, tabName) {
    // alle Variablen
    var i, tabcontent, tablinks;

    // Alle Elemente mit der Klasse="tabcontent" nehmen und verbergen
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Alle Elemente mit der Klasse="tablinks" nehmen und das Attribut "active" entfernen
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    event.currentTarget.className += " active";

} 