function updatestatus()
{
	var projectId = document.getElementById("Projektid");
	var projectstatus = document.getElementById("projektstatus");
	var imageProjectstatus = document.getElementById("imageProjectstatus");
	imageProjectstatus.src = '/images/' + projectstatus.value + '.jpg';
	var budgetstatus = updateImage('budgetstatus', 'imgBudgetstatus', 'imageBudgetstatus');
	var ressourcestatus = updateImage('ressourcestatus', 'imgRessourcestatus', 'imageRessourcestatus');
	var riskstatus = updateImage('riskstatus', 'imgRiskstatus', 'imageRiskstatus');
	//var timestatus = updateImage('timestatus', 'imgTimestatus', 'imageTimestatus');
	
	var url = "projectId=" + projectId.value + "&" +
			  "projectstatus=" + projectstatus.value + "&" +
		      "budgetstatus=" + budgetstatus + "&" +
			  "ressourcestatus=" + ressourcestatus + "&" +
			  "riskstatus=" + riskstatus; // + "&" ;
			  //"timestatus=" + timestatus;
	
	var myRequest = new XMLHttpRequest();
	myRequest.open('GET', '/status/update?' + url);
	myRequest.send();
}

function updateImage(idStatus, idImg, idImage)
{
	var status = document.getElementById(idStatus);
	var img = document.getElementById(idImg);
	var image = document.getElementById(idImage);
	image.src = '/images/' + status.value + '.jpg';
	img.src = '/images/' + status.value + '.jpg';
	return status.value;
}
