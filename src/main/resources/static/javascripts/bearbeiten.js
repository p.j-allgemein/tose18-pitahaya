function enable()
{
	document.getElementById("Projektnummer").disabled = false;
	document.getElementById("Projektname").disabled = false;
	document.getElementById("Projektleiter").disabled = false;
	document.getElementById("Projektbudget").disabled = false;
	document.getElementById("Projektstart").disabled = false;
	document.getElementById("Projektende").disabled = false;
	document.getElementById("Projektphase").disabled = false;
	document.getElementById("Projektstatus").disabled = false;
	document.getElementById("Projektdaten speichern").disabled = false;
	
	var btn = document.getElementById("Projektdaten bearbeiten");
	btn.value="Änderungen verwerfen";
	btn.type ="reset";
	btn.onclick = disable;
}

function disable()
{
	var btn = document.getElementById("Projektdaten bearbeiten");
	btn.onclick = location.href='projektsteckbrief?id=' + document.getElementById('Projektid').value;
}