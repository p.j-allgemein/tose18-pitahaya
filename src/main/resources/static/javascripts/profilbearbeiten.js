function freigeben()
{
	document.getElementById("Userid").disabled = false;
	document.getElementById("username").disabled = false;
	document.getElementById("name").disabled = false;
	document.getElementById("vorname").disabled = false;
	document.getElementById("role").disabled = false;
	document.getElementById("abteilung").disabled = false;
	document.getElementById("mail").disabled = false;
	document.getElementById("telefon").disabled = false;
	document.getElementById("Userdaten speichern").disabled = false;
	
	var btn = document.getElementById("Userdaten bearbeiten");
	btn.value="Änderungen verwerfen";
	btn.type ="reset";
	btn.onclick = disable;
}

function disable()
{
	var btn = document.getElementById("Userdaten bearbeiten");
	btn.onclick = location.href='profil?id=' + document.getElementById('Userid').value;
}