alter table roles alter column id restart with 6;
merge into roles
key(id, rolename)
values(1, 'Admin'), (2, 'Portfoliomanager'), (3, 'Projektleiter'), (4, 'Stakeholder'), (5, 'Projektmitarbeiter');

alter table projectphases alter column id restart with 5;
merge into projectphases
key(id, phasenname)
values (1, 'Initialisierung'), (2, 'Konzeption'), (3, 'Realisierung'), (4, 'Einführung');