create sequence if not exists projectnumber start with 1 nocache;

create table if not exists items
(
	id long identity,
	name varchar(200),
	quantity decimal
);
create table if not exists roles
(
	id long identity,
	rolename varchar(50)
);
create table if not exists users
(
	id long identity, 
	username varchar(50),
	name varchar(200),
	vorname varchar(200),
	role long,
	abteilung varchar(200),
	mail varchar(200),
	telefon varchar(10),
	FOREIGN KEY (role) REFERENCES roles(id)
);
create table if not exists projects
(
	id long identity,
	projektnummer long,
	projektname varchar(200),
	projektleiter long,
	projektstart date,
	projektende date,
	projektphase varchar(200),
	projektstatus varchar(200),
	projektbudget decimal,
	FOREIGN KEY (projektleiter) references users(id)
);
create table if not exists Projectrisks
(
	id long identity,
	risikoname varchar(200),
	ursache varchar(200),
	gefaehrdungsart varchar(50),
	eintrittswahrscheinlichkeit decimal,
	auswirkungen decimal,
	massnahmen varchar(200),
	verantwortlicher long,
	prioritaet long,
	project long,
	FOREIGN KEY (verantwortlicher) references users(id),
	FOREIGN KEY (project) references projects(id)
);
create table if not exists Users2Projects
(
	userId long not null,
	projectId long not null,
	roleId long,
	PRIMARY KEY (userId, projectId),
	FOREIGN KEY (userId) references users(id),
	FOREIGN KEY (projectId) references projects(id),
	FOREIGN KEY (roleId) references roles(id)
);
create table if not exists Projectstatus
(
	id long identity,
	projectId long,
	projectstatus varchar(20),
	ressourcestatus varchar(20),
	riskstatus varchar(20),
	timestatus varchar(20),
	budgetstatus varchar(20),
	FOREIGN KEY (projectId) references projects(id),
	CONSTRAINT CK_PROJECTSTATUS CHECK (Projectstatus IN ('Grün', 'Neutral', 'Rot', 'Gelb')),
	CONSTRAINT CK_RESSOURCESTATUS CHECK (Ressourcestatus IN ('Grün', 'Neutral', 'Rot', 'Gelb')),
	CONSTRAINT CK_RISKSTATUS CHECK (Riskstatus IN ('Grün', 'Neutral', 'Rot', 'Gelb')),
	CONSTRAINT CK_TIMESTATUS CHECK (Timestatus IN ('Grün', 'Neutral', 'Rot', 'Gelb')),
	CONSTRAINT CK_BUDGETSTATUS CHECK (Budgetstatus IN ('Grün', 'Neutral', 'Rot', 'Gelb'))
);
create table if not exists Projectphases
(
	id long identity,
	phasenname varchar(200)
);
create table if not exists Workpackages
(
	id long identity,
	projectId long,
	phaseId long,
	workpackagename varchar(200),
	plannedValue decimal,
	earnedValue decimal,
	startdate date,
	enddate date,
	done boolean,
	FOREIGN KEY (projectId) references projects(id),
	FOREIGN KEY (phaseId) references Projectphases(id)
);
create table if not exists Costs
(
	id long identity,
	workpackageId long,
	userId long,
	beschreibung varchar(200),
	betrag decimal,
	datum date,
	FOREIGN KEY (workpackageId) references Workpackages(id),
	FOREIGN KEY (userId) references Users(id)
);
