package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.CostDeleteController;
import ch.briggen.bfh.sparklist.web.CostEditController;
import ch.briggen.bfh.sparklist.web.CostNewController;
import ch.briggen.bfh.sparklist.web.EmployeeDeleteController;
import ch.briggen.bfh.sparklist.web.EmployeeEditController;
import ch.briggen.bfh.sparklist.web.EmployeeNewController;
import ch.briggen.bfh.sparklist.web.IndexController;
import ch.briggen.bfh.sparklist.web.ProjectDeleteController;
import ch.briggen.bfh.sparklist.web.ProjectEditController;
import ch.briggen.bfh.sparklist.web.ProjectNewController;
import ch.briggen.bfh.sparklist.web.ProjectReadController;
import ch.briggen.bfh.sparklist.web.ProjectReadDetailController;
//import ch.briggen.bfh.sparklist.web.ProjectReadUserController;
import ch.briggen.bfh.sparklist.web.ProjectriskNewController;
//import ch.briggen.bfh.sparklist.web.ProjectriskReadController;
import ch.briggen.bfh.sparklist.web.ProjectriskDeleteController;
import ch.briggen.bfh.sparklist.web.ProjectriskEditController;
import ch.briggen.bfh.sparklist.web.ProjectriskUpdateController;
import ch.briggen.bfh.sparklist.web.ProjectUpdateController;
import ch.briggen.bfh.sparklist.web.RoleReadController;
import ch.briggen.bfh.sparklist.web.RootController;
import ch.briggen.bfh.sparklist.web.UserDeleteController;
import ch.briggen.bfh.sparklist.web.UserEditController;
import ch.briggen.bfh.sparklist.web.ProjectstatusUpdateController;
import ch.briggen.bfh.sparklist.web.LoginController;
import ch.briggen.bfh.sparklist.web.LogoutController;
import ch.briggen.bfh.sparklist.web.UserReadDetailController;
import ch.briggen.bfh.sparklist.web.UserNewController;
import ch.briggen.bfh.sparklist.web.UserUpdateController;
import ch.briggen.bfh.sparklist.web.WorkpackageDeleteController;
import ch.briggen.bfh.sparklist.web.WorkpackageEditController;
import ch.briggen.bfh.sparklist.web.WorkpackageNewController;
import ch.briggen.bfh.sparklist.web.WorkpackageUpdateController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
	
		get("/", new RootController(), new UTF8ThymeleafTemplateEngine());
		get("/index", new IndexController(), new UTF8ThymeleafTemplateEngine());
		// Die Seite laden entspricht einem GET
		// Der Login mit Username und Password entspricht einem POST
		// Daher werden beide Methoden angegeben
		get("/login", new LoginController(),  new UTF8ThymeleafTemplateEngine());
		post("/login", new LoginController(), new UTF8ThymeleafTemplateEngine());
		get("/logout", new LogoutController(),  new UTF8ThymeleafTemplateEngine());
		get("/roles", new RoleReadController(), new UTF8ThymeleafTemplateEngine());
		get("/profil", new UserReadDetailController(), new UTF8ThymeleafTemplateEngine());
		get("/users/edit", new UserEditController(), new UTF8ThymeleafTemplateEngine());
		get("/registration", new UserEditController(), new UTF8ThymeleafTemplateEngine());
		post("/users/update", new UserUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/users/delete", new UserDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/users/new", new UserNewController(), new UTF8ThymeleafTemplateEngine());
		get("/projekte", new ProjectReadController(), new UTF8ThymeleafTemplateEngine());
		get("/projekt_erfassen", new ProjectEditController(), new UTF8ThymeleafTemplateEngine());
		post("/project/update", new ProjectUpdateController(), new UTF8ThymeleafTemplateEngine()); 
		get("/project/update", new ProjectUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/project/delete", new ProjectDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/projects/new", new ProjectNewController(), new UTF8ThymeleafTemplateEngine());
		//get("/auswertungen", new ProjectReadUserController(), new UTF8ThymeleafTemplateEngine()); /* /projects/users */
		get("/projektsteckbrief", new ProjectReadDetailController(), new UTF8ThymeleafTemplateEngine());
		post("/projektrisiko/new", new ProjectriskNewController(), new UTF8ThymeleafTemplateEngine());
		//get("/projektrisiko", new ProjectriskReadController(), new UTF8ThymeleafTemplateEngine());
		get("/projektrisiko/delete", new ProjectriskDeleteController(), new UTF8ThymeleafTemplateEngine());
		get("/projektrisiko/update", new ProjectriskUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/projektrisiko/edit", new ProjectriskEditController(), new UTF8ThymeleafTemplateEngine());
		get("/mitarbeiter_erfassen", new EmployeeEditController(), new UTF8ThymeleafTemplateEngine());
		post("/mitarbeiter/new", new EmployeeNewController(), new UTF8ThymeleafTemplateEngine());
		get("/mitarbeiter/delete", new EmployeeDeleteController(), new UTF8ThymeleafTemplateEngine());
		get("/status/update", new ProjectstatusUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/workpackage_erfassen", new WorkpackageEditController(), new UTF8ThymeleafTemplateEngine());
		post("/workpackage/new", new WorkpackageNewController(), new UTF8ThymeleafTemplateEngine());
		get("/workpackage/update", new WorkpackageUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/workpackage/delete", new WorkpackageDeleteController(), new UTF8ThymeleafTemplateEngine());
		get("/kosten_erfassen", new CostEditController(), new UTF8ThymeleafTemplateEngine());
		post("/kosten/new", new CostNewController(), new UTF8ThymeleafTemplateEngine());
		get("/kosten/delete", new CostDeleteController(), new UTF8ThymeleafTemplateEngine());
	}

}
