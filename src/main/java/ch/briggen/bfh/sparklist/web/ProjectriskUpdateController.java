package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Projectrisk;
import ch.briggen.bfh.sparklist.domain.ProjectriskRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectriskUpdateController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectriskUpdateController.class);
		
	private ProjectriskRepository repo = new ProjectriskRepository();
	private UserRepository userRepo = new UserRepository();

	/**
	 * Schreibt das geänderte Projektrisiken zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/projektrisiko/update) mit der Projektrisiko-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /projektrisiko/update
	 * 
	 * @return redirect nach /projektrisiko: via Browser wird /projektrisiko aufgerufen.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		log.trace("queryParams " + request.queryParams().toString());
		Projectrisk projectriskDetail = ProjectriskWebHelper.projectriskFromWeb(request);
		
		if (projectriskDetail.getId() == 0)
		{
			log.trace("projectriskDetail.getId == 0");
			log.trace("projectriskDetail: " + projectriskDetail.toString());
			HashMap<String, Object> model = new HashMap<String, Object>();
			model.put("error", "projectriskDetail ist leer, scheint ein Fehler zu sein");
			model.put("projektname", request.queryParams("projectrisikoDetail.projektname"));
			model.put("userDetail", userRepo.getAll());
			model.put("projectrisikoDetail", projectriskDetail);
			String username = request.session().attribute("USERNAME");
			model.put("session_user", username);
			return new ModelAndView(model, "projektrisiko");
		}
		
		log.trace("POST /projektrisiko/update mit projectriskDetail " + projectriskDetail);
		
		//Speichern des Projektrisikos in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /projektrisiko&id=3 (wenn projectriskDetail.getId == 3 war)
		repo.save(projectriskDetail);
		response.redirect("/projektrisiko");
		return null;
	}
}
