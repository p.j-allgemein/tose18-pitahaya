package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Projectrisk;
import ch.briggen.bfh.sparklist.domain.ProjectriskRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectriskReadController implements TemplateViewRoute {

	
	private final Logger log = LoggerFactory.getLogger(ProjectReadController.class);
	
	private ProjectriskRepository repo = new ProjectriskRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Users. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Projektrisiko erstellt (Aufruf von /projektrisiko/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars dar Projektrisiko mit der übergebenen id upgedated (Aufruf /projektrisiko/save)
	 * Hört auf GET /projektrisiko
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "projektrisiko" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		if(null == idString || "0".equals(idString))
		{
			log.trace("GET /projektrisiko fehlende id " + idString);
			model.put("error", "projekt-id unbekannt bzw. nicht mitgegeben");
		}
		else
		{
			log.trace("GET /projektrisiko mit id " + idString);
			List<Projectrisk> risks = new ArrayList<>(repo.getByProject(idString));
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "projektrisikoDetail" hinzugefügt. projektrisikoDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			model.put("postAction", "/projektrisiko/update");
			model.put("projectrisikoDetail", risks);
		}
		
		//das Template projektrisiko verwenden und dann "anzeigen".
		return new ModelAndView(model, "projektsteckbrief");
	}

}
