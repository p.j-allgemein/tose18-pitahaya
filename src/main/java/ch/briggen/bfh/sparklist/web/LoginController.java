package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class LoginController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(LoginController.class);
	
	private UserRepository userRepo = new UserRepository();
	
	/**
	 * Requesthandler zum Bearbeiten eines Users. 
	 * Liefert das Formular (bzw. Template) gemäss definition zurück.
	 * Wenn der username null oder leer ist, ist das Login fehlgeschlagen und der Benutzer erhält wieder die Login Maske
	 * Wenn der username gesetzt ist, wird kontrolliert, ob der Username vorhanden ist.
	 * Hört auf GET /login
	 * @return gibt den Namen des zu verwendenden Templates zurück.
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		log.trace("request method: " + request.requestMethod());
		String username = request.queryParams("username");
		//String password = request.queryParams("password");
		String template = "login";
		
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		if ("POST".equalsIgnoreCase(request.requestMethod()))
		{
			if(null == username || username.isEmpty())
			{
				log.trace("GET /login with empty username");
				// wenn kein Benutzer eingegeben wurde, geht's wieder zurück
				// auf die Login Seite
				model.put("postAction", "/login");
				model.put("error", "Username ist leer. Bitte Username eingeben.");
			}
			else
			{
				log.trace("GET /login with username " + username);
				
				//prüfen, ob der Benutzer in der Datenbank existiert 
				List<User> u = new ArrayList<>(userRepo.getByName(username));
				
				//falls Benutzer unbekannt ist, geht es wieder zurück
				//auf die Login Seite
				if (u.isEmpty())
				{
					model.put("postAction", "/login");
					model.put("error", "Username " + username + " konnte nicht gefunden werden");
				}
				else
				{
					request.session().attribute("USER", u.get(0));
					request.session().attribute("USERNAME", u.get(0).getUsername());
					model.put("userDetail", u.get(0));
					model.put("session_user", u.get(0).getUsername());
					template = "index";
				}
			}
		}
		
		//template wird vorgängig richtig gesetzt
		return new ModelAndView(model, template);
	}
}
