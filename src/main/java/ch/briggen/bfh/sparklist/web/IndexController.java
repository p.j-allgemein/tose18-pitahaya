package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class IndexController implements TemplateViewRoute{
	
	private UserRepository repo = new UserRepository();
	ProjectRepository projrepo = new ProjectRepository();
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(IndexController.class);
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		HashMap<String, Object> model = new HashMap<String, Object>();
		String username = request.session().attribute("USERNAME");
		model.put("session_user", username);
		List<User> userList = new ArrayList<>(repo.getByName(username));
		model.put("list", projrepo.getAll());
		if (!userList.isEmpty())
		{
			model.put("userDetail", userList.get(0));
		}
		else
		{
			throw new IllegalArgumentException("User mit Name " + username + " konnte nicht gefunden werden");
		}
		
		return new ModelAndView(model, "index");
	}
}
