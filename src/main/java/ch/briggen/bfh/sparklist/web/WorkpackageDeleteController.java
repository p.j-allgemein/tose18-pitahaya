package ch.briggen.bfh.sparklist.web;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import ch.briggen.bfh.sparklist.domain.Workpackage;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class WorkpackageDeleteController implements TemplateViewRoute {
	private final Logger log = LoggerFactory.getLogger(WorkpackageDeleteController.class);
	
	private WorkpackageRepository repo = new WorkpackageRepository();
	

	/**
	 * Löscht das Workpackage mit der übergebenen id in der Datenbank
	 * /workpackage/delete&id=987 löscht das Workpackage mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /workpackage/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zum Projektsteckbrief
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /workpackage/delete mit id " + id);
		
		Workpackage wp = null;
		if (NumberUtils.isNumber(id))
		{
			Long longId = Long.parseLong(id);
			wp = repo.getById(longId);
			repo.delete(longId);
		}
		else
		{
			List<Workpackage> list = new ArrayList<>(repo.getByName(id));
			if (!list.isEmpty())
			{
				repo.delete(list.get(0).getId());
			}
		}
		
		if (wp != null)
		{
			response.redirect("/projektsteckbrief?id=" + wp.getProject().getId());
		}
		else
		{
			response.redirect("/projekte");
		}
		return null;
	}
}
