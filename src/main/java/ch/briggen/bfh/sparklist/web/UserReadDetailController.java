package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class UserReadDetailController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(UserReadDetailController.class);
	
	private UserRepository userRepo = new UserRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Users. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Projekt erstellt (Aufruf von /profil)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars dar Project mit der übergebenen id upgedated (Aufruf /users/update)
	 * Hört auf GET /profil
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "profil" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String username = request.session().attribute("USERNAME");		
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		if(null == idString || "0".equals(idString))
		{
			log.trace("GET /profil fehlende id " + idString);
			log.trace("use username from session " + username);
			List<User> u = new ArrayList<>(userRepo.getByName(username));
			model.put("userDetail", u.get(0));
		}
		else
		{
			log.trace("GET /profil mit id " + idString);
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "userDetail" hinzugefügt. itemDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			User u = userRepo.getById(id);
			model.put("userDetail", u);
		}
		
		model.put("session_user", username);
		model.put("postAction", "users/update");
		
		//das Template userDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "profil");
	}
}
