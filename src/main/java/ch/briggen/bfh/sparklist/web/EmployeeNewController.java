package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.RoleRepository;
import ch.briggen.bfh.sparklist.domain.User2Project;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import ch.briggen.bfh.sparklist.domain.UserProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class EmployeeNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(EmployeeNewController.class);
		
	private UserRepository userRepo = new UserRepository();
	private RoleRepository roleRepo = new RoleRepository();
	private UserProjectRepository u2pRepo = new UserProjectRepository();
	
	/**
	 * Erstellt ein neuen Projektmitarbeiter in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /projektsteckbrief)
	 * 
	 * Hört auf POST /mitarbeiter/new
	 * 
	 * @return Redirect zurück zum Projektsteckbrief
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		User2Project employeeDetail = EmployeeWebHelper.employeeFromWeb(request);
		log.trace("POST /mitarbeiter/new mit detail " + employeeDetail);
		
		try
		{
			employeeDetail.validate();
			List<User2Project> employees = new ArrayList<>(u2pRepo.getAll()).stream().filter(e -> e.getUser().getUsername() == employeeDetail.getUser().getUsername() && e.getProject().getProjektname() == employeeDetail.getProject().getProjektname()).collect(Collectors.toList());
			
			if (employees.size() > 0)
			{
				throw new IllegalArgumentException("Kombination Username und Projekt gibt es bereits schon.");
			}
			u2pRepo.insert(employeeDetail);
			//der redirect erfolgt auf /projectsteckbrief
			response.redirect("/projektsteckbrief?id=" + employeeDetail.getProject().getId());
			return null;
		}
		catch (IllegalArgumentException e)
		{
			HashMap<String, Object> model = new HashMap<String, Object>();
			// Beim Ausfüllen ist ein Fehler aufgetreten
			model.put("roleList", roleRepo.getAll());
			model.put("projectId", request.queryParams("employeeDetail.project"));
			model.put("userList", userRepo.getAll());
			model.put("error", e.getMessage());
			model.put("postAction", "/mitarbeiter/new");
			model.put("employeeDetail", employeeDetail);
			
			String username = request.session().attribute("USERNAME");
			model.put("session_user", username);
			
			return new ModelAndView(model, "mitarbeiter_erfassen");
		}
	}
}
