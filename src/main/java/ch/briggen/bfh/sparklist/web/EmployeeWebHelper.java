package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.RoleRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.User2Project;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.Request;

public class EmployeeWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(EmployeeWebHelper.class);
	
	private static ProjectRepository projectRepo = new ProjectRepository();
	private static RoleRepository roleRepo = new RoleRepository();
	private static UserRepository userRepo = new UserRepository();
	
	public static User2Project employeeFromWeb(Request request)
	{
		Role r = null;
		Project p = null;
		User u = null;
		String role = request.queryParams("employeeDetail.role");
		String user = request.queryParams("employeeDetail.user");
		String project = request.queryParams("employeeDetail.project");
		if (NumberUtils.isNumber(role))
		{
			r = roleRepo.getById(Long.parseLong(role));
		}
		else
		{
			List<Role> rList = new ArrayList<>(roleRepo.getByName(role));
			if (!rList.isEmpty())
			{
				r = rList.get(0);
			}
		}
		
		if (NumberUtils.isNumber(project))
		{
			p = projectRepo.getById(Long.parseLong(project));
		}
		else
		{
			List<Project> pList = new ArrayList<>(projectRepo.getByName(project));
			if (!pList.isEmpty())
			{
				p = pList.get(0);
			}
		}
		
		if (NumberUtils.isNumber(user))
		{
			u = userRepo.getById(Long.parseLong(user));
		}
		else
		{
			List<User> uList = new ArrayList<>(userRepo.getByName(user));
			if (!uList.isEmpty())
			{
				u = uList.get(0);
			}
		}
			
		return new User2Project(u, p, r);
	}
}
