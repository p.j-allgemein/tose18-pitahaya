package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Projectrisk;
import ch.briggen.bfh.sparklist.domain.ProjectriskRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectriskDeleteController  implements TemplateViewRoute {
private final Logger log = LoggerFactory.getLogger(ProjectriskDeleteController.class);
	
	private ProjectriskRepository repo = new ProjectriskRepository();
	

	/**
	 * Löscht das Projektrisiko mit der übergebenen id in der Datenbank
	 * /projektrisiko/delete&id=987 löscht das Projektrisiko mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /projektrisiko/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /projectrisiko/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		Long projectId = null;
		Projectrisk p = repo.getById(longId);
		if (p != null && p.getProject() != null)
		{
			projectId = p.getProject().getId();
		}
		repo.delete(longId);
		response.redirect("/projektsteckbrief?id=" + projectId);
		return null;
	}
}
