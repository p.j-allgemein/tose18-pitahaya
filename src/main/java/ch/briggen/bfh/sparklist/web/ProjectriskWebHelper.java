package ch.briggen.bfh.sparklist.web;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Projectrisk;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.Request;

public class ProjectriskWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjectriskWebHelper.class);
	
	private static ProjectRepository projectRepo = new ProjectRepository();
	private static UserRepository userRepo = new UserRepository();
	
	public static Projectrisk projectriskFromWeb(Request request)
	{
		try
		{
			long id = 0;
			long pr = 0;
			double ew = 0.0;
			double aw = 0.0;
			NumberFormat format = NumberFormat.getInstance(new Locale("de", "CH"));
			String username = request.queryParams("projectrisikoDetail.verantwortlicher");
			List<User> u = new ArrayList<>();
			if (NumberUtils.isNumber(username))
			{
				u = Arrays.asList(userRepo.getById(Long.parseLong(username)));
			}
			else
			{
				u = new ArrayList<>(userRepo.getByName(username));
			}
			String projectname = request.queryParams("projectrisikoDetail.projektname");
			List<Project> p = new ArrayList<>();
			if (NumberUtils.isNumber(projectname))
			{
				p = Arrays.asList(projectRepo.getById(Long.parseLong(projectname)));
			}
			else
			{
				p = new ArrayList<>(projectRepo.getByName(projectname));
			}
			
			if (request.queryParams("projectrisikoDetail.id") != null)
			{
				Number number = format.parse(request.queryParams("projectrisikoDetail.id"));
				id = number.longValue();
			}
			if (request.queryParams("projectrisikoDetail.prioritaet") != null)
			{
				Number number = format.parse(request.queryParams("projectrisikoDetail.prioritaet"));
				pr = number.longValue();
			}
			if (request.queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit") != null)
			{
				Number number = format.parse(request.queryParams("projectrisikoDetail.eintrittswahrscheinlichkeit"));
				ew = number.doubleValue();
			}
			if (request.queryParams("projectrisikoDetail.auswirkungen") != null)
			{
				Number number = format.parse(request.queryParams("projectrisikoDetail.auswirkungen"));
				aw = number.doubleValue();
			}
			return new Projectrisk(id, request.queryParams("projectrisikoDetail.risikoname"),
								   request.queryParams("projectrisikoDetail.ursache"), 
								   request.queryParams("projectrisikoDetail.gefaehrdungsart"), ew,
								   aw, request.queryParams("projectrisikoDetail.massnahmen"),
								   !u.isEmpty() ? u.get(0) : null, pr, !p.isEmpty() ? p.get(0) : null);
		}
		catch (ParseException e)
		{
			throw new IllegalArgumentException (e.getMessage());
		}
	}
}
