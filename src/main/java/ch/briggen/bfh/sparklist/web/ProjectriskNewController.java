package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Projectrisk;
import ch.briggen.bfh.sparklist.domain.ProjectriskRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectriskNewController implements TemplateViewRoute {
	private final Logger log = LoggerFactory.getLogger(ProjectriskNewController.class);
	
	private ProjectriskRepository repo = new ProjectriskRepository();
	private UserRepository userRepo = new UserRepository();
	
	/**
	 * Erstellt ein neues Projektrisiko in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /projektrisiko/&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /projektrisiko/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Projectrisk projectrisikoDetail = ProjectriskWebHelper.projectriskFromWeb(request);
		log.trace("POST /projektrisiko/new mit projektrisikoDetail " + projectrisikoDetail);
		
		try
		{
			if (projectrisikoDetail.getVerantwortlicher() == null)
			{
				throw new IllegalArgumentException("Verantwortlicher " + request.queryParams("projectrisikoDetail.verantwortlicher") + " wurde nicht gefunden");
			}
			
			if (projectrisikoDetail.getProject() == null)
			{
				throw new IllegalArgumentException("Projekt " + request.queryParams("projectrisikoDetail.projektname") + " wurde nicht gefunden");
			}
			
			projectrisikoDetail.validate();
			//insert gibt die von der DB erstellte id zurück.
			repo.insert(projectrisikoDetail);
			
			//die neue Id wird dem Redirect als Parameter hinzugefügt
			//der redirect erfolgt dann auf /projektrisiko?id=432932
			response.redirect("/projektsteckbrief?id=" + projectrisikoDetail.getProject().getId());
			return null;
		}
		catch (IllegalArgumentException e)
		{
			HashMap<String, Object> model = new HashMap<String, Object>();
			// Beim Ausfüllen ist ein Fehler aufgetreten
			model.put("projectrisikoDetail", projectrisikoDetail);
			model.put("error", e.getMessage());
			model.put("userDetail", userRepo.getAll());
			model.put("projektname", request.queryParams("projectrisikoDetail.projektname"));
			String username = request.session().attribute("USERNAME");
			model.put("session_user", username);
			return new ModelAndView(model, "projektrisiko");
		}
	}
}
