package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.RoleRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class UserUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(UserUpdateController.class);
	
	private RoleRepository roleRepo = new RoleRepository();
	private UserRepository userRepo = new UserRepository();
	


	/**
	 * Schreibt den geänderten User zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/users) mit der User-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /users/update
	 * 
	 * @return redirect nach /profil: via Browser wird /profil aufgerufen.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		User userDetail = UserWebHelper.userFromWeb(request);
		
		log.trace("POST /users/update mit userDetail " + userDetail);
		
		try
		{
			userDetail.validate();
		}
		catch (IllegalArgumentException e)
		{
			HashMap<String, Object> model = new HashMap<String, Object>();
			model.put("postAction", "/users/update");
			model.put("error", e.getMessage());
			model.put("userDetail", userDetail);
			model.put("roleList", roleRepo.getAll());
			String username = request.session().attribute("USERNAME");
			model.put("session_user", username);
			return new ModelAndView(model, "registration");
		}
		
		//Speichern des Users in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /users&id=3 (wenn userDetail.getId == 3 war)
		userRepo.save(userDetail);
		response.redirect("/profil?id="+userDetail.getId());
		return null;
	}
}
