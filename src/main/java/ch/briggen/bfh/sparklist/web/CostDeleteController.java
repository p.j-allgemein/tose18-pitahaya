package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Cost;
import ch.briggen.bfh.sparklist.domain.CostRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class CostDeleteController implements TemplateViewRoute {
	private final Logger log = LoggerFactory.getLogger(WorkpackageDeleteController.class);
	
	private CostRepository repo = new CostRepository();
	

	/**
	 * Löscht die Kosten mit der übergebenen id in der Datenbank
	 * /kosten/delete&id=987 löscht das Workpackage mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /kosten/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zum Projektsteckbrief
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /kosten/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		Cost c = repo.getById(longId);
		repo.delete(longId);
		if (c != null)
		{
			response.redirect("/projektsteckbrief?id=" + c.getWorkpackage().getProject().getId());
		}
		else
		{
			response.redirect("/projekte");
		}
		return null;
	}
}
