package ch.briggen.bfh.sparklist.web;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Cost;
import ch.briggen.bfh.sparklist.domain.CostRepository;
import ch.briggen.bfh.sparklist.domain.EarnedValueAnalysis;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Projectphase;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.Projectrisk;
import ch.briggen.bfh.sparklist.domain.ProjectriskRepository;
import ch.briggen.bfh.sparklist.domain.Projectstatus;
import ch.briggen.bfh.sparklist.domain.ProjectstatusRepository;
import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.RoleRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.User2Project;
import ch.briggen.bfh.sparklist.domain.UserProjectRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import ch.briggen.bfh.sparklist.domain.Workpackage;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectReadDetailController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectReadDetailController.class);
	
	private CostRepository costRepo = new CostRepository();
	private ProjectRepository projectRepo = new ProjectRepository();
	private ProjectphaseRepository phasenRepo = new ProjectphaseRepository();
	private ProjectriskRepository riskRepo = new ProjectriskRepository();
	private ProjectstatusRepository statusRepo = new ProjectstatusRepository();
	private RoleRepository roleRepo = new RoleRepository();
	private UserRepository userRepo = new UserRepository();
	private UserProjectRepository employeeRepo = new UserProjectRepository();
	private WorkpackageRepository workRepo = new WorkpackageRepository();
	private LocalDate currentDate = LocalDate.now();
	
	public ProjectReadDetailController()
	{
	}
	
	public ProjectReadDetailController(LocalDate currentDate)
	{
		this();
		this.currentDate = currentDate;
	}
	
	/**
	 * Requesthandler zum Lesen von Projekt-Details. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Projekt erstellt (Aufruf von /projects/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars dar Project mit der übergebenen id upgedated (Aufruf /project/update)
	 * Hört auf GET /projektsteckbrief
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "projektsteckbrief" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		log.trace(request.headers().toString());
		log.trace(request.headers("Accept"));
				
		if(null == idString || "0".equals(idString))
		{
			log.trace("GET /projektsteckbrief fehlende id " + idString);
			model.put("error", "projekt-id unbekannt bzw. nicht mitgegeben");
		}
		else
		{
			log.trace("GET /projektsteckbrief mit id " + idString);
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "projectDetail" hinzugefügt. projectDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Project p = projectRepo.getById(id);
			if (p != null)
			{
				// falls das Projekt gefunden wurde, werden noch weitere Angaben zum Projekt geladen und mitgegeben
				// Dies sind Projektrisiken, Stakeholder und Projektmitarbeiter
				List<Projectrisk> risks = new ArrayList<>(riskRepo.getByProject(p.getProjektname()));
				model.put("projectrisikoDetail", risks);
				List<User2Project> projectEmpl = new ArrayList<>(employeeRepo.getByProjectname(p.getProjektname()));
				List<Role> roles = new ArrayList<>(roleRepo.getByName("Stakeholder"));
				projectEmpl = projectEmpl.stream().filter(e -> e.getRole() != null ? e.getRole().getId() != roles.get(0).getId() : false).collect(Collectors.toList());
				model.put("userDetail", projectEmpl);
				List<User2Project> projectStakeholder = new ArrayList<>(employeeRepo.getByRole("Stakeholder"));
				List<User> stakeholders = projectStakeholder.stream().filter(e -> {
					if (e.getProject() != null && e.getProject().getId() == p.getId())
					{
						return true;
					}
					
					return false;
				}).map(User2Project::getUser).collect(Collectors.toCollection(ArrayList::new));
				model.put("stakeholder", stakeholders);
				List<Projectstatus> status = new ArrayList<>(statusRepo.getByProject(p.getProjektname()));
				if (status.isEmpty())
				{
					status.add(new Projectstatus(0, p, "Grün", "Grün", "Grün", "Grün", "Grün"));
				}
				model.put("projektstatus", status.get(0));
				model.put("wpKosten", generateListOfCostsPerWorkpackage(p.getId()));
				model.put("evaProject", generateEarnedValueProject(p));
				model.put("evaPhasen", generateEarnedValuePerProjectphase(p));
				model.put("evaWP", generateEarnedValuePerWorkpackage(p));
				log.trace("wpKosten: " + model.get("wpKosten").toString());
			}
			model.put("postAction", "/project/update");
			model.put("projectDetail", p);
			model.put("projektphasen", phasenRepo.getAll());
		}
		
		List<User> userList = new ArrayList<>(userRepo.getByRolename("Projektleiter"));
		log.trace("found " + userList.size() + " user with rolename \"Projektleiter\"");
		
		String username = request.session().attribute("USERNAME");
		model.put("session_user", username);
		model.put("plList", userList);
		
		//das Template projectDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "projektsteckbrief");
	}
	
	private Map<String, List<Cost>> generateListOfCostsPerWorkpackage(long projectId)
	{
		List<Cost> costs = new ArrayList<>(costRepo.getAll());
		costs = costs.stream().filter(e -> (e.getWorkpackage() != null && e.getWorkpackage().getProject() != null && e.getWorkpackage().getProject().getId() == projectId)).collect(Collectors.toList());
		return costs.stream().collect(Collectors.groupingBy(cost -> cost.getWorkpackage().getWorkpackagename()));
	}
	
	/**
	 * Berechnet die Werte für PlannedValue, EarnedValue und ActualCost für die Workpackages des Projektes
	 * @param project das Projekt, welches die Analyse gemacht wird.
	 * @return Eine Map für jedes Workpackage eine EarnedValueAnalysis
	 */
	private Map<String, EarnedValueAnalysis> generateEarnedValuePerWorkpackage(Project project)
	{
		Map<String, EarnedValueAnalysis> map = new HashMap<>();
		
		List<Workpackage> wps = new ArrayList<>(workRepo.getByProject(Long.toString(project.getId())));
		Map<String, List<Cost>> costs = generateListOfCostsPerWorkpackage(project.getId());
		List<Cost> flatCost = costs.values().stream().collect(ArrayList::new, List::addAll, List::addAll);
		
		log.trace(wps.toString());
		
		wps.forEach(e -> {
			BigDecimal days = BigDecimal.valueOf(ChronoUnit.DAYS.between(e.getStartdate(), currentDate));
			BigDecimal totalDays = BigDecimal.valueOf(ChronoUnit.DAYS.between(e.getStartdate(), e.getEnddate()));
			BigDecimal difference = days.divide(totalDays, 2, BigDecimal.ROUND_HALF_UP);
			EarnedValueAnalysis eva = new EarnedValueAnalysis();
			eva.setName(e.getProjectphase().getPhasenname());
			eva.setBudget(e.getPlannedValue());
			eva.setPlannedValue(e.getPlannedValue());
			eva.setEarnedValue(e.isDone() ? e.getEarnedValue() : BigDecimal.valueOf(e.getPlannedValue()).multiply(difference).doubleValue());
			eva.setActualCost(flatCost.stream().filter(c -> e.getId() == c.getWorkpackage().getId()).map(Cost::getBetrag).reduce(0.0, Double::sum));
			eva.setDegreeOfCompletion((BigDecimal.valueOf(e.getPlannedValue()).multiply(difference).doubleValue()));
			if (eva.getActualCost() > 0.0)
			{
				eva.setCostEfficency(BigDecimal.valueOf(eva.getDegreeOfCompletion()).divide(BigDecimal.valueOf(eva.getActualCost()), 2, BigDecimal.ROUND_HALF_UP).doubleValue());
				eva.setCostDifference(eva.getDegreeOfCompletion() - eva.getActualCost());
			}
			eva.setPlanDifference(eva.getDegreeOfCompletion() - eva.getPlannedValue());
			map.put(e.getWorkpackagename(), eva);
		});
		return map;
	}
	
	/**
	 * Berechnet die Werte für PlannedValue, EarnedValue und ActualCost für die Phasen des Projektes
	 * @param project das Projekt, welches die Analyse gemacht wird.
	 * @return Eine Map für jede Phase eine EarnedValueAnalysis
	 */
	private Map<String, EarnedValueAnalysis> generateEarnedValuePerProjectphase(Project project)
	{
		Map<String, EarnedValueAnalysis> map = new HashMap<>();
		Map<Long, LocalDate> lowestPhaseStart = new HashMap<>();
		Map<Long, LocalDate> highestPhaseEnd = new HashMap<>();
		List<Workpackage> wps = new ArrayList<>(workRepo.getByProject(Long.toString(project.getId())));
		
		Map<String, List<Cost>> costs = generateListOfCostsPerWorkpackage(project.getId());
		List<Cost> flatCost = costs.values().stream().collect(ArrayList::new, List::addAll, List::addAll);
		
		List<Projectphase> phases = new ArrayList<>(phasenRepo.getAll());
		
		wps.forEach(e -> {
			Projectphase phase = phases.stream().filter(p -> e.getProjectphase().getId() == p.getId()).findFirst().get();
			lowestPhaseStart.computeIfAbsent(phase.getId(), k -> LocalDate.now());
			highestPhaseEnd.computeIfAbsent(phase.getId(), k -> LocalDate.now());
			LocalDate current = lowestPhaseStart.get(phase.getId());
			if (current.compareTo(e.getStartdate()) > 0)
			{
				lowestPhaseStart.put(phase.getId(), e.getStartdate());
			}
			
			current = highestPhaseEnd.get(phase.getId());
			if (current.compareTo(e.getEnddate()) < 0)
			{
				highestPhaseEnd.put(phase.getId(), e.getEnddate());
			}
		});
		
		log.trace(lowestPhaseStart.toString());
		log.trace(highestPhaseEnd.toString());
		
		phases.forEach(e -> {
			BigDecimal days = BigDecimal.ZERO;
			BigDecimal totalDays = BigDecimal.ZERO;
			if (lowestPhaseStart.containsKey(e.getId()))
			{
				days = BigDecimal.valueOf(ChronoUnit.DAYS.between(lowestPhaseStart.get(e.getId()), currentDate));
			}
			else
			{
				days = BigDecimal.valueOf(ChronoUnit.DAYS.between(project.getProjektstart(), currentDate));
			}
			
			if (highestPhaseEnd.containsKey(e.getId()))
			{
				totalDays = BigDecimal.valueOf(ChronoUnit.DAYS.between(lowestPhaseStart.get(e.getId()), highestPhaseEnd.get(e.getId())));
			}
			else
			{
				totalDays = BigDecimal.valueOf(ChronoUnit.DAYS.between(project.getProjektstart(), project.getProjektende()));
			}
			BigDecimal difference = days.divide(totalDays, 2, BigDecimal.ROUND_HALF_UP);
			
			EarnedValueAnalysis eva = new EarnedValueAnalysis();
			eva.setName(e.getPhasenname());
			eva.setBudget(project.getProjektbudget());
			eva.setPlannedValue((BigDecimal.valueOf(wps.stream().filter(w -> w.getProjectphase().getId() == e.getId()).map(Workpackage::getPlannedValue).reduce(0.0, Double::sum)).multiply(difference).doubleValue()));
			eva.setEarnedValue(wps.stream().filter(w -> w.getProjectphase().getId() == e.getId()).map(w -> {
				if (w.isDone())
				{
					return w.getEarnedValue();
				}
				else
				{
					return BigDecimal.valueOf(w.getPlannedValue()).multiply(difference).doubleValue();
				}
			}).reduce(0.0, Double::sum));
			eva.setActualCost(flatCost.stream().filter(w -> w.getWorkpackage().getProjectphase().getId() == e.getId()).map(Cost::getBetrag).reduce(0.0, Double::sum));
			eva.setDegreeOfCompletion(BigDecimal.valueOf(project.getProjektbudget()).multiply(difference).doubleValue());
			if (eva.getActualCost() > 0.0)
			{
				eva.setCostEfficency(BigDecimal.valueOf(eva.getDegreeOfCompletion()).divide(BigDecimal.valueOf(eva.getActualCost()), 2, BigDecimal.ROUND_HALF_UP).doubleValue());
				eva.setCostDifference(eva.getDegreeOfCompletion() - eva.getActualCost());
			}
			eva.setPlanDifference(eva.getDegreeOfCompletion() - eva.getPlannedValue());
			map.put(e.getPhasenname(), eva);
		});
		return map;
	}
	
	/**
	 * Berechnet die Werte für PlannedValue, EarnedValue und ActualCost für das entsprechende Projekt
	 * @param project das Projekt, welches die Analyse gemacht wird.
	 * @return Ein Objekt vom Type EarnedValueAnalysis
	 */
	private EarnedValueAnalysis generateEarnedValueProject(Project project)
	{
		EarnedValueAnalysis eva = new EarnedValueAnalysis();
		BigDecimal days = BigDecimal.valueOf(ChronoUnit.DAYS.between(project.getProjektstart(), currentDate));
		BigDecimal totalDays = BigDecimal.valueOf(ChronoUnit.DAYS.between(project.getProjektstart(), project.getProjektende()));
		BigDecimal difference = days.divide(totalDays, 2, BigDecimal.ROUND_HALF_UP);
		
		log.trace("difference: " + difference.doubleValue());
		
		List<Workpackage> wps = new ArrayList<>(workRepo.getByProject(Long.toString(project.getId())));
		Map<String, List<Cost>> costs = generateListOfCostsPerWorkpackage(project.getId());
		List<Cost> flatCost = costs.values().stream().collect(ArrayList::new, List::addAll, List::addAll);
		
		eva.setName(project.getProjektname());
		eva.setBudget(project.getProjektbudget());
		eva.setPlannedValue((BigDecimal.valueOf(wps.stream().map(Workpackage::getPlannedValue).reduce(0.0, Double::sum)).multiply(difference)).doubleValue());
		eva.setEarnedValue(wps.stream().map(e -> {
			if (e.isDone())
			{
				return e.getEarnedValue();
			}
			else
			{
				return BigDecimal.valueOf(e.getPlannedValue()).multiply(difference).doubleValue();
			}
		}).reduce(0.0, Double::sum));
		eva.setActualCost(flatCost.stream().map(Cost::getBetrag).reduce(0.0, Double::sum));
		eva.setDegreeOfCompletion(BigDecimal.valueOf(project.getProjektbudget()).multiply(difference).doubleValue());
		if (eva.getActualCost() > 0.0)
		{
			eva.setCostEfficency(BigDecimal.valueOf(eva.getDegreeOfCompletion()).divide(BigDecimal.valueOf(eva.getActualCost()), 2, BigDecimal.ROUND_HALF_UP).doubleValue());
			eva.setCostDifference(eva.getDegreeOfCompletion() - eva.getActualCost());
		}
		eva.setPlanDifference(eva.getDegreeOfCompletion() - eva.getPlannedValue());
		log.debug("eva: " + eva.toString());
		return eva;
	}
}
