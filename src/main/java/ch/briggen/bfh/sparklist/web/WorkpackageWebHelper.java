package ch.briggen.bfh.sparklist.web;

import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Projectphase;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.Workpackage;
import spark.Request;

public class WorkpackageWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(WorkpackageWebHelper.class);
	
	private static ProjectRepository projectRepo = new ProjectRepository();
	private static ProjectphaseRepository phaseRepo = new ProjectphaseRepository();
	
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	public static Workpackage workpackageFromWeb(Request request)
	{
		try
		{
			long id = 0;
			double pv = 0.0;
			double ev = 0.0;
			NumberFormat format = NumberFormat.getInstance(new Locale("de", "CH"));
			String projectname = request.queryParams("workpackageDetail.projektname");
			String phasenname = request.queryParams("workpackageDetail.phasenname");
			List<Projectphase> u = new ArrayList<>();
			if (NumberUtils.isNumber(phasenname))
			{
				u = Arrays.asList(phaseRepo.getById(Long.parseLong(phasenname)));
			}
			else
			{
				u = new ArrayList<>(phaseRepo.getByName(phasenname));
			}
			
			List<Project> p = new ArrayList<>();
			if (NumberUtils.isNumber(projectname))
			{
				p = Arrays.asList(projectRepo.getById(Long.parseLong(projectname)));
			}
			else
			{
				p = new ArrayList<>(projectRepo.getByName(projectname));
			}
			
			if (request.queryParams("workpackageDetail.id") != null)
			{
				Number number = format.parse(request.queryParams("workpackageDetail.id"));
				id = number.longValue();
			}
			if (request.queryParams("workpackageDetail.plannedValue") != null)
			{
				Number number = format.parse(request.queryParams("workpackageDetail.plannedValue"));
				pv = number.doubleValue();
			}
			if (request.queryParams("workpackageDetail.earnedValue") != null)
			{
				Number number = format.parse(request.queryParams("workpackageDetail.earnedValue"));
				ev = number.doubleValue();
			}
			Boolean done = BooleanUtils.toBooleanObject(request.queryParams("workpackageDetail.done"), "true", "false", "false");
			LocalDate start = request.queryParams("workpackageDetail.startdate") != null && !request.queryParams("workpackageDetail.startdate").isEmpty() ? LocalDate.parse(request.queryParams("workpackageDetail.startdate"), formatter) : null;
			LocalDate ende = request.queryParams("workpackageDetail.enddate") != null && !request.queryParams("workpackageDetail.enddate").isEmpty() ? LocalDate.parse(request.queryParams("workpackageDetail.enddate"), formatter) : null;
			return new Workpackage(id, !p.isEmpty() ? p.get(0) : null, !u.isEmpty() ? u.get(0) : null,
								   request.queryParams("workpackageDetail.workpackagename"), pv, ev, start, ende, done);
		}
		catch (ParseException e)
		{
			throw new IllegalArgumentException (e.getMessage());
		}
	}
}
