package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.RoleRepository;
import ch.briggen.bfh.sparklist.domain.User2Project;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class EmployeeEditController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(EmployeeEditController.class);
	
	private UserRepository userRepo = new UserRepository();
	private RoleRepository roleRepo = new RoleRepository();
	
	/**
	 * Requesthandler zum Erfassen eines Projektmitarbeiters. 
	 * Liefert das Formular (bzw. Template) zum Erfassen der einzelnen Felder
	 * Ein Update bei den Projektmitarbeiter gibt es nicht, daher wird immer /mitarbeiter/new zurückgegeben
	 * Hört auf GET /mitarbeiter_erfassen
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "mitarbeiter_erfassen" .
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("projectId");
		log.trace("GET /mitarbeiter_erfassen für INSERT Projektmitarbeiter");
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("roleList", roleRepo.getAll());
		model.put("userList", userRepo.getAll());
		model.put("postAction", "/mitarbeiter/new");
		model.put("projectId", idString);
		model.put("employeeDetail", new User2Project());
		
		String username = request.session().attribute("USERNAME");
		model.put("session_user", username);
		
		//das Template mitarbeiter_erfassen verwenden und dann "anzeigen".
		return new ModelAndView(model, "mitarbeiter_erfassen");
	}
}
