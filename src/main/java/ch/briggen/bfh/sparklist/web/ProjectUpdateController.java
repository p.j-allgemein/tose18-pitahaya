package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectUpdateController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectUpdateController.class);
		
	private ProjectRepository projectRepo = new ProjectRepository();
	private ProjectphaseRepository phaseRepo = new ProjectphaseRepository();
	private UserRepository userRepo = new UserRepository();

	/**
	 * Schreibt das geänderte Projekt zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/projektsteckbrief) mit der Projekt-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /project/update
	 * 
	 * @return redirect nach /projekte: via Browser wird /projekte aufgerufen.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		log.trace("queryParams " + request.queryParams().toString());
		Project projectDetail = ProjectWebHelper.projectFromWeb(request);
		
		try
		{
			if (projectDetail.getId() == 0)
			{
				log.trace("projectDetail.getId == 0");
				log.trace("projectDetail: " + projectDetail.toString());
				HashMap<String, Object> model = new HashMap<String, Object>();
				model.put("postAction", "/project/update");
				model.put("error", "ProjectDetail ist leer, scheint ein Fehler zu sein");
				model.put("projectDetail", projectDetail);
				List<User> userList = new ArrayList<>(userRepo.getByRolename("Projektleiter"));
				log.trace("found " + userList.size() + " user with rolename \"Projektleiter\"");
				
				String username = request.session().attribute("USERNAME");
				model.put("session_user", username);
				model.put("userDetail", userList);
				model.put("phaseDetail", phaseRepo.getAll());
				return new ModelAndView(model, "projekt_erfassen");
			}
			
			log.trace("POST /projects/update mit projectDetail " + projectDetail);
			
			//Speichern des Projektes in dann den Parameter für den Redirect abfüllen
			//der Redirect erfolgt dann z.B. auf /projekte&id=3 (wenn projectDetail.getId == 3 war)
			projectRepo.save(projectDetail);
			response.redirect("/projekte");
			return null;
		}
		catch (IllegalArgumentException e)
		{
			HashMap<String, Object> model = new HashMap<String, Object>();
			// Beim Ausfüllen ist ein Ferhl aufgetreten
			model.put("postAction", "/project/update");
			model.put("projectDetail", projectDetail);
			model.put("error", e.getMessage());
			List<User> userList = new ArrayList<>(userRepo.getByRolename("Projektleiter"));
			log.trace("found " + userList.size() + " user with rolename \"Projektleiter\"");
			
			String username = request.session().attribute("USERNAME");
			model.put("session_user", username);
			model.put("userDetail", userList);
			model.put("phaseDetail", phaseRepo.getAll());
			return new ModelAndView(model, "projekt_erfassen");
		}
	}
}
