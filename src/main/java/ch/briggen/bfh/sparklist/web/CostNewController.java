package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Cost;
import ch.briggen.bfh.sparklist.domain.CostRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class CostNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(CostNewController.class);
	
	private CostRepository repo = new CostRepository();
	private UserRepository userRepo = new UserRepository();
	private WorkpackageRepository workRepo = new WorkpackageRepository();
	
	/**
	 * Erstellt ein neues Workpackage in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /workpackage&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /workpackage/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Cost costDetail = CostWebHelper.costFromWeb(request);
		log.trace("POST /kosten/new mit costDetail " + costDetail);
		
		try
		{
			if (costDetail.getWorkpackage() == null)
			{
				throw new IllegalArgumentException("Workpackage " + request.queryParams("costDetail.workpackagename") + " wurde nicht gefunden");
			}
			
			if (costDetail.getUser() == null)
			{
				throw new IllegalArgumentException("User " + request.queryParams("costDetail.username") + " wurde nicht gefunden");
			}
			
			
			costDetail.validate();
			//insert gibt die von der DB erstellte id zurück.
			repo.insert(costDetail);
			
			//die neue Id wird dem Redirect als Parameter hinzugefügt
			//der redirect erfolgt dann auf /projektsteckbrief?id=432932
			response.redirect("/projektsteckbrief?id=" + costDetail.getWorkpackage().getProject().getId());
			return null;
		}
		catch (IllegalArgumentException e)
		{
			HashMap<String, Object> model = new HashMap<String, Object>();
			// Beim Ausfüllen ist ein Fehler aufgetreten
			model.put("postAction", "/kosten/new");
			model.put("costDetail", costDetail);
			model.put("error", e.getMessage());
			
			model.put("projektId", request.queryParams("projectId"));
			model.put("userList", userRepo.getAll());
			model.put("workpackageList", workRepo.getByProject(request.queryParams("projectId")));
			
			String username = request.session().attribute("USERNAME");
			model.put("session_user", username);
			
			return new ModelAndView(model, "kosten_erfassen");
		}
	}
}
