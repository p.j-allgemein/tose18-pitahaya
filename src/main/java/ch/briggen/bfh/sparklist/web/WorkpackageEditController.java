package ch.briggen.bfh.sparklist.web;

import java.util.Arrays;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Done;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.Workpackage;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class WorkpackageEditController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(WorkpackageEditController.class);
	
	private ProjectRepository projectRepo = new ProjectRepository();
	private ProjectphaseRepository phaseRepo = new ProjectphaseRepository();
	private WorkpackageRepository repo = new WorkpackageRepository();
	
	/**
	 * Requesthandler zum Bearbeiten eines Workpackage. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neuer Workpackage erstellt (Aufruf von /workpackage/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars dar Workpackage mit der übergebenen id upgedated (Aufruf /workpackage/update)
	 * Hört auf GET /workpackage_erfassen
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "workpackage_erfassen" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("phaseDetail", phaseRepo.getAll());
		model.put("projectList", projectRepo.getAll());
		model.put("projectId", request.queryParams("projectId"));
				
		if(null == idString || "0".equals(idString))
		{
			log.trace("GET /workpackage_erfassen für INSERT mit id " + idString);
			//der Submit-Button ruft /workpackage/new auf --> INSERT
			model.put("postAction", "/workpackage/new");
			model.put("workpackageDetail", new Workpackage());
		}
		else
		{
			log.trace("GET /workpackage_erfassen für UPDATE mit id " + idString);
			//der Submit-Button ruft /workpackage/update auf --> UPDATE
			model.put("postAction", "/workpackage/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "workpackageDetail" hinzugefügt. workpackageDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Workpackage w = repo.getById(id);
			model.put("workpackageDetail", w);
		}
		
		String username = request.session().attribute("USERNAME");
		model.put("session_user", username);
		model.put("doneList", Arrays.asList(new Done("false"), new Done("true")));
		
		//das Template workpackage_erfassen verwenden und dann "anzeigen".
		return new ModelAndView(model, "workpackage_erfassen");
	}

}
