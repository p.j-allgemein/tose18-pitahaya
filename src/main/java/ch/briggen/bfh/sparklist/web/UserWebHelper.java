package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Role;
import ch.briggen.bfh.sparklist.domain.RoleRepository;
import ch.briggen.bfh.sparklist.domain.User;
import spark.Request;

public class UserWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(UserWebHelper.class);
	
	private static RoleRepository roleRepo = new RoleRepository();
	
	public static User userFromWeb(Request request)
	{
		Role r = null;
		String role = request.queryParams("userDetail.role");
		if (NumberUtils.isNumber(role))
		{
			r = roleRepo.getById(Long.parseLong(role));
		}
		else
		{
			List<Role> rList = new ArrayList<>(roleRepo.getByName(role));
			if (rList != null && !rList.isEmpty())
			{
				r = rList.get(0);
			}
		}
		
		long l = request.queryParams("userDetail.id") != null ? Long.parseLong(request.queryParams("userDetail.id")) : 0;
		return new User(l,
					    request.queryParams("userDetail.username"),
					    request.queryParams("userDetail.name"),
					    request.queryParams("userDetail.vorname"),
					    r != null ? r : null, request.queryParams("userDetail.abteilung"),
					    request.queryParams("userDetail.mail"),
					    request.queryParams("userDetail.telefon"));
	}
}
