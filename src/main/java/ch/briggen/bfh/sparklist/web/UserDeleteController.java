package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class UserDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(UserDeleteController.class);
		
	private UserRepository userRepo = new UserRepository();
	

	/**
	 * Löscht den User mit der übergebenen id in der Datenbank
	 * /users/delete&id=987 löscht den User mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /users/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /users/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		userRepo.delete(longId);
		response.redirect("/users");
		return null;
	}

}
