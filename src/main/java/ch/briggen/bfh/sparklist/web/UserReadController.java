package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class UserReadController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(UserReadController.class);

	UserRepository repository = new UserRepository();

	/**
	 *Liefert die Liste die Seite als /profil zurück
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		HashMap<String, Object> model = new HashMap<String, Object>();
		String username = request.session().attribute("USERNAME");
		model.put("session_user", username);
		model.put("list", repository.getAll());
		return new ModelAndView(model, "profil");
	}
}
