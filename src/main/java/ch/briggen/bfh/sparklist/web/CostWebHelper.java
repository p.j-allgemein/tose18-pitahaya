package ch.briggen.bfh.sparklist.web;

import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Cost;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import ch.briggen.bfh.sparklist.domain.Workpackage;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import spark.Request;

public class CostWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(CostWebHelper.class);
	
	private static UserRepository userRepo = new UserRepository();
	private static WorkpackageRepository workRepo = new WorkpackageRepository();
	
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	public static Cost costFromWeb(Request request)
	{
		try
		{
			long id = 0;
			double betrag = 0.0;
			NumberFormat format = NumberFormat.getInstance(new Locale("de", "CH"));
			String workpackagename = request.queryParams("costDetail.workpackagename");
			String username = request.queryParams("costDetail.username");
			List<User> u = new ArrayList<>();
			if (NumberUtils.isNumber(username))
			{
				u = Arrays.asList(userRepo.getById(Long.parseLong(username)));
			}
			else
			{
				u = new ArrayList<>(userRepo.getByName(username));
			}
			
			List<Workpackage> w = new ArrayList<>();
			if (NumberUtils.isNumber(workpackagename))
			{
				w = Arrays.asList(workRepo.getById(Long.parseLong(workpackagename)));
			}
			else
			{
				w = new ArrayList<>(workRepo.getByName(workpackagename));
			}
			
			if (request.queryParams("costDetail.id") != null)
			{
				Number number = format.parse(request.queryParams("costDetail.id"));
				id = number.longValue();
			}
			if (request.queryParams("costDetail.betrag") != null)
			{
				Number number = format.parse(request.queryParams("costDetail.betrag"));
				betrag = number.doubleValue();
			}
			LocalDate datum = request.queryParams("costDetail.datum") != null && !request.queryParams("costDetail.datum").isEmpty() ? LocalDate.parse(request.queryParams("costDetail.datum"), formatter) : null;
			return new Cost(id, !w.isEmpty() ? w.get(0) : null, !u.isEmpty() ? u.get(0) : null, request.queryParams("costDetail.beschreibung"), betrag, datum);
		}
		catch (ParseException e)
		{
			throw new IllegalArgumentException (e.getMessage());
		}
	}
}
