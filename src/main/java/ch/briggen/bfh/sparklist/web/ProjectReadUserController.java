package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectReadUserController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectReadUserController.class);

	ProjectRepository repository = new ProjectRepository();

	/**
	 *Liefert die Liste als /auswertungen zurück
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String username = request.queryParams("username");
		log.trace("GET /auswertungen mit username " + username);
		
		HashMap<String, Object> model = new HashMap<String, Object>();
		String sessionUser = request.session().attribute("USERNAME");
		model.put("session_user", sessionUser);
		model.put("list", repository.getByUsername(username));
		return new ModelAndView(model, "auswertungen");
	}
}
