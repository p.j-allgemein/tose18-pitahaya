package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.UserProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class EmployeeDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(EmployeeDeleteController.class);
		
	private UserProjectRepository repo = new UserProjectRepository();

	/**
	 * Löscht den Projektmitarbeiter mit der übergebenen id in der Datenbank
	 * 
	 * Hört auf GET /mitarbeiter/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zum projektsteckbrief
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String userId = request.queryParams("employeeDetail.userId");
		String projectId = request.queryParams("employeeDetail.projectId");
		String roleId = request.queryParams("employeeDetail.roleId");
		log.trace("GET /mitarbeiter/delete mit userId " + userId + ", projectId " + projectId + ", roleId " + roleId);
		
		if (roleId != null && !roleId.isEmpty() && !"0".equalsIgnoreCase(roleId))
		{
			repo.delete(Long.parseLong(userId), Long.parseLong(projectId), Long.parseLong(roleId));
		}
		else
		{
			repo.delete(Long.parseLong(userId), Long.parseLong(projectId), 0);
		}
		response.redirect("/projektsteckbrief");
		return null;
	}
}
