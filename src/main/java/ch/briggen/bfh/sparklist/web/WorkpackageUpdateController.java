package ch.briggen.bfh.sparklist.web;

import java.util.Arrays;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.Workpackage;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class WorkpackageUpdateController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectUpdateController.class);
	
	private ProjectRepository projectRepo = new ProjectRepository();
	private ProjectphaseRepository phaseRepo = new ProjectphaseRepository();
	private WorkpackageRepository repo = new WorkpackageRepository();

	/**
	 * Schreibt das geänderte Workpackage zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/projektsteckbrief) mit der Projekt-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /workpackage/update
	 * 
	 * @return redirect nach /projektsteckbrief: via Browser wird /projektsteckbrief aufgerufen.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		log.trace("queryParams " + request.queryParams().toString());
		Workpackage workpackageDetail = WorkpackageWebHelper.workpackageFromWeb(request);;
		try
		{
			if (workpackageDetail.getId() == 0)
			{
				log.trace("workpackageDetail.getId == 0");
				log.trace("workpackageDetail: " + workpackageDetail.toString());
				HashMap<String, Object> model = new HashMap<String, Object>();
				model.put("error", "workpackageDetail ist leer, scheint ein Fehler zu sein");
				return new ModelAndView(model, "projektsteckbrief");
			}
			
			log.trace("POST /workpackage/update mit workpackageDetail " + workpackageDetail);
			
			repo.save(workpackageDetail);
			
			//die neue Id wird dem Redirect als Parameter hinzugefügt
			//der redirect erfolgt dann auf /projektrisiko?id=432932
			response.redirect("/projektsteckbrief?id=" + workpackageDetail.getProject().getId());
			return null;
		}
		catch (IllegalArgumentException e)
		{
			HashMap<String, Object> model = new HashMap<String, Object>();
			// Beim Ausfüllen ist ein Ferhl aufgetreten
			model.put("workpackageDetail", workpackageDetail);
			model.put("error", e.getMessage());
			
			model.put("phaseDetail", phaseRepo.getAll());
			model.put("projectList", projectRepo.getAll());
			model.put("projectId", request.queryParams("workpackageDetail.projektname"));
			String username = request.session().attribute("USERNAME");
			model.put("session_user", username);
			model.put("doneList", Arrays.asList("false","true"));
			
			return new ModelAndView(model, "workpackage_erfassen");
		}
	}
}
