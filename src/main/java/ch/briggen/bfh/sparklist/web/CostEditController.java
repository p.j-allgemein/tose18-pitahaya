package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Cost;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class CostEditController implements TemplateViewRoute {

private final Logger log = LoggerFactory.getLogger(CostEditController.class);
	
	private WorkpackageRepository workRepo = new WorkpackageRepository();
	private UserRepository userRepo = new UserRepository();
	
	/**
	 * Requesthandler zum Bearbeiten von Kosten. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Hört auf GET /kosten_erfassen
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "kosten_erfassen" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("workpackageList", workRepo.getByProject(request.queryParams("projectId")));
				
		log.trace("GET /kosten/new für INSERT");
		//der Submit-Button ruft /kosten/new auf --> INSERT
		model.put("postAction", "/kosten/new");
		model.put("costDetail", new Cost());
		model.put("projektId", request.queryParams("projectId"));
		model.put("userList", userRepo.getAll());
		
		String username = request.session().attribute("USERNAME");
		model.put("session_user", username);
		
		//das Template kosten_erfassen verwenden und dann "anzeigen".
		return new ModelAndView(model, "kosten_erfassen");
	}
}
