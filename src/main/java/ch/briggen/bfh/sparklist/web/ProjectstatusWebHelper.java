package ch.briggen.bfh.sparklist.web;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Projectstatus;
import spark.Request;

public class ProjectstatusWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjectstatusWebHelper.class);
	
	private static ProjectRepository projectRepo = new ProjectRepository();
	
	public static Projectstatus projectstatusFromWeb(Request request)
	{
		String projectId = request.queryParams("projectId");
		Project p = new Project();
		if (NumberUtils.isNumber(projectId))
		{
			p = projectRepo.getById(Long.parseLong(projectId));
		}
		
		return new Projectstatus(0L, p, request.queryParams("projectstatus"),
							   request.queryParams("ressourcestatus"), 
							   request.queryParams("riskstatus"),
							   request.queryParams("timestatus"),
							   request.queryParams("budgetstatus"));
	}
}
