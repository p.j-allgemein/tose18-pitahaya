package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectNewController.class);
		
	private ProjectRepository projectRepo = new ProjectRepository();
	private ProjectphaseRepository phaseRepo = new ProjectphaseRepository();
	private UserRepository userRepo = new UserRepository();
	
	/**
	 * Erstellt ein neues Projekt in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /projekte&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /projects/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Project projectDetail = ProjectWebHelper.projectFromWeb(request);
		log.trace("POST /projects/new mit projectDetail " + projectDetail);
		
		try
		{
			if (projectDetail.getProjektleiter() == null)
			{
				throw new IllegalArgumentException("Projektleiter " + request.queryParams("projectDetail.projektleiter") + " wurde nicht gefunden");
			}
			
			if (projectRepo.checkProjectname(projectDetail.getProjektname()))
			{
				throw new IllegalArgumentException("Projektname existiert bereits in der Datenbank");
			}
			
			projectDetail.validate();
			//insert gibt die von der DB erstellte id zurück.
			projectRepo.insert(projectDetail);
			
			//die neue Id wird dem Redirect als Parameter hinzugefügt
			//der redirect erfolgt dann auf /projekte?id=432932
			response.redirect("/projekte");
			return null;
		}
		catch (IllegalArgumentException e)
		{
			HashMap<String, Object> model = new HashMap<String, Object>();
			// Beim Ausfüllen ist ein Fehler aufgetreten
			model.put("postAction", "/projects/new");
			model.put("projectDetail", projectDetail);
			model.put("error", e.getMessage());
			List<User> userList = new ArrayList<>(userRepo.getByRolename("Projektleiter"));
			log.trace("found " + userList.size() + " user with rolename \"Projektleiter\"");
			
			String username = request.session().attribute("USERNAME");
			model.put("session_user", username);
			model.put("userDetail", userList);
			model.put("phaseDetail", phaseRepo.getAll());
			return new ModelAndView(model, "projekt_erfassen");
		}
	}
}
