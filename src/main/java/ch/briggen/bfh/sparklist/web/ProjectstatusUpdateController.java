package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Projectstatus;
import ch.briggen.bfh.sparklist.domain.ProjectstatusRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectstatusUpdateController implements TemplateViewRoute {

private final Logger log = LoggerFactory.getLogger(ProjectstatusUpdateController.class);
	
	private ProjectstatusRepository repo = new ProjectstatusRepository();
	
	/**
	 * Requesthandler zum Bearbeiten eines Projektstatus. 
	 * Hört auf GET /status/update
	 * @return ist ein asynchroner request, gibt nichts zurück .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		log.trace("update Projektstatus");
		
		Projectstatus status = ProjectstatusWebHelper.projectstatusFromWeb(request);
		
		List<Projectstatus> fromDB = new ArrayList<>(repo.getByProject(request.queryParams("projectId")));
		
		if (fromDB.isEmpty())
		{
			repo.insert(status);
		}
		else
		{
			Projectstatus s = fromDB.get(0);
			s.setProjectstatus(status.getProjectstatus());
			s.setRessourcestatus(status.getRessourcestatus());
			s.setRiskstatus(status.getRiskstatus());
			s.setTimestatus(status.getTimestatus());
			s.setBudgetstatus(status.getBudgetstatus());
			repo.save(s);
		}
		return null;
	}
}
