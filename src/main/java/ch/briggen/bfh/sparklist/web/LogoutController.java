package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class LogoutController implements TemplateViewRoute{
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(LogoutController.class);
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		request.session().attributes().clear();
		request.session(true);
		response.redirect("/");
		return null;
	}
}
