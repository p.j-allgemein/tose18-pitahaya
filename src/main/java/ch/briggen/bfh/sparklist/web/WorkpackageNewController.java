package ch.briggen.bfh.sparklist.web;

import java.util.Arrays;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.Workpackage;
import ch.briggen.bfh.sparklist.domain.WorkpackageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class WorkpackageNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(WorkpackageNewController.class);
	
	private ProjectRepository projectRepo = new ProjectRepository();
	private ProjectphaseRepository phaseRepo = new ProjectphaseRepository();
	private WorkpackageRepository repo = new WorkpackageRepository();
	
	/**
	 * Erstellt ein neues Workpackage in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /workpackage&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /workpackage/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Workpackage workpackageDetail = WorkpackageWebHelper.workpackageFromWeb(request);
		log.trace("POST /workpackage/new mit workpackageDetail " + workpackageDetail);
		
		try
		{
			if (workpackageDetail.getProject() == null)
			{
				throw new IllegalArgumentException("Projekt " + request.queryParams("workpackageDetail.projektname") + " wurde nicht gefunden");
			}
			
			if (repo.checkWorkpackage(workpackageDetail.getWorkpackagename()))
			{
				throw new IllegalArgumentException("Workpackagename existiert bereits in der Datenbank");
			}
			
			workpackageDetail.validate();
			//insert gibt die von der DB erstellte id zurück.
			repo.insert(workpackageDetail);
			
			//die neue Id wird dem Redirect als Parameter hinzugefügt
			//der redirect erfolgt dann auf /projektsteckbrief?id=432932
			response.redirect("/projektsteckbrief?id=" + workpackageDetail.getProject().getId());
			return null;
		}
		catch (IllegalArgumentException e)
		{
			HashMap<String, Object> model = new HashMap<String, Object>();
			// Beim Ausfüllen ist ein Fehler aufgetreten
			model.put("workpackageDetail", workpackageDetail);
			model.put("error", e.getMessage());
			
			model.put("phaseDetail", phaseRepo.getAll());
			model.put("projectList", projectRepo.getAll());
			model.put("projectId", request.queryParams("workpackageDetail.projektname"));
			String username = request.session().attribute("USERNAME");
			model.put("session_user", username);
			model.put("doneList", Arrays.asList("false","true"));
			
			return new ModelAndView(model, "workpackage_erfassen");
		}
	}
}
