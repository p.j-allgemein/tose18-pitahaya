package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectEditController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectEditController.class);
	
	private ProjectRepository projectRepo = new ProjectRepository();
	private ProjectphaseRepository phaseRepo = new ProjectphaseRepository();
	private UserRepository userRepo = new UserRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Projektes. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Projekt erstellt (Aufruf von /projects/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars dar Project mit der übergebenen id upgedated (Aufruf /project/update)
	 * Hört auf GET /project/edit
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "projekt_erfassen" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		if(null == idString || "0".equals(idString))
		{
			log.trace("GET /projekt_erfassen für INSERT mit id " + idString);
			//der Submit-Button ruft /project/new auf --> INSERT
			Project p = new Project();
			p.setProjektnummer(projectRepo.getProjectNumberId());
			model.put("postAction", "/projects/new");
			model.put("projectDetail", p);
		}
		else
		{
			log.trace("GET /project/edit für UPDATE mit id " + idString);
			//der Submit-Button ruft /project/update auf --> UPDATE
			model.put("postAction", "/project/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "projectDetail" hinzugefügt. projectDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Project p = projectRepo.getById(id);
			model.put("projectDetail", p);
		}
		
		List<User> userList = new ArrayList<>(userRepo.getByRolename("Projektleiter"));
		log.trace("found " + userList.size() + " user with rolename \"Projektleiter\"");
		
		String username = request.session().attribute("USERNAME");
		model.put("session_user", username);
		model.put("userDetail", userList);
		model.put("phaseDetail", phaseRepo.getAll());
		
		//das Template userDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "projekt_erfassen");
	}
}
