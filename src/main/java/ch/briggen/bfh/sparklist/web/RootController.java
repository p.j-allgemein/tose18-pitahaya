package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class RootController implements TemplateViewRoute{
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(RootController.class);
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		request.session(true);
		HashMap<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView(model, "startseite");
	}
}
