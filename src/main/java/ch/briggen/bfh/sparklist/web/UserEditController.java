package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.RoleRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class UserEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(UserEditController.class);
	
	private UserRepository userRepo = new UserRepository();
	private RoleRepository roleRepo = new RoleRepository();
	
	/**
	 * Requesthandler zum Bearbeiten eines Users. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neuer User erstellt (Aufruf von /users/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars dar User mit der übergebenen id upgedated (Aufruf /users/update)
	 * Hört auf GET /registration
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "registration" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("roleList", roleRepo.getAll());
				
		if(null == idString || "0".equals(idString))
		{
			log.trace("GET /registration für INSERT mit id " + idString);
			//der Submit-Button ruft /users/new auf --> INSERT
			model.put("postAction", "/users/new");
			model.put("userDetail", new User());
		}
		else
		{
			log.trace("GET /registration für UPDATE mit id " + idString);
			//der Submit-Button ruft /user/update auf --> UPDATE
			model.put("postAction", "/users/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "userDetail" hinzugefügt. userDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			User u = userRepo.getById(id);
			model.put("userDetail", u);
		}
		
		String username = request.session().attribute("USERNAME");
		model.put("session_user", username);
		
		//das Template userDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "registration");
	}
}
