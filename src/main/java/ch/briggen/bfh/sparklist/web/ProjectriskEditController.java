package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Projectrisk;
import ch.briggen.bfh.sparklist.domain.ProjectriskRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectriskEditController implements TemplateViewRoute {

private final Logger log = LoggerFactory.getLogger(ProjectriskEditController.class);
	
	private ProjectRepository projectRepo = new ProjectRepository();
	private ProjectriskRepository repo = new ProjectriskRepository();
	private UserRepository userRepo = new UserRepository();
	
	/**
	 * Requesthandler zum Bearbeiten eines Projektrisikos. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neuer User erstellt (Aufruf von /projektrisiko/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars dar User mit der übergebenen id upgedated (Aufruf /projektrisiko/update)
	 * Hört auf GET /registration
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "registration" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("projectList", projectRepo.getAll());
				
		if(null == idString || "0".equals(idString))
		{
			log.trace("GET /projektriskio/new für INSERT mit id " + idString);
			//der Submit-Button ruft /projectrisiko/new auf --> INSERT
			model.put("postAction", "/projektrisiko/new");
			model.put("projectrisikoDetail", new Projectrisk());
			model.put("projektname", request.queryParams("projectId"));
		}
		else
		{
			log.trace("GET /projektrisiko für UPDATE mit id " + idString);
			//der Submit-Button ruft /user/update auf --> UPDATE
			model.put("postAction", "/projektrisiko/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "projectrisikoDetail" hinzugefügt. projectrisikoDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Projectrisk p = repo.getById(id);
			model.put("projectrisikoDetail", p);
			if (p != null)
			{
				model.put("projektname", p.getProject().getId());
			}
			else
			{
				model.put("projektname", null);
			}
		}
		model.put("userDetail", userRepo.getAll());
		
		String username = request.session().attribute("USERNAME");
		model.put("session_user", username);
		
		//das Template projectrisikoDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "projektrisiko");
	}
}
