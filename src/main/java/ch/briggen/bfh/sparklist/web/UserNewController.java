package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.RoleRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class UserNewController  implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(UserNewController.class);
		
	private UserRepository userRepo = new UserRepository();
	private RoleRepository roleRepo = new RoleRepository();
	
	/**
	 * Erstellt ein neuen User in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /users&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /users/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		User userDetail = UserWebHelper.userFromWeb(request);
		log.trace("POST /users/new mit userDetail " + userDetail);
		
		List<User> u = new ArrayList<>(userRepo.getByName(userDetail.getUsername()));
		
		try
		{
			if (u.size() > 0)
			{
				HashMap<String, Object> model = new HashMap<String, Object>();
				// User existiert bereits, gib ein Fehler zurück.
				model.put("userDetail", u.get(0));
				model.put("roleList", roleRepo.getAll());
				model.put("error", "User mit Name " + userDetail.getUsername() + " existiert schon");
				model.put("postAction", "/users/new");
				String username = request.session().attribute("USERNAME");
				model.put("session_user", username);
				return new ModelAndView(model, "registration");
			}
			else
			{
				userDetail.validate();
				//insert gibt die von der DB erstellte id zurück.
				userRepo.insert(userDetail);
				
				//die neue Id wird dem Redirect als Parameter hinzugefügt
				//der redirect erfolgt dann auf /users?id=432932
				response.redirect("/");
				return null;
			}
		}
		catch (IllegalArgumentException e)
		{
			HashMap<String, Object> model = new HashMap<String, Object>();
			// Beim Ausfüllen ist ein Fehler aufgetreten
			model.put("userDetail", userDetail);
			model.put("roleList", roleRepo.getAll());
			model.put("error", e.getMessage());
			model.put("postAction", "/users/new");
			String username = request.session().attribute("USERNAME");
			model.put("session_user", username);
			return new ModelAndView(model, "registration");
		}
	}
}
