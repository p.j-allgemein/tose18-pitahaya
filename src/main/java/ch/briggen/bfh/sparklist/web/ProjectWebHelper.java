package ch.briggen.bfh.sparklist.web;

import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.Projectphase;
import ch.briggen.bfh.sparklist.domain.ProjectphaseRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.Request;

public class ProjectWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjectWebHelper.class);
	
	private static UserRepository userRepo = new UserRepository();
	private static ProjectphaseRepository phaseRepo = new ProjectphaseRepository();
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	public static Project projectFromWeb(Request request)
	{
		try
		{
			long id = 0;
			long pn = 0;
			double d = 0.0;
			NumberFormat format = NumberFormat.getInstance(new Locale("de", "CH"));
			String username = request.queryParams("projectDetail.projektleiter");
			String phasename = request.queryParams("projectDetail.projektphase");
			List<User> u = new ArrayList<>();
			List<Projectphase> p = new ArrayList<>();
			if (NumberUtils.isNumber(username))
			{
				u = Arrays.asList(userRepo.getById(Long.parseLong(username)));
			}
			else
			{
				u = new ArrayList<>(userRepo.getByName(username));
			}
			
			if (NumberUtils.isNumber(phasename))
			{
				p = Arrays.asList(phaseRepo.getById(Long.parseLong(phasename)));
			}
			else
			{
				p = new ArrayList<>(phaseRepo.getByName(phasename));
			}
			
			if (request.queryParams("projectDetail.id") != null)
			{
				Number number = format.parse(request.queryParams("projectDetail.id"));
				id = number.longValue();
			}
			if (request.queryParams("projectDetail.projektnummer") != null)
			{
				Number number = format.parse(request.queryParams("projectDetail.projektnummer"));
				pn = number.longValue();
			}
			if (request.queryParams("projectDetail.projektbudget") != null)
			{
				Number number = format.parse(request.queryParams("projectDetail.projektbudget"));
				d = number.doubleValue();
			}
			LocalDate start = request.queryParams("projectDetail.projektstart") != null && !request.queryParams("projectDetail.projektstart").isEmpty() ? LocalDate.parse(request.queryParams("projectDetail.projektstart"), formatter) : null;
			LocalDate ende = request.queryParams("projectDetail.projektende") != null && !request.queryParams("projectDetail.projektende").isEmpty() ? LocalDate.parse(request.queryParams("projectDetail.projektende"), formatter) : null;
			return new Project(id, pn,
						    request.queryParams("projectDetail.projektname"),
						    !u.isEmpty() ? u.get(0) : null,
						    start, ende,
						    !p.isEmpty() ? p.get(0).getPhasenname() : null,
						    request.queryParams("projectDetail.projektstatus"),
						    d);
		}
		catch (ParseException e)
		{
			throw new IllegalArgumentException (e.getMessage());
		}
	}
}
