package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

public class WorkpackageRepository extends AbstractRepository<Workpackage>
{
	private ProjectRepository projectRepo = new ProjectRepository();
	private ProjectphaseRepository phaseRepo = new ProjectphaseRepository();
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	@Override
	protected Collection<Workpackage> mapItems(ResultSet rs) throws SQLException {
		LinkedList<Workpackage> list = new LinkedList<Workpackage>();
		while(rs.next())
		{
			Project p = projectRepo.getById(rs.getLong("projectId"));
			Projectphase pp = phaseRepo.getById(rs.getLong("phaseId"));
			Workpackage w = new Workpackage(rs.getLong("id"), p, pp, rs.getString("workpackagename"), rs.getDouble("plannedValue"), rs.getDouble("earnedValue"),
									rs.getString("startdate") != null ? LocalDate.parse(rs.getString("startdate"), formatter) : null,
									rs.getString("enddate") != null ? LocalDate.parse(rs.getString("enddate"), formatter) : null,
								    rs.getBoolean("done"));
			list.add(w);
		}
		return list;
	}
	
	@Override
	protected String getAllQuery()
	{
		return "select id, projectId, phaseId, workpackagename, plannedValue, earnedValue, startdate, enddate, done from workpackages";
	}

	@Override
	protected String getByNameQuery()
	{
		return getAllQuery() + " where workpackagename=?";
	}

	@Override
	protected String getByIdQuery()
	{
		return getAllQuery() + " where id=?";
	}

	@Override
	protected String getSaveQuery() {
		return "update workpackages set projectId = ?, phaseId = ?, workpackagename = ?, plannedValue = ?, earnedValue = ?, startdate = ?, enddate = ?, done = ? where id = ?";
	}

	@Override
	protected String getDeleteQuery() {
		return "delete from workpackages where id = ?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into workpackages(projectId, phaseId, workpackagename, plannedValue, earnedValue, startdate, enddate, done) " +
				"values(?, ?, ?, ?, ?, ?, ?, ?)";
	}

	@Override
	protected void setByNameStatementParameter(PreparedStatement stmt, String name) throws SQLException {
		stmt.setString(1, name);
	}

	@Override
	protected void setSaveStatementParameter(PreparedStatement stmt, Workpackage item) throws SQLException {
		Long projectId = null;
		Long phaseId = null;
		if (item.getProject() != null)
		{
			Project project = projectRepo.getById(item.getProject().getId());
			if (project == null)
			{
				projectId = projectRepo.insert(item.getProject());
			}
			else
			{
				projectId = project.getId();
			}
		}
		
		if (item.getProjectphase() != null)
		{
			Projectphase phase = phaseRepo.getById(item.getProjectphase().getId());
			if (phase == null)
			{
				phaseId = phaseRepo.insert(item.getProjectphase());
			}
			else
			{
				phaseId = phase.getId();
			}
		}
		
		if (item.isDone())
		{
			item.setEarnedValue(item.getPlannedValue());
		}
		
		handleStatementNullValues(stmt, 1, projectId);
		handleStatementNullValues(stmt, 2, phaseId);
		handleStatementNullValues(stmt, 3, item.getWorkpackagename());
		handleStatementNullValues(stmt, 4, item.getPlannedValue());
		handleStatementNullValues(stmt, 5, item.getEarnedValue());
		handleStatementNullValues(stmt, 6, item.getStartdate());
		handleStatementNullValues(stmt, 7, item.getEnddate());
		handleStatementNullValues(stmt, 8, item.isDone());
		handleStatementNullValues(stmt, 9, item.getId());
	}

	@Override
	protected void setInsertStatementParameter(PreparedStatement stmt, Workpackage item) throws SQLException {
		Long projectId = null;
		Long phaseId = null;
		if (item.getProject() != null)
		{
			Project project = projectRepo.getById(item.getProject().getId());
			if (project == null)
			{
				projectId = projectRepo.insert(item.getProject());
			}
			else
			{
				projectId = project.getId();
			}
		}
		
		if (item.getProjectphase() != null)
		{
			Projectphase phase = phaseRepo.getById(item.getProjectphase().getId());
			if (phase == null)
			{
				phaseId = phaseRepo.insert(item.getProjectphase());
			}
			else
			{
				phaseId = phase.getId();
			}
		}
		
		handleStatementNullValues(stmt, 1, projectId);
		handleStatementNullValues(stmt, 2, phaseId);
		handleStatementNullValues(stmt, 3, item.getWorkpackagename());
		handleStatementNullValues(stmt, 4, item.getPlannedValue());
		handleStatementNullValues(stmt, 5, item.getEarnedValue());
		handleStatementNullValues(stmt, 6, item.getStartdate());
		handleStatementNullValues(stmt, 7, item.getEnddate());
		handleStatementNullValues(stmt, 8, item.isDone());
	}
	
	/**
	 * Liefert alle Workpackage mit dem angegebenen Phasenname als Projektphase
	 * @param phasenname
	 * @return Collection mit dem Phasenname "phasenname"
	 */
	public Collection<Workpackage> getByPhase(String phasenname)
	{
		log.trace("getByPhase " + phasenname);
		
		try(Connection conn = getConnection())
		{
			List<Projectphase> phases = new ArrayList<>(phaseRepo.getByName(phasenname));
			
			if (phases.isEmpty())
			{
				return new ArrayList<>();
			}
			
			PreparedStatement stmt = conn.prepareStatement(getAllQuery() + " where phaseId=?");
			stmt.setLong(1, phases.get(0).getId());
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + Project.class.toString().toLowerCase() + " by phasenname " + phasenname;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert alle Workpackage mit dem angegebenen Projectname als Project
	 * @param projectname
	 * @return Collection mit dem Projectname "projectname"
	 */
	public Collection<Workpackage> getByProject(String projectname)
	{
		log.trace("getByProject " + projectname);
		
		try(Connection conn = getConnection())
		{
			List<Project> projects = new ArrayList<>();
			if (NumberUtils.isNumber(projectname))
			{
				Project p = projectRepo.getById(Long.parseLong(projectname));
				if (p != null)
				{
					projects.add(p);
				}
			}
			else
			{	
				projects = new ArrayList<>(projectRepo.getByName(projectname));
			}
			
			log.trace(projects.toString());
			
			if (projects.isEmpty())
			{
				return new ArrayList<>();
			}
			
			PreparedStatement stmt = conn.prepareStatement(getAllQuery() + " where projectId=?");
			stmt.setLong(1, projects.get(0).getId());
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + Projectrisk.class.toString().toLowerCase() + " by projectname " + projectname;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	@Override
	public void delete(long id)
	{
		CostRepository costRepo = new CostRepository();
		List<Cost> costs = new ArrayList<>(costRepo.getAll());
		if (!costs.isEmpty())
		{
			costs.stream()
				 .filter(e -> (e.getWorkpackage() != null && e.getWorkpackage().getId() == id))
				 .forEach(e -> costRepo.delete(e.getId()));
		}
		super.delete(id);
	}
	
	/**
	 * Liefert true zurück, wenn der Workpackagename bereits existiert ansonsten false
	 * @return boolean Wert, ob Workpackage vorhanden ist oder nicht
	 */
	public boolean checkWorkpackage(String workpackage)
	{
		log.trace("checkWorkpackage " + workpackage + " if it exists");
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select count(*) as counter from workpackages where workpackagename=?");
			stmt.setString(1, workpackage);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			return rs.getLong("counter") != 0 ? true : false;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + Workpackage.class.toString().toLowerCase() + " check workpackename ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
}
