package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractRepository<E>
{
	protected static final Logger log = LoggerFactory.getLogger(AbstractRepository.class);
	private String className;
	
	public AbstractRepository()
	{
		className = ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
	}
	
	protected abstract Collection<E> mapItems(ResultSet rs) throws SQLException;
	protected abstract String getAllQuery();
	protected abstract String getByNameQuery();
	protected abstract String getByIdQuery();
	protected abstract String getSaveQuery();
	protected abstract String getDeleteQuery();
	protected abstract String getInsertQuery();
	protected abstract void setByNameStatementParameter(PreparedStatement stmt, String name) throws SQLException;
	protected abstract void setSaveStatementParameter(PreparedStatement stmt, E item) throws SQLException;
	protected abstract void setInsertStatementParameter(PreparedStatement stmt, E item) throws SQLException;
	
	/**
	 * Liefert alle items in der Datenbank
	 * @return Collection aller E
	 */
	public Collection<E> getAll()
	{
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement(getAllQuery());
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all " + className.toLowerCase() + ". ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert alle E mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<E> getByName(String name)
	{
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement(getByNameQuery());
			setByNameStatementParameter(stmt, name);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + className.toLowerCase() + " by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert das Item mit der übergebenen Id
	 * @param id id des Item
	 * @return Item oder NULL
	 */
	public E getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement(getByIdQuery());
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			Collection<E> list = mapItems(rs);
			if (list.size() > 0)
			{
				return list.iterator().next();
			}
			return null;		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + className.toLowerCase() + " by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}			
	}

	/**
	 * Speichert das übergebene item in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(E i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement(getSaveQuery());
			setSaveStatementParameter(stmt, i);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating " + className.toLowerCase() + " " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Löscht das Item mit der angegebenen Id von der DB
	 * @param id Item ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement(getDeleteQuery());
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing " + className + " by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Speichert das angegebene Item in der DB. INSERT.
	 * @param i neu zu erstellendes Item
	 * @return Liefert die von der DB generierte id des neuen Items zurück
	 */
	public long insert(E i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement(getInsertQuery());
			setInsertStatementParameter(stmt, i);
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while inserting item " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
	}
	
	protected void handleStatementNullValues(PreparedStatement stmt, int index, Object value) throws SQLException
	{
		if (value == null)
		{
			stmt.setNull(index, java.sql.Types.NULL);
		}
		else
		{
			if (value instanceof String)
			{
				stmt.setString(index, (String)value);
			}
			else if (value instanceof Long)
			{
				stmt.setLong(index, (Long)value);
			}
			else if (value instanceof Double)
			{
				stmt.setDouble(index, (Double)value);
			}
			else if (value instanceof java.sql.Date)
			{
				stmt.setDate(index, (java.sql.Date)value);
			}
			else if (value instanceof LocalDate)
			{
				stmt.setDate(index, java.sql.Date.valueOf((LocalDate)value));
			}
			else if (value instanceof Boolean)
			{
				stmt.setBoolean(index, (Boolean)value);
			}
		}
	}
}
