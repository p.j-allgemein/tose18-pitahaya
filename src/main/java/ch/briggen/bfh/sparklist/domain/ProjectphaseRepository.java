package ch.briggen.bfh.sparklist.domain;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

public class ProjectphaseRepository extends AbstractRepository<Projectphase>
{	
	@Override
	protected String getAllQuery()
	{
		return "select id, phasenname from projectphases";
	}

	@Override
	protected String getByNameQuery()
	{
		return getAllQuery() + " where phasenname=?";
	}

	@Override
	protected String getByIdQuery()
	{
		return getAllQuery() + " where id=?";
	}

	@Override
	protected String getSaveQuery()
	{
		return "update projectphases set phasenname=? where id=?";
	}

	@Override
	protected String getDeleteQuery()
	{
		return "delete from projectphases where id=?";
	}

	@Override
	protected String getInsertQuery()
	{
		return "insert into projectphases (phasenname) values (?)";
	}

	@Override
	protected void setByNameStatementParameter(PreparedStatement stmt, String name) throws SQLException
	{
		stmt.setString(1, name);
	}

	@Override
	protected void setSaveStatementParameter(PreparedStatement stmt, Projectphase phase) throws SQLException
	{
		handleStatementNullValues(stmt, 1, phase.getPhasenname());
		handleStatementNullValues(stmt, 2,  phase.getId());
	}

	@Override
	protected void setInsertStatementParameter(PreparedStatement stmt, Projectphase phase) throws SQLException
	{
		handleStatementNullValues(stmt, 1, phase.getPhasenname());
	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Projectphase-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	protected Collection<Projectphase> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Projectphase> list = new LinkedList<Projectphase>();
		while(rs.next())
		{
			Projectphase p = new Projectphase(rs.getLong("id"), rs.getString("phasenname"));
			list.add(p);
		}
		return list;
	}
}
