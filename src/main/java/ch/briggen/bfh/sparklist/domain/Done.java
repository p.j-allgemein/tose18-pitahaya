package ch.briggen.bfh.sparklist.domain;

public class Done {
	private String done;
	
	public Done()
	{
	}
	
	public Done(String done)
	{
		this.done = done;
	}

	public String getDone() {
		return done;
	}

	public void setDone(String done) {
		this.done = done;
	}
	
	@Override
	public String toString() {
		return String.format("Done:{done: %s}", done);
	}
}
