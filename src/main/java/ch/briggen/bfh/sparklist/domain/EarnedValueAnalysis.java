package ch.briggen.bfh.sparklist.domain;

public class EarnedValueAnalysis {
	private String name;
	private double budget;
	private double plannedValue;
	private double earnedValue;
	private double actualCost;
	private double costDifference;
	private double costEfficency;
	private double degreeOfCompletion;
	private double planDifference;
	
	public EarnedValueAnalysis()
	{
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public double getPlannedValue() {
		return plannedValue;
	}

	public void setPlannedValue(double plannedValue) {
		this.plannedValue = plannedValue;
	}

	public double getEarnedValue() {
		return earnedValue;
	}

	public void setEarnedValue(double earnedValue) {
		this.earnedValue = earnedValue;
	}

	public double getActualCost() {
		return actualCost;
	}

	public void setActualCost(double actualCost) {
		this.actualCost = actualCost;
	}

	public double getCostDifference() {
		return costDifference;
	}

	public void setCostDifference(double costDifference) {
		this.costDifference = costDifference;
	}

	public double getCostEfficency() {
		return costEfficency;
	}

	public void setCostEfficency(double costEfficency) {
		this.costEfficency = costEfficency;
	}

	public double getDegreeOfCompletion() {
		return degreeOfCompletion;
	}

	public void setDegreeOfCompletion(double degreeOfCompletion) {
		this.degreeOfCompletion = degreeOfCompletion;
	}

	public double getPlanDifference() {
		return planDifference;
	}

	public void setPlanDifference(double planDifference) {
		this.planDifference = planDifference;
	}
	
	@Override
	public String toString() {
		return String.format("EarnedValueAnalysis:{name: %s; budget: %f; plannedValue: %f; earnedValue: %f; actualCost: %f; " +
				"costDifference: %f; costEfficency: %f; degreeOfCompletion: %f; planDifference: %f}", name,
				budget, plannedValue, earnedValue, actualCost, costDifference, costEfficency, degreeOfCompletion, planDifference);
	}
}
