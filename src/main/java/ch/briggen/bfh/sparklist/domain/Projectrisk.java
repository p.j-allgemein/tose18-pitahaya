package ch.briggen.bfh.sparklist.domain;

public class Projectrisk {
	private long id;
	private String risikoname;
	private String ursache;
	private String gefaehrdungsart;
	private double eintrittswahrscheinlichkeit;
	private double auswirkungen;
	private String massnahmen;
	private User verantwortlicher;
	private long prioritaet;
	private Project project;
	
	public Projectrisk()
	{
	}
	
	public Projectrisk(long id, String risikoname, String ursache, String gefaehrdungsart, double eintrittswahrscheinlichkeit,
					   double auswirkungen, String massnahmen, User verantwortlicher, long prioritaet, Project project)
	{
		this.id = id;
		this.risikoname = risikoname;
		this.ursache = ursache;
		this.gefaehrdungsart = gefaehrdungsart;
		this.eintrittswahrscheinlichkeit = eintrittswahrscheinlichkeit;
		this.auswirkungen = auswirkungen;
		this.massnahmen = massnahmen;
		this.verantwortlicher = verantwortlicher;
		this.prioritaet = prioritaet;
		this.project = project;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRisikoname() {
		return risikoname;
	}

	public void setRisikoname(String risikoname) {
		this.risikoname = risikoname;
	}

	public String getUrsache() {
		return ursache;
	}

	public void setUrsache(String ursache) {
		this.ursache = ursache;
	}

	public String getGefaehrdungsart() {
		return gefaehrdungsart;
	}

	public void setGefaehrdungsart(String gefaehrdungsart) {
		this.gefaehrdungsart = gefaehrdungsart;
	}

	public double getEintrittswahrscheinlichkeit() {
		return eintrittswahrscheinlichkeit;
	}

	public void setEintrittswahrscheinlichkeit(double eintrittswahrscheinlichkeit) {
		this.eintrittswahrscheinlichkeit = eintrittswahrscheinlichkeit;
	}

	public double getAuswirkungen() {
		return auswirkungen;
	}

	public void setAuswirkungen(double auswirkungen) {
		this.auswirkungen = auswirkungen;
	}

	public String getMassnahmen() {
		return massnahmen;
	}

	public void setMassnahmen(String massnahmen) {
		this.massnahmen = massnahmen;
	}

	public User getVerantwortlicher() {
		return verantwortlicher;
	}

	public void setVerantwortlicher(User verantwortlicher) {
		this.verantwortlicher = verantwortlicher;
	}

	public long getPrioritaet() {
		return prioritaet;
	}

	public void setPrioritaet(long prioritaet) {
		this.prioritaet = prioritaet;
	}
	
	public Project getProject() {
		return project;
	}
	
	public void setProject(Project project) {
		this.project = project;
	}
	
	public void validate()
	{
		if (risikoname == null || risikoname.isEmpty())
		{
			throw new IllegalArgumentException("Risikoname fehlt");
		}
		
		if (ursache == null || ursache.isEmpty())
		{
			throw new IllegalArgumentException("Ursache fehlt");
		}
		
		if (gefaehrdungsart == null || gefaehrdungsart.isEmpty())
		{
			throw new IllegalArgumentException("Gefährdungsart fehlt");
		}
		
		if (eintrittswahrscheinlichkeit <= 0.0)
		{
			throw new IllegalArgumentException("Eintrittswahrscheinlichkeit fehlt");
		}
		
		if (auswirkungen <= 0.0)
		{
			throw new IllegalArgumentException("Auswirkungen fehlt");
		}
		
		if (massnahmen == null || massnahmen.isEmpty())
		{
			throw new IllegalArgumentException("Massnahmen fehlt");
		}
		
		if (verantwortlicher == null)
		{
			throw new IllegalArgumentException("Verantwortlicher fehlt");
		}
		
		if (prioritaet <= 0)
		{
			throw new IllegalArgumentException("Priorität fehlt");
		}
		
		if (project == null)
		{
			throw new IllegalArgumentException("Project fehlt");
		}
	}
	
	@Override
	public String toString() {
		return String.format("Projectrisk:{id: %d; risikoname: %s; ursache: %s; gefaehrdungsart: %s; " + 
							 "eintrittswahrscheinlichkeit: %f; auswirkungen: %f; massnahmen: %s; " +
							 "verantwortlicher: %s; prioritaet: %d; project: %s}", id, risikoname, ursache, gefaehrdungsart,
							 eintrittswahrscheinlichkeit, auswirkungen, massnahmen,
							 verantwortlicher != null ? verantwortlicher.toString() : null, prioritaet,
							 project != null ? project.toString() : null);
	}
}
