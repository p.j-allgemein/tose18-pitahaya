package ch.briggen.bfh.sparklist.domain;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;


/**
 * Repository für alle Items. 
 * Hier werden alle Funktionen für die DB-Operationen zu Items implementiert
 * @author Marcel Briggen
 *
 */


public class ItemRepository extends AbstractRepository<Item> {
	
	@Override
	protected String getAllQuery()
	{
		return "select id, name, quantity from items";
	}

	@Override
	protected String getByNameQuery()
	{
		return "select id, name, quantity from items where name=?";
	}

	@Override
	protected String getByIdQuery()
	{
		return "select id, name, quantity from items where id=?";
	}

	@Override
	protected String getSaveQuery()
	{
		return "update items set name=?, quantity=? where id=?";
	}

	@Override
	protected String getDeleteQuery()
	{
		return "delete from items where id=?";
	}

	@Override
	protected String getInsertQuery()
	{
		return "insert into items (name, quantity) values (?,?)";
	}

	@Override
	protected void setByNameStatementParameter(PreparedStatement stmt, String name) throws SQLException
	{
		stmt.setString(1, name);
	}

	@Override
	protected void setSaveStatementParameter(PreparedStatement stmt, Item item) throws SQLException
	{
		stmt.setString(1, item.getName());
		stmt.setInt(2, item.getQuantity());
		stmt.setLong(3, item.getId());
	}

	@Override
	protected void setInsertStatementParameter(PreparedStatement stmt, Item item) throws SQLException
	{
		stmt.setString(1, item.getName());
		stmt.setInt(2, item.getQuantity());		
	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Item-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	protected Collection<Item> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Item> list = new LinkedList<Item>();
		while(rs.next())
		{
			Item i = new Item(rs.getLong("id"),rs.getString("name"),rs.getInt("quantity"));
			list.add(i);
		}
		return list;
	}

}
