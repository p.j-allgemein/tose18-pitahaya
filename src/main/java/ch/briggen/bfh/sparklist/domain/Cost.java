package ch.briggen.bfh.sparklist.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Cost {
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	private long id;
	private Workpackage workpackage;
	private User user;
	private String beschreibung;
	private double betrag;
	private LocalDate datum;
	
	public Cost()
	{
	}
	
	public Cost(long id, Workpackage workpackage, User user,
				String beschreibung, double betrag, LocalDate datum)
	{
		this.id = id;
		this.workpackage = workpackage;
		this.user = user;
		this.beschreibung = beschreibung;
		this.betrag = betrag;
		this.datum = datum;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Workpackage getWorkpackage() {
		return workpackage;
	}

	public void setWorkpackage(Workpackage workpackage) {
		this.workpackage = workpackage;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public double getBetrag() {
		return betrag;
	}

	public void setBetrag(double betrag) {
		this.betrag = betrag;
	}

	public LocalDate getDatum() {
		return datum;
	}

	public void setDatum(LocalDate datum) {
		this.datum = datum;
	}
	
	public void validate()
	{
		if (workpackage == null)
		{
			throw new IllegalArgumentException("Workpackage fehlt");
		}
		
		if (user == null)
		{
			throw new IllegalArgumentException("User fehlt");
		}
		
		if (beschreibung == null || beschreibung.isEmpty())
		{
			throw new IllegalArgumentException("Beschreibung fehlt");
		}
		
		if (betrag <= 0.0)
		{
			throw new IllegalArgumentException("Betrag fehlt");
		}
		
		if (datum == null)
		{
			throw new IllegalArgumentException("Datum fehlt");
		}
	}
	
	@Override
	public String toString() {
		return String.format("Cost:{id: %d; workpackage: %s; user: %s; " +
				"beschreibung: %s; betrag: %f; datum: %s}", id,
				workpackage != null ? workpackage.toString() : null,
				user != null ? user.toString() : null, beschreibung,
				betrag, datum != null ? formatter.format(datum) : null);
	}
}
