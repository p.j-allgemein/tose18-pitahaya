package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserProjectRepository {
	protected static final Logger log = LoggerFactory.getLogger(UserProjectRepository.class);
	
	private UserRepository userRepo = new UserRepository();
	private ProjectRepository projectRepo = new ProjectRepository();
	private RoleRepository roleRepo = new RoleRepository();

	protected Collection<User2Project> mapItems(ResultSet rs) throws SQLException {
		LinkedList<User2Project> list = new LinkedList<User2Project>();
		while(rs.next())
		{
			User2Project up = new User2Project();
			up.setUser(userRepo.getById(rs.getLong("userId")));
			up.setProject(projectRepo.getById(rs.getLong("projectId")));
			if (rs.getLong("roleId") != 0)
			{
				up.setRole(roleRepo.getById(rs.getLong("roleId")));
			}
			list.add(up);
		}
		return list;
	}
		
	/**
	 * Liefert alle items in der Datenbank
	 * @return Collection aller User2Project
	 */
	public Collection<User2Project> getAll()
	{
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select userId, projectId, roleId from users2projects");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all " + User2Project.class.toString().toLowerCase();
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert das Item mit der übergebenen Id
	 * @param userId userId des Item
	 * @param projectId projectId des Item
	 * @param roleId roleId des Item
	 * @return Item oder NULL
	 */
	public User2Project getById(long userId, long projectId, long roleId) {
		log.trace("getById " + userId + " " + projectId + " " + roleId);
		
		try(Connection conn = getConnection())
		{
			String sql = "select userId, projectId, roleId from users2projects where userId = ? and projectId = ?";
			if (roleId != 0)
			{
				sql += " and roleId = ?";
			}
			
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, userId);
			stmt.setLong(2, projectId);
			if (roleId != 0)
			{
				stmt.setLong(3, roleId);
			}
			ResultSet rs = stmt.executeQuery();
			Collection<User2Project> list = mapItems(rs);
			if (list.size() > 0)
			{
				return list.iterator().next();
			}
			return null;		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + User2Project.class.toString().toLowerCase() + " by userId " + userId + ", projectId " + projectId + " and roleId " + roleId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}		
	}
	
	public User2Project getById(User user, Project project, Role role) {
		if (user == null || project == null)
		{
			throw new IllegalArgumentException("getById missing user and/or project");
		}
		return getById(user.getId(), project.getId(), role != null ? role.getId() : 0);
	}
	
	/**
	 * Speichert das übergebene item in der Datenbank. UPDATE.
	 * @param i
	 */
	public void insert(User2Project i) {
		log.trace("insert " + i);
		
		try(Connection conn = getConnection())
		{
			if (getById(i.getUser(), i.getProject(), i.getRole()) != null)
			{
				throw new IllegalArgumentException("Combination of userId, projectId and roleid already exist");
			}
			
			PreparedStatement stmt = conn.prepareStatement("insert into users2projects(userId, projectId, roleId) values(?, ?, ?)");
			stmt.setLong(1, i.getUser().getId());
			stmt.setLong(2, i.getProject().getId());
			
			if (i.getRole() == null)
			{
				stmt.setNull(3, java.sql.Types.NULL);
			}
			else
			{
				stmt.setLong(3, i.getRole().getId());
			}
			
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating " + User2Project.class.toString().toLowerCase() + " " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Löscht das Item mit der übergebenen Id
	 * @param userId userId des Item
	 * @param projectId projectId des Item
	 * @param roleId roleId des Item
	 * @return Item oder NULL
	 */
	public void delete(long userId, long projectId, long roleId) {
		log.trace("getById " + userId + " " + projectId + " " + roleId);
		
		try(Connection conn = getConnection())
		{
			String sql = "delete from users2projects where userId = ? and projectId = ?";
			if (roleId != 0)
			{
				sql += " and roleId = ?";
			}
			
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, userId);
			stmt.setLong(2, projectId);
			if (roleId != 0)
			{
				stmt.setLong(3, roleId);
			}
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + User2Project.class.toString().toLowerCase() + " by userId " + userId + ", projectId " + projectId + " and roleId " + roleId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}		
	}
	
	public void delete(User user, Project project, Role role) {
		if (user == null || project == null)
		{
			throw new IllegalArgumentException("delete missing user and/or project");
		}
		delete(user.getId(), project.getId(), role != null ? role.getId() : 0);
	}
	
	/**
	 * Liefert alle Projektmitarbeiter mit dem angegebenen Username
	 * @param username
	 * @return Collection mit dem Username "username"
	 */
	public Collection<User2Project> getByUsername(String username)
	{
		log.trace("getByUsername " + username);
		
		List<User> users = new ArrayList<>(userRepo.getByName(username));
		
		if (users.isEmpty())
		{
			return new ArrayList<>();
		}
		
		List<User2Project> list = new ArrayList<>(getAll());
		return list.stream().filter(e-> e.getUser().getId() == users.get(0).getId())
				.collect(Collectors.toCollection(ArrayList::new));
	}
	
	/**
	 * Liefert alle Projektmitarbeiter mit dem angegebenen projectname
	 * @param projectname
	 * @return Collection mit dem Projectname "projectname"
	 */
	public Collection<User2Project> getByProjectname(String projectname)
	{
		log.trace("getByProjectname " + projectname);
		
		List<Project> projects = new ArrayList<>(projectRepo.getByName(projectname));
		
		if (projects.isEmpty())
		{
			return new ArrayList<>();
		}
		
		List<User2Project> list = new ArrayList<>(getAll());
		return list.stream().filter(e-> e.getProject().getId() == projects.get(0).getId())
				.collect(Collectors.toCollection(ArrayList::new));
	}
	
	/**
	 * Liefert alle Projektmitarbeiter mit dem angegebenen rolename
	 * @param rolename
	 * @return Collection mit dem Rollennamen "rolename"
	 */
	public Collection<User2Project> getByRole(String rolename)
	{
		log.trace("getByRole " + rolename);
		
		List<Role> roles = new ArrayList<>(roleRepo.getByName(rolename));
		
		if (roles.isEmpty())
		{
			return new ArrayList<>();
		}
		
		List<User2Project> list = new ArrayList<>(getAll());
		return list.stream().filter(e-> e.getRole() != null ? e.getRole().getId() == roles.get(0).getId() : false)
				.collect(Collectors.toCollection(ArrayList::new));
	}
}
