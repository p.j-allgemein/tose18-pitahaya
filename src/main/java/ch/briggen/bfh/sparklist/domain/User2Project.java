package ch.briggen.bfh.sparklist.domain;

public class User2Project {
	private User user;
	private Project project;
	private Role role;
	
	public User2Project()
	{
	}
	
	public User2Project(User user, Project project, Role role)
	{
		this.user = user;
		this.project = project;
		this.role = role;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public void validate()
	{
		if (user == null)
		{
			throw new IllegalArgumentException("User fehlt");
		}
		
		if (project == null)
		{
			throw new IllegalArgumentException("Project fehlt");
		}
	}

	@Override
	public String toString() {
		return String.format("User2Project:{user: %s; project: %s; role: %s}", user, project, role);
	}
}
