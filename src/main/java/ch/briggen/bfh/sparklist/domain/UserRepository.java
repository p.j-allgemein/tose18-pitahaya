package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class UserRepository extends AbstractRepository<User>{
	private RoleRepository roleRepo = new RoleRepository();

	@Override
	protected Collection<User> mapItems(ResultSet rs) throws SQLException {
		LinkedList<User> list = new LinkedList<User>();
		while(rs.next())
		{
			Role r = roleRepo.getById(rs.getLong("role"));
			User u = new User(rs.getLong("id"), rs.getString("username"), rs.getString("name"), rs.getString("vorname"),
							  r, rs.getString("abteilung"), rs.getString("mail"), rs.getString("telefon"));
			list.add(u);
		}
		return list;
	}

	@Override
	protected String getAllQuery() {
		return "select id, username, name, vorname, role, abteilung, mail, telefon from users";
	}

	@Override
	protected String getByNameQuery() {
		return getAllQuery() + " where username = ?";
	}

	@Override
	protected String getByIdQuery() {
		return getAllQuery() + " where id = ?";
	}

	@Override
	protected String getSaveQuery() {
		return "update users set username = ?, name = ?, vorname = ?, role = ?, abteilung = ?, mail = ?, telefon = ? where id = ?";
	}

	@Override
	protected String getDeleteQuery() {
		return "delete from users where id = ?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into users(username, name, vorname, role, abteilung, mail, telefon) " +
				"values(?, ?, ?, ?, ?, ?, ?)";
	}

	@Override
	protected void setByNameStatementParameter(PreparedStatement stmt, String name) throws SQLException {
		stmt.setString(1, name);
	}

	@Override
	protected void setSaveStatementParameter(PreparedStatement stmt, User item) throws SQLException {
		handleStatementNullValues(stmt, 1, item.getUsername());
		handleStatementNullValues(stmt, 2, item.getName());
		handleStatementNullValues(stmt, 3, item.getVorname());
		handleStatementNullValues(stmt, 4, (item.getRole() != null ? item.getRole().getId() : null));
		handleStatementNullValues(stmt, 5, item.getAbteilung());
		handleStatementNullValues(stmt, 6, item.getMail());
		handleStatementNullValues(stmt, 7, item.getTelefon());
		handleStatementNullValues(stmt, 8, item.getId());
	}

	@Override
	protected void setInsertStatementParameter(PreparedStatement stmt, User item) throws SQLException {
		handleStatementNullValues(stmt, 1, item.getUsername());
		handleStatementNullValues(stmt, 2, item.getName());
		handleStatementNullValues(stmt, 3, item.getVorname());
		handleStatementNullValues(stmt, 4, (item.getRole() != null ? item.getRole().getId() : null));
		handleStatementNullValues(stmt, 5, item.getAbteilung());
		handleStatementNullValues(stmt, 6, item.getMail());
		handleStatementNullValues(stmt, 7, item.getTelefon());
	}
	
	/**
	 * Liefert alle Project mit der angegebenen Rolle
	 * @param username
	 * @return Collection mit von Usernamen mit der Rolle "rolename"
	 */
	public Collection<User> getByRolename(String rolename)
	{
		log.trace("getByRolename " + rolename);
		
		try(Connection conn = getConnection())
		{
			List<Role> roles = new ArrayList<>(roleRepo.getByName(rolename));
			
			if (roles.isEmpty())
			{
				throw new RepositoryException("no role with name " + rolename + " found in Database");
			}
			
			PreparedStatement stmt = conn.prepareStatement(getAllQuery() + " where role in (select * from table(x int = ?))");
			Array array = stmt.getConnection().createArrayOf("BIGINT", roles.stream().map(Role::getId).toArray(size -> new Long[size]));
			stmt.setArray(1, array);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			String msg = "SQL error while retreiving User by rolename " + rolename;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
}
