package ch.briggen.bfh.sparklist.domain;

public class User {
	private long id;
	private String username;
	private String name;
	private String vorname;
	private Role role;
	private String abteilung;
	private String mail;
	private String telefon;
	
	public User()
	{
		
	}
	
	public User(long id, String username, String name, String vorname, Role role, String abteilung, String mail, String telefon)
	{
		this.id = id;
		this.username = username;
		this.name = name;
		this.vorname = vorname;
		this.role = role;
		this.abteilung = abteilung;
		this.mail = mail;
		//this.telefon = telefon;
		setTelefon(telefon);
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public String getAbteilung() {
		return abteilung;
	}
	public void setAbteilung(String abteilung) {
		this.abteilung = abteilung;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		if (telefon != null && !telefon.isEmpty())
		{
			telefon = telefon.replaceAll("\\s+", "");
		}
		this.telefon = telefon;
	}
	
	public void validate()
	{
		if (username == null || username.isEmpty())
		{
			throw new IllegalArgumentException("Username fehlt");
		}
		
		if (name == null || name.isEmpty())
		{
			throw new IllegalArgumentException("Name fehlt");
		}
		
		if (vorname == null || vorname.isEmpty())
		{
			throw new IllegalArgumentException("Vorname fehlt");
		}
		
		if (role == null)
		{
			throw new IllegalArgumentException("Rolle für den User wurde nicht gesetzt/gefunden");
		}
		
		if (abteilung == null || abteilung.isEmpty())
		{
			throw new IllegalArgumentException("Abteilung fehlt");
		}
		
		if (mail == null || mail.isEmpty())
		{
			throw new IllegalArgumentException("Mail fehlt");
		}
		
		if (telefon == null || telefon.isEmpty())
		{
			throw new IllegalArgumentException("Telefon fehlt");
		}
	}

	@Override
	public String toString() {
		return String.format("User:{id: %d; username: %s; name: %s; vorname: %s; role: %s; abteilung: %s; mail: %s; telefon: %s}",
				id, username, name, vorname, role != null ? role.toString() : null, abteilung, mail, telefon);
	}
	
	
}
