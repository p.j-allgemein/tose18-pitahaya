package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

public class CostRepository extends AbstractRepository<Cost>
{
	private UserRepository userRepo = new UserRepository();
	private WorkpackageRepository wpRepo = new WorkpackageRepository();
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	@Override
	protected Collection<Cost> mapItems(ResultSet rs) throws SQLException {
		LinkedList<Cost> list = new LinkedList<Cost>();
		while(rs.next())
		{
			User u = userRepo.getById(rs.getLong("userId"));
			Workpackage w = wpRepo.getById(rs.getLong("workpackageId"));
			Cost c = new Cost(rs.getLong("id"), w, u, rs.getString("beschreibung"), rs.getDouble("betrag"),
							  rs.getString("datum") != null ? LocalDate.parse(rs.getString("datum"), formatter) : null);
			list.add(c);
		}
		return list;
	}
	
	@Override
	protected String getAllQuery()
	{
		return "select id, workpackageId, userId, beschreibung, betrag, datum from costs";
	}

	@Override
	protected String getByNameQuery()
	{
		return getAllQuery() + " where beschreibung=?";
	}

	@Override
	protected String getByIdQuery()
	{
		return getAllQuery() + " where id=?";
	}

	@Override
	protected String getSaveQuery() {
		return "update costs set workpackageId = ?, userId = ?, beschreibung = ?, betrag = ?, datum = ? where id = ?";
	}

	@Override
	protected String getDeleteQuery() {
		return "delete from costs where id = ?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into costs(workpackageId, userId, beschreibung, betrag, datum) values(?, ?, ?, ?, ?)";
	}

	@Override
	protected void setByNameStatementParameter(PreparedStatement stmt, String name) throws SQLException {
		stmt.setString(1, name);
	}

	@Override
	protected void setSaveStatementParameter(PreparedStatement stmt, Cost item) throws SQLException {
		Long userId = null;
		Long workpackageId = null;
		if (item.getUser() != null)
		{
			User user = userRepo.getById(item.getUser().getId());
			if (user == null)
			{
				userId = userRepo.insert(item.getUser());
			}
			else
			{
				userId = user.getId();
			}
		}
		
		if (item.getWorkpackage() != null)
		{
			Workpackage workpackage = wpRepo.getById(item.getWorkpackage().getId());
			if (workpackage == null)
			{
				workpackageId = wpRepo.insert(item.getWorkpackage());
			}
			else
			{
				workpackageId = workpackage.getId();
			}
		}
		
		handleStatementNullValues(stmt, 1, workpackageId);
		handleStatementNullValues(stmt, 2, userId);
		handleStatementNullValues(stmt, 3, item.getBeschreibung());
		handleStatementNullValues(stmt, 4, item.getBetrag());
		handleStatementNullValues(stmt, 5, item.getDatum());
		handleStatementNullValues(stmt, 6, item.getId());
	}

	@Override
	protected void setInsertStatementParameter(PreparedStatement stmt, Cost item) throws SQLException {
		Long userId = null;
		Long workpackageId = null;
		if (item.getUser() != null)
		{
			User user = userRepo.getById(item.getUser().getId());
			if (user == null)
			{
				userId = userRepo.insert(item.getUser());
			}
			else
			{
				userId = user.getId();
			}
		}
		
		if (item.getWorkpackage() != null)
		{
			Workpackage workpackage = wpRepo.getById(item.getWorkpackage().getId());
			if (workpackage == null)
			{
				workpackageId = wpRepo.insert(item.getWorkpackage());
			}
			else
			{
				workpackageId = workpackage.getId();
			}
		}
		
		handleStatementNullValues(stmt, 1, workpackageId);
		handleStatementNullValues(stmt, 2, userId);
		handleStatementNullValues(stmt, 3, item.getBeschreibung());
		handleStatementNullValues(stmt, 4, item.getBetrag());
		handleStatementNullValues(stmt, 5, item.getDatum());
	}
	
	/**
	 * Liefert alle Kosten mit dem angegebenen Workpackagename als Workpackage
	 * @param workpackagename
	 * @return Collection mit dem Workpackagename "workpackage"
	 */
	public Collection<Cost> getByWorkpackage(String workpackagename)
	{
		log.trace("getByWorkpackage " + workpackagename);
		
		try(Connection conn = getConnection())
		{
			List<Workpackage> wps = new ArrayList<>();
			if (NumberUtils.isNumber(workpackagename))
			{
				Workpackage w = wpRepo.getById(Long.parseLong(workpackagename));
				if (w != null)
				{
					wps.add(w);
				}
			}
			else
			{	
				wps = new ArrayList<>(wpRepo.getByName(workpackagename));
			}
			
			log.trace(wps.toString());
			
			if (wps.isEmpty())
			{
				return new ArrayList<>();
			}
			
			PreparedStatement stmt = conn.prepareStatement(getAllQuery() + " where workpackageId=?");
			stmt.setLong(1, wps.get(0).getId());
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + Projectrisk.class.toString().toLowerCase() + " by workpackage " + workpackagename;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
}
