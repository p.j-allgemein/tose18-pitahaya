package ch.briggen.bfh.sparklist.domain;

public class Projectstatus {
	private long id;
	private Project project;
	private String projectstatus;
	private String ressourcestatus;
	private String riskstatus;
	private String timestatus;
	private String budgetstatus;
	
	public Projectstatus()
	{
	}
	
	public Projectstatus(long id, Project project, String projectstatus, String ressourcestatus, String riskstatus, String timestatus, String budgetstatus)
	{
		this.id = id;
		this.project = project;
		this.projectstatus = projectstatus;
		this.ressourcestatus = ressourcestatus;
		this.riskstatus = riskstatus;
		this.timestatus = timestatus;
		this.budgetstatus = budgetstatus;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getProjectstatus() {
		return projectstatus;
	}

	public void setProjectstatus(String projectstatus) {
		this.projectstatus = projectstatus;
	}

	public String getRessourcestatus() {
		return ressourcestatus;
	}

	public void setRessourcestatus(String ressourcestatus) {
		this.ressourcestatus = ressourcestatus;
	}

	public String getRiskstatus() {
		return riskstatus;
	}

	public void setRiskstatus(String riskstatus) {
		this.riskstatus = riskstatus;
	}

	public String getTimestatus() {
		return timestatus;
	}

	public void setTimestatus(String timestatus) {
		this.timestatus = timestatus;
	}
	
	public String getBudgetstatus() {
		return budgetstatus;
	}

	public void setBudgetstatus(String budgetstatus) {
		this.budgetstatus = budgetstatus;
	}

	@Override
	public String toString() {
		return String.format("Projectstatus:{id: %d; project: %s; projectstatus: %s; ressourcestatus: %s; " +
				"riskstatus: %s; timestatus: %s; budgetstatus: %s}", id, project, projectstatus, ressourcestatus, riskstatus,
				timestatus, budgetstatus);
	}
}
