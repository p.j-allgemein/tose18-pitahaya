package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

public class ProjectstatusRepository extends AbstractRepository<Projectstatus>
{
	private ProjectRepository projectRepo = new ProjectRepository();
	
	@Override
	protected String getAllQuery()
	{
		return "select projectstatus.id, projectId, projectstatus, ressourcestatus, riskstatus, timestatus, budgetstatus from projectstatus";
	}

	@Override
	protected String getByNameQuery()
	{
		return getAllQuery() + ", projects where projectstatus.projectId=projects.id and projects.projektname=?";
	}

	@Override
	protected String getByIdQuery()
	{
		return getAllQuery() + " where id=?";
	}

	@Override
	protected String getSaveQuery()
	{
		return "update projectstatus set projectId=?, projectstatus=?, ressourcestatus=?, riskstatus=?, timestatus=?, budgetstatus=? where id=?";
	}

	@Override
	protected String getDeleteQuery()
	{
		return "delete from projectstatus where id=?";
	}

	@Override
	protected String getInsertQuery()
	{
		return "insert into projectstatus (projectId, projectstatus, ressourcestatus, riskstatus, timestatus, budgetstatus) values (?, ?, ?, ?, ?, ?)";
	}

	@Override
	protected void setByNameStatementParameter(PreparedStatement stmt, String name) throws SQLException
	{
		stmt.setString(1, name);
	}

	@Override
	protected void setSaveStatementParameter(PreparedStatement stmt, Projectstatus status) throws SQLException
	{
		handleStatementNullValues(stmt, 1, status.getProject() != null ? status.getProject().getId() : null);
		handleStatementNullValues(stmt, 2, status.getProjectstatus());
		handleStatementNullValues(stmt, 3, status.getRessourcestatus());
		handleStatementNullValues(stmt, 4, status.getRiskstatus());
		handleStatementNullValues(stmt, 5, status.getTimestatus());
		handleStatementNullValues(stmt, 6, status.getBudgetstatus());
		handleStatementNullValues(stmt, 7, status.getId());
	}

	@Override
	protected void setInsertStatementParameter(PreparedStatement stmt, Projectstatus status) throws SQLException
	{
		handleStatementNullValues(stmt, 1, status.getProject() != null ? status.getProject().getId() : null);
		handleStatementNullValues(stmt, 2, status.getProjectstatus());
		handleStatementNullValues(stmt, 3, status.getRessourcestatus());
		handleStatementNullValues(stmt, 4, status.getRiskstatus());
		handleStatementNullValues(stmt, 5, status.getTimestatus());
		handleStatementNullValues(stmt, 6, status.getBudgetstatus());
	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Projectstatus-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	protected Collection<Projectstatus> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Projectstatus> list = new LinkedList<Projectstatus>();
		while(rs.next())
		{
			Project j = projectRepo.getById(rs.getLong("projectId"));
			Projectstatus p = new Projectstatus(rs.getLong("id"), j, rs.getString("projectstatus"),
											    rs.getString("ressourcestatus"), rs.getString("riskstatus"),
											    rs.getString("timestatus"), rs.getString("budgetstatus"));
			list.add(p);
		}
		return list;
	}
	
	/**
	 * Liefert alle Projectstatus mit dem angegebenen Projectname als Project
	 * @param projectname
	 * @return Collection mit dem Projectname "projectname"
	 */
	public Collection<Projectstatus> getByProject(String projectname)
	{
		log.trace("getByProjectname " + projectname);
		
		try(Connection conn = getConnection())
		{
			List<Project> projects = new ArrayList<>();
			if (NumberUtils.isNumber(projectname))
			{
				Project p = projectRepo.getById(Long.parseLong(projectname));
				if (p != null)
				{
					projects.add(p);
				}
			}
			else
			{	
				projects = new ArrayList<>(projectRepo.getByName(projectname));
			}
			
			log.trace(projects.toString());
			
			if (projects.isEmpty())
			{
				return new ArrayList<>();
			}
			
			PreparedStatement stmt = conn.prepareStatement(getAllQuery() + " where projectId=?");
			stmt.setLong(1, projects.get(0).getId());
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + Projectstatus.class.toString().toLowerCase() + " by projectname " + projectname;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
}
