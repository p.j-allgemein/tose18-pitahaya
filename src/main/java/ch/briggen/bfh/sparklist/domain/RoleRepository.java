package ch.briggen.bfh.sparklist.domain;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;


public class RoleRepository extends AbstractRepository<Role>
{	
	@Override
	protected String getAllQuery()
	{
		return "select id, rolename from roles";
	}

	@Override
	protected String getByNameQuery()
	{
		return getAllQuery() + " where rolename=?";
	}

	@Override
	protected String getByIdQuery()
	{
		return getAllQuery() + " where id=?";
	}

	@Override
	protected String getSaveQuery()
	{
		return "update roles set rolename=? where id=?";
	}

	@Override
	protected String getDeleteQuery()
	{
		return "delete from roles where id=?";
	}

	@Override
	protected String getInsertQuery()
	{
		return "insert into roles (rolename) values (?)";
	}

	@Override
	protected void setByNameStatementParameter(PreparedStatement stmt, String name) throws SQLException
	{
		stmt.setString(1, name);
	}

	@Override
	protected void setSaveStatementParameter(PreparedStatement stmt, Role role) throws SQLException
	{
		handleStatementNullValues(stmt, 1, role.getRolename());
		handleStatementNullValues(stmt, 2,  role.getId());
	}

	@Override
	protected void setInsertStatementParameter(PreparedStatement stmt, Role role) throws SQLException
	{
		handleStatementNullValues(stmt, 1, role.getRolename());
	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Role-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	protected Collection<Role> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Role> list = new LinkedList<Role>();
		while(rs.next())
		{
			Role r = new Role(rs.getLong("id"), rs.getString("rolename"));
			list.add(r);
		}
		return list;
	}

}
