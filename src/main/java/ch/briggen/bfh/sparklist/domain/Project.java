package ch.briggen.bfh.sparklist.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Project {
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	private long id;
	private long projektnummer;
	private String projektname;
	private User projektleiter;
	private LocalDate projektstart;
	private LocalDate projektende;
	private String projektphase;
	private String projektstatus;
	private double projektbudget;
	
	public Project()
	{
		
	}
	
	public Project(long id, long projektnummer, String projektname, User projektleiter, LocalDate projektstart, LocalDate projektende,
					String projektphase, String projektstatus, double projektbudget)
	{
		this.id = id;
		this.projektnummer = projektnummer;
		this.projektname = projektname;
		this.projektleiter = projektleiter;
		this.projektstart = projektstart;
		this.projektende = projektende;
		this.projektphase = projektphase;
		this.projektstatus = projektstatus;
		this.projektbudget = projektbudget;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getProjektnummer() {
		return projektnummer;
	}
	public void setProjektnummer(long projektnummer) {
		this.projektnummer = projektnummer;
	}
	public String getProjektname() {
		return projektname;
	}
	public void setProjektname(String projektname) {
		this.projektname = projektname;
	}
	public User getProjektleiter() {
		return projektleiter;
	}
	public void setProjektleiter(User projektleiter) {
		this.projektleiter = projektleiter;
	}
	public LocalDate getProjektstart() {
		return projektstart;
	}
	public void setProjektstart(LocalDate projektstart) {
		this.projektstart = projektstart;
	}
	public LocalDate getProjektende() {
		return projektende;
	}
	public void setProjektende(LocalDate projektende) {
		this.projektende = projektende;
	}
	public String getProjektphase() {
		return projektphase;
	}
	public void setProjektphase(String projektphase) {
		this.projektphase = projektphase;
	}
	public String getProjektstatus() {
		return projektstatus;
	}
	public void setProjektstatus(String projektstatus) {
		this.projektstatus = projektstatus;
	}
	public double getProjektbudget() {
		return projektbudget;
	}
	public void setProjektbudget(double projektbudget) {
		this.projektbudget = projektbudget;
	}
	
	public void validate()
	{
		if (projektnummer == 0)
		{
			throw new IllegalArgumentException("Projektnummer fehlt");
		}
		
		if (projektname == null || projektname.isEmpty())
		{
			throw new IllegalArgumentException("Projektname fehlt");
		}
		
		if (projektleiter == null)
		{
			throw new IllegalArgumentException("Projektleiter fehlt");
		}
		
		if (projektstart == null)
		{
			throw new IllegalArgumentException("Projektstart fehlt");
		}
		
		if (projektende == null)
		{
			throw new IllegalArgumentException("Projektende fehlt");
		}
		
		if (projektphase == null || projektphase.isEmpty())
		{
			throw new IllegalArgumentException("Projektphase fehlt");
		}
		
		if (projektstatus == null || projektstatus.isEmpty())
		{
			throw new IllegalArgumentException("Projektstatus fehlt");
		}
		
		if (projektbudget <= 0.0)
		{
			throw new IllegalArgumentException("Projektbudget fehlt");
		}
	}

	@Override
	public String toString() {
		return String.format("Project:{id: %d; projektnummer: %d; projektname: %s; projektleiter: %s; " +
				"start: %s; end: %s; projektphase: %s; projekstatus: %s; projektbudget: %f}",
				id, projektnummer, projektname, projektleiter != null ? projektleiter.toString() : null,
				projektstart != null ? formatter.format(projektstart) : null, projektende != null ? formatter.format(projektende) : null,
				projektphase, projektstatus, projektbudget);
	}
}
