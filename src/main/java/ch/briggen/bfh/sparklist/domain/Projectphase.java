package ch.briggen.bfh.sparklist.domain;

public class Projectphase {
	private long id;
	private String phasenname;
	
	public Projectphase()
	{
	}
	
	public Projectphase(long id, String phasenname)
	{
		this.id = id;
		this.phasenname = phasenname;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPhasenname() {
		return phasenname;
	}

	public void setPhasenname(String phasenname) {
		this.phasenname = phasenname;
	}
	
	@Override
	public String toString() {
		return String.format("Projectphase:{id: %d; phasenname: %s}", id, phasenname);
	}
}
