package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project>
{
	private UserRepository userRepo = new UserRepository();
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	@Override
	protected Collection<Project> mapItems(ResultSet rs) throws SQLException {
		LinkedList<Project> list = new LinkedList<Project>();
		while(rs.next())
		{
			User u = userRepo.getById(rs.getLong("projektleiter"));
			Project p = new Project(rs.getLong("id"), rs.getLong("projektnummer"), rs.getString("projektname"), u,
									rs.getString("projektstart") != null ? LocalDate.parse(rs.getString("projektstart"), formatter) : null,
									rs.getString("projektende") != null ? LocalDate.parse(rs.getString("projektende"), formatter) : null,
								    rs.getString("projektphase"), rs.getString("projektstatus"), rs.getDouble("projektbudget"));
			list.add(p);
		}
		return list;
	}
	
	@Override
	protected String getAllQuery()
	{
		return "select id, projektnummer, projektname, projektleiter, projektstart, projektende, " +
				"projektphase, projektstatus, projektbudget from projects";
	}

	@Override
	protected String getByNameQuery()
	{
		return getAllQuery() + " where projektname=?";
	}

	@Override
	protected String getByIdQuery()
	{
		return getAllQuery() + " where id=?";
	}

	@Override
	protected String getSaveQuery() {
		return "update projects set projektnummer = ?, projektname = ?, projektleiter = ?, projektstart = ?, projektende = ?, projektphase = ?, projektstatus = ?, projektbudget = ? where id = ?";
	}

	@Override
	protected String getDeleteQuery() {
		return "delete from projects where id = ?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into projects(projektnummer, projektname, projektleiter, projektstart, projektende, projektphase, projektstatus, projektbudget) " +
				"values(?, ?, ?, ?, ?, ?, ?, ?)";
	}

	@Override
	protected void setByNameStatementParameter(PreparedStatement stmt, String name) throws SQLException {
		stmt.setString(1, name);
	}

	@Override
	protected void setSaveStatementParameter(PreparedStatement stmt, Project item) throws SQLException {
		Long userId = null;
		if (item.getProjektleiter() != null)
		{
			User user = userRepo.getById(item.getProjektleiter().getId());
			if (user == null)
			{
				userId = userRepo.insert(item.getProjektleiter());
			}
			else
			{
				userId = user.getId();
			}
		}
		
		handleStatementNullValues(stmt, 1, item.getProjektnummer());
		handleStatementNullValues(stmt, 2, item.getProjektname());
		handleStatementNullValues(stmt, 3, userId);
		handleStatementNullValues(stmt, 4, item.getProjektstart());
		handleStatementNullValues(stmt, 5, item.getProjektende());
		handleStatementNullValues(stmt, 6, item.getProjektphase());
		handleStatementNullValues(stmt, 7, item.getProjektstatus());
		handleStatementNullValues(stmt, 8, item.getProjektbudget());
		handleStatementNullValues(stmt, 9, item.getId());
	}

	@Override
	protected void setInsertStatementParameter(PreparedStatement stmt, Project item) throws SQLException {
		Long userId = null;
		if (item.getProjektleiter() != null)
		{
			User user = userRepo.getById(item.getProjektleiter().getId());
			if (user == null)
			{
				userId = userRepo.insert(item.getProjektleiter());
			}
			else
			{
				userId = user.getId();
			}
		}
		
		handleStatementNullValues(stmt, 1, item.getProjektnummer());
		handleStatementNullValues(stmt, 2, item.getProjektname());
		handleStatementNullValues(stmt, 3, userId);
		handleStatementNullValues(stmt, 4, item.getProjektstart());
		handleStatementNullValues(stmt, 5, item.getProjektende());
		handleStatementNullValues(stmt, 6, item.getProjektphase());
		handleStatementNullValues(stmt, 7, item.getProjektstatus());
		handleStatementNullValues(stmt, 8, item.getProjektbudget());
	}
	
	@Override
	public void delete(long id)
	{
		ProjectriskRepository riskRepo = new ProjectriskRepository();
		ProjectstatusRepository statusRepo = new ProjectstatusRepository();
		UserProjectRepository u2pRepo = new UserProjectRepository();
		WorkpackageRepository wpRepo = new WorkpackageRepository();
		List<Projectrisk> risks = new ArrayList<>(riskRepo.getAll());
		if (!risks.isEmpty())
		{
			risks.stream()
				 .filter(e -> (e.getProject() != null && e.getProject().getId() == id))
				 .forEach(e -> riskRepo.delete(e.getId()));
		}
		List<Projectstatus> status = new ArrayList<>(statusRepo.getAll());
		if (!status.isEmpty())
		{
			status.stream()
				 .filter(e -> (e.getProject() != null && e.getProject().getId() == id))
				 .forEach(e -> statusRepo.delete(e.getId()));
		}
		List<User2Project> u2p = new ArrayList<>(u2pRepo.getAll());
		if (!u2p.isEmpty())
		{
			u2p.stream()
				 .filter(e -> (e.getProject() != null && e.getProject().getId() == id))
				 .forEach(e -> u2pRepo.delete(e.getUser(), e.getProject(), e.getRole()));
		}
		List<Workpackage> wp = new ArrayList<>(wpRepo.getAll());
		if (!wp.isEmpty())
		{
			wp.stream()
				 .filter(e -> (e.getProject() != null && e.getProject().getId() == id))
				 .forEach(e -> wpRepo.delete(e.getId()));
		}
		super.delete(id);
	}
	
	/**
	 * Liefert alle Project mit dem angegebenen Username
	 * @param username
	 * @return Collection mit dem Username "username"
	 */
	public Collection<Project> getByUsername(String username)
	{
		log.trace("getByUsername " + username);
		
		try(Connection conn = getConnection())
		{
			List<User> users = new ArrayList<>(userRepo.getByName(username));
			
			if (users.isEmpty())
			{
				return new ArrayList<>();
			}
			
			PreparedStatement stmt = conn.prepareStatement(getAllQuery() + " where projektleiter=?");
			stmt.setLong(1, users.get(0).getId());
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + Project.class.toString().toLowerCase() + " by username " + username;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert eine neue Projektnummer zurück
	 * @return die neue Projektnummer Id aus der Sequence
	 */
	public long getProjectNumberId()
	{
		log.trace("getProjectNumberId");
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select projectnumber.nextval as id from dual");
			ResultSet rs = stmt.executeQuery();
			rs.next();
			return rs.getLong("id");		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + Project.class.toString().toLowerCase() + " project number id ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert true zurück, wenn der Projektname bereits existiert ansonsten false
	 * @return boolean Wert, ob Projektname vorhanden ist oder nicht
	 */
	public boolean checkProjectname(String projektname)
	{
		log.trace("checkProjectname " + projektname + " if it exists");
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select count(*) as counter from projects where projektname=?");
			stmt.setString(1, projektname);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			return rs.getLong("counter") != 0 ? true : false;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + Project.class.toString().toLowerCase() + " project name ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
}
