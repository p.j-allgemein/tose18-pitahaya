package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

public class ProjectriskRepository extends AbstractRepository<Projectrisk> {
	private UserRepository userRepo = new UserRepository();
	private ProjectRepository projectRepo = new ProjectRepository();

	@Override
	protected Collection<Projectrisk> mapItems(ResultSet rs) throws SQLException {
		LinkedList<Projectrisk> list = new LinkedList<Projectrisk>();
		while(rs.next())
		{
			User u = userRepo.getById(rs.getLong("verantwortlicher"));
			Project j = projectRepo.getById(rs.getLong("project"));
			Projectrisk p = new Projectrisk(rs.getLong("id"), rs.getString("risikoname"), rs.getString("ursache"), rs.getString("gefaehrdungsart"),
											rs.getDouble("eintrittswahrscheinlichkeit"), rs.getDouble("auswirkungen"),
											rs.getString("massnahmen"), u, rs.getLong("prioritaet"), j);
			list.add(p);
		}
		return list;
	}

	@Override
	protected String getAllQuery() {
		return "select id, risikoname, ursache, gefaehrdungsart, eintrittswahrscheinlichkeit, auswirkungen, massnahmen, " +
				"verantwortlicher, prioritaet, project from projectrisks";
	}

	@Override
	protected String getByNameQuery() {
		return getAllQuery() + " where ursache=?";
	}

	@Override
	protected String getByIdQuery() {
		return getAllQuery() + " where id=?";
	}

	@Override
	protected String getSaveQuery() {
		return "update projectrisks set risikoname = ?, ursache = ?, gefaehrdungsart = ?, eintrittswahrscheinlichkeit = ?, auswirkungen = ?, massnahmen = ?, verantwortlicher = ?, prioritaet = ?, project = ? where id = ?";
	}

	@Override
	protected String getDeleteQuery() {
		return "delete from projectrisks where id = ?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into projectrisks(risikoname, ursache, gefaehrdungsart, eintrittswahrscheinlichkeit, auswirkungen, massnahmen, verantwortlicher, prioritaet, project) " +
				"values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	}

	@Override
	protected void setByNameStatementParameter(PreparedStatement stmt, String name) throws SQLException {
		stmt.setString(1, name);
		
	}

	@Override
	protected void setSaveStatementParameter(PreparedStatement stmt, Projectrisk item) throws SQLException {
		Long userId = null;
		if (item.getVerantwortlicher() != null)
		{
			User user = userRepo.getById(item.getVerantwortlicher().getId());
			if (user == null)
			{
				userId = userRepo.insert(item.getVerantwortlicher());
			}
			else
			{
				userId = user.getId();
			}
		}
		
		Long projectId = null;
		if (item.getProject() != null)
		{
			Project project = projectRepo.getById(item.getProject().getId());
			if (project == null)
			{
				projectId = projectRepo.insert(item.getProject());
			}
			else
			{
				projectId = project.getId();
			}
		}
		
		handleStatementNullValues(stmt, 1, item.getRisikoname());
		handleStatementNullValues(stmt, 2, item.getUrsache());
		handleStatementNullValues(stmt, 3, item.getGefaehrdungsart());
		handleStatementNullValues(stmt, 4, item.getEintrittswahrscheinlichkeit());
		handleStatementNullValues(stmt, 5, item.getAuswirkungen());
		handleStatementNullValues(stmt, 6, item.getMassnahmen());
		handleStatementNullValues(stmt, 7, userId);
		handleStatementNullValues(stmt, 8, item.getPrioritaet());
		handleStatementNullValues(stmt, 9, projectId);
		handleStatementNullValues(stmt, 10, item.getId());
		
	}

	@Override
	protected void setInsertStatementParameter(PreparedStatement stmt, Projectrisk item) throws SQLException {
		Long userId = null;
		if (item.getVerantwortlicher() != null)
		{
			User user = userRepo.getById(item.getVerantwortlicher().getId());
			if (user == null)
			{
				userId = userRepo.insert(item.getVerantwortlicher());
			}
			else
			{
				userId = user.getId();
			}
		}
		
		Long projectId = null;
		if (item.getProject() != null)
		{
			Project project = projectRepo.getById(item.getProject().getId());
			if (project == null)
			{
				projectId = projectRepo.insert(item.getProject());
			}
			else
			{
				projectId = project.getId();
			}
		}
		
		handleStatementNullValues(stmt, 1, item.getRisikoname());
		handleStatementNullValues(stmt, 2, item.getUrsache());
		handleStatementNullValues(stmt, 3, item.getGefaehrdungsart());
		handleStatementNullValues(stmt, 4, item.getEintrittswahrscheinlichkeit());
		handleStatementNullValues(stmt, 5, item.getAuswirkungen());
		handleStatementNullValues(stmt, 6, item.getMassnahmen());
		handleStatementNullValues(stmt, 7, userId);
		handleStatementNullValues(stmt, 8, item.getPrioritaet());
		handleStatementNullValues(stmt, 9, projectId);
	}
	
	/**
	 * Liefert alle Projectrisk mit dem angegebenen Username als Verantwortlicher
	 * @param username
	 * @return Collection mit dem Username "username"
	 */
	public Collection<Projectrisk> getByUsername(String username)
	{
		log.trace("getByUsername " + username);
		
		try(Connection conn = getConnection())
		{
			List<User> users = new ArrayList<>(userRepo.getByName(username));
			
			if (users.isEmpty())
			{
				return new ArrayList<>();
			}
			
			PreparedStatement stmt = conn.prepareStatement(getAllQuery() + " where verantwortlicher=?");
			stmt.setLong(1, users.get(0).getId());
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + Project.class.toString().toLowerCase() + " by username " + username;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert alle Projectrisk mit dem angegebenen Projectname als Project
	 * @param projectname
	 * @return Collection mit dem Projectname "projectname"
	 */
	public Collection<Projectrisk> getByProject(String projectname)
	{
		log.trace("getByProjectname " + projectname);
		
		try(Connection conn = getConnection())
		{
			List<Project> projects = new ArrayList<>();
			if (NumberUtils.isNumber(projectname))
			{
				Project p = projectRepo.getById(Long.parseLong(projectname));
				if (p != null)
				{
					projects.add(p);
				}
			}
			else
			{	
				projects = new ArrayList<>(projectRepo.getByName(projectname));
			}
			
			log.trace(projects.toString());
			
			if (projects.isEmpty())
			{
				return new ArrayList<>();
			}
			
			PreparedStatement stmt = conn.prepareStatement(getAllQuery() + " where project=?");
			stmt.setLong(1, projects.get(0).getId());
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving " + Projectrisk.class.toString().toLowerCase() + " by projectname " + projectname;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
}
