package ch.briggen.bfh.sparklist.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Workpackage {
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	private long id;
	private Project project;
	private Projectphase projectphase;
	private String workpackagename;
	private double plannedValue;
	private double earnedValue;
	private LocalDate startdate;
	private LocalDate enddate;
	private boolean done = false;
	
	public Workpackage()
	{
	}
	
	public Workpackage(long id, Project project, Projectphase projectphase,
					   String workpackagename, double plannedValue,
					   double earnedValue, LocalDate startdate,
					   LocalDate enddate, boolean done)
	{
		this.id = id;
		this.project = project;
		this.projectphase = projectphase;
		this.workpackagename = workpackagename;
		this.plannedValue = plannedValue;
		this.earnedValue = earnedValue;
		this.startdate = startdate;
		this.enddate = enddate;
		this.done = done;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Projectphase getProjectphase() {
		return projectphase;
	}

	public void setProjectphase(Projectphase projectphase) {
		this.projectphase = projectphase;
	}

	public String getWorkpackagename() {
		return workpackagename;
	}

	public void setWorkpackagename(String workpackagename) {
		this.workpackagename = workpackagename;
	}

	public double getPlannedValue() {
		return plannedValue;
	}

	public void setPlannedValue(double plannedValue) {
		this.plannedValue = plannedValue;
	}

	public double getEarnedValue() {
		return earnedValue;
	}

	public void setEarnedValue(double earnedValue) {
		this.earnedValue = earnedValue;
	}

	public LocalDate getStartdate() {
		return startdate;
	}

	public void setStartdate(LocalDate startdate) {
		this.startdate = startdate;
	}

	public LocalDate getEnddate() {
		return enddate;
	}

	public void setEnddate(LocalDate enddate) {
		this.enddate = enddate;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}
	
	public void validate()
	{
		if (project == null)
		{
			throw new IllegalArgumentException("Projekt fehlt");
		}
		
		if (projectphase == null)
		{
			throw new IllegalArgumentException("Projektphase fehlt");
		}
		
		if (workpackagename == null || workpackagename.isEmpty())
		{
			throw new IllegalArgumentException("Workpackagename fehlt");
		}
		
		if (plannedValue < 0.0)
		{
			throw new IllegalArgumentException("PlannedValue fehlt");
		}
		
		if (earnedValue < 0.0)
		{
			throw new IllegalArgumentException("EarnedValue fehlt");
		}
		
		if (startdate == null)
		{
			throw new IllegalArgumentException("Startdate fehlt");
		}
		
		if (enddate == null)
		{
			throw new IllegalArgumentException("Enddate fehlt");
		}
	}
	
	@Override
	public String toString() {
		return String.format("Workpackage:{id: %d; project: %s; projectphase: %s; workpackagename: %s; " +
				"plannedValue: %f; earnedValue: %f; startdate: %s; enddate: %s; " +
				"done: %b}",
				id, project != null ? project.toString() : null,
				projectphase != null ? projectphase.toString() : null, workpackagename,
				plannedValue, earnedValue,
				startdate != null ? formatter.format(startdate) : null,
				enddate != null ? formatter.format(enddate) : null, done);
	}
}
